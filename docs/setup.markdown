

### Configuring Gradle properties

Building this project requires an some external setup.
The build-script has been configured to expect an external `secrets directory`.



#### Letting the build-script know about the `secrets directory`

1) Create a new directory where you wish to store your secrets. Get the full-path of this directory.
2) Open the following file (creating it if necessary) `~/.gradle/gradle.properties`.
3) Append the following line to the file `BAEMAIL_SECRETS_FOLDER={full-path-to-secrets-directory}`

Mine looks like this `BAEMAIL_SECRETS_FOLDER=/Users/jaanis/Documents/proj/beamail-app-secrets`

4) Save it & restart Android Studio

5) Projects build is setup in a way where it expects the BAEMAIL_SECRETS_FOLDER to contain some
specific files necessary for debug & release builds.
Expected content:
    a) `debug.keystore`
    b) `release.keystore`
    c) `keystore.properties`
    c) `config.properties`

Expected content of `keystore.properties`:
```
keystore_pass=dummyPassDummyPassDummyPassDummyPass
key_alias=dummyAlias
key_pass=dummyKeyPassDummyKeyPass
```

`debug.keystore` can just be a copy of `~/.android/debug.keystore`.
`release.keystore` only needs to be created when making a release-build.


Expected content of `config.properties`:
```
crawlToken=aaaaaaaaaaaaaa
bitdbKey=bbbbbbbbbbbbbbbbb
```
Bitdb API key - https://bitdb.network/v3/dashboard
Planaria token (crawlToken) - https://token.planaria.network/


#### Bundling of the node-project

Location of node-project directory inside of the project - `app/src/main/nodejs-project`.

This project is bundled inside of the android-app by including a `zip` of the node-project inside
of the app's `assets`. This `zip` is then extracted by the android-app inside of it's sand-box
once per app-update. The responsible code can be found in `Node#start`.

Setup like this means that `npm install --production` might be a good idea to run before
building of a release version to reduce the resulting app & app-install size.

Run `nxp tsc` if you are writing TypeScript.

Run `npm install` after checking out the project form git; `node_modules` has been added to `.gitignore`.

Execute gradle-task `compressNodeProjectAsset` to create an asset-zip of the current state of the node-project.

`build.gradle` of app-module overrides resource-integer-value for key `nodejs_project__version`.
Value of this key is checked when app-code decides whether to reapply node-project to the app's sandbox
upon app-startup. Increment this if you have made any changes to the node-project & want to see this
code after install of apk.





As it should be obvious, the bundled node-project-asset-zip needs to be prepared before doing app-build.


