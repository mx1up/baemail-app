package wrap.nakasendo.kit.bigNumbers

import android.util.Log

class BigNumber private constructor() {



    private var nativeHandle: Long = 0

    external fun fromDecInternal(value: String): Long


    init {
        Log.d("BigNumber", "#init $this")
    }


    fun getHandle(): Long = nativeHandle


    private external fun dispose()

    protected fun finalize() {
        // ...executed on GC
        Log.d("BigNumber", "#finalize $this")
        dispose()
    }

    companion object {

//        external fun add(a: BigNumber, b: BigNumber): BigNumber
//        external fun add(a: BigNumber, b: Int): BigNumber
//        external fun subtract(a: BigNumber, b: BigNumber): BigNumber
//        external fun subtract(a: BigNumber, b: Int): BigNumber
//        external fun multiply(a: BigNumber, b: BigNumber): BigNumber
//        external fun divide(a: BigNumber, b: BigNumber): BigNumber
//        external fun modulo(a: BigNumber, b: BigNumber): BigNumber
//        external fun gt(a: BigNumber, b: BigNumber): BigNumber todo boolean?
//        external fun lt(a: BigNumber, b: BigNumber): BigNumber todo boolean?
//        external fun eq(a: BigNumber, b: BigNumber): BigNumber todo boolean?
//        external fun bitwiseShiftRight(a: BigNumber, b: BigNumber): BigNumber
//        external fun bitwiseShiftRight(a: BigNumber, b: Int): BigNumber
//        external fun bitwiseShiftLeft(a: BigNumber, b: BigNumber): BigNumber
//        external fun bitwiseShiftLeft(a: BigNumber, b: Int): BigNumber
//
//        external fun invertMod(a: BigNumber, b: BigNumber): BigNumber
//        external fun addMod(a: BigNumber, b: BigNumber): BigNumber
//        external fun subtractMod(a: BigNumber, b: BigNumber): BigNumber
//        external fun multiplyMod(a: BigNumber, b: BigNumber): BigNumber
//        external fun divideMod(a: BigNumber, b: BigNumber): BigNumber
//
//
//        external fun fromHex(value: String): BigNumber

        fun fromDec(value: String): BigNumber {
            val bn = BigNumber()
            bn.fromDecInternal(value)

            return bn
        }



//        fun fromDec(value: String): BigNumber {
//            val n = BigNumberInternal()
//            n.initialiseFromDec(value)
//
//            return BigNumber(n.nativeHandle)
//        }

//        external fun fromBin(value: ByteArray): BigNumber
//
//        external fun generateOne(): BigNumber

        @JvmStatic
        external fun generateZero(): BigNumber

        /*


            BigNumbers_API BigNumber GenerateRand ( const int& )  ;
            BigNumbers_API BigNumber GenerateRandNegative (const int&);
            BigNumbers_API BigNumber GenerateRandWithSeed(const std::string&, const int&);
            BigNumbers_API BigNumber GenerateRandRange(const BigNumber& min, const BigNumber& max,const int& nsize=512);

            BigNumbers_API BigNumber GenerateRandPrime(const int& nsize = 512);

         */
    }

//    external fun one()
//    external fun zero()
//    external fun negative()
//    external fun positive()
//
//    external fun increment(by: Int)
//    external fun increment()
//
//    external fun decrement(by:Int)
//    external fun decrement()

    external fun toHex(): String
//    external fun toDec(): String
//    external fun toBin(): ByteArray

    /*
        // Generate & return string Representation
        std::string generateRandHex (const int& nsize=512) ;
        std::string generateRandDec (const int& nsize=512) ;
        std::string generateNegRandHex (const int& nsize=512);
        std::string generateNegRandDec (const int& nsize=512);
        std::string generateRandRange (const BigNumber&);


        void seedRNG (const std::string& ) ;
        std::string generateRandHexWithSeed(const std::string&, const int& nsize=512);
        std::string generateRandDecWithSeed(const std::string&, const int& nsize=512);
        std::string generateNegRandHexWithSeed (const std::string&, const int& nsize=512);
        std::string generateNegRandDecWithSeed (const std::string&, const int& nsize=512);
        std::string generateRangRandHexWithSeed (const std::string&, const BigNumber&);
        std::string generateRangRandDecWithSeed (const std::string&, const BigNumber&);

        // Generate random prime & return string Representation
        std::string generateRandPrimeHex(const int& nsize = 512);
        std::string generateRandPrimeDec(const int& nsize = 512);
        std::string generateRandPrimeHexWithSeed(const std::string& seed, const int& nsize = 512);
        std::string generateRandPrimeDecWithSeed(const std::string& seed, const int& nsize = 512);

        bool isPrime() const;
        bool isPrimeFasttest() const;
     */












//    class BigNumberInternal {
//        var nativeHandle = 0L
//
//
//        external fun initialiseFromDec(value: String)
//    }

}