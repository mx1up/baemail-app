
####
####

### Instructions on how the Nakasendo SDK was cross-compiled for Android

Android apps can include native-libraries within them & use them through JNI.

The native libraries are CPU/OS specific and need to be compiled specifically for each supported `abi`.
Targeting of the following `abi's` should cover most Android-devices on the market right now:

* armeabi-v7a
* arm64-v8a
* x86
* x86_64

These instructions cover the big steps taken to get the SDK rebuilt as required.


####
####

##### The hackiness

During the whole mess of going through this process, there were a few issues that had to be addressed to get the SDK to compile.

####

The following modification had to be made to the Nakasendo library.  
The char-array `b64revt` had to become a signed-char-array.

![](img/nakasendo_modif_00.png)

####
####

When targeting the compilation for the 32bit CPUs there were issues with the python-interpreter used  
by the build-machine and the following actions were taken to address the issue.

!!!!! TODO !!!!!

This `smells` wrong; the 32bit libraries probably need to rebuilt with 32bit-python or something; will see.

!!!!! TODO !!!!!

There still is an issue that is happening during the `make` of the nakasendo-sdk. Currently proceeding as if this is irrelevant.
```
Scanning dependencies of target TSCore
[ 85%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/__/__/__/__/generated/protobuf/ts_messages.pb.cc.o
[ 87%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/__/__/__/__/generated/protobuf/ts_enums.pb.cc.o
[ 89%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/GroupMetaData.cpp.o
[ 91%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/orchestrator.cpp.o
[ 92%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/player.cpp.o
[ 94%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/TSMessageFactory.cpp.o
[ 96%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/TSProtoBufHelper.cpp.o
[ 98%] Building CXX object src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/TSState.cpp.o
[100%] Linking CXX shared library ../../../../x32/debug/libTSCored.so
/Users/jaanis/Library/Android/sdk/ndk/21.0.6113669/toolchains/llvm/prebuilt/darwin-x86_64/bin/../lib/gcc/i686-linux-android/4.9.x/../../../../i686-linux-android/bin/ld: error: /usr/local/lib/libprotobuf.a: no archive symbol table (run ranlib)
clang++: error: linker command failed with exit code 1 (use -v to see invocation)
make[2]: *** [x32/debug/libTSCored.so] Error 1
make[1]: *** [src/applications/TS_protobuf/cpp/CMakeFiles/TSCore.dir/all] Error 2
make: *** [all] Error 2
```


... v8a version seem to be compiling fine, without the python-modification; it also didn't result in the TSCore-error.

!!!!! TODO !!!!!

![](img/nakasendo_modif_01.png)


####
####

The following modification had to be made to the build-script of the Nakasendo library.
No clue if it is necessary; too lazy to test it by trying out undoing change.

![](img/nakasendo_modif_02.png)


####
####



##### Docs used during research

https://nakasendoproject.org/docs/DeveloperSetup/index.html
https://github.com/nakasendo/nakasendo/blob/release/nakasendo/SDK-BUILD-README
https://cmake.org/cmake/help/latest/guide/tutorial/index.html#cmake-tutorial
https://developer.android.com/ndk/guides/cmake
https://cmake.org/cmake/help/v3.7/manual/cmake-toolchains.7.html#cross-compiling-for-android-with-the-ndk
https://developer.android.com/studio/projects/add-native-code
https://github.com/openssl/openssl/blob/OpenSSL_1_1_1-stable/NOTES.ANDROID
https://proandroiddev.com/tutorial-compile-openssl-to-1-1-1-for-android-application-87137968fee


####
####

##### Environment variable setup
```
export PATH="/Applications/CMake.app/Contents/bin:$PATH"
export Protobuf_ROOT="/usr/local/bin"
export ANDROID_NDK_HOME="/Users/jaanis/Library/Android/sdk/ndk/21.0.6113669"
```


####
####

##### Building

####### Building platform-specific version of OpenSSL and then Nakasendo

This helps you find the appropriate build-toolchain. Will be used later.

toolchains_path.py
```
#!/usr/bin/env python
"""
    Get the toolchains path
"""
import argparse
import atexit
import inspect
import os
import shutil
import stat
import sys
import textwrap

def get_host_tag_or_die():
    """Return the host tag for this platform. Die if not supported."""
    if sys.platform.startswith('linux'):
        return 'linux-x86_64'
    elif sys.platform == 'darwin':
        return 'darwin-x86_64'
    elif sys.platform == 'win32' or sys.platform == 'cygwin':
        host_tag = 'windows-x86_64'
        if not os.path.exists(os.path.join(NDK_DIR, 'prebuilt', host_tag)):
            host_tag = 'windows'
        return host_tag
    sys.exit('Unsupported platform: ' + sys.platform)


def get_toolchain_path_or_die(ndk, host_tag):
    """Return the toolchain path or die."""
    toolchain_path = os.path.join(ndk, 'toolchains/llvm/prebuilt',
                                  host_tag)
    if not os.path.exists(toolchain_path):
        sys.exit('Could not find toolchain: {}'.format(toolchain_path))
    return toolchain_path

def main():
    """Program entry point."""
    parser = argparse.ArgumentParser(description='Optional app description')
    parser.add_argument('--ndk', required=True,
                    help='The NDK Home directory')
    args = parser.parse_args()

    host_tag = get_host_tag_or_die()
    toolchain_path = get_toolchain_path_or_die(args.ndk, host_tag)
    print toolchain_path

if __name__ == '__main__':
    main()
```


####

Building.

```
# start a terminal-session

# We will first build an android-abi specific version of openssl.
# Afterwards we will point the openssl-env-variable at that version 
# and build the Nakasendo SDK.
# You will most likely want to 'loop' through this script 4 times, 
# to build SDK's for all abi versions.

NAKASENDO_BUILD_DIR="/Users/jaanis/apps/buildForNakasendo"

# get the source-code of OpenSSL

# set the path to OpenSSL source; make sure that this directory is fresh
OPENSSL_DIR="/Users/jaanis/apps/openssl-1.1.1e"

# find the toolchain for your build machine
toolchains_path=$(python ../toolchains_path.py --ndk ${ANDROID_NDK_HOME})

# Add toolchains bin directory to PATH
PATH=$toolchains_path/bin:$PATH

# Set the supported Android API level to use for OpenSSL native-library.
ANDROID_API=21

# Set the target architecture
# Can be: android-arm, android-arm64, android-x86, android-x86_64
# We will have to make OpenSSL builds for each of these separately.
architecture=android-x86

# This is modified for each target architecture
path_openssl_output="/Users/jaanis/apps/openssl_x86"

mkdir -p $path_openssl_output

cd ${OPENSSL_DIR}

# Create the make file
# !!!!!!!! NOTE THAT `--prefix` & `--openssldir` have values equal to `path_openssl_output`
./Configure ${architecture} -D__ANDROID_API__=$ANDROID_API --prefix=/Users/jaanis/apps/openssl_x86 --openssldir=/Users/jaanis/apps/openssl_x86 shared

# Build
make

make install







# Time to build Nakasendo. This also has to be done separately for each `abi`.

cd $NAKASENDO_BUILD_DIR

# Make sure to point at the matching version.
export OPENSSL_ROOT_DIR="/Users/jaanis/apps/openssl_x86"

# Update `-DCMAKE_ANDROID_ARCH_ABI=` appropriately.
cmake ../nakasendo -DINCLUDE_CPACK=OFF -DBUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_SYSTEM_NAME=Android -DCMAKE_ANDROID_STL_TYPE=c++_shared -DCMAKE_ANDROID_NDK=/Users/jaanis/Library/Android/sdk/ndk/21.0.6113669 -DCMAKE_SYSTEM_VERSION=21 -DCMAKE_ANDROID_ARCH_ABI=x86

make

# the outputs will be inside `x32` or `x64` sub-directory of nakasendo-build-directory

# end the terminal session & start a new one for building a different build-target

```


TODO
TODO
TODO IS THE OPENSSL BUILT AS DEBUG? ACORDING TO DOCS, IT SHOULD IMPLICITYLY MAKE AS RELEASE
TODO
TODO
TODO





















