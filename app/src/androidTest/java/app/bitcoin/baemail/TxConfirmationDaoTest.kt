package app.bitcoin.baemail

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.dao.CoinDao
import app.bitcoin.baemail.room.dao.TxConfirmationDao
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class TxConfirmationDaoTest {

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var coinDao: CoinDao
    private lateinit var txConfirmationDao: TxConfirmationDao
    private lateinit var appDatabase: AppDatabase


    @Before
    fun coroutinesSetup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun coroutinesTearDown() {
        Dispatchers.resetMain()

        testDispatcher.cleanupTestCoroutines()
    }

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        txConfirmationDao = appDatabase.txConfirmationDao()
        coinDao = appDatabase.coinDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.close()
    }


    @Test
    @Throws(Exception::class)
    fun testGetForPaymail() {
        runBlocking {
            TxConfirmation(
                "abc0123",
                1000,
                "0000abc0123",
                123400L
            ).let {
                txConfirmationDao.insertConfirmationInfo(it)
            }

            TxConfirmation(
                "abc0124",
                1001,
                "0000abc0124",
                123401L
            ).let {
                txConfirmationDao.insertConfirmationInfo(it)
            }

            TxConfirmation(
                "abc0125",
                1002,
                "0000abc0125",
                123402L
            ).let {
                txConfirmationDao.insertConfirmationInfo(it)
            }

            Coin(
                "p0@moneybutton.com",
                "/0/3",
                3,
                "addr03",
                "abc0124",
                0,
                13,
                null
            ).let {
                coinDao.insertCoin(it)
            }

            Coin(
                "p1@moneybutton.com",
                "/0/4",
                4,
                "addr04",
                "abc01245",
                0,
                13,
                null
            ).let {
                coinDao.insertCoin(it)
            }

            Coin(
                "p2@moneybutton.com",
                "/0/5",
                5,
                "addr05",
                "000abc012456",
                0,
                13,
                "abc0125"
            ).let {
                coinDao.insertCoin(it)
            }










            val unspentCoinsOfPaymail = coinDao.getUnspentFundingCoins("p0@moneybutton.com")
            Assert.assertEquals(1, unspentCoinsOfPaymail.size)


            val txConfirmation = txConfirmationDao.getByTxHash("abc0124")
            Assert.assertNotNull(txConfirmation)
            Assert.assertEquals(1001, txConfirmation!!.blockHeight)


            val confirmationsForPaymail = txConfirmationDao.getForPaymail("p0@moneybutton.com")
            Assert.assertEquals(1, confirmationsForPaymail.size)
            Assert.assertEquals(1001, confirmationsForPaymail[0].blockHeight)


            val confirmationsForPaymail1 = txConfirmationDao.getForPaymail("p1@moneybutton.com")
            Assert.assertEquals(0, confirmationsForPaymail1.size)

            val confirmationsForPaymail2 = txConfirmationDao.getForPaymail("p2@moneybutton.com")
            Assert.assertEquals(1, confirmationsForPaymail2.size)
            Assert.assertEquals(1002, confirmationsForPaymail2[0].blockHeight)






        }
    }

//    @Test
//    @Throws(Exception::class)
//    fun testClearForHeight() {
//        runBlocking {
//
//            TxConfirmation(
//                "abc0123",
//                1000,
//                "0000abc0123",
//                123400L
//            ).let {
//                txConfirmationDao.insertConfirmationInfo(it)
//            }
//
//            TxConfirmation(
//                "abc0124",
//                2000,
//                "0000abc0124",
//                123400L
//            ).let {
//                txConfirmationDao.insertConfirmationInfo(it)
//            }
//
//            TxConfirmation(
//                "abc0125",
//                3000,
//                "0000abc0125",
//                123400L
//            ).let {
//                txConfirmationDao.insertConfirmationInfo(it)
//            }
//
//            TxConfirmation(
//                "abc0126",
//                4000,
//                "0000abc0126",
//                123400L
//            ).let {
//                txConfirmationDao.insertConfirmationInfo(it)
//            }
//
//            TxConfirmation(
//                "abc0127",
//                4000,
//                "0000abc0127",
//                123400L
//            ).let {
//                txConfirmationDao.insertConfirmationInfo(it)
//            }
//
//
//
//        }
//    }


}