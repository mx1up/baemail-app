"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.instance = exports.SuperAssetClient = exports.SA10 = exports.AssetID = void 0;
const axios_1 = __importDefault(require("axios"));
const StandardContracts_1 = require("./StandardContracts");
const minercraft_1 = __importDefault(require("minercraft"));
//import * as Minercraft from 'minercraft';
const { bsv, buildContractClass, getPreimage, getPreimageOpt, toHex, num2bin, Bytes, signTx, PubKey, SigHashPreimage, Sig, Ripemd160, SigHash } = require('scryptlib');
const defaultOptions = {
    mapi_base_url: 'https://public.txq-app.com',
    utxo_url: 'https://api.mattercloud.io/api/v3/main/address/ADDRESS_STR/utxo',
    feeb: 0.5,
    minfee: 1000,
    verbose: false,
};
class AssetID {
    constructor(assetId) {
        this.assetId = assetId;
    }
    toString() {
        return this.assetId;
    }
    toLE() {
        const txid = Buffer.from(this.assetId.substr(0, 64), 'hex').reverse().toString('hex');
        const reversed = Buffer.from(this.assetId.substr(64, 8), 'hex').reverse().toString('hex');
        console.log('toLE', this.assetId, txid, reversed);
        return txid + reversed;
    }
}
exports.AssetID = AssetID;
class SA10 {
    constructor(providedOptions) {
        this.options = Object.assign({}, defaultOptions, providedOptions);
    }
    setOptions(newOptions) {
        this.options = Object.assign({}, this.options, newOptions);
    }
    getUtxoUrl(addresses) {
        return this.options.utxo_url.replace('ADDRESS_STR', addresses);
    }
    createLockingTx(address, amountSatoshis, fee) {
        return __awaiter(this, void 0, void 0, function* () {
            let { data: utxos } = yield axios_1.default.get(this.getUtxoUrl(address));
            /**
            utxos = utxos.map((utxo) => ({
              txId: utxo.tx_hash,
              outputIndex: utxo.tx_pos,
              satoshis: utxo.value,
              script: bsv.Script.buildPublicKeyHashOut(address).toHex(),
            }))
            **/
            const tx = new bsv.Transaction().from(utxos);
            tx.addOutput(new bsv.Transaction.Output({
                script: new bsv.Script(),
                satoshis: amountSatoshis,
            }));
            tx.change(address).fee(fee || this.options.minfee);
            const hexCode = bsv.Script.fromASM(StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc.asm);
            tx.outputs[0].setScript(hexCode);
            return tx;
        });
    }
    deploy(initialOwnerPublicKey, satoshis, fundingPrivateKey) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                const Token = buildContractClass(StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc);
                const token = new Token();
                const fundingAddress = new bsv.PrivateKey(fundingPrivateKey).toAddress().toString();
                let fee = 0;
                // Simulate tx first
                {
                    const lockingTx = yield this.createLockingTx(fundingAddress, satoshis, 1000);
                    const initialState = `OP_RETURN 000000000000000000000000000000000000000000000000000000000000000000000000 ${initialOwnerPublicKey}`;
                    const initialLockingScript = bsv.Script.fromASM(`${token.lockingScript.toASM()} ${initialState}`);
                    lockingTx.outputs[0].setScript(initialLockingScript);
                    lockingTx.sign(fundingPrivateKey);
                    fee = Math.ceil((lockingTx.toString().length / 2) * this.options.feeb);
                }
                // Now actually create it.
                const lockingTx = yield this.createLockingTx(fundingAddress, satoshis, fee);
                const initialState = `OP_RETURN 000000000000000000000000000000000000000000000000000000000000000000000000 ${initialOwnerPublicKey}`;
                const initialLockingScript = bsv.Script.fromASM(`${token.lockingScript.toASM()} ${initialState}`);
                lockingTx.outputs[0].setScript(initialLockingScript);
                lockingTx.sign(fundingPrivateKey);
                if (this.options.verbose) {
                    console.log('deploy::lockingTx', lockingTx);
                }
                // Publish initial deploy
                console.log('mapi api', this.options.mapi_base_url);
                const miner = new minercraft_1.default({
                    url: this.options.mapi_base_url,
                    headers: { 'content-type': 'application/json' }
                });
                try {
                    const response = yield miner.tx.push(lockingTx.toString(), {});
                    if (response && response.returnResult === 'success' && response.txid === lockingTx.hash) {
                        return resolve({
                            txid: lockingTx.hash,
                            index: 0,
                            assetId: new AssetID(`${lockingTx.hash}00000000`),
                            assetStaticCode: StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc.asm,
                            assetLockingScript: initialLockingScript.toASM(),
                            assetOwnerPublicKey: initialOwnerPublicKey,
                            assetSatoshis: satoshis,
                            assetPayload: null,
                        });
                    }
                    reject(response);
                }
                catch (err) {
                    reject(err);
                }
            }));
        });
    }
    fetchUtxoLargeThan(address, value) {
        return __awaiter(this, void 0, void 0, function* () {
            let { data: utxos } = yield axios_1.default.get(this.getUtxoUrl(address));
            /** utxos = utxos.map((utxo) => ({
              txId: utxo.tx_hash,
              outputIndex: utxo.tx_pos,
              satoshis: utxo.value,
              script: bsv.Script.buildPublicKeyHashOut(address).toHex(),
            }))**/
            for (const utxo of utxos) {
                if (utxo.satoshis > value) {
                    return utxo;
                }
            }
            throw new Error('Insufficient funds for ' + address + ', largerThan: ' + value);
        });
    }
    unlockP2PKHInput(privateKey, tx, inputIndex, sigtype) {
        const sig = new bsv.Transaction.Signature({
            publicKey: privateKey.publicKey,
            prevTxId: tx.inputs[inputIndex].prevTxId,
            outputIndex: tx.inputs[inputIndex].outputIndex,
            inputIndex,
            signature: bsv.Transaction.Sighash.sign(tx, privateKey, sigtype, inputIndex, tx.inputs[inputIndex].output.script, tx.inputs[inputIndex].output.satoshisBN),
            sigtype,
        });
        tx.inputs[inputIndex].setScript(bsv.Script.buildPublicKeyHashIn(sig.publicKey, sig.signature.toDER(), sig.sigtype));
    }
    transfer(assetState, currentOwnerPrivateKey, nextOwnerPublicKey, fundingPrivateKey, payloadUpdate) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.options.verbose) {
                //console.log('assetState', assetState);
                console.log('assetStateId', assetState.assetId.toString(), assetState.assetId.toLE());
                console.log('currentOwnerPrivateKey', currentOwnerPrivateKey);
                console.log('nextOwnerPublicKey', nextOwnerPublicKey);
                console.log('fundingPrivateKey', fundingPrivateKey);
                console.log('payloadUpdate', payloadUpdate);
            }
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                const HEX_REGEX = new RegExp('^[0-9a-fA-F]+$');
                if (payloadUpdate && payloadUpdate !== '' && (!HEX_REGEX.test(payloadUpdate) || (payloadUpdate.length % 2 !== 0))) {
                    return reject(new Error('Invalid payload. Even length hex string required.'));
                }
                const Token = buildContractClass(StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc);
                const token = new Token();
                const fundingPrivatePK = new bsv.PrivateKey(fundingPrivateKey);
                const fundingPublicKey = bsv.PublicKey.fromPrivateKey(fundingPrivatePK);
                const currentOwnerPK = new bsv.PrivateKey(currentOwnerPrivateKey);
                const payloadData = payloadUpdate && payloadUpdate !== '' ? payloadUpdate : null;
                // Note the usage of toLE(), this is because in bitcoin script the outpoint is in little endian and we save script size by doing it here in js land
                const newState = payloadData ? `${assetState.assetId.toLE()} ${nextOwnerPublicKey} ${payloadData}` : `${assetState.assetId.toLE()} ${nextOwnerPublicKey}`;
                const newLockingScript = bsv.Script.fromASM(`${token.codePart.toASM()} ${newState}`);
                let estimatedFee = 0;
                {
                    const tx = new bsv.Transaction();
                    const utxo = yield this.fetchUtxoLargeThan(fundingPrivatePK.toAddress(), 2000);
                    if (this.options.verbose) {
                        console.log('utxo', utxo);
                    }
                    token.setDataPart(newState);
                    tx.addInput(new bsv.Transaction.Input({
                        prevTxId: assetState.txid,
                        outputIndex: assetState.index,
                        script: ''
                    }), bsv.Script.fromHex(assetState.assetLockingScript), assetState.assetSatoshis);
                    // Add funding input
                    tx.addInput(new bsv.Transaction.Input({
                        prevTxId: utxo.txid,
                        outputIndex: utxo.outputIndex,
                        script: ''
                    }), bsv.Script.fromHex(utxo.script), utxo.satoshis);
                    const FEE = 10000; // Just a guess. It is used only for estimation
                    const changeSatoshis = Math.floor(utxo.satoshis - FEE);
                    tx.addOutput(new bsv.Transaction.Output({
                        script: newLockingScript,
                        satoshis: assetState.assetSatoshis
                    }));
                    const changeOutputScript = bsv.Script.buildPublicKeyHashOut(fundingPublicKey);
                    tx.addOutput(new bsv.Transaction.Output({
                        script: changeOutputScript,
                        satoshis: changeSatoshis
                    }));
                    const Signature = bsv.crypto.Signature;
                    const sighashType = Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_ALL | Signature.SIGHASH_FORKID;
                    const preimage = getPreimage(tx, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                    const sig = signTx(tx, currentOwnerPK, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                    if (this.options.verbose) {
                        console.log('---------------------------------------------------------------------------');
                        console.log('preimage', preimage.toJSON(), preimage.toString(), 'signature', toHex(sig));
                    }
                    const pkh = bsv.crypto.Hash.sha256ripemd160(fundingPublicKey.toBuffer());
                    const changeAddress = pkh.toString('hex'); // Needs to be unprefixed address
                    const unlockingScript = token.transfer(new Sig(toHex(sig)), new PubKey(nextOwnerPublicKey), preimage, new Ripemd160(changeAddress), changeSatoshis, payloadData ? new Bytes(payloadData) : new Bytes('')).toScript();
                    tx.inputs[0].setScript(unlockingScript);
                    this.unlockP2PKHInput(fundingPrivatePK, tx, 1, Signature.SIGHASH_ALL | Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_FORKID);
                    estimatedFee = Math.ceil((tx.toString().length / 2) * this.options.feeb) + 1;
                }
                const tx = new bsv.Transaction();
                const utxo = yield this.fetchUtxoLargeThan(fundingPrivatePK.toAddress(), 2000);
                if (this.options.verbose) {
                    console.log('utxo', utxo);
                }
                token.setDataPart(newState);
                tx.addInput(new bsv.Transaction.Input({
                    prevTxId: assetState.txid,
                    outputIndex: assetState.index,
                    script: ''
                }), bsv.Script.fromHex(assetState.assetLockingScript), assetState.assetSatoshis);
                // Add funding input
                tx.addInput(new bsv.Transaction.Input({
                    prevTxId: utxo.txid,
                    outputIndex: utxo.outputIndex,
                    script: ''
                }), bsv.Script.fromHex(utxo.script), utxo.satoshis);
                const FEE = estimatedFee;
                const changeSatoshis = Math.floor(utxo.satoshis - FEE);
                tx.addOutput(new bsv.Transaction.Output({
                    script: newLockingScript,
                    satoshis: assetState.assetSatoshis
                }));
                const changeOutputScript = bsv.Script.buildPublicKeyHashOut(fundingPublicKey);
                tx.addOutput(new bsv.Transaction.Output({
                    script: changeOutputScript,
                    satoshis: changeSatoshis
                }));
                const Signature = bsv.crypto.Signature;
                const sighashType = Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_ALL | Signature.SIGHASH_FORKID;
                const preimage = getPreimage(tx, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                const sig = signTx(tx, currentOwnerPK, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                if (this.options.verbose) {
                    console.log('---------------------------------------------------------------------------');
                    console.log('preimage', preimage.toJSON(), preimage.toString(), 'signature', toHex(sig));
                }
                const pkh = bsv.crypto.Hash.sha256ripemd160(fundingPublicKey.toBuffer());
                const changeAddress = pkh.toString('hex'); // Needs to be unprefixed address
                const unlockingScript = token.transfer(new Sig(toHex(sig)), new PubKey(nextOwnerPublicKey), preimage, new Ripemd160(changeAddress), changeSatoshis, payloadData ? new Bytes(payloadData) : new Bytes('')).toScript();
                tx.inputs[0].setScript(unlockingScript);
                this.unlockP2PKHInput(fundingPrivatePK, tx, 1, Signature.SIGHASH_ALL | Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_FORKID);
                // Publish initial deploy
                const miner = new minercraft_1.default({
                    url: this.options.mapi_base_url,
                    headers: { 'content-type': 'application/json' }
                });
                if (this.options.verbose) {
                    console.log('tx', tx.toString());
                }
                const response = yield miner.tx.push(tx.toString(), {});
                if (response && response.returnResult === 'success' && response.txid === tx.hash) {
                    return resolve({
                        txid: tx.hash,
                        index: 0,
                        txoutpoint: `${tx.hash}_o0`,
                        assetId: assetState.assetId,
                        assetStaticCode: StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc.asm,
                        assetLockingScript: newLockingScript.toASM(),
                        assetOwnerPublicKey: nextOwnerPublicKey,
                        assetSatoshis: assetState.assetSatoshis,
                        assetPayload: payloadData,
                    });
                }
                reject(response);
            }));
        });
    }
    melt(assetState, currentOwnerPrivateKey, receiverPublicKey, fundingPrivateKey) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.options.verbose) {
                //console.log('assetState', assetState);
                console.log('assetStateId', assetState.assetId.toString(), assetState.assetId.toLE());
                console.log('currentOwnerPrivateKey', currentOwnerPrivateKey);
                console.log('receiverPublicKey', receiverPublicKey);
                console.log('fundingPrivateKey', fundingPrivateKey);
            }
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                const Token = buildContractClass(StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc);
                const token = new Token();
                const fundingPrivatePK = new bsv.PrivateKey(fundingPrivateKey);
                const fundingPublicKey = bsv.PublicKey.fromPrivateKey(fundingPrivatePK);
                const currentOwnerPK = new bsv.PrivateKey(currentOwnerPrivateKey);
                const receiverPublicKeyPK = new bsv.PublicKey(receiverPublicKey);
                // Note the usage of toLE(), this is because in bitcoin script the outpoint is in little endian and we save script size by doing it here in js land
                let estimatedFee = 0;
                {
                    const tx = new bsv.Transaction();
                    const utxo = yield this.fetchUtxoLargeThan(fundingPrivatePK.toAddress(), 2000);
                    if (this.options.verbose) {
                        console.log('utxo', utxo);
                    }
                    tx.addInput(new bsv.Transaction.Input({
                        prevTxId: assetState.txid,
                        outputIndex: assetState.index,
                        script: ''
                    }), bsv.Script.fromHex(assetState.assetLockingScript), assetState.assetSatoshis);
                    // Add funding input
                    tx.addInput(new bsv.Transaction.Input({
                        prevTxId: utxo.txid,
                        outputIndex: utxo.outputIndex,
                        script: ''
                    }), bsv.Script.fromHex(utxo.script), utxo.satoshis);
                    const FEE = 10000; // Just a guess. It is used only for estimation
                    const changeSatoshis = Math.floor(utxo.satoshis - FEE);
                    const receiverOutputScript = bsv.Script.buildPublicKeyHashOut(receiverPublicKeyPK);
                    tx.addOutput(new bsv.Transaction.Output({
                        script: receiverOutputScript,
                        satoshis: assetState.assetSatoshis
                    }));
                    const changeOutputScript = bsv.Script.buildPublicKeyHashOut(fundingPublicKey);
                    tx.addOutput(new bsv.Transaction.Output({
                        script: changeOutputScript,
                        satoshis: changeSatoshis
                    }));
                    const Signature = bsv.crypto.Signature;
                    const sighashType = Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_ALL | Signature.SIGHASH_FORKID;
                    const preimage = getPreimage(tx, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                    const sig = signTx(tx, currentOwnerPK, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                    if (this.options.verbose) {
                        console.log('---------------------------------------------------------------------------');
                        console.log('preimage', preimage.toJSON(), preimage.toString(), 'signature', toHex(sig));
                    }
                    const pkh = bsv.crypto.Hash.sha256ripemd160(fundingPublicKey.toBuffer());
                    const changeAddress = pkh.toString('hex'); // Needs to be unprefixed address
                    const recpkh = bsv.crypto.Hash.sha256ripemd160(receiverPublicKeyPK.toBuffer());
                    const recAddress = recpkh.toString('hex'); // Needs to be unprefixed address
                    const unlockingScript = token.melt(new Sig(toHex(sig)), new Ripemd160(recAddress), preimage, new Ripemd160(changeAddress), changeSatoshis).toScript();
                    tx.inputs[0].setScript(unlockingScript);
                    this.unlockP2PKHInput(fundingPrivatePK, tx, 1, Signature.SIGHASH_ALL | Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_FORKID);
                    estimatedFee = Math.ceil((tx.toString().length / 2) * this.options.feeb) + 1;
                }
                const tx = new bsv.Transaction();
                const utxo = yield this.fetchUtxoLargeThan(fundingPrivatePK.toAddress(), 2000);
                if (this.options.verbose) {
                    console.log('utxo', utxo);
                }
                tx.addInput(new bsv.Transaction.Input({
                    prevTxId: assetState.txid,
                    outputIndex: assetState.index,
                    script: ''
                }), bsv.Script.fromHex(assetState.assetLockingScript), assetState.assetSatoshis);
                // Add funding input
                tx.addInput(new bsv.Transaction.Input({
                    prevTxId: utxo.txid,
                    outputIndex: utxo.outputIndex,
                    script: ''
                }), bsv.Script.fromHex(utxo.script), utxo.satoshis);
                const FEE = estimatedFee;
                const changeSatoshis = Math.floor(utxo.satoshis - FEE);
                const receiverOutputScript = bsv.Script.buildPublicKeyHashOut(receiverPublicKeyPK);
                tx.addOutput(new bsv.Transaction.Output({
                    script: receiverOutputScript,
                    satoshis: assetState.assetSatoshis
                }));
                const changeOutputScript = bsv.Script.buildPublicKeyHashOut(fundingPublicKey);
                tx.addOutput(new bsv.Transaction.Output({
                    script: changeOutputScript,
                    satoshis: changeSatoshis
                }));
                const Signature = bsv.crypto.Signature;
                const sighashType = Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_ALL | Signature.SIGHASH_FORKID;
                const preimage = getPreimage(tx, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                const sig = signTx(tx, currentOwnerPK, assetState.assetLockingScript, assetState.assetSatoshis, 0, sighashType);
                if (this.options.verbose) {
                    console.log('---------------------------------------------------------------------------');
                    console.log('preimage', preimage.toJSON(), preimage.toString(), 'signature', toHex(sig));
                }
                const pkh = bsv.crypto.Hash.sha256ripemd160(fundingPublicKey.toBuffer());
                const changeAddress = pkh.toString('hex'); // Needs to be unprefixed address
                const recpkh = bsv.crypto.Hash.sha256ripemd160(receiverPublicKeyPK.toBuffer());
                const recAddress = recpkh.toString('hex'); // Needs to be unprefixed address
                const unlockingScript = token.melt(new Sig(toHex(sig)), new Ripemd160(recAddress), preimage, new Ripemd160(changeAddress), changeSatoshis).toScript();
                tx.inputs[0].setScript(unlockingScript);
                this.unlockP2PKHInput(fundingPrivatePK, tx, 1, Signature.SIGHASH_ALL | Signature.SIGHASH_ANYONECANPAY | Signature.SIGHASH_FORKID);
                // Publish initial deploy
                const miner = new minercraft_1.default({
                    url: this.options.mapi_base_url,
                    headers: { 'content-type': 'application/json' }
                });
                if (this.options.verbose) {
                    console.log('tx', tx.toString());
                }
                const response = yield miner.tx.push(tx.toString(), {});
                if (response && response.returnResult === 'success' && response.txid === tx.hash) {
                    return resolve({
                        txid: tx.hash,
                        index: 0,
                        txoutpoint: `${tx.hash}_o0`,
                        meltedAssetId: assetState.assetId,
                        meltedAssetStaticCode: StandardContracts_1.StandardContracts.getSuperAsset10().scryptDesc.asm,
                        meltedAssetOwnerPublicKey: receiverPublicKey,
                        meltedAssetSatoshis: assetState.assetSatoshis
                    });
                }
                reject(response);
            }));
        });
    }
}
exports.SA10 = SA10;
class SuperAssetClient {
    constructor(providedOptions) {
        this.options = Object.assign({}, defaultOptions, providedOptions);
    }
    setOptions(newOptions) {
        this.options = Object.assign({}, this.options, newOptions);
    }
    getUtxoUrl(addresses) {
        return this.options.utxo_url.replace('ADDRESS_STR', addresses);
    }
    SA10(newOptions) {
        const mergedOptions = Object.assign({}, this.options, newOptions);
        return new SA10(mergedOptions);
    }
    instance(newOptions) {
        const mergedOptions = Object.assign({}, defaultOptions, newOptions);
        return new SuperAssetClient(mergedOptions);
    }
}
exports.SuperAssetClient = SuperAssetClient;
function instance(newOptions) {
    const mergedOptions = Object.assign({}, defaultOptions, newOptions);
    return new SuperAssetClient(mergedOptions);
}
exports.instance = instance;
// try {
//   if (window) {
//     window['superasset'] = {
//       instance: instance
//     };
//   }
// }
// catch (ex) {
//   // Window is not defined, must be running in windowless node env...
// }
