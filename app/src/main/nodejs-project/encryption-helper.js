'use strict';


//possible improvements ... https://proandroiddev.com/security-best-practices-symmetric-encryption-with-aes-in-java-and-android-part-2-b3b80e99ad36
function EncryptionHelper(aes) {
    this.aes = aes;
    this.paddingContent = this.aes.utils.utf8.toBytes('BitCoinBitCoinBitCoinBitCoinBitCoinBitCoinBitCoinBitCoin');
}

module.exports = EncryptionHelper;



EncryptionHelper.prototype.initialise = function(hexKey, hexIv) {
    this.key = this.aes.utils.hex.toBytes(hexKey);
    this.iv = this.aes.utils.hex.toBytes(hexIv);

    this.initialised = true;
};


EncryptionHelper.prototype.checkInitialised = function() {
    if (!this.initialised) throw new Error();
};


EncryptionHelper.prototype.intFromBytes = function(x){
    var val = 0;
    for (var i = 0; i < 4; ++i) {
        val += x[i];
        if (i < x.length-1) {
            val = val << 8;
        }
    }
    return val;
};


EncryptionHelper.prototype.getInt32Bytes = function(x){
    var bytes = [];
    var i = 4;
    do {
        bytes[--i] = x & (255);
        x = x>>8;
    } while (i);
    return bytes;
};


/**
 *
 * @param {string} message text to encrypt
 * @returns hex-string of cipher-text
 */
EncryptionHelper.prototype.encrypt = function(message) {
    this.checkInitialised();

    var textBytes = this.aes.utils.utf8.toBytes(message);

    var bytesMissingUntilRound = 32 - (textBytes.length % 32);

    var totalPadding = 0;
    if (bytesMissingUntilRound < 4) {
        totalPadding = 32 + bytesMissingUntilRound;
    } else {
        totalPadding = bytesMissingUntilRound;
    }

    var lengthOfFollowingPadding = totalPadding - 4;

    var paddingArray = Array.from(this.paddingContent.slice(0, lengthOfFollowingPadding));

    var paddedBytes = [];
    paddedBytes = paddedBytes.concat(this.getInt32Bytes(lengthOfFollowingPadding));
    paddedBytes = paddedBytes.concat(paddingArray);
    paddedBytes = paddedBytes.concat(Array.from(textBytes));

    var encCipher = new this.aes.ModeOfOperation.cbc(this.key, this.iv);
    var encryptedBytes = encCipher.encrypt(paddedBytes);

    return this.aes.utils.hex.fromBytes(encryptedBytes);
};

/**
 *
 * @param {string} hexCipherText
 * @returns plaintext message
 */
EncryptionHelper.prototype.decrypt = function(hexCipherText) {
    this.checkInitialised();

    var encryptedBytes = this.aes.utils.hex.toBytes(hexCipherText);

    var decCipher = new this.aes.ModeOfOperation.cbc(this.key, this.iv);
    var decryptedBytes = decCipher.decrypt(encryptedBytes);

    var remainingPadding = this.intFromBytes(decryptedBytes.slice(0, 4));

    var plainBytes = decryptedBytes.slice(4 + remainingPadding);

    return this.aes.utils.utf8.fromBytes(plainBytes);
};




