'use strict';

const aesjs = require('aes-js');

const path = require('path');
const fs = require('fs');

const WebSocket = require('ws');

const EncryptionHelper = require('./encryption-helper');



// Both the node project & the secrets file are located inside of the
// android-application's sand-boxed directories. Rooted android-devices can RIP.
// It seems like all of the secret stuff can theoretically be passed in as args when starting Node;
// in that scenario, everything should be secure from spying by other android-apps, not sure about
// required node-modules.


function locateSecrets() {
    var appDir = path.dirname(require.main.filename);

    var projectParentDir = path.resolve(appDir, '..');
    var secretsFile = projectParentDir + path.sep + 'NodeComms' + path.sep + 'secrets';

    var secretsContent = fs.readFileSync(secretsFile, 'utf8').toString().split('\n');

    //todo delete the secrets-file

    return {
        key: secretsContent[0],
        iv: secretsContent[1],
        url: secretsContent[2]
    };
}





function CommsHelper() {
    this.encryptionHelper = new EncryptionHelper(aesjs);
}

module.exports = CommsHelper;



CommsHelper.prototype.connect = function() {

    var secrets = locateSecrets();

    this.encryptionHelper.initialise(
        secrets.key,
        secrets.iv
    );

    var that = this

    this.ws = new WebSocket(secrets.url);

    this.ws.on('open', function open() {
        that.onOpen();
    });

    this.ws.on('message', function incoming(data) {
        var decryptedMessage = that.encryptionHelper.decrypt(data);
        that.onMessage(decryptedMessage);
    });

    this.ws.on('error', function incoming(data) {
        that.onError(data);
     });
};

/**
 * @param {string}
 */
CommsHelper.prototype.sendMessage = function(message) {
    var encryptedHex = this.encryptionHelper.encrypt(message);
    this.ws.send(encryptedHex);
};

CommsHelper.prototype.onMessage = function(message) {
    console.log('on received message from web-socket; override this to do something with it; ' + message);
};

CommsHelper.prototype.onOpen = function() {
    console.log('on connected to web-socket; override this to do something now');
};

CommsHelper.prototype.onError = function(error) {
    console.log('on error to web-socket; override this to do something now; ' + error);
};






