'use strict';




function AddXpriv(context) {
    this.context = context
    this.command = 'ADD_XPRIV'
}


module.exports = AddXpriv;


AddXpriv.prototype.buildRequest = function(callback, xpriv) {
    var params = [xpriv];

    return {
        command: this.command,
        params: params,
        callback: callback
    };
};

AddXpriv.prototype.onResponse = function(status, dataArray, request) {

    if (status == 'success') {
        var alias = dataArray[0];
        request.callback(true, alias);
        return
    }

    request.callback(false, null);

};