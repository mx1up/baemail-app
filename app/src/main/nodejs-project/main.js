

const bsv = require('bsv')
bsv.Message = require('bsv/message')

const mnemonic = require('bsv/mnemonic');

const ECIES = require('bsv/ecies');



const context = {
    bsv: bsv,
    mnemonic: mnemonic,
    ECIES: ECIES,
    moneyButtonDerivationPath: "m/44'/0'/0'/0/0/0",
    simplyCashDerivationPath: "m/44'/145'/0'/2/0",
    relayXPath: "m/44'/236'/0'",
    fundingWalletDerivationPathRoot: "m/44'/0'/0'"
}






console.log('start of `main.js`; node version: ' + process.version);






//prepare all of the requests for android-work
const AddXpriv = require('./requests/add-xpriv');
const SignRequest = require('./requests/sign-request');

const androidWorkRequests = {
    addXpriv: new AddXpriv(context),
    signRequest: new SignRequest(context)
    //todo add others
}





///////



const CommsHelper = require('./comms-helper');

const MoneyButtonPublicKeyFromSeed = require('./handlers/money-button-public-key-from-seed');
const PrivateKeyForPathFromSeed = require('./handlers/private-key-for-path');
const SignMessage = require('./handlers/sign-message');
const CheckMnemonicIsValid = require('./handlers/check-mnemonic-is-valid');
const DeriveNextAddressSet = require('./handlers/derive-next-address-set');
const CreateSimpleTx = require('./handlers/create-simple-tx');
const CreateSimpleOutputScriptTx = require('./handlers/create-simple-output-script-tx');
const CreateBaemailMessageTx = require('./handlers/create-baemail-message-tx');
const DecryptPkiEncryptedData = require('./handlers/decrypt-pki-encrypted-data');
const SignMessageWithXpriv = require('./handlers/sign-message-with-xpriv');
const SuperAssetDeployNft = require('./handlers/super-asset-deploy-nft');



///////////////////////////

//all supported handlers
const requestHandlers = [
    new MoneyButtonPublicKeyFromSeed(context),
    new PrivateKeyForPathFromSeed(context),
    new SignMessage(context),
    new CheckMnemonicIsValid(context),
    new DeriveNextAddressSet(context),
    new CreateSimpleTx(context),
    new CreateSimpleOutputScriptTx(context),
    new CreateBaemailMessageTx(context),
    new DecryptPkiEncryptedData(context),
    new SignMessageWithXpriv(context),
    new SuperAssetDeployNft(context),
    //add others
];


function respondWithHandlerNotFound(request, sendResponse) {
    var response = {
            token: request.token,
            content: {
                message_type: 'node_work_request',
                status: 'fail',
                data: ['HANDLER_NOT_FOUND']
            }
        };

    sendResponse(response);
}


////////////////////////


function HandlerCallback(request, sendResponse) {
    this.request = request;
    this.sendResponse = sendResponse;
};

HandlerCallback.prototype.processingCallback = function(isSuccess, data) {
    var responseStatus = 'fail';
    if (isSuccess) {
        responseStatus = 'success';
    }

    var response = {
                message_type: 'node_work_request',
        token: this.request.token,
        content: {
            status: responseStatus,
            data: data
        }
    };

    this.sendResponse(JSON.stringify(response));
};



var commsHelper = new CommsHelper();

commsHelper.onOpen = function() {
    console.log('socket-on-open');
};

commsHelper.onError = function(error) {
    console.log('socket-on-error: ' + error);
};

commsHelper.onMessage = function(message) {
    if (message === '') {
        console.log('onMessage: `' + message + '`; ignoring !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        return;
    }
    var parsedMessage = JSON.parse(message);

    var messageType = parsedMessage.message_type;
    if (messageType == 'node_work_request') {
        handleIncomingWorkRequest(parsedMessage);

    } else if (messageType == 'android_work_request') {
        handleAndroidWorkResponse(parsedMessage);

    } else {
        console.log('socket message is missing message_type');
        return;
    }

};

commsHelper.connect();












console.log('ws should be connecting to the app')

setInterval(function(){ console.log('teee-hee')}, 4000)










// an example usage of android-work
//setTimeout(function() {
//    console.log('add xpriv...');
//
//
//    var xpriv = 'xprv9s21ZrQH143K2CMe3bWsWU9FDU1GLFXRG67SX3AjZ614cgD2fCQjBJDC4gWZ7fjzWvJJSBDG4PZWDYS1nPfbHDiwmqDRvjwhPJ86TZZ6byb';
//
//
//    var addXprivCallback = (status, keyAlias) => {
//        console.log('response to -- add xpriv... status:' + status + ' keyAlias:' + keyAlias);
//
//        if (!status) return
//
//
//        //attempt signing req
//
//        var signRequestCallback = (status, signature) => {
//            console.log('response to -- signRequest... status:' + status + ' signature:' + signature);
//
//        };
//
//
//        var workRequest = context.activeAndroidWork.requests.signRequest
//                .buildRequest(signRequestCallback, keyAlias, "m/44'/0'/0'/0/0/0", 'message00');
//
//        context.activeAndroidWork.requestAndroidWork(workRequest)
//
//        console.log('submitted android work -- signRequest');
//
//
//    };
//
//    var workRequest = context.activeAndroidWork.requests.addXpriv
//        .buildRequest(addXprivCallback, xpriv);
//
//    context.activeAndroidWork.requestAndroidWork(workRequest);
//
//    console.log('submitted android work -- addXpriv');
//
//}, 4000);








////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////
////////////////////////////

const activeAndroidWork = {
    uniqueId: 0, //keeps the last-used token value; this is used to gen new, unique ones
    map: {}, //keeps track of all running android-work-requests, mapped by request-token
    requests: androidWorkRequests //keeps all supported/implemented android-work-requests
};

context.activeAndroidWork = activeAndroidWork;






/**
* Get the next token valid for use with android-work-requests.
*/
activeAndroidWork.getNextId = function() {
    var id = activeAndroidWork.uniqueId;
    activeAndroidWork.uniqueId++;
    return id;
};

/**
* Update global state so this work-request-response can be handled correctly.
* Send the request to android-code.
*/
activeAndroidWork.requestAndroidWork = function(request) {

    try {
        //crash on an unsupported situation
        var existingWork = activeAndroidWork.map[request.token];
        if (existingWork != undefined) throw Error('This work-request is already active.');

        //remember it, it will be required when work-result comes back
        var token = activeAndroidWork.getNextId();
        request.token = token;
        activeAndroidWork.map[token] = request;

        //send the request
        commsHelper.sendMessage(JSON.stringify({
           message_type: 'android_work_request',
           command: request.command,
           token: request.token,
           params: request.params
        }));

        console.log('AndroidWork requested; token: ' + request.token);


    } catch(error) {
        console.log(error, request, 'Failed to request android-work.');
    }

};


var handleIncomingWorkRequest = function(parsedMessage) {
    console.log('requestCommand:' + parsedMessage.command);

    //find the appropriate handler
    var matchingHandler = requestHandlers.find(handler => {
        return handler.command == parsedMessage.command;
    })

    //handle the request
    if (matchingHandler == null) {
        console.log('unexpected request; command: ' + parsedMessage.command);
        respondWithHandlerNotFound(parsedMessage, commsHelper.sendMessage.bind(commsHelper));

    } else {
        var handlerCallback = new HandlerCallback(
            parsedMessage,
            commsHelper.sendMessage.bind(commsHelper)
        );

        matchingHandler.handle(
            parsedMessage.params,
            handlerCallback.processingCallback.bind(handlerCallback)
        );

    }
};

var handleAndroidWorkResponse = function(parsedMessage) {

    try {

        //find the matching callback to execute

        var token = parsedMessage.token;
        var content = parsedMessage.content;

        var resultStatus = content.status;
        var resultDataArray = content.data;

        //cleanup
        var matchingFinishedAndroidWork = activeAndroidWork.map[token];
        delete activeAndroidWork.map[token];

        //find the android-work-request matching the returned work result
        var matchingRequest = Object.values(activeAndroidWork.requests).find((item) => {
            return item.command == matchingFinishedAndroidWork.command
        });

        //execute on-response
        matchingRequest.onResponse(resultStatus, resultDataArray, matchingFinishedAndroidWork)

        console.log('android-work-response successful; callback executed; token ' + token);

    } catch(error) {
        console.log(error, parsedMessage, 'error when handling android-work-response');
    }


};


////////////////////////////




//todo context.requestEnterForeground && context.requestExitForeground
//todo
//todo
//todo
//todo







