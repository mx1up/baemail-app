'use strict';



var index = require('../dist/superasset-lib/index.js');









function SuperAssetDeployNft(context) {
    this.context = context
    this.command = 'SUPER_ASSET_DEPLOY_NFT'

    this.sa10 = index.instance({
        feeb: 0.5,
    }).SA10({ verbose: true });

}



module.exports = SuperAssetDeployNft;









/*


#expecting params
[
    {
        "funding": {
            seed: ['seed', 'words', ..],
            rootPath: 'm/44'/0'/ ..',
            coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
            changeAddress: '1abcedf..'
        },
        "owner": {
            "path": "m/44'/0'/0'/1/0",
            "publicKey": "00..ff",
            "privateKey": "00..ff"
        }
    }
]


# response
//todo
//todo
//todo

*/
SuperAssetDeployNft.prototype.handle = function(params, callback) {
    console.log('start of ........ SuperAssetDeployNft');


    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        //verify params
        if (params.length != 1) throw new Error('invalid params');

        var objectParam = params[0];
        var fundingParam = objectParam.funding;
        var ownerParams = objectParam.owner;

        var biggestCoin = fundingParam.coins.reduce((accumulator, currentValue) => {
            if (currentValue.sats > accumulator.sats) {
                return currentValue
            } else {
                return accumulator
            }
        }, fundingParam.coins[0]);
        console.log('biggestCoin' + JSON.stringify(biggestCoin));
        console.log('owner' + JSON.stringify(ownerParams));


        //todo
        //todo
        //todo




        var fun0 = async () => {
            console.log('start of async ........ SuperAssetDeployNft');
            // Step 1: Deploy NFT with initial owner and satoshis value of 2650 (Lower than this may hit dust limit)


            const assetValue = 20000;

            if (biggestCoin.sats < assetValue + 9999) {
                throw Error("coin not big-enough");
            }


            //todo
            //todo

            var fundingWalletMnemonic = this.context.mnemonic.fromString(fundingParam.seed.join(' '));
            var fundingWalletHdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(fundingWalletMnemonic.toSeed());
            var fundingWalletHdPrivateKeyRoot = fundingWalletHdPrivateKey.deriveChild(fundingParam.rootPath);

            var fundingCoinPrivateKey = fundingWalletHdPrivateKeyRoot.deriveChild("m" + biggestCoin.derivation_path).privateKey;

            //todo
            //todo









            const initialOwnerPublicKey = ownerParams.publicKey.toString();
            const fundingPrivateKey = fundingCoinPrivateKey.toString();



            var txId = null
            try {
                var assetState = await this.sa10.deploy(initialOwnerPublicKey, assetValue, fundingPrivateKey);
                txId = assetState.txid
                console.log('Deploy result txid: ', txId , JSON.stringify(assetState));


            } catch(e) {
                console.log(e)
            }


            //finish the response
            responseData = [{
                txId: txId
            }];
            isSuccess = true;

            callback(isSuccess, responseData);

        }

        console.log('pre fun0');
        fun0()
        console.log('post fun0');

        //todo
        //todo











    } catch (e) {
        responseData = [
            e.stack
        ];
        callback(isSuccess, responseData);
    }



}






