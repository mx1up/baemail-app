'use strict';




function PrivateKeyForPathFromSeed(context) {
    this.context = context
    this.command = "PRIVATE_KEY_FOR_PATH_FROM_SEED"
}

module.exports = PrivateKeyForPathFromSeed;





// moneyButtonDerivationPath: "m/44'/0'/0'/0/0/0"



/*

# expected param structure
[
    "m/44'/0'/0'/0/0/0",
    [
        "aaa", "bbb", "ccc", ..
    ]
]



# expected structure of the response data passed in the callback
[
    {
        "publicKey": "publicKey",
        "privateKey": "wif_private_key"
    }
]

*/
PrivateKeyForPathFromSeed.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var derivationPath = params[0];
        var phrase = params[1].join(' ')

        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());
        var pathPrivateKey = hdPrivateKey.deriveChild(derivationPath);
        var publicKey = this.context.bsv.PublicKey.fromPrivateKey(pathPrivateKey.privateKey);


        responseData = [
            {
                publicKey: publicKey.toString(),
                privateKey: pathPrivateKey.privateKey.toWIF()
            }
        ];


        isSuccess = true;

    } catch (e) {
        //console.log(e);
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)
};
