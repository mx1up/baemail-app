'use strict';




function CreateBaemailMessageTx(context) {
    this.context = context
    this.command = 'CREATE_BAEMAIL_MESSAGE_TX'


    this.baemailPki = '02ae01ff46a4abdd66aaf1752174cb7d53bd92d3a6a3dc06186b10e22aa2fd7602'; // Baemail identity public key
    this.protocolPrefix = '1BAESxZMweg2mG4FG2DEZmB1Ury2ruAr9K'; // Baemail protocol prefix


}

module.exports = CreateBaemailMessageTx;



/*
#expecting params
[
    {
        sendingPaymail: {
            paymail: 'aa@moneybutton.com',
            seed: ['seed', 'words', ..],
            pkiPath: 'm/44'/0'/ ..'
        },
        fundingWallet: {
            seed: ['seed', 'words', ..],
            rootPath: 'm/44'/0'/ ..',
            coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
            changeAddress: '1abcedf..'
        },
        message: {
            thread: 'reply-to-tx-id',
            destination: 'receiver@moneybutton.com',
            destinationPkiPublicKey: '00abc',
            subject: 'subject01',
            content: 'content 01',
            amountToPaymailUsd: 0.123,
            amountToPaymailSats: 2345,
            payToPaymailOutputScript: '0a010203..',
            amountToBaemailSats: 2345,
            amountToAppSats: 2345
        }
    }
]

# response
[
    {
        'hexTx': '1f1f1f1f1f',
        'spentCoinIds': ['aa..00', 'aa..11', 'aa..22', ..]
    }
]
*/
CreateBaemailMessageTx.prototype.handle = function(params, callback) {

    var Transaction = this.context.bsv.Transaction;
    var Script = this.context.bsv.Script;





    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        //verify params
        if (params.length != 1) throw new Error('invalid params');
        var objParam = params[0];

        var sendingPaymail = objParam.sendingPaymail;
        var fundingWallet = objParam.fundingWallet;
        var message = objParam.message;



        //get hd-priv-key
        var sendingPaymailMnemonic = this.context.mnemonic.fromString(sendingPaymail.seed.join(' '));
        var sendingPaymailHdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(sendingPaymailMnemonic.toSeed());


        var sendingPaymailPkiHdPrivateKey = sendingPaymailHdPrivateKey.deriveChild(sendingPaymail.pkiPath);
        var sendingPaymailPkiPublicKey = this.context.bsv.PublicKey.fromPrivateKey(sendingPaymailPkiHdPrivateKey.privateKey);






        // encrypt data (string) to a pubkey
        let encryptForPki = (data, pubkey) => {
            var cid = this.context.ECIES();
            cid.publicKey(pubkey);
            return cid.encrypt(data);
        };

        // sign data with privateKey tied to sender publicKey
        let senderPkiSign = (data) => {
            var privKey = sendingPaymailPkiHdPrivateKey.privateKey
            var signature = this.context.bsv.Message.sign(data, privKey);
            return signature;
        };





        //create the op-return and structure it as required by baemail
        //
        //

        //if `message.thread` is undefined, then this var will not be included in JSON#stringify
        var serializedBaemailFull = JSON.stringify({
            thread: message.thread,
            summary: 'Baemail From Android',
            amountUsd: Number(message.amountToPaymailUsd),
            to: message.destination,
            cc: [],
            subject: message.subject,
            salesPitch: '',
            from: {
                name: sendingPaymail.paymail,
                primaryPaymail: sendingPaymail.paymail,
                pki: sendingPaymailPkiPublicKey.toString()
            }
        });

        var serializedBaemail = JSON.stringify({
            body: {
                time: Date.now(),
                blocks: message.content,
                version: '3.0.0'
            }
        });




        var assembledOpReturn = [this.protocolPrefix];
        assembledOpReturn.push(encryptForPki(serializedBaemailFull, this.baemailPki));
        assembledOpReturn.push(encryptForPki(serializedBaemail, sendingPaymailPkiPublicKey.toString()));
        assembledOpReturn.push(encryptForPki(serializedBaemail, message.destinationPkiPublicKey));

        const baeHash = this.context.bsv.crypto.Hash.sha256(
            Buffer.from(this.protocolPrefix + serializedBaemailFull)
        ).toString('hex');

        assembledOpReturn.push('|');
        assembledOpReturn.push('15igChEkUWgx4dsEcSuPitcLNZmNDfUvgA');
        assembledOpReturn.push(baeHash);
        assembledOpReturn.push(senderPkiSign(baeHash));
        assembledOpReturn.push(sendingPaymailPkiPublicKey.toString());
        assembledOpReturn.push(sendingPaymail.paymail);





        // create a tx for the assembled baemail-message-op-return
        //
        //

        var sizeOfOpReturn = assembledOpReturn.reduce((acc, v) => { return acc + v.length}, 0);

        var satsRequired =
            300 + //base fee ... todo avoid this hack
            message.amountToPaymailSats + //amount sent to receiver
            message.amountToBaemailSats +
            message.amountToAppSats +
            sizeOfOpReturn;



        var fundingWalletMnemonic = this.context.mnemonic.fromString(fundingWallet.seed.join(' '));
        var fundingWalletHdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(fundingWalletMnemonic.toSeed());
        var fundingWalletHdPrivateKeyRoot = fundingWalletHdPrivateKey.deriveChild(fundingWallet.rootPath);

        var usedCoinSet = [];
        var privateKeySet = [];

        for (var i = 0; i < fundingWallet.coins.length; i++) {
            var spendingCoin = fundingWallet.coins[i];
            var path = spendingCoin.derivation_path;
            var pathPrivateKey = fundingWalletHdPrivateKeyRoot.deriveChild("m" + path).privateKey;

            usedCoinSet.push(spendingCoin);
            privateKeySet.push(pathPrivateKey);

            //check if enough
            var satsInUsedCoins = usedCoinSet.reduce((acc, v) => { return acc + v.sats }, 0);
            if (satsInUsedCoins > satsRequired) {
                break;
            }
        }

        var satsInUsedCoins = usedCoinSet.reduce((acc, v) => { return acc + v.sats }, 0);
        if (satsInUsedCoins < satsRequired) {
            throw new Error('request dropped, not enough BSV');
        }




        //assemble spending-output-set
        var spendingOutputSet = [];
        var spendingOutputTxIdSet = [];
        var totalSats = 0;

        for (var i = 0; i < usedCoinSet.length; i++) {
            var coin = usedCoinSet[i];
            spendingOutputSet.push({
                address: coin.address,
                txId: coin.tx_id,
                outputIndex: coin.index,
                script: Script.buildPublicKeyHashOut(coin.address).toString(),
                satoshis: coin.sats
            });
            spendingOutputTxIdSet.push(coin.tx_id)

            totalSats += coin.sats;
        }


        // create the tx
        var transaction = new Transaction().from(spendingOutputSet);

        transaction.change(fundingWallet.changeAddress);

        transaction.addOutput(new Transaction.Output({
            script: Script.buildSafeDataOut(assembledOpReturn),
            satoshis: 0
        }));

        if (message.amountToBaemailSats > 0) {
            // pay baemail fee
            transaction.to('1BaemaiLK15EnJyFEhwLbrYJmRjqYoBMTe', message.amountToBaemailSats);
        }

        if (message.amountToAppSats > 0) {
            // pay app fee
            transaction.to('1Hih9JbUauUNGhz8uTQtBSCCzkvUa4trM9', message.amountToAppSats);
        }

        if (message.amountToPaymailSats > 0) {
            // pay recipient
            transaction.addOutput(new Transaction.Output({
                script: Script.fromHex(message.payToPaymailOutputScript),
                satoshis: message.amountToPaymailSats
            }));
        }

        //todo a lower fee can be used if applied manually

        transaction.sign(privateKeySet);

        console.log('tx-fee: ' + transaction.getFee());

        var txInHex = transaction.serialize(true);



        //finish the response
        responseData = [{
            hexTx: txInHex,
            spentCoinIds: spendingOutputTxIdSet
        }];
        isSuccess = true;


    } catch (e) {
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData);


};
