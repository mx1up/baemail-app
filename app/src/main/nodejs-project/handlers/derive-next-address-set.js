'use strict';




function DeriveNextAddressSet(context) {
    this.context = context
    this.command = "DERIVE_NEXT_ADDRESS_SET"
}

module.exports = DeriveNextAddressSet;

/* #expecting params
[
    ['seed', 'words', ..],
    0 //derived key-set index
]
with index `0` should generate paths
/0/0
/0/1
/0/2
/0/3
/0/4
..
/0/18
/0/19

# response
[
    {'path': '/0/0', address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ'},
    {'path': '/0/1', address: '1G7tZTw1z1XTuE7xsDR5J5s6uz57PrmYe1'},
    ..
]
*/
DeriveNextAddressSet.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        if (params.length != 2) throw new Error("invalid params");
        var seedPhraseArray = params[0]
        var keySetIndex = params[1]



        var phrase = seedPhraseArray.join(' ')
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());

        var hdPrivateKeyRoot = hdPrivateKey.deriveChild(this.context.fundingWalletDerivationPathRoot);

        var generatedAddresses = []

        for (var i = 0; i < 20; i++) {
            var path = "/0/" + (keySetIndex * 20 + i)
            // var path = "/" + keySetIndex + "/" + i
            var hdPrivateKeyDerived = hdPrivateKeyRoot.deriveChild("m" + path);
            var publicKey = this.context.bsv.PublicKey.fromPrivateKey(hdPrivateKeyDerived.privateKey);
            var address = this.context.bsv.Address.fromPublicKey(publicKey)

            generatedAddresses.push({
                path: path,
                address: address.toString()
            })
        }

        responseData = generatedAddresses

        isSuccess = true;

    } catch (e) {
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)
};
