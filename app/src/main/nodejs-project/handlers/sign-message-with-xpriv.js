'use strict';




function SignMessageWithXpriv(context) {
    this.context = context
    this.command = "SIGN_MESSAGE_WITH_XPRIV"
}

module.exports = SignMessageWithXpriv;


/*

# expected structure of params array
[
    "xpriv",
    "m/some/path/0",
    "message content"
]



# expected structure of result array
[
    "signature"
]


*/
SignMessageWithXpriv.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var xpriv = params[0];
        var derivationPath = params[1];
        var message = params[2];

        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromString(xpriv);

        var pathPrivateKey = hdPrivateKey.deriveChild(derivationPath).privateKey;



        const bufferOfMessage = Buffer.from(message);
        var bsvMessage = new this.context.bsv.Message(bufferOfMessage);

        var signature = bsvMessage.sign(pathPrivateKey);

        responseData = [
            signature
        ];

        isSuccess = true;

    } catch (e) {
        //console.log(e);
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)
};