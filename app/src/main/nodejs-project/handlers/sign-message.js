'use strict';




function SignMessage(context) {
    this.context = context
    this.command = "SIGN_MESSAGE"
}

module.exports = SignMessage;



/*

# expected structure of params
[
    "this is the message",
    "this is the wif-private-key"
]


# expected structure of the response data passed in the callback
[
    "signature"
]

*/
SignMessage.prototype.handle = function(params, callback) {

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        var message = params[0];
        var wifPrivateKey = params[1]

        const bufferOfMessage = Buffer.from(message)
        var bsvMessage = new this.context.bsv.Message(bufferOfMessage)

        var signature = bsvMessage.sign(this.context.bsv.PrivateKey.fromWIF(wifPrivateKey))

        responseData = [
            signature
        ];

        isSuccess = true;

    } catch (e) {
        //console.log(e);
        responseData = [
            e.stack
        ];
    }

    callback(isSuccess, responseData)
};