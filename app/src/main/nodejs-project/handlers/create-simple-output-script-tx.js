'use strict';




function CreateSimpleOutputScriptTx(context) {
    this.context = context
    this.command = "CREATE_SIMPLE_OUTPUT_SCRIPT_TX"
}

module.exports = CreateSimpleOutputScriptTx;



/*
#expecting params
[
    ['seed', 'words', ..],
    [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}]
    '76a914e2a623699e81b291c0327f408fea765d534baa2a88ac', //hex-string output script
    '1FFF..CC' //change address
]

# response
[
    {'hexTx': '1f1f1f1f1f'}
]
*/
CreateSimpleOutputScriptTx.prototype.handle = function(params, callback) {

    var Transaction = this.context.bsv.Transaction
    var Script = this.context.bsv.Script

    var isSuccess = false;

    var responseData = [
        null
    ];

    try {
        //verify params
        if (params.length != 4) throw new Error("invalid params");
        var seedPhraseArray = params[0];
        var spendingCoinsArray = params[1];
        var hexOutputScript = params[2];
        var changeAddress = params[3];

        //get hd-priv-key
        var phrase = seedPhraseArray.join(' ')
        var mnemonic = this.context.mnemonic.fromString(phrase);
        var hdPrivateKey = this.context.bsv.HDPrivateKey.fromSeed(mnemonic.toSeed());

        var hdPrivateKeyRoot = hdPrivateKey.deriveChild(this.context.fundingWalletDerivationPathRoot);

        //assemble private-key-set
        var privateKeySet = [];

        for (var i = 0; i < spendingCoinsArray.length; i++) {
            var spendingCoin = spendingCoinsArray[i];
            var path = spendingCoin.derivation_path;
            var pathPrivateKey = hdPrivateKeyRoot.deriveChild("m" + path).privateKey;

            privateKeySet.push(pathPrivateKey)
        }

        //assemble spending-output-set
        var spendingCoinsSet = [];
        var totalSats = 0;

        for (var i = 0; i < spendingCoinsArray.length; i++) {
            var spendingCoin = spendingCoinsArray[i];
            spendingCoinsSet.push({
                address: spendingCoin.address,
                txId: spendingCoin.tx_id,
                outputIndex: spendingCoin.index,
                script: Script.buildPublicKeyHashOut(spendingCoin.address).toString(),
                satoshis: spendingCoin.sats
            });

            totalSats += spendingCoin.sats;
        }


        //make and serialize the tx

        var transaction = new Transaction().from(spendingCoinsSet);


        //send the whole coin to the specified output...
        // must first calculate the minimum fee for the tx

        transaction.change(changeAddress);

        var fee = transaction.getFee();
        var available = transaction._getInputAmount();

        var destinationOutputSize = new Transaction.Output({
            script: Script.fromHex(hexOutputScript),
            satoshis: available - fee
        }).getSize();

        var finalDestinationSats = available - fee - destinationOutputSize;


        //add the final version of the output; max amount of sats are being sent there

        transaction.addOutput(new Transaction.Output({
            script: Script.fromHex(hexOutputScript),
            satoshis: finalDestinationSats
        }));

        //console.log(' !!!!!!! >>>>> destinationOutputSize: ' + destinationOutputSize + '\nfinalDestinationSats: ' + finalDestinationSats + '\nfee: ' + transaction.getFee());

        transaction.sign(privateKeySet);

        var txInHex = transaction.serialize(true);

        //finish the response
        responseData = [{
            hexTx: txInHex
        }];
        isSuccess = true;

    } catch (e) {
        responseData = [
            e.stack
        ];
    }

     callback(isSuccess, responseData);



//todo https://medium.com/namjungsoo/bitcoinsv-4-%EB%8B%A4%EC%A4%91-%EC%9E%85%EB%A0%A5-transaction-a8f362768de


}