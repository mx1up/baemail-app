package app.bitcoin.baemail.auth

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import app.bitcoin.baemail.paymail.PaymailRepository
import javax.inject.Inject

class PaymailViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository
) : ViewModel() {

    private val _isAuthLD = MediatorLiveData<Boolean>()
    val authLD: LiveData<Boolean> get() = _isAuthLD

    init {
        _isAuthLD.addSource(paymailRepository.activePaymailLD) { paymail ->
            _isAuthLD.value = paymail != null
        }
    }

    override fun onCleared() {
        super.onCleared()

        //something
    }
}