package app.bitcoin.baemail

object Tr {

    const val METHOD_SEND_BAEMAIL_MESSAGE = "send-baemail-message"
    const val METHOD_ASSEMBLE_BAEMAIL_MESSAGE_TX = "assemble-baemail-message-tx"
    const val METHOD_SEND_TX_TO_MINER = "send-tx-to-miner"
    const val METHOD_GET_PAYMAIL_DESTINATION_OUTPUT = "get-paymail-destination-output"
    const val METHOD_GET_UNUSED_ADDRESSES_OF_FUNDING_WALLET = "get-unused-addresses-of-funding-wallet"
    const val METHOD_SIGN_MESSAGE_WITH_PRIVATE_KEY = "sign-message-with-private-key"
    const val METHOD_REQUEST_PAYMENT_DESTINATION_OUTPUT = "request-payment-destination-output"
    const val METHOD_DECRYPT_PKI_ENCRYPTED_DATA = "decrypt-pki-encrypted-data"

}