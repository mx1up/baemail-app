package app.bitcoin.baemail.message

import android.content.Context
import app.bitcoin.baemail.net.AppApiService
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.AppStateLiveData
import app.bitcoin.baemail.wallet.BlockchainDataSource
import app.bitcoin.baemail.wallet.WalletRepository
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import kotlin.coroutines.resume

@Suppress("unused")
@ExperimentalCoroutinesApi
@FlowPreview
class BaemailRepository(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val apiService: AppApiService,
    private val appDatabase: AppDatabase,
    private val appStateLD: AppStateLiveData,
    private val gson: Gson,
    private val walletRepository: WalletRepository,
    private val secureDataSource: SecureDataSource,
    private val blockchainDataSource: BlockchainDataSource,
    private val baemailStateHelper: BaemailStateHelper
) {

    private val pageSize = 20

    private val requestFilterCache = HashMap<BaemailMessageFetchHelper.CacheKey, MessageFilterKey>()

    val messageFetchHelper = BaemailMessageFetchHelper(
        appDatabase,
        requestFilterCache,
        pageSize
    ) { key ->
        fetchAndHandlePageOfBaemailMessages(key)
    }



    //todo singleMessageStore


    val messageDraftHelper = MessageDraftHelper(
        appContext
    )



    private suspend fun fetchAndHandlePageOfBaemailMessages(
        key: MessageFilterKey
    ): List<DecryptedBaemailMessage> {

        val fetched = requestPageOfBaemailMessages(key)

        if (fetched.isEmpty()) return emptyList()

        return processQueriedMessages(key, fetched)
    }



    private suspend fun processQueriedMessages(
        key: MessageFilterKey,
        fetched: List<QueriedBaemailMessage>
    ): List<DecryptedBaemailMessage> {

        //the content of the retrieved messages must be decrypted
        val toBeDecryptedMessageMap = mutableMapOf<String, String>()
        fetched.forEach {
            toBeDecryptedMessageMap[it.txId] = it.getMessageCypherText(key.paymail)
        }
        //avoid re-decrypting messages
        val messageTxIdList = fetched.map { it.txId }
        val foundMatchingMessages = appDatabase.decryptedBaemailMessageDao()
            .queryMessagesByOwnerAndTxId(key.paymail, messageTxIdList).let { list ->
                val map = mutableMapOf<String, DecryptedBaemailMessage>()

                list.forEach {
                    map[it.txId] = it
                }

                map
            }
        foundMatchingMessages.forEach {
            toBeDecryptedMessageMap.remove(it.key)
        }

        val decryptedMessageContent = decryptBaemailMessages(toBeDecryptedMessageMap, key.paymail)
        //normalize decrypted-message-map by re-adding messages that were already decrypted
        foundMatchingMessages.forEach {
            decryptedMessageContent[it.key] = it.value.decryptedMessage
        }

        return fetched.map {
            val decryptedContent = decryptedMessageContent[it.txId]!!

            val numAmountUsd = try {
                it.head.amountUsd.toFloat()
            } catch (e: Exception) {
                0f
            }

            DecryptedBaemailMessage(
                it.txId,
                key.paymail,
                it.createdAt,
                it.updatedAt,
                it.head.thread,
                it.head.amountUsd,
                numAmountUsd,
                it.head.to,
                it.head.cc,
                it.head.subject,
                it.head.salesPitch,
                it.head.from.name,
                it.head.from.primaryPaymail,
                decryptedContent,
                it.hasDestinationSeen(),
                it.hasDestinationPaid(),
                key.bucket
            )
        }
    }







    private fun buildReqJsonOfBaemailMessagesPage(
        token: String,
        paymail: String,
        key: MessageFilterKey
    ): Pair<String, JsonObject> {

        val limit = pageSize
        val skip = key.page * limit

        val reqJson = JsonObject()
        reqJson.addProperty("code", token)
        reqJson.addProperty("paymail", paymail)
        reqJson.addProperty("limit", limit)
        reqJson.addProperty("skip", skip)
        reqJson.addProperty("search", key.query ?: "")

        var endpoint = ""

        if (!key.query.isNullOrBlank()) {
            // search endpoint
            endpoint = "https://baemail.me/api/get/search"


            //todo something is wrong in web-baemail
            when (key.bucket) {
                MessageBucket.INBOX -> {
                    //searching in inbox
                    when (key.sort) {
                        SortType.VALUE -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("status.0.seen", 1)
                            sortJson.addProperty("baemailData.amountUsd", -1)
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                        SortType.TIME -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                    }


                }
                MessageBucket.SENT -> {
                    //searching in sent

                    //ignores sort
                    val sortJson = JsonObject()
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)

                }
                MessageBucket.ARCHIVE -> {
                    //searching in archive

                    when (key.sort) {
                        SortType.VALUE -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("status.0.seen", 1)
                            sortJson.addProperty("baemailData.amountUsd", -1)
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                        SortType.TIME -> {
                            val sortJson = JsonObject()
                            sortJson.addProperty("createdAt", -1)

                            reqJson.add("sort", sortJson)
                        }
                    }

                }
            }


            //todo
            //todo
            //todo
            //todo


        } else if (MessageBucket.INBOX == key.bucket) {

            endpoint = "https://baemail.me/api/get/inbox"


            when (key.sort) {
                SortType.VALUE -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("status.0.seen", 1)
                    sortJson.addProperty("baemailData.amountUsd", -1)
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
                SortType.TIME -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
            }


        } else if (MessageBucket.SENT == key.bucket) {
            endpoint = "https://baemail.me/api/get/sent"


            val sortJson = JsonObject()
            sortJson.addProperty("createdAt", -1)

            reqJson.add("sort", sortJson)


        } else if (MessageBucket.ARCHIVE == key.bucket) {
            endpoint = "https://baemail.me/api/get/archive"

            when (key.sort) {
                SortType.VALUE -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("status.0.seen", 1)
                    sortJson.addProperty("baemailData.amountUsd", -1)
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
                SortType.TIME -> {
                    val sortJson = JsonObject()
                    sortJson.addProperty("createdAt", -1)

                    reqJson.add("sort", sortJson)
                }
            }
        }


        return endpoint to reqJson
    }




    private suspend fun requestPageOfBaemailMessages(
        key: MessageFilterKey
    ): List<QueriedBaemailMessage> {

        val status = ensureBaemailLogin()

        status ?: let {
            Timber.d("status == null")
            return emptyList()
        }



        val (endpoint, reqJson) = buildReqJsonOfBaemailMessagesPage(
            status.token,
            status.paymail,
            key
        )


        Timber.d(/*RuntimeException(), */"pre request to:$endpoint bucket:${key.bucket} json:$reqJson")

        val inboxPage: List<QueriedBaemailMessage>? = suspendCancellableCoroutine { cancellableContinuation ->
            try {
                val call = blockchainDataSource.apiService.post(
                    endpoint,
                    reqJson.toString().toRequestBody()
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {

                    //parse the body

                    val jsonMessageArray = gson.fromJson(
                        String(responseBody.bytes()),
                        JsonArray::class.java
                    )

                    val list = mutableListOf<QueriedBaemailMessage>()
                    (0 until jsonMessageArray.size()).map { i ->
                        QueriedBaemailMessage.parse(gson, jsonMessageArray[i].asJsonObject)
                    }.toCollection(list)

                    cancellableContinuation.resume(list)

                    return@suspendCancellableCoroutine
                }

            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }



        inboxPage?.let {
            Timber.d("page: ${key.page} ..messages: ${it.size}")
        } ?: let {
            return emptyList()
        }



        return inboxPage
    }






    private suspend fun decryptBaemailMessages(
        mappedCypherText: Map<String, String>,
        paymail: String
    ): MutableMap<String, String> {


        val activePaymailWalletSeed = try {
            secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            throw e
        }

        val pkiPath = try {
            secureDataSource.getPkiPathForPrivateKeySeed(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            throw e
        }

        val decrypted =  walletRepository.decryptPkiEncryptedData(
            activePaymailWalletSeed,
            pkiPath,
            mappedCypherText
        ) ?: throw RuntimeException()

        return HashMap(decrypted)
    }










    /////////////////////////////////


    suspend fun archiveMessage(txId: String): Boolean = withContext(Dispatchers.IO) {

        val status = ensureBaemailLogin()

        status ?: let {
            Timber.d("status == null")
            return@withContext false
        }


        val endpoint = "https://baemail.me/api/set/archived"

        val reqJson: JsonObject = JsonObject().let {
            it.addProperty("code", status.token)
            it.addProperty("pki", status.paymailPkiPublicKey)
            it.addProperty("paymail", status.paymail)
            it.addProperty("txid", txId)

            it
        }



        var isUpdated = false

        try {
            val call = blockchainDataSource.apiService.post(
                endpoint,
                reqJson.toString().toRequestBody()
            )
            val response = call.execute()
            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {
                //parse the body
                val jsonResponse = gson.fromJson(
                    String(responseBody.bytes()),
                    JsonObject::class.java
                )

                isUpdated = jsonResponse.get("updated").asBoolean

            }

        } catch (e: Exception) {
            Timber.e(e)
            return@withContext false
        }



        if (!isUpdated) return@withContext false



        //update the db-entry
        appDatabase.decryptedBaemailMessageDao().updateMessageBucket(
            status.paymail,
            txId,
            MessageBucket.ARCHIVE
        )

        return@withContext true
    }




    /////////////////////////////////

    suspend fun unarchiveMessage(txId: String): Boolean = withContext(Dispatchers.IO) {

        val status = ensureBaemailLogin()

        status ?: let {
            Timber.d("status == null")
            return@withContext false
        }


        val endpoint = "https://baemail.me/api/set/unarchived"

        val reqJson: JsonObject = JsonObject().let {
            it.addProperty("code", status.token)
            it.addProperty("pki", status.paymailPkiPublicKey)
            it.addProperty("paymail", status.paymail)
            it.addProperty("txid", txId)

            it
        }


        var isUpdated = false


        try {
            val call = blockchainDataSource.apiService.post(
                endpoint,
                reqJson.toString().toRequestBody()
            )
            val response = call.execute()
            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {
                //parse the body
                val jsonResponse = gson.fromJson(
                    String(responseBody.bytes()),
                    JsonObject::class.java
                )

                isUpdated = jsonResponse.get("updated").asBoolean

            }

        } catch (e: Exception) {
            Timber.e(e)
            return@withContext false
        }



        if (!isUpdated) return@withContext false



        //update the db-entry
        appDatabase.decryptedBaemailMessageDao().updateMessageBucket(
            status.paymail,
            txId,
            MessageBucket.INBOX
        )

        return@withContext true
    }


    /////////////////////////////////

    suspend fun deleteMessage(txId: String): Boolean = withContext(Dispatchers.IO) {

        val status = ensureBaemailLogin()

        status ?: let {
            Timber.d("status == null")
            return@withContext false
        }


        val endpoint = "https://baemail.me/api/set/deleted"

        val reqJson: JsonObject = JsonObject().let {
            it.addProperty("code", status.token)
            it.addProperty("pki", status.paymailPkiPublicKey)
            it.addProperty("paymail", status.paymail)
            it.addProperty("txid", txId)

            it
        }


        var isUpdated = false


        try {
            val call = blockchainDataSource.apiService.post(
                endpoint,
                reqJson.toString().toRequestBody()
            )
            val response = call.execute()
            val responseBody = response.body()

            if (response.isSuccessful && responseBody != null) {
                //parse the body
                val jsonResponse = gson.fromJson(
                    String(responseBody.bytes()),
                    JsonObject::class.java
                )

                isUpdated = jsonResponse.get("updated").asBoolean

            }

        } catch (e: Exception) {
            Timber.e(e)
            return@withContext false
        }



        if (!isUpdated) return@withContext false



        //remove the db-entry
        appDatabase.decryptedBaemailMessageDao().deleteMessageForOwner(
            status.paymail,
            txId
        )

        return@withContext true
    }







    /////////////////////////////////

    private val activeRequestsPostReadReceipt = mutableMapOf<Pair<String, String>, Job>()


    fun requestPostReadReceipt(
        txId: String,
        seenByPaymail: String
    ) {

        activeRequestsPostReadReceipt[Pair(txId, seenByPaymail)]?.let {
            //the app is already in the process of doing this
            return
        }

        val postReceiptJob = coroutineUtil.appScope.launch(Dispatchers.IO) {
            try {


                val status = ensureBaemailLogin()

                status ?: let {
                    Timber.d("status == null")
                    return@launch
                }

                require(status.paymail == seenByPaymail)


                val jsonReq = JsonObject()
                jsonReq.addProperty("code", status.token)
                jsonReq.addProperty("txid", txId)
                jsonReq.addProperty("paymail", seenByPaymail)
                jsonReq.addProperty("pki", status.paymailPkiPublicKey)


                Timber.d("pre request to: SEEN json:$jsonReq")

                val wasSuccess: Boolean? =
                    suspendCancellableCoroutine { cancellableContinuation ->
                        try {
                            val call = blockchainDataSource.apiService.post(
                                "https://baemail.me/api/set/seen",
                                jsonReq.toString().toRequestBody()
                            )
                            val response = call.execute()

                            val responseBody = response.body()

                            if (response.isSuccessful && responseBody != null) {

                                //parse the body

                                val jsonResponse = gson.fromJson(
                                    String(responseBody.bytes()),
                                    JsonObject::class.java
                                )

                                if (jsonResponse.get("updated").asBoolean) {
                                    cancellableContinuation.resume(true)
                                }

                                return@suspendCancellableCoroutine
                            }

                        } catch (e: Exception) {
                            Timber.e(e)
                        }

                        cancellableContinuation.resume(null)
                    }


                if (wasSuccess == true) {
                    appDatabase.decryptedBaemailMessageDao().updateMessageAsSeen(
                        seenByPaymail, txId
                    )
                }
                Timber.d("Read receipt updated ... TODO SEND TX ... wasSuccess:$wasSuccess ..... txId:$txId seenByPaymail:$seenByPaymail")


            } finally {
                //assuming success
                activeRequestsPostReadReceipt.remove(Pair(txId, seenByPaymail))
                Timber.d("Read receipt job cleaned-up... txId:$txId seenByPaymail:$seenByPaymail")

            }
        }

        activeRequestsPostReadReceipt[Pair(txId, seenByPaymail)] = postReceiptJob
    }







    /////////////////////////////////









    private suspend fun executeLogin(paymail: String): BaemailState? = withContext(Dispatchers.IO) {

        val loginToken: String? = suspendCancellableCoroutine { cancellableContinuation ->
            try {
                val call = blockchainDataSource.apiService.post(
                    "https://baemail.me/api/login/request",
                    "".toRequestBody()
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {
                    String(responseBody.bytes()).let {
                        Timber.d("login-response-content-string: $it")
                        cancellableContinuation.resume(it)
                    }
                    return@suspendCancellableCoroutine
                }

            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }

        loginToken ?: let {
            Timber.d("failed to retrieve login-token")
            return@withContext null
        }


        val paymailSeed = try {
            secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail-wallet-seed")
            return@withContext null
        }

        val pkiPath = try {
            secureDataSource.getPkiPathForPrivateKeySeed(AuthenticatedPaymail(paymail))
        } catch (e: Exception) {
            Timber.d("failed to retrieve active-paymail  pki-path")
            return@withContext null
        }

        //keys for pki-path
        val keyHolder = walletRepository.getPrivateKeyForPathFromSeed(
            pkiPath,
            paymailSeed
        ) ?: let {
            Timber.d("error retrieving the wif-private-key for path")
            return@withContext null
        }

        val signature = walletRepository.signMessage(
            loginToken,
            keyHolder.privateKey
        ) ?: let {
            Timber.d("error signing the login token with pki-key")
            return@withContext null
        }


        val loginConfirmResponse: String? = suspendCancellableCoroutine { cancellableContinuation ->
            try {

                val jsonReq = JsonObject()
                jsonReq.addProperty("paymail", paymail)
                jsonReq.addProperty("pki", keyHolder.publicKey)
                jsonReq.addProperty("code", loginToken)
                jsonReq.addProperty("sig", signature)
                jsonReq.addProperty("name", paymail)


                val call = blockchainDataSource.apiService.post(
                    "https://baemail.me/api/login/confirm",
                    jsonReq.toString().toRequestBody()
                )
                val response = call.execute()

                val responseBody = response.body()

                if (response.isSuccessful && responseBody != null) {
                    String(responseBody.bytes()).let {
                        Timber.d("login-confirmation-response-content-string: $it")
                        cancellableContinuation.resume(it)
                    }
                    return@suspendCancellableCoroutine
                }

            } catch (e: Exception) {
                Timber.e(e)
            }

            cancellableContinuation.resume(null)
        }

        val isLoginSuccessful = loginConfirmResponse.let {
            var isSuccess = false
            try {
                val responseJson = gson.fromJson(it, JsonObject::class.java)
                val value = responseJson.getAsJsonPrimitive("added").asBoolean
                if (value) {
                    isSuccess = true
                }
            } catch (e: Exception) {
                Timber.e(e)
            }

            isSuccess
        }

        if (!isLoginSuccessful) {
            Timber.d("Baemail login failed !!!!!!!")
            return@withContext null
        }

        return@withContext BaemailState(
            loginToken,
            signature,
            System.currentTimeMillis(),
            paymail,
            keyHolder.publicKey
        )
    }




    private suspend fun doBaemailLogin(paymail: String): BaemailState? {
        Timber.d("doBaemailLogin")
        val state = executeLogin(paymail) ?: let {
            Timber.d("error logging in")
            return null
        }

        baemailStateHelper.writeState(state)
        return state
    }





    private suspend fun ensureBaemailLogin(): BaemailState? {
        Timber.d("ensureBaemailLogin")

        val paymail: String = appStateLD.value?.activePaymail ?: let {
            Timber.d("active paymail is NULL")
            return null
        }

        if (baemailStateHelper.hasState(paymail)) {
            Timber.d("read persisted state")
            return baemailStateHelper.requireStateForPaymail(paymail)
        }

        return doBaemailLogin(paymail)
    }





}