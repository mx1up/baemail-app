package app.bitcoin.baemail.message

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.*
import kotlinx.coroutines.flow.first

class MessageDraftHelper(
    private val appContext: Context
) {

    companion object {
        private const val PREF_NEW_MESSAGE_DRAFT = "draft_new_message"
    }

    val ds: DataStore<Preferences>

    init {

        ds = appContext.createDataStore(
            name = PREF_NEW_MESSAGE_DRAFT
        )

    }

    suspend fun getDraft(paymail: String): Draft? {
        val keyMillisCreated = preferencesKey<Long>("millis_created__$paymail")
        val keyDestination = preferencesKey<String>("destination__$paymail")
        val keySubject = preferencesKey<String>("subject__$paymail")
        val keyContent = preferencesKey<String>("content__$paymail")
        val keyThread = preferencesKey<String>("thread__$paymail")
        val keyPayCurrency = preferencesKey<String>("pay_currency__$paymail")
        val keyPayMax = preferencesKey<Float>("pay_max__$paymail")
        val keyPayAmount = preferencesKey<Float>("pay_amount__$paymail")

        val prefs = ds.data.first()

        var millisCreated: Long? = null
        if (prefs.contains(keyMillisCreated)) {
            millisCreated = prefs[keyMillisCreated]
        }
        var destination: String? = null
        if (prefs.contains(keyDestination)) {
            destination = prefs[keyDestination]
        }
        var subject: String? = null
        if (prefs.contains(keySubject)) {
            subject = prefs[keySubject]
        }
        var content: String? = null
        if (prefs.contains(keyContent)) {
            content = prefs[keyContent]
        }
        var thread: String? = null
        if (prefs.contains(keyThread)) {
            thread = prefs[keyThread]
        }
        var payCurrency: String? = null
        if (prefs.contains(keyPayCurrency)) {
            payCurrency = prefs[keyPayCurrency]
        }
        var payMax: Float? = null
        if (prefs.contains(keyPayMax)) {
            payMax = prefs[keyPayMax]
        }
        var payAmount: Float? = null
        if (prefs.contains(keyPayAmount)) {
            payAmount = prefs[keyPayAmount]
        }

        payMax = payMax ?: 0.1f

        return Draft(
            paymail,
            millisCreated ?: 0L,
            destination,
            subject,
            content,
            thread,
            payCurrency ?: "USD",
            payMax,
            payAmount ?: payMax / 10
        )
    }


    suspend fun applyNewReply(
        owner: String,
        destinationPaymail: String,
        subject: String,
        threadId: String,
        millisCreated: Long
    ) {
        val keyMillisCreated = preferencesKey<Long>("millis_created__$owner")
        val keyDestination = preferencesKey<String>("destination__$owner")
        val keySubject = preferencesKey<String>("subject__$owner")
        val keyThread = preferencesKey<String>("thread__$owner")


        ds.edit { prefs ->
            prefs[keyMillisCreated] = millisCreated
            prefs[keyDestination] = destinationPaymail
            prefs[keySubject] = subject
            prefs[keyThread] = threadId
        }

    }

    suspend fun applyNewDraftContent(
        owner: String,
        destinationPaymail: String,
        subject: String,
        content: String,
        threadId: String?,
        payCurrency: String,
        payMax: Float,
        payAmount: Float
    ) {


        val keyDestination = preferencesKey<String>("destination__$owner")
        val keySubject = preferencesKey<String>("subject__$owner")
        val keyContent = preferencesKey<String>("content__$owner")
        val keyThread = preferencesKey<String>("thread__$owner")
        val keyPayCurrency = preferencesKey<String>("pay_currency__$owner")
        val keyPayMax = preferencesKey<Float>("pay_max__$owner")
        val keyPayAmount = preferencesKey<Float>("pay_amount__$owner")

        ds.edit { prefs ->
            prefs[keyDestination] = destinationPaymail
            prefs[keySubject] = subject
            prefs[keyContent] = content

            threadId?.let {
                prefs[keyThread] = it
            } ?: let {
                prefs.remove(keyThread)
            }

            prefs[keyPayCurrency] = payCurrency
            prefs[keyPayMax] = payMax
            prefs[keyPayAmount] = payAmount

        }
    }

    suspend fun discardDraft(owner: String) {

        val keyDestination = preferencesKey<String>("destination__$owner")
        val keySubject = preferencesKey<String>("subject__$owner")
        val keyContent = preferencesKey<String>("content__$owner")
        val keyThread = preferencesKey<String>("thread__$owner")
        val keyPayCurrency = preferencesKey<String>("pay_currency__$owner")
        val keyPayMax = preferencesKey<Float>("pay_max__$owner")
        val keyPayAmount = preferencesKey<Float>("pay_amount__$owner")


        ds.edit { prefs ->
            prefs.remove(keyDestination)
            prefs.remove(keySubject)
            prefs.remove(keyContent)
            prefs.remove(keyThread)
            prefs.remove(keyPayCurrency)
            prefs.remove(keyPayMax)
            prefs.remove(keyPayAmount)

        }
    }


    suspend fun clearReplyTo(owner: String) {

        val keyThread = preferencesKey<String>("thread__$owner")

        ds.edit { prefs ->
            prefs.remove(keyThread)

        }
    }





    data class Draft(
        val owner: String,
        val millisCreated: Long,
        val destination: String?,
        val subject: String?,
        val content: String?,
        val thread: String?,
        val payCurrency: String,
        val payMax: Float,
        val payAmount: Float
    )


}