package app.bitcoin.baemail.message


data class MessageFilterKey(
    val paymail: String,
    val query: String?,
    val sort: SortType,
    val bucket: MessageBucket,
    val page: Int
) {

    //this is not included in the data-class constructor so it does not affect the #equals method
    val id = getNextId()

    companion object {
        private var uniqueId = 0L

        @Synchronized
        fun getNextId(): Long {
            return uniqueId++
        }
    }
}

enum class SortType(val const: String) {
    VALUE("value"),
    TIME("time");

    companion object {
        fun parse(serialized: String): SortType {
            return values().find { it.const == serialized }!!
        }
    }
}

enum class MessageBucket(val const: String) {
    INBOX("inbox"),
    ARCHIVE("archive"),
    SENT("sent");

    companion object {
        fun parse(serialized: String): MessageBucket {
            return values().find { it.const == serialized }!!
        }
    }
}