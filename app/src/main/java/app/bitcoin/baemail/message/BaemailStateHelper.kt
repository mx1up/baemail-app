package app.bitcoin.baemail.message

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.first

class BaemailStateHelper(
    val appContext: Context,
    val gson: Gson
) {

    companion object {
        private const val NAME = "baemail"
    }

    val ds: DataStore<Preferences>

    init {

        ds = appContext.createDataStore(
            name = NAME
        )

    }

    suspend fun writeState(state: BaemailState) {
        ds.edit { prefs ->
            prefs[preferencesKey<String>("state_${state.paymail}")] = state.serialize()
        }
    }

    suspend fun getStateForPaymail(paymail: String): BaemailState? {
        val key = preferencesKey<String>("state_$paymail")
        val prefs = ds.data.first()

        if (prefs.contains(key)) {
            val serializedState = prefs[key]
            return BaemailState.parse(gson.fromJson(serializedState, JsonObject::class.java))
        }

        return null
    }

    suspend fun requireStateForPaymail(paymail: String): BaemailState {
        val key = preferencesKey<String>("state_$paymail")
        val prefs = ds.data.first()

        if (prefs.contains(key)) {
            val serializedState = prefs[key]
            return BaemailState.parse(gson.fromJson(serializedState, JsonObject::class.java))
        }

        throw RuntimeException()
    }

    suspend fun hasState(paymail: String): Boolean {
        val prefs = ds.data.first()

        return prefs.contains(preferencesKey<String>("state_$paymail"))
    }

}