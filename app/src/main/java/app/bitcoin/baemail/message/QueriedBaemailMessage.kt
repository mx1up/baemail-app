package app.bitcoin.baemail.message

import androidx.annotation.Keep
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import org.threeten.bp.ZonedDateTime

data class QueriedBaemailMessage(
    val txId: String,
    val createdAt: Long,
    val updatedAt: Long,
    val head: Head,
    val encryptedData: List<EncryptedMessage>,
    val status: List<Status>
) {

    fun getMessageCypherText(paymail: String): String {
        val matchingData = encryptedData.find { it.paymail == paymail } ?: throw RuntimeException()

        return matchingData.value
    }

    fun hasDestinationSeen(): Boolean {
        head.to.let { destinationPaymail ->
            status.find { it.paymail == destinationPaymail }!!.let { statusForDestination ->
                return statusForDestination.seen
            }
        }
    }

    fun hasDestinationPaid(): Boolean {
        head.to.let { destinationPaymail ->
            status.find { it.paymail == destinationPaymail }!!.let { statusForDestination ->
                return statusForDestination.paid
            }
        }
    }

    @Keep
    data class Head(
        @SerializedName("summary") val summary: String,
        @SerializedName("amountUsd") val amountUsd: String,
        @SerializedName("to") val to: String,
        @SerializedName("cc") val cc: List<String>,
        @SerializedName("subject") val subject: String,
        @SerializedName("salesPitch") val salesPitch: String?,
        @SerializedName("from") val from: HeadFrom,
        @SerializedName("thread") val thread: String?

    )

    @Keep
    data class HeadFrom(
        @SerializedName("name") val name: String?,
        @SerializedName("primaryPaymail") val primaryPaymail: String,
        @SerializedName("pki") val pki: String
    )

    @Keep
    data class EncryptedMessage(
        @SerializedName("name") val name: String,
        @SerializedName("paymail") val paymail: String,
        @SerializedName("pki") val pki: String,
        @SerializedName("value") val value: String,
    )

    @Keep
    data class Status(
        @SerializedName("paymail") val paymail: String,
        @SerializedName("seen") val seen: Boolean,
        @SerializedName("placement") val placement: String,
        @SerializedName("paid") val paid: Boolean
    )

    companion object {
        fun parse(gson: Gson, jsonObject: JsonObject): QueriedBaemailMessage {
            val txId = jsonObject.get("txid").asString
            val createdAt = jsonObject.get("createdAt").asString
            val millisCreatedAt = ZonedDateTime.parse(createdAt).toInstant().toEpochMilli()


            val millisUpdatedAt = if (jsonObject.has("lastUpdated")) {
                val updatedAt = jsonObject.get("lastUpdated").asString
                ZonedDateTime.parse(updatedAt).toInstant().toEpochMilli()
            } else {
                millisCreatedAt
            }

            //if (jsonObject.has("lastUpdated")
//            val updatedAt = jsonObject.get("lastUpdated").asString
//            val millisUpdatedAt = ZonedDateTime.parse(updatedAt).toInstant().toEpochMilli()

            val headJson = jsonObject.getAsJsonObject("baemailData")
            val head = gson.fromJson(headJson, Head::class.java)


            val encryptedDataJson = jsonObject.getAsJsonArray("encryptedData")
            val encryptedDataList = mutableListOf<EncryptedMessage>()
            for (i in 0 until encryptedDataJson.size()) {
                val encryptedMessageJson = encryptedDataJson.get(i)
                encryptedDataList.add(
                    gson.fromJson(encryptedMessageJson, EncryptedMessage::class.java)
                )
            }


            val statusArrayJson = jsonObject.getAsJsonArray("status")
            val statusList = mutableListOf<Status>()
            for (i in 0 until statusArrayJson.size()) {
                val statusJson = statusArrayJson.get(i)
                statusList.add(
                    gson.fromJson(statusJson, Status::class.java)
                )
            }

            return QueriedBaemailMessage(
                txId,
                millisCreatedAt,
                millisUpdatedAt,
                head,
                encryptedDataList,
                statusList
            )
        }
    }

}