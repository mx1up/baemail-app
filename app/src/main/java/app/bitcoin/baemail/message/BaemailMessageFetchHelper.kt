@file:Suppress("PropertyName", "unused")

package app.bitcoin.baemail.message

import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import com.dropbox.android.external.store4.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import kotlin.collections.HashMap

@ExperimentalCoroutinesApi
@FlowPreview
class BaemailMessageFetchHelper(
    private val appDatabase: AppDatabase,
    private val filterCache: HashMap<CacheKey, MessageFilterKey>,
    private val pageSize: Int,
    private val fetchAndHandlePageOfBaemailMessages: suspend (key: MessageFilterKey)->List<DecryptedBaemailMessage>
) {

    private val fetchHolderMap: HashMap<Long, Ref> = HashMap()

    fun getPageForCachedKey(paymail: String, query: String?, sort: SortType, bucket: MessageBucket): Int {
        return filterCache[CacheKey(
                paymail,
                query,
                sort,
                bucket
            )]?.page ?: 0
    }

    fun resetCachedKey(filter: MessageFilterKey) {
        filterCache.remove(CacheKey(
            filter.paymail,
            filter.query,
            filter.sort,
            filter.bucket
        ))
    }

    fun getFlowForFilter(
        filter: MessageFilterKey,
        wipeBucketOnPreInsert: Boolean
    ): PagedFetchHolder {

        val key = CacheKey(
            filter.paymail,
            filter.query,
            filter.sort,
            filter.bucket
        )
        val cached = filterCache[key]

        var isNew = cached == null

        cached?.let {
            //update the cached value
            if (it.page < filter.page) {
                isNew = true
                filterCache[key] = filter
            }
        } ?: let {
            //initialize an entry in the cache
            filterCache[key] = filter

        }

        val flow = messageListStore.stream(
            StoreRequest.cached(
                filter,
                refresh = isNew
            )
        )

        val fetchHolder = InternalPagedFetchHolder(
            filter,
            System.currentTimeMillis(),
            flow
        )

        val runOnPreInsert: suspend ()->Unit = if (wipeBucketOnPreInsert) {
            {
                appDatabase.decryptedBaemailMessageDao().deleteByBucketAndOwner(
                    filter.bucket,
                    filter.paymail
                )
            }
        } else {
            {}
        }

        fetchHolderMap[filter.id] = Ref(fetchHolder, runOnPreInsert)

        return fetchHolder
    }

    private val messageListStore: Store<MessageFilterKey, List<DecryptedBaemailMessage>> by lazy {
        StoreBuilder.from(
            fetcher = Fetcher.ofFlow { filterKey ->
                flow {
                    Timber.d(">>>>> start of while in `flow` filterKey:$filterKey")

                    val gotList = try {
                        fetchAndHandlePageOfBaemailMessages(filterKey)
                    } catch (e: Exception) {
                        Timber.e(e)
                        emptyList<DecryptedBaemailMessage>()
                    }

                    if (gotList.isEmpty()) {
                        Timber.d(">>>>> requestPageOfBaemailMessages isEmpty == true")

                        //indicate that there are no more pages
                        fetchHolderMap[filterKey.id]?.holder?.setHasNextPage(false)

                    }

                    emit(gotList)


                    fetchHolderMap[filterKey.id]?.let {
                        it.holder.setHasFetchCompleted(true)
                        it.holder.setHasNextPage(gotList.size == pageSize)
                    }

                    fetchHolderMap.remove(filterKey.id)

                    Timber.d(">>>>> end of flow $filterKey ...hasNextPage:${gotList.size == pageSize}")


                }
            },
            sourceOfTruth = SourceOfTruth.of(
                reader = ::queryByFilter,
                writer = ::insertAll,
                delete = ::clearByBucket,
                deleteAll = ::deleteAllByActivePaymail
            )

        ).build()
    }





    private fun queryByFilter(key: MessageFilterKey): Flow<List<DecryptedBaemailMessage>> {
        val byTimeNotValue = key.sort == SortType.TIME

        val limit = pageSize * (key.page + 1)

        val dbQueryFlow = appDatabase.decryptedBaemailMessageDao()
            .queryByBucketAndOwner(key.bucket, key.paymail, "%${key.query}%", byTimeNotValue, limit)


        if (!byTimeNotValue && MessageBucket.INBOX == key.bucket) {

            val unreadMessages = appDatabase.decryptedBaemailMessageDao().queryUnreadByOwner(
                key.paymail, "%${key.query}%", limit
            )

            return dbQueryFlow.combine(unreadMessages) { value, unread ->

                val finalList = mutableListOf<DecryptedBaemailMessage>()
                val addedMessageIdSet = mutableSetOf<String>()

                finalList.addAll(unread)
                unread.map { it.txId }.toCollection(addedMessageIdSet)

                value.forEach {
                    if (addedMessageIdSet.contains(it.txId)) return@forEach

                    finalList.add(it)
                }

                return@combine if (finalList.size > limit) {
                    mutableListOf<DecryptedBaemailMessage>().also { list ->
                        (0 until limit).forEach { i ->
                            list.add(finalList[i])
                        }
                    }
                } else {
                    finalList
                }
            }
        }

        return dbQueryFlow
    }

    private suspend fun deleteAllByActivePaymail() {
        throw RuntimeException()
        //appDatabase.decryptedBaemailMessageDao().deleteAllByOwner(activePaymail)
    }

    private suspend fun insertAll(key: MessageFilterKey, list: List<DecryptedBaemailMessage>) {
        fetchHolderMap[key.id]?.runOnPreInsert?.invoke()
        Timber.d(".......................... #insertAll")

        appDatabase.decryptedBaemailMessageDao().insertList(list)
    }

    private suspend fun clearByBucket(key: MessageFilterKey) {
        appDatabase.decryptedBaemailMessageDao().deleteByBucketAndOwner(key.bucket, key.paymail)
    }




    data class Ref(
        val holder: InternalPagedFetchHolder,
        val runOnPreInsert: suspend ()->Unit
    )


    data class CacheKey(
        val paymail: String,
        val query: String?,
        val sort: SortType,
        val bucket: MessageBucket
    )

}

open class PagedFetchHolder(
    val filter: MessageFilterKey,
    val millisCreated: Long,
    val flow: Flow<StoreResponse<List<DecryptedBaemailMessage>>>
) {
    protected var _hasNextPage = false
    val hasNextPage get() = _hasNextPage

    protected var _hasFetchCompleted = false
    val hasFetchCompleted get() = _hasFetchCompleted

}

class InternalPagedFetchHolder(
    filter: MessageFilterKey,
    millisCreated: Long,
    flow: Flow<StoreResponse<List<DecryptedBaemailMessage>>>
) : PagedFetchHolder(
    filter,
    millisCreated,
    flow
) {
    fun setHasNextPage(isTrue: Boolean) {
        _hasNextPage = isTrue
    }

    fun setHasFetchCompleted(isTrue: Boolean) {
        _hasFetchCompleted = isTrue
    }
}
