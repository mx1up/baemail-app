package app.bitcoin.baemail.message

import com.google.gson.JsonObject

data class BaemailState(
    val token: String,
    val sig: String,
    val millisConfirmed: Long,
    val paymail: String,
    val paymailPkiPublicKey: String
) {

    fun serialize(): String {
        return JsonObject().also { json ->
            json.addProperty(KEY_TOKEN, token)
            json.addProperty(KEY_SIGNATURE, sig)
            json.addProperty(KEY_MILLIS_CONFIRMED, millisConfirmed)
            json.addProperty(KEY_PAYMAIL, paymail)
            json.addProperty(KEY_PAYMAIL_PKI_PUBLIC_KEY, paymailPkiPublicKey)
        }.toString()
    }

    companion object {

        private val KEY_TOKEN = "token"
        private val KEY_SIGNATURE = "signature"
        private val KEY_MILLIS_CONFIRMED = "millisConfirmed"
        private val KEY_PAYMAIL = "paymail"
        private val KEY_PAYMAIL_PKI_PUBLIC_KEY = "paymail_pki_public_key"

        fun parse(stateJson: JsonObject) : BaemailState {
            return BaemailState(
                stateJson.get(KEY_TOKEN).asString,
                stateJson.get(KEY_SIGNATURE).asString,
                stateJson.get(KEY_MILLIS_CONFIRMED).asLong,
                stateJson.get(KEY_PAYMAIL).asString,
                stateJson.get(KEY_PAYMAIL_PKI_PUBLIC_KEY).asString
            )
        }

    }
}