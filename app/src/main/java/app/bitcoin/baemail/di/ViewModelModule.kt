package app.bitcoin.baemail.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.bitcoin.baemail.auth.PaymailViewModel
import app.bitcoin.baemail.di.helper.AppViewModelFactory
import app.bitcoin.baemail.fundingWallet.CoinViewModel
import app.bitcoin.baemail.fundingWallet.SendToAddressViewModel
import app.bitcoin.baemail.fundingWallet.SendToPaymailViewModel
import app.bitcoin.baemail.fundingWallet.TopUpViewModel
import app.bitcoin.baemail.paymail.PaymailManagementViewModel
import app.bitcoin.baemail.superasset.SuperAssetViewModel
import app.bitcoin.baemail.uiCentral.CentralViewModel
import app.bitcoin.baemail.uiMemories.BaemailMessageViewModel
import app.bitcoin.baemail.uiMemories.MessagesViewModel
import app.bitcoin.baemail.uiMemories.NewMessageViewModel
import app.bitcoin.baemail.uiSetup.SetupFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PaymailViewModel::class)
    abstract fun bindAuthViewModel(viewModel: PaymailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CentralViewModel::class)
    abstract fun bindCentralViewModel(viewModel: CentralViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymailManagementViewModel::class)
    abstract fun bindPaymailManagementViewModel(viewModel: PaymailManagementViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SetupFragmentViewModel::class)
    abstract fun bindSetupFragmentViewModel(viewModel: SetupFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CoinViewModel::class)
    abstract fun bindCoinViewModel(viewModel: CoinViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendToAddressViewModel::class)
    abstract fun bindSendToAddressViewModel(viewModel: SendToAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendToPaymailViewModel::class)
    abstract fun bindSendToPaymailViewModel(viewModel: SendToPaymailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TopUpViewModel::class)
    abstract fun bindTopUpViewModel(viewModel: TopUpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewMessageViewModel::class)
    abstract fun bindNewMessageViewModel(viewModel: NewMessageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BaemailMessageViewModel::class)
    abstract fun bindBaemailMessageViewModel(viewModel: BaemailMessageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SuperAssetViewModel::class)
    abstract fun bindSuperAssetViewModel(viewModel: SuperAssetViewModel): ViewModel





    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}