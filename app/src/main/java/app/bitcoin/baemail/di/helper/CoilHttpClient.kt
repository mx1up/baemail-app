package app.bitcoin.baemail.di.helper

import okhttp3.OkHttpClient

data class CoilHttpClient(
    val httpClient: OkHttpClient
)