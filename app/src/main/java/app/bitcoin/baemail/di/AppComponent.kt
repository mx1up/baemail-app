package app.bitcoin.baemail.di

import app.bitcoin.baemail.App
import app.bitcoin.baemail.di.helper.CoilHttpClient
import app.bitcoin.baemail.work.WorkerFactoryFactory
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    WorkerModule::class,
    ActivityBindingModule::class,
    ViewModelModule::class,
    AndroidInjectionModule::class
])
abstract class AppComponent {

    abstract fun inject(app: App)

    //abstract fun provideViewModelFactory(): ViewModelProvider.Factory

    abstract fun providerWorkerFactoryFactory(): WorkerFactoryFactory

    abstract fun provideCoilHttpClient(): CoilHttpClient

}