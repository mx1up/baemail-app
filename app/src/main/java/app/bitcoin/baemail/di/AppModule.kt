package app.bitcoin.baemail.di

import android.content.Context
import androidx.room.Room
import coil.util.CoilUtils
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import app.bitcoin.baemail.BuildConfig
import app.bitcoin.baemail.auth.AuthRepository
import app.bitcoin.baemail.util.ConnectivityLiveData
import app.bitcoin.baemail.di.helper.CoilHttpClient
import app.bitcoin.baemail.message.BaemailRepository
import app.bitcoin.baemail.message.BaemailStateHelper
import app.bitcoin.baemail.net.AppApiService
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.room.APP_DB_NAME
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.util.AppForegroundLiveData
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.*
import app.bitcoin.node.NodeConnectionHelper
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Provider
import javax.inject.Singleton

@Module
class AppModule(
    private val context: Context
) {

    @Provides
    @Singleton
    fun getContext(): Context = context

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val builder = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return builder.setLenient().create()
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {

        val httpClient = OkHttpClient.Builder()

        httpClient
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            // Careful when changing the log level to Level.BODY - this causes an OOM error when
            // processing large requests.
            loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
            //loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)
        }

        return httpClient.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @Singleton
    internal fun provideApiService(retrofit: Retrofit): AppApiService {
        return retrofit.create(AppApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideCoilHttpClient(context: Context): CoilHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.cache(CoilUtils.createDefaultCache(context))
        return CoilHttpClient(httpClient.build())
    }

    @Provides
    @Singleton
    fun provideConnectivityLiveData(context: Context): ConnectivityLiveData {
        return ConnectivityLiveData().apply {
            initialize(context)
        }
    }

    @Provides
    @Singleton
    fun provideAppForegroundLiveData(): AppForegroundLiveData {
        return AppForegroundLiveData.get()
    }

    @Provides
    @Singleton
    fun provideCoroutineUtil(): CoroutineUtil {
        return CoroutineUtil()
    }

    @Provides
    @Singleton
    fun provideAppStateLiveData(
        context: Context,
        coroutineUtil: CoroutineUtil
    ): AppStateLiveData {
        return AppStateLiveData(
            context,
            coroutineUtil
        )
    }

    @Provides
    @Singleton
    fun providePaymailRepository(
        context: Context,
        coroutineUtil: CoroutineUtil,
        appStateLiveData: AppStateLiveData,
        appApiService: AppApiService,
        gson: Gson,
        walletRepository: WalletRepository,
        secureDataSource: SecureDataSource,
        dynamicChainSync: DynamicChainSync
    ): PaymailRepository {
        return PaymailRepository(
            context,
            coroutineUtil,
            appStateLiveData,
            appApiService,
            gson,
            walletRepository,
            secureDataSource,
            dynamicChainSync
        )
    }

    @Provides
    @Singleton
    fun provideBaemailStateHelper(
        appContext: Context,
        gson: Gson
    ): BaemailStateHelper {
        return BaemailStateHelper(
            appContext,
            gson
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Provides
    @Singleton
    fun provideBaemailRepository(
        context: Context,
        coroutineUtil: CoroutineUtil,
        appApiService: AppApiService,
        appDatabase: AppDatabase,
        gson: Gson,
//        paymailRepository: PaymailRepository,
        appStateLD: AppStateLiveData,
        walletRepository: WalletRepository,
        secureDataSource: SecureDataSource,
        baemailStateHelper: BaemailStateHelper,
        blockchainDataSource: BlockchainDataSource
    ): BaemailRepository {
        return BaemailRepository(
            context,
            coroutineUtil,
            appApiService,
            appDatabase,
            appStateLD,
            gson,
//            paymailRepository,
            walletRepository,
            secureDataSource,
            blockchainDataSource,
            baemailStateHelper
        )
    }

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, APP_DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideBaseListAdapterFactory(context: Context): BaseListAdapterFactory {
        return BaseListAdapterFactory(context)
    }

    @Provides
    @Singleton
    fun provideWalletRepository(
        context: Context,
        foregroundLiveData: AppForegroundLiveData,
        requestManager: RequestManager,
        secureDataSource: SecureDataSource,
        gson: Gson
    ): WalletRepository {
        return WalletRepository(context, requestManager, secureDataSource, gson)
    }

    @Provides
    @Singleton
    fun provideAuthRepository(context: Context): AuthRepository {
        return AuthRepository(context)
    }

    @Provides
    @Singleton
    fun provideNodeConnectionHelper(
        context: Context,
        foregroundLiveData: AppForegroundLiveData
    ): NodeConnectionHelper {

        val scope = CoroutineScope(SupervisorJob() + Dispatchers.Default)
        return NodeConnectionHelper(context, scope, foregroundLiveData)
    }

    @Provides
    @Singleton
    fun provideSecureDataSource(
        context: Context,
        coroutineUtil: CoroutineUtil
    ): SecureDataSource {
        val ds = SecureDataSource(context, coroutineUtil)
        ds.setup()
        return ds
    }

    @Provides
    @Singleton
    fun provideBlockchainDataSource(
        context: Context,
        apiService: AppApiService,
        gson: Gson
    ): BlockchainDataSource {
        return BlockchainDataSource(context, apiService, gson)
    }

    @Provides
    @Singleton
    fun provideUnusedAddressHelper(
        context: Context,
        secureDataSource: SecureDataSource,
        walletRepository: WalletRepository,
        appDatabase: AppDatabase,
        appStateLiveData: AppStateLiveData,
        coroutineUtil: CoroutineUtil
    ): UnusedAddressHelper {
        return UnusedAddressHelper(
            context,
            secureDataSource,
            walletRepository,
            appDatabase,
            appStateLiveData,
            coroutineUtil
        )
    }

    @Provides
//    @Singleton
    fun provideReceivedCoinHelper(
        appDatabase: AppDatabase,
        apiService: AppApiService,
        unusedAddressHelper: UnusedAddressHelper,
        gson: Gson
    ): ReceivedCoinHelper {
        val scope = CoroutineScope(SupervisorJob() + Dispatchers.Default)

        val helper = ReceivedCoinHelper(
            appDatabase,
            apiService,
            unusedAddressHelper,
            scope,
            gson
        )

        return helper
    }


    @Provides
//    @Singleton
    fun provideFullSyncHelper(
        blockchainDataSource: BlockchainDataSource,
        walletRepository: WalletRepository,
        appDatabase: AppDatabase
    ): FullSyncHelper {
        return FullSyncHelper(
            walletRepository,
            blockchainDataSource,
            appDatabase
        )
    }

    @Provides
    @Singleton
    fun provideNewBlockPollingHelper(
        blockchainDataSource: BlockchainDataSource,
        coroutineUtil: CoroutineUtil
    ): NewBlockPollingHelper {
        return NewBlockPollingHelper(
            coroutineUtil,
            blockchainDataSource
        )
    }

    @Provides
//    @Singleton
    fun provideChainSync(
        context: Context,
        appDatabase: AppDatabase,
        coroutineUtil: CoroutineUtil,
        blockchainDataSource: BlockchainDataSource,
        fullSyncHelper: FullSyncHelper,
        newBlockPollingHelper: NewBlockPollingHelper,
        receivedCoinHelper: ReceivedCoinHelper,
        unusedAddressHelper: UnusedAddressHelper
    ): ChainSync {
        val scope = CoroutineScope(SupervisorJob() + Dispatchers.Default)
        //todo  scope

        return ChainSync(
            context,
            appDatabase,
            coroutineUtil,
            blockchainDataSource,
            fullSyncHelper,
            newBlockPollingHelper,
            receivedCoinHelper,
            unusedAddressHelper
        )
    }

    @Provides
    @Singleton
    fun provideDynamicChainSync(
        appStateLD: AppStateLiveData,
        appForegroundLD: AppForegroundLiveData,
        connectivityLiveData: ConnectivityLiveData,
        coroutineUtil: CoroutineUtil,
        secureDataSource: SecureDataSource,
        chainSyncProvider: Provider<ChainSync>
    ): DynamicChainSync {


        return DynamicChainSync(
            appStateLD,
            appForegroundLD,
            connectivityLiveData,
            coroutineUtil.appScope,
            secureDataSource,
            chainSyncProvider
        ).apply {
            setup()
        }
    }

    @Provides
    @Singleton
    fun provideExchangeRateHelper(
        coroutineUtil: CoroutineUtil,
        blockchainDataSource: BlockchainDataSource
    ): ExchangeRateHelper {
        return ExchangeRateHelper(
            blockchainDataSource,
            coroutineUtil
        ).apply {
            doRefresh()
        }
    }

}