package app.bitcoin.baemail.di

import app.bitcoin.baemail.SplashActivity
import app.bitcoin.baemail.fundingWallet.CoinActivity
import app.bitcoin.baemail.fundingWallet.SweepActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.paymail.PaymailActivity
import app.bitcoin.baemail.uiCentral.CentralActivity
import app.bitcoin.baemail.uiThreading.ThreadingActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun centralActivity(): CentralActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun paymailActivity(): PaymailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun threadingActivity(): ThreadingActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun topUpActivity(): TopUpActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun sweepActivity(): SweepActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun coinActivity(): CoinActivity

}