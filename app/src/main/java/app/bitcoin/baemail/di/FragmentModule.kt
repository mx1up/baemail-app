package app.bitcoin.baemail.di

import app.bitcoin.baemail.fundingWallet.*
import app.bitcoin.baemail.paymail.*
import app.bitcoin.baemail.superasset.SuperAssetFragment
import app.bitcoin.baemail.uiMemories.BaemailMessageFragment
import app.bitcoin.baemail.uiMemories.MessagesFragment
import app.bitcoin.baemail.uiMemories.NewMessageFragment
import app.bitcoin.baemail.uiSetup.SetupFragment
import app.bitcoin.baemail.uiThreading.MessageThreadFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeMessagesFragment(): MessagesFragment

    @ContributesAndroidInjector
    abstract fun contributeMessageThreadFragment(): MessageThreadFragment

    @ContributesAndroidInjector
    abstract fun contributeAddedPaymailsFragment(): AddedPaymailsFragment

    @ContributesAndroidInjector
    abstract fun contributeSelectPaymailProviderFragment(): SelectPaymailProviderFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailIdInputFragment(): PaymailIdInputFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailDerivPathFragment(): PaymailDerivPathFragment

    @ContributesAndroidInjector
    abstract fun contributeSeedPhraseInputFragment(): SeedPhraseInputFragment

    @ContributesAndroidInjector
    abstract fun contributePaymailVerifyingFragment(): PaymailVerifyingFragment

    @ContributesAndroidInjector
    abstract fun contributeSetupFragment(): SetupFragment

    @ContributesAndroidInjector
    abstract fun contributeFundingCoinFragment(): FundingCoinFragment

    @ContributesAndroidInjector
    abstract fun contributeCoinOptionsFragment(): CoinOptionsFragment

    @ContributesAndroidInjector
    abstract fun contributeSendCoinToAddressFragment(): SendCoinToAddressFragment

    @ContributesAndroidInjector
    abstract fun contributeSendCoinToPaymailFragment(): SendCoinToPaymailFragment

    @ContributesAndroidInjector
    abstract fun contributeTopUpFragment(): TopUpFragment

    @ContributesAndroidInjector
    abstract fun contributeNewMessageFragment(): NewMessageFragment

    @ContributesAndroidInjector
    abstract fun contributeBaemailMessageFragment(): BaemailMessageFragment

    @ContributesAndroidInjector
    abstract fun contributeAppDetailedInfoDialog(): AppDetailedInfoDialog

    @ContributesAndroidInjector
    abstract fun contributeSuperAssetFragment(): SuperAssetFragment
}