package app.bitcoin.baemail.superasset

import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import app.bitcoin.baemail.R
import app.bitcoin.baemail.uiMemories.NewMessageViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class SuperAssetFragment : Fragment(R.layout.fragment_super_asset) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SuperAssetViewModel









    private lateinit var buttonDeploy: Button





    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)



        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(SuperAssetViewModel::class.java)








        buttonDeploy = requireView().findViewById(R.id.deploy)

        buttonDeploy.setOnClickListener {
            //todo
            //todo
            //todo
            //todo

            viewModel.deploy()


        }


    }

}