package app.bitcoin.baemail.superasset

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.bitcoin.baemail.message.BaemailRepository
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.*
import app.bitcoin.baemail.wallet.request.SuperAssetDeployNft
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class SuperAssetViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val walletRepository: WalletRepository,
    private val baemailRepository: BaemailRepository,
    private val coroutineUtil: CoroutineUtil,
    private val appDatabase: AppDatabase,
    private val unusedAddressHelper: UnusedAddressHelper,
    private val secureDataSource: SecureDataSource,
    private val blockchainDataSource: BlockchainDataSource,
    private val exchangeRateHelper: ExchangeRateHelper,
    private val dynamicChainSync: DynamicChainSync
) : ViewModel() {






    fun deploy() {
        viewModelScope.launch {

            val activePaymail = paymailRepository.activePaymailLD.value?.paymail ?: let {
                Timber.e("active-payamail not found")
                return@launch
            }

            if (dynamicChainSync.statusLD.value != ChainSync.Status.LIVE) {
                Timber.e("coins are still syncing")
                return@launch
            }

            val fundingWalletSeed = try {
                secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(activePaymail))
            } catch (e: Exception) {
                Timber.e(e, "error retrieving seed")
                return@launch
            }



            val keyHolder = walletRepository
                .getPrivateKeyForPathFromSeed("m/44'/0'/0'/1/0", fundingWalletSeed) ?: let {
                Timber.e("error looking up key")
                return@launch
            }


//            val f = File("a")
//            f.




            val unspentCoinsList = appDatabase.coinDao().getFundingCoins(activePaymail)
                .mapNotNull {
                    if (it.spendingTxId != null) return@mapNotNull null
                    it
                }


            if (unspentCoinsList.isEmpty()) {
                Timber.e("there are no unspent coins")
                return@launch
            }

            val unspentCoinsMap: Map<String, ReceivedCoins> = unspentCoinsList.let {
                val map = mutableMapOf<String, ReceivedCoins>()

                it.forEach { c ->
                    map[c.derivationPath] = ReceivedCoins(
                        c.txId,
                        c.sats,
                        c.outputIndex,
                        c.address
                    )
                }

                map
            }




            val upcomingUnusedFundingWalletAddresses = unusedAddressHelper
                .getUnusedAddressesOfFundingWallet(activePaymail) // will be used for a change-address

            val changeAddress = upcomingUnusedFundingWalletAddresses[0]
            Timber.d("provided changeAddress for the deploy message: $changeAddress")







            val response = walletRepository.superAssetDeploy(
                SuperAssetDeployNft.Owner(
                    keyHolder.path,
                    keyHolder.publicKey,
                    keyHolder.privateKey
                ),
                SuperAssetDeployNft.Funds(
                    fundingWalletSeed,
                    "m/44'/0'/0'",
                    unspentCoinsMap,
                    changeAddress.address
                )
            )





            Timber.d("depoly response:$response")
        }
    }

}