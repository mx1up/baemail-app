package app.bitcoin.baemail.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.util.match
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.LabelFormatter.LABEL_GONE
import com.google.android.material.slider.Slider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class ViewBaePayOptions : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp1: Float


    private val valueToPayee: TextView
    private val buttonMinus: Button
    private val buttonPlus: Button
    private val slider: Slider


    private val positionListTouchListener = PositionSliderTouchListener()



    private var valueModel: Model = Model(
        0.01f,
        0.1f,
        0.01f,
        "USD"
    )


    var onMinusClicked: ()->Unit = {
        Timber.d("onMinusClicked")
    }

    var onPlusClicked: ()->Unit = {
        Timber.d("onPlusClicked")
    }

    var onValueChanged: (Float)->Unit = {
        Timber.d("onValueChanged")
    }


    init {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.view_bae_pay_options, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)

        set.clone(this)
        set.match(view, this)

        dp1 = context.resources.displayMetrics.density


        valueToPayee = view.findViewById(R.id.value_to_payee)
        buttonMinus = view.findViewById(R.id.button_minus)
        buttonPlus = view.findViewById(R.id.button_plus)
        slider = view.findViewById(R.id.slider)


        val colorStroke = ContextCompat.getColor(context, R.color.green1)
        val colorBackground = ContextCompat.getColor(context, R.color.green2)

        val drawableStroke = DrawableStroke().also {
            it.setStrokeColor(colorStroke)
            it.setStrokeWidths(0, (8 * dp1).toInt(), 0, 0)
            it.setPadding(0, 0, 0, 0)
        }

        background = LayerDrawable(arrayOf(
            ColorDrawable(colorBackground),
            drawableStroke
        ))




        slider.isSaveEnabled = false
        //slider.setLabelFormatter(Slider.BasicLabelFormatter())
        slider.setOnTouchListener(positionListTouchListener)
        slider.addOnChangeListener { slider, value, fromUser ->
            if (!fromUser) return@addOnChangeListener

            refreshLabelToPayee(value)
        }
        slider.labelBehavior = LabelFormatter.LABEL_GONE


        buttonMinus.setOnClickListener {
            onMinusClicked()
        }

        buttonPlus.setOnClickListener {
            onPlusClicked()
        }


        refresh()
    }

    private fun refresh() {
        refreshLabelToPayee(valueModel.valueCurrent)


        slider.apply {
            valueFrom = 0f

            valueTo = valueModel.valueMax
            valueFrom = valueModel.valueMin
            stepSize = 0f

            value = valueModel.valueCurrent
            invalidate()
        }
    }

    private fun refreshLabelToPayee(value: Float) {
        valueToPayee.text = String.format(Locale.US, "%.2f %s", value, valueModel.currency)
    }

    fun applyModel(model: Model) {
        if (model == valueModel) return
        valueModel = model

        refresh()
    }






    inner class PositionSliderTouchListener : OnTouchListener {
        private val scope = CoroutineScope(Dispatchers.Main)

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            event ?: return false
            v ?: return false

            when (event.actionMasked) {
                MotionEvent.ACTION_UP -> {
                    onActionUp(v)
                }
            }

            return false
        }

        private fun onActionUp(view: View) {
            view as Slider

            scope.launch {
                delay(50)

//                val isPressed = view.isPressed
//                Timber.d("OnTouchListener#onActionUp ...inside deferred lambda value:$view isPressed:$isPressed")

                onValueChanged(view.value)


            }
        }

    }



    data class Model(
        val valueMin: Float,
        val valueMax: Float,
        val valueCurrent: Float,
        val currency: String
    )
}