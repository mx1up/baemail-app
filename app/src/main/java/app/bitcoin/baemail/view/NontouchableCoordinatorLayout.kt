package app.bitcoin.baemail.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.coordinatorlayout.widget.CoordinatorLayout

class NontouchableCoordinatorLayout : CoordinatorLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private var isTouchBroken = false

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (isTouchBroken) {
            return true
        }
        return super.dispatchTouchEvent(ev)
    }

    fun setTouchBroken(broken: Boolean) {
        isTouchBroken = broken
    }
}