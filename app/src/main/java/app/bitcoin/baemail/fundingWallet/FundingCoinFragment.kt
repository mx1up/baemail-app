package app.bitcoin.baemail.fundingWallet

import android.graphics.Outline
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableOval
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.recycler.BaseListAdapter
import app.bitcoin.baemail.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import com.google.android.material.appbar.AppBarLayout
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class FundingCoinFragment : Fragment(R.layout.fragment_funding_coins) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: CoinViewModel


    lateinit var container: ConstraintLayout
    lateinit var recycler: RecyclerView
    lateinit var appBar: AppBarLayout
    lateinit var close: ImageView
    lateinit var fullSync: TextView
    lateinit var paymail: TextView
    lateinit var unspentCoins: TextView
    lateinit var unspentSats: TextView
//    lateinit var lastSync: TextView
    lateinit var syncedCoins: TextView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory
    lateinit var adapter: BaseListAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_coins)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(CoinViewModel::class.java)



        container = requireView().findViewById(R.id.container)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        close = requireView().findViewById(R.id.close)
        fullSync = requireView().findViewById(R.id.full_sync)
        paymail = requireView().findViewById(R.id.paymail)
        unspentCoins = requireView().findViewById(R.id.unspent_coins)
        unspentSats = requireView().findViewById(R.id.unspent_sats)
//        lastSync = requireView().findViewById(R.id.last_sync)
        syncedCoins = requireView().findViewById(R.id.synced_coins)

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)

        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            windowInsets
        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atCoinListener = object : BaseListAdapter.ATCoinListener {
                override fun onClick(coin: Coin) {
                    Timber.d("coin clicked: $coin")


                    val args = Bundle()
                    args.putString("tx_hash", coin.txId)
                    args.putString("address", coin.address)
                    args.putInt("output_index", coin.outputIndex)
                    args.putInt("sats", coin.sats)
                    args.putString("derivation_path", coin.derivationPath)
                    args.putString("spending_tx_hash", coin.spendingTxId)


                    findNavController().navigate(R.id.action_fundingCoinFragment_to_coinOptionsFragment, args)
                }

            }
        )

        layoutManager = LinearLayoutManager(context)
        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER




        appBar.elevation = 0f
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (adapter.itemCount == 0) {
                    appBar.elevation = 0f
                    return
                }
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }
        })

        val colorAccent = R.color.colorPrimary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }


        val colorSelector = R.color.selector_on_light.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorSelectorOnAccent = R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        close.foreground = DrawableState.getNew(colorSelector)

        val drawableCloseIcon = R.drawable.ic_clear_24.let { id ->
            val d = ContextCompat.getDrawable(requireContext(), id)!!
            d.setTint(colorAccent)
            d
        }
        close.setImageDrawable(drawableCloseIcon)

        close.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        close.clipToOutline = true
        close.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setOval(0, 0, view.width, view.height)
            }

        }


        val backgroundFullSync = DrawableOval(requireContext()).also {
            it.setBgColor(colorAccent)
            it.setStrokeWidth(0f)
            it.setPaddings(0f, 0f, 0f, 0f)
        }
        fullSync.background = backgroundFullSync
        fullSync.clipToOutline = true
        fullSync.outlineProvider = backgroundFullSync.getOutlineProvider()

        fullSync.foreground = DrawableState.getNew(colorSelectorOnAccent)

        fullSync.setOnClickListener {
            viewModel.startFullSync()
        }


        viewModel.contentLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            paymail.text = it.paymail
            unspentCoins.text = "${it.unspentCoins} unspent coins"
            unspentSats.text = "${it.unspentSats} sats"
//            if (it.hadSyncError) {
//                lastSync.text = "error"
//                //todo color red
//            } else {
//                val dateString = DateUtils.getRelativeTimeSpanString(requireContext(), it.millisSynced)
//                lastSync.text = "Last full sync: $dateString"
//            }

            syncedCoins.text = "Total found coins: ${it.coinsTotal}"

            adapter.setItems(it.coinsList)

        })


    }

}