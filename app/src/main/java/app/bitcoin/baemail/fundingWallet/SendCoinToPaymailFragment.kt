package app.bitcoin.baemail.fundingWallet

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.paymail.SeedPhraseInputFragment
import app.bitcoin.baemail.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class SendCoinToPaymailFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: SendToPaymailViewModel



    lateinit var container: FrameLayout
    lateinit var layoutContainer: ConstraintLayout
    lateinit var processingContainer: ConstraintLayout

    lateinit var input: EditText
    lateinit var errorLabel: TextView
    lateinit var cancel: TextView
    lateinit var next: TextView
    lateinit var value: TextView




    val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorNextBackground: Int by lazy {
        R.color.paymail_id_input_fragment__next_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorNextBackgroundDisabled: Int by lazy {
        R.color.paymail_id_input_fragment__next_background_disabled.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }



    val colorInputErrorSuccess: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_success.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }
    val colorInputErrorProcessing: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_processing.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }
    val colorInputErrorFail: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_fail.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }






    val appliedInsets = SeedPhraseInputFragment.AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_send_coin_to_paymail, container, false)
    }








    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        val txHash = requireArguments().getString("tx_hash")!!
        val address = requireArguments().getString("address")!!
        val derivationPath = requireArguments().getString("derivation_path")!!
        val outputIndex = requireArguments().getInt("output_index")
        val sats = requireArguments().getInt("sats")


        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(SendToPaymailViewModel::class.java)

        viewModel.init(
            txHash,
            address,
            derivationPath,
            outputIndex,
            sats
        )

        container = requireView().findViewById(R.id.container)
        layoutContainer = requireView().findViewById(R.id.layout_container)
        processingContainer = requireView().findViewById(R.id.processing_container)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        errorLabel = requireView().findViewById(R.id.error_label)
        value = requireView().findViewById(R.id.value)



        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }



        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height - appliedInsets.top - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")


            next.translationY = -1f * keyboardHeight
            cancel.translationY = -1f * keyboardHeight
            //todo coordinator.translationY = -1f * keyboardHeight

        })




        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }





        val colorCancelBackground = R.color.paymail_id_input_fragment__cancel_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))






        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))





        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.onDestinationPaymailChanged(input.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })






        cancel.setOnClickListener {
            //todo viewModel.onAddingCancelled()
            findNavController().popBackStack()
        }

        next.setOnClickListener {
            viewModel.onRequestedSend()
        }





        viewModel.sendToPaymailContentLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            if (SendToPaymailViewModel.State.SENDING_SUCCESS == it.state) {
                findNavController().popBackStack()
                return@Observer
            }

            value.text = it.value.toString()

            if (input.text.toString() != it.input) {
                input.setText(it.input)
            }

            when (it.state) {
                SendToPaymailViewModel.State.EMPTY -> {
                    errorLabel.visibility = View.INVISIBLE
                }
                SendToPaymailViewModel.State.COIN_INVALID -> {
                    errorLabel.visibility = View.VISIBLE
                    errorLabel.text = "This coin is spent" //todo localize
                    errorLabel.setTextColor(colorInputErrorFail)
                }
                SendToPaymailViewModel.State.CHECKING_PAYMAIL -> {
                    errorLabel.visibility = View.VISIBLE
                    errorLabel.text = "Checking paymail exists..." //todo localize
                    errorLabel.setTextColor(colorInputErrorProcessing)
                }
                SendToPaymailViewModel.State.PAYMAIL_NOT_VALID -> {
                    errorLabel.visibility = View.VISIBLE
                    errorLabel.text = "Paymail not recognized" //todo localize
                    errorLabel.setTextColor(colorInputErrorFail)
                }
                SendToPaymailViewModel.State.PAYMAIL_VALID -> {
                    errorLabel.visibility = View.VISIBLE
                    errorLabel.text = "Paymail destination found" //todo localize
                    errorLabel.setTextColor(colorInputErrorSuccess)
                }
                SendToPaymailViewModel.State.SENDING_IN_PROGRESS -> {
                    errorLabel.visibility = View.INVISIBLE
                }
                SendToPaymailViewModel.State.SENDING_ERROR -> {
                    errorLabel.visibility = View.VISIBLE
                    errorLabel.text = "Error sending coin" //todo localize
                    errorLabel.setTextColor(colorInputErrorFail)
                }
                SendToPaymailViewModel.State.SENDING_SUCCESS -> {
                    errorLabel.visibility = View.VISIBLE
                    errorLabel.text = "Coin sent" //todo localize
                    errorLabel.setTextColor(colorInputErrorSuccess)
                }
            }

            if (SendToPaymailViewModel.State.SENDING_IN_PROGRESS == it.state) {
                processingContainer.visibility = View.VISIBLE

                //todo hide the floating buttons
            } else {
                processingContainer.visibility = View.GONE
            }


            if (it.isCoinValid && SendToPaymailViewModel.State.PAYMAIL_VALID == it.state) {
                next.isEnabled = true
                drawableNextBackground.color = colorNextBackground
            } else {
                next.isEnabled = false
                drawableNextBackground.color = colorNextBackgroundDisabled
            }

        })

    }



    private fun focusOnInput() {
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        focusOnInput()
    }


}