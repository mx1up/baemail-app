package app.bitcoin.baemail.fundingWallet

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Outline
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableOval
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.recycler.BaseListAdapter
import app.bitcoin.baemail.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import app.bitcoin.baemail.wallet.request.DerivedAddress
import com.google.android.material.appbar.AppBarLayout
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class TopUpFragment : Fragment(R.layout.fragment_topup_funding_wallet) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: TopUpViewModel


    lateinit var container: ConstraintLayout
    lateinit var recycler: RecyclerView
    lateinit var appBar: AppBarLayout
    lateinit var close: ImageView
    lateinit var topupFromMb: TextView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory
    lateinit var adapter: BaseListAdapter



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_topup)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(TopUpViewModel::class.java)



        container = requireView().findViewById(R.id.container)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        close = requireView().findViewById(R.id.close)
        topupFromMb = requireView().findViewById(R.id.topup_from_mb)

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)

        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            windowInsets
        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atUnusedAddressListener = object : BaseListAdapter.ATUnusedAddressListener {
                override fun onClick(coin: DerivedAddress) {
                    (requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?)?.let { manager ->
                        val clip = ClipData.newPlainText("Receiving address", coin.address)

                        manager.setPrimaryClip(clip)
                        Toast.makeText(requireContext(), "Receiving address copied", Toast.LENGTH_LONG).show()
                    }
                }

            }
        )

        layoutManager = LinearLayoutManager(context)
        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER




        appBar.elevation = 0f
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (adapter.itemCount == 0) {
                    appBar.elevation = 0f
                    return
                }
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }
        })

        val colorAccent = R.color.colorPrimary.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }


        val colorSelector = R.color.selector_on_light.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorSelectorOnAccent = R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        close.foreground = DrawableState.getNew(colorSelector)

        val drawableCloseIcon = R.drawable.ic_clear_24.let { id ->
            val d = ContextCompat.getDrawable(requireContext(), id)!!
            d.setTint(colorAccent)
            d
        }
        close.setImageDrawable(drawableCloseIcon)

        close.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        close.clipToOutline = true
        close.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setOval(0, 0, view.width, view.height)
            }

        }


        val backgroundFullSync = DrawableOval(requireContext()).also {
            it.setBgColor(colorAccent)
            it.setStrokeWidth(0f)
            it.setPaddings(0f, 0f, 0f, 0f)
        }
        topupFromMb.background = backgroundFullSync
        topupFromMb.clipToOutline = true
        topupFromMb.outlineProvider = backgroundFullSync.getOutlineProvider()

        topupFromMb.foreground = DrawableState.getNew(colorSelectorOnAccent)

        topupFromMb.setOnClickListener {
            viewModel.topUpFromMoneyButtonLD.value?.let { url ->
                val builder = CustomTabsIntent.Builder()
                val customTabsIntent = builder.build()
                customTabsIntent.launchUrl(requireActivity(), Uri.parse(url))

            }
        }

        viewModel.topUpFromMoneyButtonLD.observe(viewLifecycleOwner, Observer {
            if (it == null) {
                topupFromMb.isEnabled = false
                topupFromMb.alpha = 0.5f
                return@Observer
            }

            topupFromMb.isEnabled = true
            topupFromMb.alpha = 1f
        })

        viewModel.contentLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            adapter.setItems(it)

        })


    }


}