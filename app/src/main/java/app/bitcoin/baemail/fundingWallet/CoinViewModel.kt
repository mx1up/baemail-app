package app.bitcoin.baemail.fundingWallet

import android.content.Context
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.recycler.ATCoin
import app.bitcoin.baemail.recycler.AdapterType
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.DynamicChainSync
import app.bitcoin.baemail.wallet.WalletRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class CoinViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val walletRepository: WalletRepository,
    private val appDatabase: AppDatabase,
    private val coroutineUtil: CoroutineUtil,
    private val secureDataSource: SecureDataSource,
    private val dynamicChainSync: DynamicChainSync
) : ViewModel() {


    private var coinsOfPaymailLD: LiveData<List<Coin>>? = null

    private val _contentLD = MediatorLiveData<Model>()
    val contentLD: LiveData<Model>
        get() = _contentLD



    private var previousPaymail: String? = null
    val activePaymailObserver = object : Observer<AuthenticatedPaymail> {
//        private var previousPaymail: String? = null

        override fun onChanged(it: AuthenticatedPaymail?) {
            val paymail = it?.paymail

            if (previousPaymail == paymail) return //unchanged
            previousPaymail = paymail

            coinsOfPaymailLD?.let {
                _contentLD.removeSource(it)
            }
            coinsOfPaymailLD = null

            if (paymail == null) return //todo consider clearing content

            val coinsLD = appDatabase.coinDao().getFundingCoinsLD(paymail)
            coinsOfPaymailLD = coinsLD

            _contentLD.addSource(coinsLD) {
                Timber.d("db-coins changed")
                refreshContent()
            }
        }

    }


    init {
        //...
        //

        paymailRepository.activePaymailLD.observeForever(activePaymailObserver)

    }


    override fun onCleared() {
        paymailRepository.activePaymailLD.removeObserver(activePaymailObserver)

        super.onCleared()
    }


//    fun onSyncRequested() {
//        viewModelScope.launch {
//            val paymail = paymailRepository.activePaymailLD.value?.paymail ?: return@launch
//            walletRepository.requestWalletSync(paymail)
//        }
//    }

    var isInSync = false
    fun startFullSync() {
        if (isInSync) return
        isInSync = true

        coroutineUtil.appScope.launch(Dispatchers.IO) {
            try {
                walletRepository.startForeground()

//                //todo  rm
//                appDatabase.coinDao().deleteAll()
//                appDatabase.blockSyncInfoDao().clearForHeight(0, previousPaymail!!)
//                appDatabase.txConfirmationDao().clearForHeight(0)
//                //todo  rm

                dynamicChainSync.fullSyncHelper?.syncFromStart()
            } finally {
                walletRepository.stopForeground()
                isInSync = false
            }

        }
    }

    private fun refreshContent() {
        viewModelScope.launch {
            val paymail = paymailRepository.activePaymailLD.value?.paymail ?: return@launch
            val dbCoins = coinsOfPaymailLD?.value ?: return@launch

            Timber.d("dbCoins size: ${dbCoins.size}")

            var contentList = mutableListOf<AdapterType>()

            dbCoins.map {
                ATCoin(it, false)
            }.toCollection(contentList)

            val finalList = contentList.sortedWith(object : Comparator<AdapterType>{
                override fun compare(lhs: AdapterType?, rhs: AdapterType?): Int {
                    lhs as ATCoin
                    rhs as ATCoin

                    if (lhs.coin.spendingTxId == null && rhs.coin.spendingTxId != null) {
                        return -1
                    } else if (lhs.coin.spendingTxId != null && rhs.coin.spendingTxId == null) {
                        return 1
                    }

                    if (lhs.coin.pathIndex > rhs.coin.pathIndex) {
                        return -1
                    } else if (lhs.coin.pathIndex < rhs.coin.pathIndex) {
                        return 1
                    }

                    return rhs.coin.sats - lhs.coin.sats
                }
            })

            val unspentCoins = dbCoins.mapNotNull { coin ->
                if (coin.spendingTxId == null) {
                    coin
                } else {
                    null
                }
            }
            val unspentCoinsCount = unspentCoins.size
            val unspentSats = unspentCoins.foldRight(0) { coin, acc ->
                acc + coin.sats
            }


//            val syncResult = walletRepository.syncTracking.getSyncResult(paymail)

            _contentLD.value = Model(
                paymail,
                dbCoins.size,
                unspentCoinsCount,
                unspentSats,
//                walletRepository.syncTracking.isSyncRequested(paymail),
//                syncResult.error != null,
//                syncResult.millis,
                finalList
            )
            Timber.d("content refreshed")

        }
    }


    data class Model(
        val paymail: String,
        val coinsTotal: Int,
        val unspentCoins: Int,
        val unspentSats: Int,
        val coinsList: List<AdapterType>
    )


}