package app.bitcoin.baemail.fundingWallet

import android.content.Context
import android.util.Base64
import androidx.lifecycle.*
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.recycler.ATUnusedAddress
import app.bitcoin.baemail.recycler.AdapterType
import app.bitcoin.baemail.wallet.UnusedAddressHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class TopUpViewModel @Inject constructor(
    val appContext: Context,
    val unusedAddressHelper: UnusedAddressHelper,
    val paymailRepository: PaymailRepository
) : ViewModel() {

    val activePaymailLD = paymailRepository.activePaymailLD

    val topUpFromMoneyButtonLD = MediatorLiveData<String>()

    private val _contentLD = MutableLiveData<List<AdapterType>>()
    val contentLD: LiveData<List<AdapterType>>
        get() = _contentLD

    init {

        topUpFromMoneyButtonLD.addSource(activePaymailLD) {
            refreshTopUpFromMoneyButton()
        }

    }

    private fun refreshTopUpFromMoneyButton() {
        val activePaymail = activePaymailLD.value?.paymail ?: return

        viewModelScope.launch(Dispatchers.Main) {
            val nextAddresses = unusedAddressHelper.getUnusedAddressesOfFundingWallet(activePaymail)
            val address = nextAddresses[0]

            val query = """
                {
                  "to": "${address.address}",
                  "editable": true,
                  "currency": "USD",
                  "amount": 0.01
                }
            """.trimIndent()

            val encodedQuery = Base64.encodeToString(query.toByteArray(), Base64.DEFAULT)
            val buttonUrl = "https://button.bitdb.network/#$encodedQuery"

            Timber.d("#refreshTopUpFromMoneyButton q:$query url:$buttonUrl")

            topUpFromMoneyButtonLD.postValue(buttonUrl)





            val unusedAddresses = unusedAddressHelper.getUnusedAddressesOfFundingWallet(activePaymail)


            _contentLD.value = unusedAddresses.mapIndexed { i, address ->
                ATUnusedAddress(address, i + 1 == unusedAddresses.size)
            }.mapIndexedNotNull { i, address ->
                if (i < 5) address else null
            }


        }
    }


}