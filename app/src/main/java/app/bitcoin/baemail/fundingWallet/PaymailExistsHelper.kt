package app.bitcoin.baemail.fundingWallet

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.net.BsvAlias
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.util.CoroutineUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PaymailExistsHelper(
    val paymailRepository: PaymailRepository,
    val coroutineUtil: CoroutineUtil
) {

    private val invalidPaymailFormatResponse = MutableLiveData<Pair<State, BsvAlias?>>().also {
        it.value = Pair(State.INVALID, null)
    }

    private val checkedPaymailCache = mutableMapOf<String, LiveData<Pair<State, BsvAlias?>>>()


    fun checkExistence(paymailToCheck: String): LiveData<Pair<State, BsvAlias?>> {
        val isValidAddress = Patterns.EMAIL_ADDRESS.matcher(paymailToCheck).matches()

        if (!isValidAddress) {
            return invalidPaymailFormatResponse
        }

        //return from cache if mapping exists
        checkedPaymailCache[paymailToCheck]?.let {
            return it
        }

        //start checking
        return doCheckExistence(paymailToCheck)
    }

    private fun doCheckExistence(paymailToCheck: String): LiveData<Pair<State, BsvAlias?>> {

        val ld = MutableLiveData<Pair<State, BsvAlias?>>().also {
            it.value = Pair(State.CHECKING, null)
        }

        checkedPaymailCache[paymailToCheck] = ld

        coroutineUtil.appScope.launch(Dispatchers.IO) {
            val aliasResp = paymailRepository.checkPaymailExists(paymailToCheck)
            if (aliasResp.first != null) {
                ld.postValue(Pair(State.VALID, aliasResp.first))
            } else {
                ld.postValue(Pair(State.INVALID, null))
            }
        }

        return ld
    }



    enum class State {
        CHECKING,
        VALID,
        INVALID
    }
}