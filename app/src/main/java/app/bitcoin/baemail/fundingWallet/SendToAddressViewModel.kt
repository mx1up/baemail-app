package app.bitcoin.baemail.fundingWallet

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import app.bitcoin.baemail.util.BitcoinAddressUtils
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.BlockchainDataSource
import app.bitcoin.baemail.wallet.ReceivedCoins
import app.bitcoin.baemail.wallet.UnusedAddressHelper
import app.bitcoin.baemail.wallet.WalletRepository
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SendToAddressViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val walletRepository: WalletRepository,
    private val secureDataSource: SecureDataSource,
    private val appDatabase: AppDatabase,
    private val unusedAddressHelper: UnusedAddressHelper,
    private val blockchainDataSource: BlockchainDataSource,
    private val coroutineUtil: CoroutineUtil
) : ViewModel() {



    private var coin: ReceivedCoins? = null

    private var lastDestinationAddress = ""


    private var coinsOfPaymailLD: LiveData<List<Coin>>? = null


    private val _sendToAddressContentLD = MediatorLiveData<SendToAddressModel>()
    val sendToAddressContentLD: LiveData<SendToAddressModel>
        get() = _sendToAddressContentLD

    private val activePaymailObserver = ActivePaymailObserver()

    init {
        _sendToAddressContentLD.value = SendToAddressModel(
            lastDestinationAddress,
            0,
            false,
            SendToAddressState.EMPTY
        )

        paymailRepository.activePaymailLD.observeForever(activePaymailObserver)

    }


    override fun onCleared() {
        paymailRepository.activePaymailLD.removeObserver(activePaymailObserver)

        super.onCleared()
    }


    fun init (
        txHash: String,
        address: String,
        derivationPath: String,
        outputIndex: Int,
        sats: Int
    ) {
        coin?.let {
            if (txHash == it.txHash && address == it.address && outputIndex == it.index) return

            coin = null
            lastDestinationAddress = ""
            _sendToAddressContentLD.value = SendToAddressModel(
                lastDestinationAddress,
                0,
                false,
                SendToAddressState.EMPTY
            )
        }

        doInit(
            txHash,
            address,
            derivationPath,
            outputIndex,
            sats
        )
    }

    private fun doInit(
        txHash: String,
        address: String,
        derivationPath: String,
        outputIndex: Int,
        sats: Int
    ) {
        coin = ReceivedCoins(
            txHash,
            sats,
            outputIndex,
            address
        )

        refreshModel()
    }

    private fun refreshModel() {

        val matchingCoin = coin.let {
            if (it == null) return@let null

            coinsOfPaymailLD?.value?.find { dbCoin ->
                dbCoin.address == it.address &&
                        dbCoin.txId == it.txHash &&
                        dbCoin.outputIndex == it.index
            }
        }

        val isCoinValid = matchingCoin != null && matchingCoin.spendingTxId == null

        val sats = if (isCoinValid) matchingCoin!!.sats else 0

        val state = if (lastDestinationAddress.isBlank()) {
            SendToAddressState.EMPTY
        } else if (!isCoinValid) {
            SendToAddressState.ERROR_COIN_INVALID
        } else if (!isValidBitcoinAddress(lastDestinationAddress)) {
            SendToAddressState.NOT_AN_ADDRESS
        } else {
            SendToAddressState.VALID
        }



        _sendToAddressContentLD.value = SendToAddressModel(
            lastDestinationAddress,
            sats,
            isCoinValid,
            state
        )
    }


    fun onDestinationAddressChanged(address: String) {
        val p0 = address.trim()
        if (p0 == lastDestinationAddress) return

        lastDestinationAddress = p0

        refreshModel()
    }

    fun onRequestedSend() {
        //todo
        //todo
        //todo refactor to have the relevant part of the code in a repository
        //todo

        val model = sendToAddressContentLD.value!!
        _sendToAddressContentLD.value = model.copy(state = SendToAddressState.PROCESSING)

        coroutineUtil.appScope.launch {

            val paymail = activePaymailObserver.previousPaymail ?: let {
                Timber.e("tx-sending dropped; unexpected paymail value")
                _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.ERROR))
                return@launch
            }

            val fundingWalletSeed = try {
                secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))
            } catch (e: Exception) {
                Timber.e(e, "error retrieving seed")
                _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.ERROR))
                return@launch
            }

            val sendingCoin = coin ?: let {
                Timber.e("tx-sending dropped; unexpected state, `coin` is NULL")
                _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.ERROR))
                return@launch
            }

            val matchingDbCoin = sendingCoin.let {
                coinsOfPaymailLD?.value?.find { dbCoin ->
                    dbCoin.address == it.address &&
                            dbCoin.txId == it.txHash &&
                            dbCoin.outputIndex == it.index
                }
            } ?: let {
                Timber.e("tx-sending dropped; unexpected state, `db-coin` is NULL")
                _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.ERROR))
                return@launch
            }

            val hexOfTx = walletRepository.createSimpleTx(
                fundingWalletSeed,
                mapOf(matchingDbCoin.derivationPath to sendingCoin),
                model.input
            )

            hexOfTx ?: let {
                Timber.d("tx NOT created; hex is NULL")
                _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.ERROR))
                return@launch
            }
            Timber.d("tx created; hex: $hexOfTx")

            val txId = blockchainDataSource.sendHexTx(hexOfTx)
            if (txId == null) {
                Timber.d("tx NOT sent")
                _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.ERROR))
                return@launch
            }

            Timber.d("tx broadcast result tx-id: $txId")

            // mark coin as spent by updating the db
            matchingDbCoin.copy(
                spendingTxId = txId
            ).let {
                appDatabase.coinDao().insertCoin(it)
            }

            // must add new confirmation meta-data
            appDatabase.txConfirmationDao().insertConfirmationInfo(TxConfirmation(
                txId,
                -1,
                null,
                -1L
            ))

            _sendToAddressContentLD.postValue(model.copy(state = SendToAddressState.SUCCESS))
        }
    }

    private fun isValidBitcoinAddress(value: String): Boolean {
        return BitcoinAddressUtils.isRegularAddress(value)
    }

    inner class ActivePaymailObserver : Observer<AuthenticatedPaymail> {
        var previousPaymail: String? = null

        override fun onChanged(it: AuthenticatedPaymail?) {
            val paymail = it?.paymail

            if (previousPaymail == paymail) return //unchanged
            previousPaymail = paymail

            coinsOfPaymailLD?.let {
                _sendToAddressContentLD.removeSource(it)
            }
            coinsOfPaymailLD = null

            if (paymail == null) return //todo consider clearing content

            val coinsLD = appDatabase.coinDao().getFundingCoinsLD(paymail)
            coinsOfPaymailLD = coinsLD

            _sendToAddressContentLD.addSource(coinsLD) {
                Timber.d("coins refreshed !!!!!!!") //todo
                refreshModel()
            }
        }

    }

    data class SendToAddressModel(
        val input: String,
        val value: Int,
        val isCoinValid: Boolean,
        val state: SendToAddressState
    )

    enum class SendToAddressState {
        EMPTY,
        NOT_AN_ADDRESS,
        VALID,
        PROCESSING,
//        FAIL,
        SUCCESS,
        ERROR_COIN_INVALID,
        ERROR
    }

}