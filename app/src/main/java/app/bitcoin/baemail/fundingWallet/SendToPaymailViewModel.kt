package app.bitcoin.baemail.fundingWallet

import android.content.Context
import androidx.lifecycle.*
import app.bitcoin.baemail.net.BsvAlias
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.BlockchainDataSource
import app.bitcoin.baemail.wallet.ReceivedCoins
import app.bitcoin.baemail.wallet.UnusedAddressHelper
import app.bitcoin.baemail.wallet.WalletRepository
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SendToPaymailViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val walletRepository: WalletRepository,
    private val secureDataSource: SecureDataSource,
    private val appDatabase: AppDatabase,
    private val unusedAddressHelper: UnusedAddressHelper,
    private val blockchainDataSource: BlockchainDataSource,
    private val coroutineUtil: CoroutineUtil
) : ViewModel() {



    private var coin: ReceivedCoins? = null

    private var lastDestinationPaymail = ""


    private var coinsOfPaymailLD: LiveData<List<Coin>>? = null


    private val _sendToPaymailContentLD = MediatorLiveData<SendToPaymailModel>()
    val sendToPaymailContentLD: LiveData<SendToPaymailModel>
        get() = _sendToPaymailContentLD

    private val activePaymailObserver = ActivePaymailObserver()

    private val _paymailExistsLD = MediatorLiveData<PaymailExistsHelper.State>()
    val paymailExistsLD: LiveData<PaymailExistsHelper.State>
        get() = _paymailExistsLD

    private val _paymailSendingLD = MutableLiveData<SendToPaymailState>()
    val paymailSendingLD: LiveData<SendToPaymailState>
        get() = _paymailSendingLD

    private var lastPaymailExistenceLD: LiveData<Pair<PaymailExistsHelper.State, BsvAlias?>>? = null


    init {


        _sendToPaymailContentLD.value = SendToPaymailModel(
            lastDestinationPaymail,
            0,
            false,
            State.EMPTY
        )

        paymailRepository.activePaymailLD.observeForever(activePaymailObserver)

        _sendToPaymailContentLD.addSource(paymailSendingLD) {
            refreshModel()
        }

    }


    override fun onCleared() {
        paymailRepository.activePaymailLD.removeObserver(activePaymailObserver)

        lastPaymailExistenceLD?.removeObserver(paymailExistenceObserver)

        super.onCleared()
    }


    fun init (
        txHash: String,
        address: String,
        derivationPath: String,
        outputIndex: Int,
        sats: Int
    ) {
        coin?.let {
            if (txHash == it.txHash && address == it.address && outputIndex == it.index) return

            coin = null
            lastDestinationPaymail = ""
            _sendToPaymailContentLD.value = SendToPaymailModel(
                lastDestinationPaymail,
                0,
                false,
                State.EMPTY
            )
        }

        doInit(
            txHash,
            address,
            derivationPath,
            outputIndex,
            sats
        )
    }


    private fun doInit(
        txHash: String,
        address: String,
        derivationPath: String,
        outputIndex: Int,
        sats: Int
    ) {
        coin = ReceivedCoins(
            txHash,
            sats,
            outputIndex,
            address
        )

        refreshModel()
    }

    private fun refreshModel() {

        val matchingCoin = coin.let {
            if (it == null) return@let null

            coinsOfPaymailLD?.value?.find { dbCoin ->
                dbCoin.address == it.address &&
                        dbCoin.txId == it.txHash &&
                        dbCoin.outputIndex == it.index
            }
        }

        val isCoinValid = matchingCoin != null && matchingCoin.spendingTxId == null

        val isPaymailValid = paymailExistsLD.value == PaymailExistsHelper.State.VALID
        val isCheckingPaymailValid = paymailExistsLD.value == PaymailExistsHelper.State.CHECKING

        val sats = if (isCoinValid) matchingCoin!!.sats else 0

        var state = if (lastDestinationPaymail.isBlank()) {
            State.EMPTY
        } else if (!isCoinValid) {
            State.COIN_INVALID
        } else if (isCheckingPaymailValid) {
            State.CHECKING_PAYMAIL
        } else if (isPaymailValid) {
            State.PAYMAIL_VALID
        } else {
            State.PAYMAIL_NOT_VALID
        }

        paymailSendingLD.value?.let {
            //a send was attempted
            when(it) {
                SendToPaymailState.PROCESSING -> {
                    state = State.SENDING_IN_PROGRESS
                }
                SendToPaymailState.FAIL -> {
                    state = State.SENDING_ERROR
                }
                SendToPaymailState.SUCCESS -> {
                    state = State.SENDING_SUCCESS
                }
            }
        }


        _sendToPaymailContentLD.value = SendToPaymailModel(
            lastDestinationPaymail,
            sats,
            isCoinValid,
            state
        )
    }


    //todo
    //todo

    val paymailExistenceObserver = object : Observer<Pair<PaymailExistsHelper.State, BsvAlias?>> {
        override fun onChanged(it: Pair<PaymailExistsHelper.State, BsvAlias?>?) {
            it ?: return

            _paymailExistsLD.value = it.first

            refreshModel()
        }
    }





    fun onDestinationPaymailChanged(paymail: String) {
        val p0 = paymail.trim()
        if (p0 == lastDestinationPaymail) return

        lastDestinationPaymail = p0

        /////


        lastPaymailExistenceLD?.removeObserver(paymailExistenceObserver)

        val ld = paymailRepository.paymailExistsHelper.checkExistence(paymail)
        lastPaymailExistenceLD = ld
        ld.observeForever(paymailExistenceObserver)

    }

    fun onRequestedSend() {
        //todo
        //todo
        //todo refactor to have the relevant part of the code in a repository
        //todo


        if (State.PAYMAIL_VALID != sendToPaymailContentLD.value!!.state) {
            Timber.e("tx-sending dropped; paymail not verified valid")
            return
        }

        val receivingPaymail = sendToPaymailContentLD.value!!.input

        _paymailSendingLD.value = SendToPaymailState.PROCESSING



        coroutineUtil.appScope.launch {

            val sendingPaymail = activePaymailObserver.previousPaymail ?: let {
                Timber.e("tx-sending dropped; unexpected paymail value")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }

            val fundingWalletSeed = try {
                secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(sendingPaymail))
            } catch (e: Exception) {
                Timber.e(e, "error retrieving seed")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }

            val sendingCoin = coin ?: let {
                Timber.e("tx-sending dropped; unexpected state, `coin` is NULL")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }

            val matchingDbCoin = sendingCoin.let {
                coinsOfPaymailLD?.value?.find { dbCoin ->
                    dbCoin.address == it.address &&
                            dbCoin.txId == it.txHash &&
                            dbCoin.outputIndex == it.index
                }
            } ?: let {
                Timber.e("tx-sending dropped; unexpected state, `db-coin` is NULL")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }






            val paymailOutputScript = paymailRepository.getPaymailDestinationOutput(
                sendingPaymail,
                receivingPaymail
            ) ?: let {
                Timber.e("tx-sending dropped; error retrieving paymail destination output")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }




            val upcomingUnusedFundingWalletAddresses = unusedAddressHelper
                .getUnusedAddressesOfFundingWallet(sendingPaymail) // will be used for a change-address





            val hexOfTx = walletRepository.createSimpleOutputScriptTx(
                fundingWalletSeed,
                mapOf(matchingDbCoin.derivationPath to sendingCoin),
                paymailOutputScript,
                upcomingUnusedFundingWalletAddresses[0].address
            )








            hexOfTx ?: let {
                Timber.d("tx NOT created; hex is NULL")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }
            Timber.d("tx created; hex: $hexOfTx")

            val txId = blockchainDataSource.sendHexTx(hexOfTx)
            if (txId == null) {
                Timber.d("tx NOT sent")
                _paymailSendingLD.postValue(SendToPaymailState.FAIL)
                return@launch
            }

            Timber.d("tx broadcast result tx-id: $txId")

            // mark coin as spent by updating the db
            matchingDbCoin.copy(
                spendingTxId = txId
            ).let {
                appDatabase.coinDao().insertCoin(it)
            }

            // must add new confirmation meta-data
            appDatabase.txConfirmationDao().insertConfirmationInfo(
                TxConfirmation(
                    txId,
                    -1,
                    null,
                    -1L
                )
            )

            _paymailSendingLD.postValue(SendToPaymailState.SUCCESS)
        }
    }

    inner class ActivePaymailObserver : Observer<AuthenticatedPaymail> {
        var previousPaymail: String? = null

        override fun onChanged(it: AuthenticatedPaymail?) {
            val paymail = it?.paymail

            if (previousPaymail == paymail) return //unchanged
            previousPaymail = paymail

            coinsOfPaymailLD?.let {
                _sendToPaymailContentLD.removeSource(it)
            }
            coinsOfPaymailLD = null

            if (paymail == null) return //todo consider clearing content

            val coinsLD = appDatabase.coinDao().getFundingCoinsLD(paymail)
            coinsOfPaymailLD = coinsLD

            _sendToPaymailContentLD.addSource(coinsLD) {
                Timber.d("coins refreshed !!!!!!!") //todo
                refreshModel()
            }
        }

    }

    data class SendToPaymailModel(
        val input: String,
        val value: Int,
        val isCoinValid: Boolean,
        val state: State
    )

    enum class State {
        COIN_INVALID,
        EMPTY,
        CHECKING_PAYMAIL,
        PAYMAIL_NOT_VALID,
        PAYMAIL_VALID,
        SENDING_IN_PROGRESS,
        SENDING_SUCCESS,
        SENDING_ERROR
    }

    enum class SendToPaymailState {
        PROCESSING,
        SUCCESS,
        FAIL
    }

}