package app.bitcoin.baemail

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.work.Configuration
import androidx.work.WorkManager
import coil.Coil
import coil.ImageLoader
import app.bitcoin.baemail.di.AppComponent
import app.bitcoin.baemail.di.AppModule
import app.bitcoin.baemail.di.DaggerAppComponent
import app.bitcoin.baemail.util.OnCreatedActivityLifecycleListener
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class App : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        initTimber()
        initCoil()

        AndroidThreeTen.init(this)

        app = this

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        appComponent.inject(this)

        registerActivityLifecycleCallbacks(
            object : OnCreatedActivityLifecycleListener() {
                override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                    AndroidInjection.inject(activity)
                }
            })


        WorkManager.initialize(
            this,
            Configuration.Builder()
                .setWorkerFactory(appComponent.providerWorkerFactoryFactory())
                .build()
        )

        Timber.d("end of App#onCreate")
    }



    private fun initTimber() {
        if (!BuildConfig.DEBUG) return

        Timber.plant(Timber.DebugTree())

        Timber.d("App#initTimber")
    }

    private fun initCoil() {
        Coil.setDefaultImageLoader {
            ImageLoader(this) {
                crossfade(false)
                okHttpClient {
                    appComponent.provideCoilHttpClient().httpClient
                }
            }
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    companion object {

//        init {
//            NakasendoSDK.init()
//        }

        private lateinit var app: App

        private lateinit var appComponent: AppComponent

        fun get(): App = app

        fun getComponent(): AppComponent = appComponent
    }
}