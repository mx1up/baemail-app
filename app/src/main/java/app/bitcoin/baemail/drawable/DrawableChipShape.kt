package app.bitcoin.baemail.drawable

import android.graphics.*
import android.graphics.drawable.Drawable

class DrawableChipShape : Drawable {

    private val strokeConfig: StrokeConfig?

    private val path = Path()

    private val bgPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)

    constructor() : this(Color.WHITE, null)

    constructor(backgroundColor: Int, strokeConfig: StrokeConfig? = null): super() {
        bgPaint.style = Paint.Style.FILL
        bgPaint.color = backgroundColor

        this.strokeConfig = strokeConfig
        initStrokePaint()
    }

    private fun initStrokePaint() {
        strokeConfig.let {
            if (it == null) return@let
            strokePaint.color = it.color
            strokePaint.style = Paint.Style.STROKE
            strokePaint.strokeWidth = it.widthPx
        }
    }

    override fun onBoundsChange(bounds: Rect?) {
        if (bounds == null) return

        initPath(bounds)
    }

    private fun initPath(bounds: Rect) {
        path.reset()

        val rect = RectF(bounds)
        //var radius: Float = bounds.height() / 2f

        strokeConfig.let {
            if (it == null) return@let

            rect.left += it.widthPx / 2
            rect.top += it.widthPx / 2
            rect.right -= it.widthPx / 2
            rect.bottom -= it.widthPx / 2
        }

        val radius: Float = bounds.height() / 2f


        path.addRoundRect(rect, radius, radius, Path.Direction.CW)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawPath(path, bgPaint)
        strokeConfig.let {
            if (it == null) return@let

            canvas.drawPath(path, strokePaint)
        }
    }

    override fun setAlpha(alpha: Int) {
        //not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    data class StrokeConfig(val color: Int, val widthPx: Float)
}