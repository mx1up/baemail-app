package app.bitcoin.baemail.drawable

import android.graphics.*
import android.graphics.drawable.Drawable
import android.text.TextPaint
import timber.log.Timber
import kotlin.math.max
import kotlin.math.min

class DrawableRevealSend(
    private val dp: Float,
    private val maxWidthDp: Float,
    private val fillColor: Int,
    private val hintColor: Int,
    private val topBorderColor: Int,
    private val activatedTextColor: Int
) : Drawable() {



    val labelSwipeUp = "SWIPE UP TO SEND"
    val labelSend = "SEND"

    val p = Paint(Paint.ANTI_ALIAS_FLAG)
    val pTopBorder = Paint(Paint.ANTI_ALIAS_FLAG)
    val tpSwipeUp = TextPaint()
    val tpSend = TextPaint()
    val tpSendActivated = TextPaint()

    private var progress = 0f

    private var startX = 0f
    private var startY = 0f

    private var swipeUpLabelY = 0f //startY - (60 * dp)
    private var sendLabelY = 0f //swipeUpLabelY - (60 * dp)

    private var topYOfFill = -1


    init {
        p.color = fillColor
        pTopBorder.color = topBorderColor

        tpSwipeUp.color = hintColor
        tpSwipeUp.textSize = 30 * dp
        tpSwipeUp.textAlign = Paint.Align.CENTER


        tpSend.color = hintColor
        tpSend.textSize = 80 * dp
        tpSend.textAlign = Paint.Align.CENTER

        tpSendActivated.color = activatedTextColor
        tpSendActivated.textSize = 80 * dp
        tpSendActivated.textAlign = Paint.Align.CENTER
    }

    fun getLastProgress(): Float = progress


    //todo
    //todo
    //todo
    var isInActivateStep = false


    fun setProgress(progress: Float) {
        this.progress = progress


        val swipeUpBorderMin = 0.05f
        val swipeUpBorderMid = 0.6f
        val swipeUpBorderMax = 0.90f


        val normalizedProgress = progress //min(progress, 1f)




        val labelSwipeUpAlpha = when {
            normalizedProgress < swipeUpBorderMin -> {
                0f
            }
            normalizedProgress < swipeUpBorderMid -> {
                val deltaAboveMin = normalizedProgress - swipeUpBorderMin
                val deltaInStep = swipeUpBorderMid - swipeUpBorderMin

                deltaAboveMin / deltaInStep
            }
            normalizedProgress < swipeUpBorderMax -> {
                val deltaAboveMid = normalizedProgress - swipeUpBorderMid
                val deltaInStep = swipeUpBorderMax - swipeUpBorderMid

                (deltaInStep - deltaAboveMid) / deltaInStep
            }
            else -> {
                0f
            }
        }

        tpSwipeUp.color = Color.argb(
            (255 * labelSwipeUpAlpha).toInt(),
            Color.red(hintColor),
            Color.green(hintColor),
            Color.blue(hintColor)
        )



        val sendBorderMin = 0.70f
        val sendBorderMid = 1f
        val sendBorderMax = 1.5f
        val sendBorderActivated = 1.7f

        var isInActivateStep = false

        val percentProgressToNextStep = when {
            normalizedProgress < sendBorderMin -> {
                0f
            }
            normalizedProgress < sendBorderMid -> {
                val deltaAboveMin = normalizedProgress - sendBorderMin
                val deltaInStep = sendBorderMid - sendBorderMin

                deltaAboveMin / deltaInStep
            }
            normalizedProgress < sendBorderMax -> {
                1f
            }
            normalizedProgress < sendBorderActivated -> {
                isInActivateStep = true

                val deltaAbovePreviousBorder = normalizedProgress - sendBorderMax
                val deltaInStep = sendBorderActivated - sendBorderMax

                deltaAbovePreviousBorder / deltaInStep
            }
            else -> {
                isInActivateStep = true

                1f
            }
        }

        this.isInActivateStep = isInActivateStep

//        if (isInActivateStep) {
//            val color = activatedTextColor
//            val finalColor = Color.argb(
//                255,
//                Color.red(color) + labelSendAlpha
//            )
//            tpSend.color = activatedTextColor
//        } else {
//            tpSend.color = hintColor
//        }

        if (isInActivateStep) {
            tpSend.color = Color.argb(
                (255 * (1f - percentProgressToNextStep)).toInt(),
                Color.red(hintColor),
                Color.green(hintColor),
                Color.blue(hintColor)
            )
        } else {
            tpSend.color = Color.argb(
                (255 * percentProgressToNextStep).toInt(),
                Color.red(hintColor),
                Color.green(hintColor),
                Color.blue(hintColor)
            )
        }


        tpSendActivated.color = Color.argb(
            (255 * percentProgressToNextStep).toInt(),
            Color.red(activatedTextColor),
            Color.green(activatedTextColor),
            Color.blue(activatedTextColor)
        )



        val topBorder_borderMin = 1f
        val topBorder_borderMid = 1.1f

        val topBorderAlpha = when {
            normalizedProgress < topBorder_borderMin -> {
                0f
            }
            normalizedProgress < topBorder_borderMid -> {
                val deltaAboveMin = normalizedProgress - topBorder_borderMin
                val deltaInStep = topBorder_borderMid - topBorder_borderMin

                deltaAboveMin / deltaInStep
            }
            else -> {
                1f
            }
        }
        pTopBorder.color = Color.argb(
            (255 * topBorderAlpha).toInt(),
            Color.red(topBorderColor),
            Color.green(topBorderColor),
            Color.blue(topBorderColor)
        )


        invalidateSelf()
    }

    fun setStartLoc(x: Float, y: Float) {
        startX = x
        startY = y

        swipeUpLabelY = startY - (60 * dp)
        sendLabelY = swipeUpLabelY - (60 * dp)



        topYOfFill = (sendLabelY - tpSend.textSize - (110 * dp)).toInt()
    }


    private fun resetTextSizes() {
        tpSwipeUp.textSize = 30 * dp
        tpSend.textSize = 60 * dp
        tpSendActivated.textSize = 60 * dp
    }

    fun getDrawPositionOfSend(): Point {
        return Point(
            (bounds.right + bounds.left) / 2,
            sendLabelY.toInt()
        )
    }

    fun getSizeOfSend(): Float {
        return tpSend.textSize
    }

    override fun onBoundsChange(bounds: Rect?) {
        bounds ?: return

        Timber.d("...new bounds: $bounds")


        val maxTextWidth = bounds.width() * 0.7
        //val textWidthTolerance = maxTextWidth * 0.1

        resetTextSizes()

        for (i in 0 until 100) {
            val size = tpSwipeUp.textSize
            val textWidth = tpSwipeUp.measureText(labelSwipeUp)
            if (textWidth < maxTextWidth) {
                Timber.d("...tpSwipeUp measure loop")
                tpSwipeUp.textSize = size + 5 * dp
                continue
            }
            break
        }


        for (i in 0 until 100) {
            val size = tpSend.textSize
            val textWidth = tpSend.measureText(labelSend)
            if (textWidth < maxTextWidth) {
                Timber.d("...tpSend measure loop")
                tpSend.textSize = size + 5 * dp
                continue
            }
            break
        }

        //copy
        tpSendActivated.textSize = tpSend.textSize

        Timber.d("...end of bounds: $bounds")

    }

    override fun draw(canvas: Canvas) {

        val token = canvas.save()
        canvas.clipRect(
            bounds.left,
            topYOfFill, //bounds.top,
            bounds.right,
            bounds.bottom
        )


        //draw the background
        val radius = min(1f, progress).let {
            it * maxWidthDp
        }
        canvas.drawCircle(
            startX,
            startY,
            radius,
            p
        )


        if (progress > 1f) {
            val topOfBorder = max(bounds.top, topYOfFill).toFloat()

            canvas.drawRect(
                bounds.left.toFloat(),
                topOfBorder,
                bounds.right.toFloat(),
                (topOfBorder + 10 * dp).toFloat(),
                pTopBorder
            )
        }


//        val swipeUpLabelY = startY - (60 * dp)
//        val sendLabelY = swipeUpLabelY - (60 * dp)

        canvas.drawText(
            labelSwipeUp,
            (bounds.right + bounds.left) / 2f,
            swipeUpLabelY,
            tpSwipeUp
        )



        canvas.drawText(
            labelSend,
            (bounds.right + bounds.left) / 2f,
            sendLabelY,
            tpSend
        )


        if (isInActivateStep) {
            canvas.drawText(
                labelSend,
                (bounds.right + bounds.left) / 2f,
                sendLabelY,
                tpSendActivated
            )
        }



        canvas.restoreToCount(token)

    }

    override fun setAlpha(alpha: Int) {
        //not supported
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }


}