package app.bitcoin.baemail.drawable


import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.view.ViewOutlineProvider
import androidx.annotation.IntRange
import androidx.annotation.RequiresApi

class DrawableOval(
    private var strokeColor: Int,
    private var strokeWidth: Float,
    private var backgroundColor: Int
) : Drawable() {
    private var appliedAlpha = 255


    private var paddingLeft: Float = 0f
    private var paddingTop: Float = 0f
    private var paddingRight: Float = 0f
    private var paddingBottom: Float = 0f

    private var isFullyRounded = true
    private var notFullyRoundedCornerRadius: Float = 0f

    private val path = Path()

    private val strokePaint: Paint
    private val backgroundPaint: Paint


    constructor(context: Context) : this(
        Color.RED,
        3 * context.resources.displayMetrics.density,
        Color.BLUE
    )

    init {
        notFullyRoundedCornerRadius = strokeWidth

        val padding = 2 * strokeWidth
        paddingLeft = padding
        paddingTop = padding
        paddingRight = padding
        paddingBottom = padding

        strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        strokePaint.style = Paint.Style.STROKE

        backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        backgroundPaint.style = Paint.Style.FILL

        initPaints()
    }

    override fun onBoundsChange(bounds: Rect) {
        initPath(bounds)
    }

    override fun draw(canvas: Canvas) {
        if (appliedAlpha == 0) {
            return
        }
        if (Color.alpha(backgroundPaint.color) != 0) {
            canvas.drawPath(path, backgroundPaint)
        }
        if (strokeWidth > 0) {
            canvas.drawPath(path, strokePaint)
        }
    }

    override fun setAlpha(@IntRange(from = 0, to = 255) alpha: Int) {
        appliedAlpha = alpha

        initPaints()
        invalidateSelf()
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    private fun getPaddedRect(boundRect: Rect): RectF {
        val halfStrokeWidth = strokeWidth / 2
        val paddedRect = RectF(boundRect)
        paddedRect.left += paddingLeft + halfStrokeWidth
        paddedRect.top += paddingTop + halfStrokeWidth
        paddedRect.right -= paddingRight + halfStrokeWidth
        paddedRect.bottom -= paddingBottom + halfStrokeWidth
        return paddedRect
    }

    private fun initPath(bounds: Rect) {
        path.reset()

        val radius: Float
        if (isFullyRounded) {
            val strokeHeight = bounds.height().toFloat() - paddingTop - paddingBottom
            val strokeWidth = bounds.width().toFloat() - paddingLeft - paddingRight
            radius = Math.min(strokeHeight, strokeWidth) / 2
        } else {
            radius = notFullyRoundedCornerRadius
        }

        path.addRoundRect(getPaddedRect(bounds), radius, radius, Path.Direction.CW)
    }

    fun initPaints() {
        var finalAlpha = (appliedAlpha / 255f * (Color.alpha(strokeColor) / 255f) * 255).toInt()
        strokePaint.color = Color.argb(
            finalAlpha,
            Color.red(strokeColor),
            Color.green(strokeColor),
            Color.blue(strokeColor)
        )
        strokePaint.strokeWidth = strokeWidth

        val drawableAlphaPercent = appliedAlpha / 255f
        val bgColorAlphaPercent = Color.alpha(backgroundColor) / 255f
        val finalColorAlphaPercent = drawableAlphaPercent * bgColorAlphaPercent
        finalAlpha = (finalColorAlphaPercent * 255).toInt()

        backgroundPaint.color = Color.argb(
            finalAlpha,
            Color.red(backgroundColor),
            Color.green(backgroundColor),
            Color.blue(backgroundColor)
        )
    }

    fun setPaddings(padLeft: Float, padTop: Float, padRight: Float, padBottom: Float) {
        paddingLeft = padLeft
        paddingTop = padTop
        paddingRight = padRight
        paddingBottom = padBottom

        initPath(bounds)
        invalidateSelf()
    }

    fun setStrokeColor(strokeColor: Int) {
        this.strokeColor = strokeColor

        initPaints()
        invalidateSelf()
    }

    fun setBgColor(bgColor: Int) {
        backgroundColor = bgColor

        initPaints()
        invalidateSelf()
    }

    fun setStrokeWidth(widthPx: Float) {
        strokeWidth = widthPx

        initPaints()
        invalidateSelf()
    }

    fun setFullyRounded() {
        isFullyRounded = true
        initPath(bounds)
        invalidateSelf()
    }

    fun setCornerRadius(radiusPx: Float) {
        isFullyRounded = false
        notFullyRoundedCornerRadius = radiusPx
        initPath(bounds)
        invalidateSelf()
    }

    private var mOutlineProvider: OutlineProvider? = null

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun getOutlineProvider(): OutlineProvider {
        if (mOutlineProvider == null) {
            mOutlineProvider = OutlineProvider()
        }
        return mOutlineProvider!!
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    inner class OutlineProvider : ViewOutlineProvider() {

        override fun getOutline(view: View, outline: Outline) {
            val paddedRect = getPaddedRect(bounds)

            val cornerRadius: Float
            if (isFullyRounded) {
                cornerRadius = paddedRect.height() / 2
            } else {
                cornerRadius = notFullyRoundedCornerRadius
            }

            val halfStrokeWidthPx= (strokeWidth).toInt()

            outline.setRoundRect(
                Rect(
                    paddedRect.left.toInt() - halfStrokeWidthPx,
                    paddedRect.top.toInt() - halfStrokeWidthPx,
                    paddedRect.right.toInt() + halfStrokeWidthPx,
                    paddedRect.bottom.toInt() + halfStrokeWidthPx
                ), cornerRadius
            )
        }
    }
}