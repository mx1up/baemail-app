package app.bitcoin.baemail.drawable



import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.annotation.IntRange

class DrawableStroke : Drawable() {

    private var strokeLeft = 0
    private var strokeTop = 0
    private var strokeRight = 0
    private var strokeBottom = 0

    private var paddingLeft = 0
    private var paddingTop = 0
    private var paddingRight = 0
    private var paddingBottom = 0

    private var strokeColor = Color.RED
    private var _alpha = 255

    private val strokePaint = Paint(Paint.ANTI_ALIAS_FLAG)

    companion object {
        fun clone(drawable: DrawableStroke): DrawableStroke {
            val sd = DrawableStroke()
            sd.strokeLeft = drawable.strokeLeft
            sd.strokeTop = drawable.strokeTop
            sd.strokeRight = drawable.strokeRight
            sd.strokeBottom = drawable.strokeBottom

            sd.setStrokeColor(drawable.strokeColor)
            sd.alpha = drawable._alpha

            sd.paddingLeft = drawable.paddingLeft
            sd.paddingTop = drawable.paddingTop
            sd.paddingRight = drawable.paddingRight
            sd.paddingBottom = drawable.paddingBottom

            return sd
        }
    }

    fun setStrokeColor(color: Int) {
        strokeColor = color

        val finalAlpha = (((Color.alpha(color) / 255f) * (_alpha / 255f)) * 255).toInt()

        val finalColor = Color.argb(
            finalAlpha, Color.red(strokeColor), Color.green(strokeColor), Color.blue(strokeColor)
        )

        strokePaint.color = finalColor

        invalidateSelf()
    }

    fun setStrokeWidths(left: Int, top: Int, right: Int, bottom: Int) {
        strokeLeft = left
        strokeTop = top
        strokeRight = right
        strokeBottom = bottom

        invalidateSelf()
    }

    fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        paddingLeft = left
        paddingTop = top
        paddingRight = right
        paddingBottom = bottom
    }

    override fun draw(canvas: Canvas) {
        if (_alpha == 0) return

        // left stroke
        canvas.drawRect(
            paddingLeft.toFloat(),
            paddingTop.toFloat(),
            (paddingLeft + strokeLeft).toFloat(),
            (bounds.height() - paddingBottom).toFloat(),
            strokePaint
        )

        // top stroke
        canvas.drawRect(
            paddingLeft.toFloat(),
            paddingTop.toFloat(),
            (bounds.width() - paddingRight).toFloat(),
            (strokeTop + paddingTop).toFloat(),
            strokePaint
        )

        // right stroke
        canvas.drawRect(
            (bounds.width() - strokeRight - paddingRight).toFloat(),
            paddingTop.toFloat(),
            (bounds.width() - paddingRight).toFloat(),
            (bounds.height() - paddingBottom).toFloat(),
            strokePaint
        )

        // bottom stroke
        canvas.drawRect(
            paddingLeft.toFloat(),
            (bounds.height() - strokeBottom - paddingBottom).toFloat(),
            (bounds.width() - paddingRight).toFloat(),
            (bounds.height() - paddingBottom).toFloat(),
            strokePaint
        )
    }

    override fun setAlpha(@IntRange(from = 0, to = 255) alpha: Int) {
        _alpha = alpha

        setStrokeColor(strokeColor)
    }

    override fun getAlpha(): Int {
        return _alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        // not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}