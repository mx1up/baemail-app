package app.bitcoin.baemail.drawable

import android.graphics.*
import android.graphics.drawable.Drawable
import android.text.TextPaint
import timber.log.Timber

class DrawableStyledLabel(
    private val dp: Float,
    private var value: String,
    private var labelX: Int = 0,
    private var labelY: Int = 0,
    private var labelAlign: Paint.Align = Paint.Align.CENTER,
    private val labelTextSize: Float = 50f,
    private var labelColor: Int = Color.RED
) : Drawable() {

    private val tp = TextPaint()

    init {
        tp.color = labelColor
        tp.textSize = labelTextSize
        tp.textAlign = labelAlign
    }


    override fun onBoundsChange(bounds: Rect?) {
        bounds ?: return

        Timber.d("...new bounds: $bounds")

    }

    override fun draw(canvas: Canvas) {

        val token = canvas.save()

        canvas.drawText(
            value,
            labelX.toFloat(), //(bounds.right + bounds.left) / 2f,
            labelY.toFloat(),
            tp
        )


        canvas.restoreToCount(token)

    }

    override fun setAlpha(alpha: Int) {
        //not supported
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        //not supported
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    fun updateLabel(content: String) {
        value = content
        invalidateSelf()
    }

}