package app.bitcoin.baemail.drawable

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.*
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build
import java.util.*

class DrawableState {

    companion object {
        fun getNew(pressedColor: Int, ovalShape: Boolean = false,
                        chipShape: Boolean = false, cornerRoundnessPx: Int? = null): Drawable {
            return getNew(Color.WHITE, pressedColor, ovalShape, chipShape, cornerRoundnessPx)
        }

        fun getNew(normalColor: Int, pressedColor: Int, ovalShape: Boolean = false,
                        chipShape: Boolean = false, cornerRoundnessPx: Int? = null): Drawable {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                RippleDrawable(
                    ColorStateList.valueOf(pressedColor),
                    null,
                    getRippleMask(
                        normalColor,
                        ovalShape,
                        chipShape,
                        cornerRoundnessPx
                    )
                )
            } else {
                getStateListDrawable(
                    normalColor,
                    pressedColor
                )
            }
        }

        private fun getRippleMask(color: Int, ovalShape: Boolean, chipShape: Boolean,
                                  cornerRoundnessPx: Int? = null): Drawable {
            val r = if (ovalShape) {
                OvalShape()
            } else if (chipShape) {
                return DrawableChipShape()
            } else if (cornerRoundnessPx != null) {
                val outerRadii = FloatArray(8)
                Arrays.fill(
                    outerRadii,
                    cornerRoundnessPx.toFloat()
                )
                RoundRectShape(outerRadii, null, null)
            } else {
                //square
                RoundRectShape(null, null, null)
            }

            val shapeDrawable = ShapeDrawable(r)
            shapeDrawable.paint.color = color

            return shapeDrawable
        }

        private fun getStateListDrawable(normalColor: Int, pressedColor: Int): StateListDrawable {
            val states = StateListDrawable()
            states.addState(intArrayOf(android.R.attr.state_pressed), ColorDrawable(pressedColor))
            states.addState(intArrayOf(android.R.attr.state_focused), ColorDrawable(pressedColor))
            states.addState(intArrayOf(android.R.attr.state_activated), ColorDrawable(pressedColor))
            states.addState(intArrayOf(), ColorDrawable(normalColor))
            return states
        }
    }
}