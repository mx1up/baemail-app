package app.bitcoin.baemail.wallet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.util.CoroutineUtil
import kotlinx.coroutines.*
import timber.log.Timber

class NewBlockPollingHelper(
    val coroutineUtil: CoroutineUtil,
    val blockchainDataSource: BlockchainDataSource
) {

    private var pollingJob: Job? = null

    private val _currentBlockLD = MutableLiveData<BlockInfo>()
    val currentBlockLD: LiveData<BlockInfo>
        get() = _currentBlockLD

    private lateinit var onError: ()->Unit

    fun setup(onError: ()->Unit) {
        this.onError = onError
    }

    fun initialize(currentBlock: BlockInfo) {
        _currentBlockLD.postValue(currentBlock)

        startPolling()
    }

    fun stop() {
        pollingJob?.cancel()
        pollingJob = null
    }

    private fun startPolling() {
        pollingJob?.cancel()
        pollingJob = coroutineUtil.appScope.launch(Dispatchers.IO) {
            while (true) {
                delay(POLLING_PERIOD)
                ensureActive()

                val info = blockchainDataSource.fetchCurrentStatus()
                if (info == null) {
                    onError(RuntimeException())
                    continue
                }

                val current = currentBlockLD.value!!
                if (info.hash != current.hash) {
                    //there is a new block
                    Timber.d("new block detected")

                    _currentBlockLD.postValue(info)
                } else {
                    Timber.d("completed a check for a new block")
                }
            }

        }
    }

    private fun onError(e: Exception) {
        Timber.e(e)

        onError()
    }

    companion object {
        private val POLLING_PERIOD = 1000L * 19
    }
}