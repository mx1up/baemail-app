package app.bitcoin.baemail.wallet.handler

import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.wallet.RequestManager
import app.bitcoin.baemail.wallet.WalletRepository
import com.google.gson.JsonArray
import timber.log.Timber

/*

# expected structure of params array
[
    "keyAlias",
    "m/some/path/0",
    "message content"
]



# expected structure of result array
[
    "signature"
]


*/
class SignRequest(
    val secureDataSource: SecureDataSource,
    val walletRepository: WalletRepository
) : RequestManager.WorkHandler("SIGN_REQUEST") {

    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<RequestManager.Result, JsonArray> {

        try {


            val keyAlias = params[0].asString
            val keyPath = params[1].asString
            val messageToSign = params[2].asString

            //todo check if `keyAlias` is a constant type for `activePaymail` or `activeFundingWallet`
            //todo for rest of the keys do this

            val xpriv = secureDataSource.getCustom(keyAlias) ?: let {
                //there is no matching entry for this alias
                throw RuntimeException()
            }

            val signature = walletRepository.signMessageWithXpriv(
                xpriv,
                keyPath,
                messageToSign
            ) ?: let {
                //there was an error when signing the message
                throw RuntimeException()
            }


            val jsonArray = JsonArray()
            jsonArray.add(signature)

            return Pair(RequestManager.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(RequestManager.Result.FAIL, JsonArray())
        }

    }


}