package app.bitcoin.baemail.wallet

import android.content.Context
import androidx.core.os.TraceCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.Tr
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.wallet.handler.AddXprivHandler
import app.bitcoin.baemail.wallet.handler.SignRequest
import app.bitcoin.baemail.wallet.request.*
import com.google.gson.Gson
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.random.Random

class WalletRepository @Inject constructor(
    private val appContext: Context,
    private val requestManager: RequestManager,
    private val secureDataSource: SecureDataSource,
    private val gson: Gson
) {

    //todo check / clean up following attributes
    private val _walletsInSync = MutableLiveData<HashSet<String>>()
    val walletsInSync: LiveData<HashSet<String>>
        get() = _walletsInSync


    private val workHandlers: Map<String, RequestManager.WorkHandler> = listOf(
        AddXprivHandler(
            secureDataSource,
            this
        ),
        SignRequest(
            secureDataSource,
            this
        )
    ).let { list ->
        val asPairs = list.map { it.command to it }.toTypedArray()
        mapOf(*asPairs)
    }


    init {
        _walletsInSync.value = HashSet()

        requestManager.setup(workHandlers)
    }

    fun startForeground() {
        requestManager.startForeground()
    }

    fun stopForeground() {
        requestManager.stopForeground()
    }

    suspend fun getMoneyButtonPublicKey(seed: List<String>): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val request = GetMoneyButtonPublicKeyFromSeed(seed) { publicKey ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("getMoneyButtonPublicKey got cancelled")
                    return@GetMoneyButtonPublicKeyFromSeed
                }

                cancellableContinuation.resume(publicKey)
            }

            requestManager.request(request)
        }
    }

    suspend fun getPrivateKeyForPathFromSeed(
        path: String,
        seed: List<String>
    ): GetPrivateKeyForPathFromSeed.PrivateKeyHolder? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val request = GetPrivateKeyForPathFromSeed(path, seed) { holder ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("getPrivateKeyForPathFromSeed got cancelled")
                    return@GetPrivateKeyForPathFromSeed
                }

                cancellableContinuation.resume(holder)
            }

            requestManager.request(request)
        }
    }

    suspend fun signMessageWithXpriv(
        xpriv: String,
        path: String,
        message: String
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val request = SignMessageWithXpriv(
                xpriv,
                path,
                message
            ) { signature ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("signMessageWithXpriv got cancelled")
                    return@SignMessageWithXpriv
                }

                cancellableContinuation.resume(signature)
            }

            requestManager.request(request)
        }
    }

    suspend fun signMessage(
        message: String,
        wifPrivateKey: String
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val id = Random.nextInt()
            TraceCompat.beginAsyncSection(Tr.METHOD_SIGN_MESSAGE_WITH_PRIVATE_KEY, id)

            val request = SignMessage(message, wifPrivateKey) { signature ->

                TraceCompat.endAsyncSection(Tr.METHOD_SIGN_MESSAGE_WITH_PRIVATE_KEY, id)
                if (cancellableContinuation.isCancelled) {
                    Timber.d("signMessage got cancelled")
                    return@SignMessage
                }

                cancellableContinuation.resume(signature)
            }

            requestManager.request(request)
        }
    }

    suspend fun checkMnemonic(seed: List<String>): Boolean {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val request = CheckMnemonicIsValid(seed) { isValid ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("checkMnemonic got cancelled")
                    return@CheckMnemonicIsValid
                }

                cancellableContinuation.resume(isValid)
            }

            requestManager.request(request)
        }
    }

    suspend fun deriveNextAddressSet(seed: List<String>, setIndex: Int): List<DerivedAddress>? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val request = DeriveNextAddressSet(seed, setIndex, gson) { list ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("deriveNextAddressSet got cancelled")
                    return@DeriveNextAddressSet
                }

                cancellableContinuation.resume(list)
            }

            requestManager.request(request)
        }
    }

    suspend fun createSimpleTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoins>,
        destinationAddress: String
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val request = CreateSimpleTx(seed, spendingCoins, destinationAddress, gson) { txInHex ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("createSimpleTx got cancelled")
                    return@CreateSimpleTx
                }

                cancellableContinuation.resume(txInHex)
            }

            requestManager.request(request)
        }
    }

    suspend fun createSimpleOutputScriptTx(
        seed: List<String>,
        spendingCoins: Map<String, ReceivedCoins>,
        hexOutputScript: String,
        changeAddress: String
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->
            val request = CreateSimpleOutputScriptTx(
                seed,
                spendingCoins,
                hexOutputScript,
                changeAddress,
                gson
            ) { txInHex ->
                if (cancellableContinuation.isCancelled) {
                    Timber.d("createSimpleOutputScriptTx got cancelled")
                    return@CreateSimpleOutputScriptTx
                }

                cancellableContinuation.resume(txInHex)
            }

            requestManager.request(request)
        }
    }

    suspend fun createBaemailMessageTx(
        sendingPaymail: CreateBaemailMessageTx.SendingPaymail,
        fundingWallet: CreateBaemailMessageTx.FundingWallet,
        message: CreateBaemailMessageTx.Message
    ): CreateBaemailMessageTx.Tx? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val id = Random.nextInt()
            TraceCompat.beginAsyncSection(Tr.METHOD_ASSEMBLE_BAEMAIL_MESSAGE_TX, id)

            val request = CreateBaemailMessageTx(
                sendingPaymail,
                fundingWallet,
                message,
                gson
            ) { tx ->

                TraceCompat.endAsyncSection(Tr.METHOD_ASSEMBLE_BAEMAIL_MESSAGE_TX, id)

                if (cancellableContinuation.isCancelled) {
                    Timber.d("createBaemailMessageTx got cancelled")
                    return@CreateBaemailMessageTx
                }

                cancellableContinuation.resume(tx)
            }

            requestManager.request(request)
        }
    }

    suspend fun decryptPkiEncryptedData(
        seed: List<String>,
        pkiPath: String,
        mappedCypherText: Map<String, String>
    ): Map<String, String>? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            val id = Random.nextInt()
            TraceCompat.beginAsyncSection(Tr.METHOD_DECRYPT_PKI_ENCRYPTED_DATA, id)

            val request = DecryptPkiEncryptedData(
                seed,
                pkiPath,
                mappedCypherText,
                gson
            ) { decrypted ->

                TraceCompat.endAsyncSection(Tr.METHOD_DECRYPT_PKI_ENCRYPTED_DATA, id)

                if (cancellableContinuation.isCancelled) {
                    Timber.d("createBaemailMessageTx got cancelled")
                    return@DecryptPkiEncryptedData
                }

                cancellableContinuation.resume(decrypted)
            }

            requestManager.request(request)
        }
    }

    suspend fun superAssetDeploy(
        owner: SuperAssetDeployNft.Owner,
        funds: SuperAssetDeployNft.Funds
    ): String? {
        return suspendCancellableCoroutine { cancellableContinuation ->

            //todo
            //todo
            //todo
            //todo
            //todo
            //todo

            val request = SuperAssetDeployNft(owner, funds) { response ->

                if (cancellableContinuation.isCancelled) {
                    Timber.d("createBaemailMessageTx got cancelled")
                    return@SuperAssetDeployNft
                }

                cancellableContinuation.resume(response)



            }

            requestManager.request(request)

        }
    }
}