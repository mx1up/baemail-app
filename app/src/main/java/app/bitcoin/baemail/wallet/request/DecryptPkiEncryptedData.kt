package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    {
        seed: ['seed', 'words', ..],
        pkiPath: 'm/44'/0'/ ..'
    },
    [
        {
            txId: '1fa1..ccff',
            data: 'abc...cba'
        },
        ...
    ]
]

#response
[
    [
        {
            txId: '1fa1..ccff',
            decrypted: 'This content..been decrypted'
        },
        ...
    ]
]
*/
class DecryptPkiEncryptedData(
    val seed: List<String>,
    val pkiPath: String,
    val mappedCypherText: Map<String, String>,
    val gson: Gson,
    val callback: (Map<String, String>?)->Unit
) : RequestManager.NodeRequest("DECRYPT_PKI_ENCRYPTED_DATA") {


    override fun getReqParams(): JsonArray {

        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val secretsJson = JsonObject()
        secretsJson.add("seed", jsonSeedArray)
        secretsJson.addProperty("pkiPath", pkiPath)

        val cypherTextArray = JsonArray()
        mappedCypherText.forEach {
            val jsonItem = JsonObject()
            jsonItem.addProperty("txId", it.key)
            jsonItem.addProperty("data", it.value)

            cypherTextArray.add(jsonItem)
        }

        val containingJsonArray = JsonArray()
        containingJsonArray.add(secretsJson)
        containingJsonArray.add(cypherTextArray)

        return containingJsonArray
    }





    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(
                RuntimeException(/*responseData?.toString()*/),
                "error decrypting data"
            )
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val decryptedArrayJson = dataJson.get(0).asJsonArray

            val decryptedMap = mutableMapOf<String, String>()

            (0 until decryptedArrayJson.size()).forEach { i ->
                val jsonItem = decryptedArrayJson.get(i).asJsonObject

                val txId = jsonItem.get("txId").asString
                val decryptedMessage = jsonItem.get("decrypted").asString

                decryptedMap[txId] = decryptedMessage
            }



//            //todo
//            //todo
//            decryptedMap["dummyId3"]?.let { decryptedMessage ->
//                val contentJson = gson.fromJson(decryptedMessage, JsonObject::class.java)
//                val bodyJson = contentJson.getAsJsonObject("body")
//                val time = bodyJson.get("time").asLong
//                val blocks = bodyJson.get("blocks").asString
//                val version = bodyJson.get("version").asString
//
//                Timber.d(">>>>>> extra log... txId:dummyId3 time:$time blocks:$blocks version:$version")
//                //2020-09-18 12:18:43.343 21240-21495/app.bitcoin.baemail D/DecryptPkiEncryptedData: >>>>>> extra log... txId:dummyId3 time:1599851447830 blocks:<div>wut?</div> version:3.0.0
//            }
//            //todo
//            //todo


            callback(decryptedMap)



        } catch (e: Exception) {
            callback(null)
        }
    }

}