package app.bitcoin.baemail.wallet

import android.content.Context
import android.util.Base64
import androidx.annotation.Keep
import androidx.core.os.TraceCompat
import app.bitcoin.baemail.BuildConfig
import app.bitcoin.baemail.Tr
import app.bitcoin.baemail.net.AppApiService
import app.bitcoin.baemail.room.entity.TxConfirmation
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import kotlin.Exception
import kotlin.random.Random

class BlockchainDataSource(
    val appContext: Context,
    val apiService: AppApiService,
    val gson: Gson
) {


    suspend fun findSpendingOfCoins(coins: List<ReceivedCoins>): List<Pair<ReceivedCoins, TxConfirmation>> {

        return withContext(Dispatchers.IO) {

            val jsonCoinList = JsonArray()

            coins.forEach {
                val jsonCoin = JsonObject()

                jsonCoin.addProperty("in.e.h", it.txHash)
                jsonCoin.addProperty("in.e.a", it.address)
                jsonCoin.addProperty("in.e.i", it.index)

                jsonCoinList.add(jsonCoin)
            }




            val dollar = "$"
            val query = """
                {
                    "v": 3,
                    "q": {
                    "find": { 
                        "${dollar}or" :$jsonCoinList
                    },
                    "project": { "blk": 1, "tx.h": 1, "in.e": 1, "_id": 0 }
                    }
                }
            """.trimIndent()


            try {

                val spendingResponse = apiService.getBlock(
                    query.toRequestBody()
                ).execute()



                val spendingBody = spendingResponse.body()

                if (spendingResponse.isSuccessful && spendingBody != null) {

                    val foundSends = mutableListOf<Pair<ReceivedCoins,TxConfirmation>>()

                    spendingBody.byteStream().bufferedReader().useLines { sequence ->
                        sequence.forEach { line ->

                            Timber.d(">>>> read line: $line")


                            try {
                                val json = gson.fromJson(line, JsonObject::class.java)
                                val jsonTx = json.getAsJsonObject("tx")
                                val jsonBlk = json.getAsJsonObject("blk")

                                val jsonIn = json.getAsJsonArray("in")

                                (0 until jsonIn.size()).forEach inputLoop@{ i ->
                                    val jsonInItem = jsonIn.get(i).asJsonObject
                                    val jsonE = jsonInItem.getAsJsonObject("e")


                                    val address = jsonE.get("a").asString
                                    val txId = jsonE.get("h").asString
                                    val index = jsonE.get("i").asInt

                                    val matchingCoin = coins.find {
                                        it.address == address &&
                                                it.index == index &&
                                                it.txHash == txId
                                    } ?: return@inputLoop

                                    val confirmation = TxConfirmation(
                                        jsonTx.get("h").asString,
                                        jsonBlk.get("i").asInt,
                                        jsonBlk.get("h").asString,
                                        jsonBlk.get("t").asLong
                                    )


                                    foundSends.add(matchingCoin to confirmation)
                                }



                            } catch (e: Exception) {
                                Timber.e(e)
                                throw e
                            }



                        }
                    }

                    return@withContext foundSends

                } else {
                    Timber.d(">>>> findSpendingOfCoin confirmation not found; request failed; isSuccessful:${spendingResponse.isSuccessful} non-null body:${spendingBody != null}")
                    throw RuntimeException()
                }





            } catch (e: Exception) {
                Timber.e(e)
                return@withContext emptyList<Pair<ReceivedCoins, TxConfirmation>>()

            }
        }
    }

    /**
     * @return null if not found
     * @throws RuntimeException on request failure
     */
    // findSpendingOfCoin("1AouvfFBZtReaYPfHjTnvrUy9W4tomqk5i", "b6619c699d406f3661649f720fccc373b4a253460e8a4a9e781599b88caade2d", 0)
    suspend fun findSpendingOfCoin(address: String, receivedTxId: String, receivedOutput: Int): TxConfirmation? {
        Timber.d(">>>> findSpendingOfCoin")
        return withContext(Dispatchers.IO) {

            val query = """
                {
                  "v": 3,
                  "q": {
                    "find": { 
                      "in.e.h": "$receivedTxId",
                      "in.e.a": "$address",
                      "in.e.i": $receivedOutput
                    },
                    "project": { "blk": 1, "tx.h": 1, "out.e": 1, "_id": 0 },
                    "limit": 1,
                    "skip": 0
                  }
                }
            """.trimIndent()


            try {

                val spendingOfCoinResponse = apiService.getBlock(
                    query.toRequestBody()
                ).execute()




                val spendingOfCoinBody = spendingOfCoinResponse.body()

                if (spendingOfCoinResponse.isSuccessful && spendingOfCoinBody != null) {


                    spendingOfCoinBody.byteStream().bufferedReader().useLines { sequence ->
                        sequence.forEach { line ->

                            Timber.d(">>>> read line: $line")


                            try {
                                val json = gson.fromJson(line, JsonObject::class.java)
                                val jsonTx = json.getAsJsonObject("tx")
                                val jsonBlk = json.getAsJsonObject("blk")

                                val confirmation = TxConfirmation(
                                    jsonTx.get("h").asString,
                                    jsonBlk.get("i").asInt,
                                    jsonBlk.get("h").asString,
                                    jsonBlk.get("t").asLong
                                )
                                Timber.d(">>>> findSpendingOfCoin confirmation: $confirmation")

                                return@withContext confirmation



                            } catch (e: Exception) {
                                Timber.e(e)
                                throw e
                            }

                        }
                    }

                    Timber.d(">>>> findSpendingOfCoin confirmation not found ...")

                    return@withContext null




                } else {
                    Timber.d(">>>> findSpendingOfCoin confirmation not found; request failed; isSuccessful:${spendingOfCoinResponse.isSuccessful} non-null body:${spendingOfCoinBody != null}")
                    throw RuntimeException()
                }

            } catch (e: Exception) {
                Timber.e(e, ">>>> findSpendingOfCoin exception")
                return@withContext null
            }
        }
    }

    /**
     * @return NULL on error; list when query succeeds
     */
    suspend fun findAllCoinsForAddress(addressList: List<String>): List<ConfirmedReceivedCoins>? {
        return withContext(Dispatchers.IO) {
            try {
                val limit = 20
                var skip = 0

                val list = mutableListOf<ConfirmedReceivedCoins>()

                while (true) {

                    if (skip > 1000) {
                        //todo
                        //todo
                        //todo
                        //unexpected number of coins ... something is wrong ... exit
                        return@withContext null
                    }
                    val found = findCoinsForAddress(addressList, limit, skip)

                    if (found == null) {
                        //there was an error when querying for coins
                        return@withContext null
                    }

                    if (found.isEmpty()) {
                        //no more results... return found
                        break
                    }

                    list.addAll(found)

                    skip += limit
                }


                return@withContext list

            } catch (e: Exception) {
                Timber.e(e)
                return@withContext null
            }
        }
    }


    private suspend fun findCoinsForAddress(
        addressList: List<String>,
        limit: Int,
        skip: Int
    ): List<ConfirmedReceivedCoins>? {
        return withContext(Dispatchers.IO) {


            val addressJson = JsonArray()
            addressList.forEach {
                addressJson.add(it)
            }


            val arrString = addressJson.toString()

            val query = """
                {
                  "v": 3,
                  "q": {
                    "find": { "out.e.a" : { "${"$"}in": $arrString }},
                    "limit": $limit,
                    "skip": $skip
                  }
                }
            """.trimIndent()
            //"project": { "blk": 1, "tx.h": 1, "out.e": 1, "_id": 0 },

            try {
                val receivedCoinsResponse = apiService.getBlock(
                    query.toRequestBody()
                ).execute()

                val receivedCoinsBody = receivedCoinsResponse.body()

                return@withContext if (receivedCoinsResponse.isSuccessful && receivedCoinsBody != null) {

                    val parsedList = mutableListOf<ConfirmedReceivedCoins>()

                    receivedCoinsBody.byteStream().bufferedReader().useLines { sequence ->
                        sequence.forEach { line ->

                            Timber.d(">>>> read line: $line")

                            try {
                                val json = gson.fromJson(line, JsonObject::class.java)

                                val jsonTx = json.getAsJsonObject("tx")
                                val jsonOut = json.getAsJsonArray("out")
                                val jsonBlk = json.getAsJsonObject("blk")


                                (0 until jsonOut.size()).forEach outputLoop@{ i ->
                                    val outE = jsonOut.get(i).asJsonObject.getAsJsonObject("e")
                                        ?: return@outputLoop


                                    val address = try {
                                        outE.get("a").asString


                                    } catch (e: Exception) {
                                        Timber.e(e, "received coin: $outE")
                                        null
                                    }

                                    if (!addressList.contains(address)) return@outputLoop

                                    val coin = ReceivedCoins(
                                        jsonTx.get("h").asString,
                                        outE.get("v").asInt,
                                        outE.get("i").asInt,
                                        address!!
                                    )

                                    val confirmedCoin = ConfirmedReceivedCoins(
                                        coin,
                                        jsonBlk.get("i").asInt,
                                        jsonBlk.get("h").asString,
                                        jsonBlk.get("t").asLong
                                    )

                                    parsedList.add(confirmedCoin)

                                }


                            } catch (e: Exception) {
                                Timber.e(e, "error processing tx")
                            }

                        }
                    }


                    Timber.d(">>>>> parsed: $parsedList")



                    parsedList

                } else {
                    Timber.d("request failed; isSuccessful:${receivedCoinsResponse.isSuccessful} non-null body:${receivedCoinsBody != null}")
                    return@withContext null

                }



            } catch (e: Exception) {
                Timber.e(e)
                return@withContext null
            }
        }
    }

    suspend fun getStatusOfTxs(txIds: List<String>): List<TxConfirmation>? = withContext(Dispatchers.IO) {

        val txIdJson = JsonArray()
        txIds.forEach {
            txIdJson.add(it)
        }

        val arrString = txIdJson.toString()

        val query = """
                {
                  "v": 3,
                  "q": {
                    "find": { "tx.h" : { "${"$"}in": $arrString }}
                  },
                  "r": {
                    "f": "[ .[] | {tx_id: .tx.h, block_height: .blk.i, block_time: .blk.t, block_hash: .blk.h} ]"
                  }
                }
            """.trimIndent()
        val encodedQuery = Base64.encodeToString(query.toByteArray(), Base64.DEFAULT)

        try {
            val finalQuery = "https://genesis.bitdb.network/q/1FnauZ9aUH2Bex6JzdcV4eNX7oLSSEbxtN/$encodedQuery" //todo ??diff domain?? bitgraph.network
            Timber.d("query: $finalQuery")
            val receivedCoinsResponse = apiService.fetchWithKey(
                finalQuery,
                BuildConfig.BITDB_KEY
            ).execute()

            val receivedCoinsBody = receivedCoinsResponse.body()

            return@withContext if (receivedCoinsResponse.isSuccessful && receivedCoinsBody != null) {

                val jsonString = String(receivedCoinsBody.bytes())
                val json = gson.fromJson(jsonString, JsonObject::class.java)

                //val jsonOfU = json.getAsJsonArray("u") this is irrelevant
                val jsonOfC = json.getAsJsonArray("c")

                val parsedList = mutableListOf<TxConfirmation>()

                jsonOfC.forEach {
                    val txId = it.asJsonObject.get("tx_id").asString
                    val blockTime = it.asJsonObject.get("block_time").asLong
                    val blockHeight = it.asJsonObject.get("block_height").asInt
                    val blockHash = it.asJsonObject.get("block_hash").asString

                    val txConfirmation = TxConfirmation(txId, blockHeight, blockHash, blockTime)
                    parsedList.add(txConfirmation)
                }

                parsedList
            } else {
                Timber.d("request failed; isSuccessful:${receivedCoinsResponse.isSuccessful} non-null body:${receivedCoinsBody != null}")
                return@withContext null

            }

        } catch (e: Exception) {
            Timber.e(e)
            return@withContext null
        }
    }

    /**
     * @return null if not found
     */
    suspend fun fetchBlockInfoForHeight(height: Int): BlockInfo? = withContext(Dispatchers.IO) {

        try {
            val url = "https://api.whatsonchain.com/v1/bsv/main/block/height/$height"


            val blockInfoResponse = apiService.fetch(
                url
            ).execute()

            val body = blockInfoResponse.body()


            if (blockInfoResponse.isSuccessful && body != null) {
                val jsonString = String(body.bytes())
                val json = gson.fromJson(jsonString, JsonObject::class.java)

                val blockHash = json.get("hash").asString
                val blockTime = json.get("time").asLong
                val blockMerkleRoot = json.get("merkleroot").asString
                val previousBlockHash = json.get("previousblockhash").asString

                return@withContext BlockInfo(
                    height,
                    blockTime,
                    blockHash,
                    blockMerkleRoot,
                    previousBlockHash
                )
            } else {
                return@withContext null
            }

        } catch (e: Exception) {
            Timber.d(e)

            return@withContext null
        }

    }


    suspend fun fetchExchangeRate(): ExchangeRate? = withContext(Dispatchers.IO) {
        try {
            val url = "https://api.whatsonchain.com/v1/bsv/main/exchangerate"


            val blockInfoResponse = apiService.fetch(
                url
            ).execute()

            val body = blockInfoResponse.body()


            if (blockInfoResponse.isSuccessful && body != null) {
                val jsonString = String(body.bytes())
                val json = gson.fromJson(jsonString, JsonObject::class.java)

                val currency = json.get("currency").asString
                val rate = json.get("rate").asDouble

                return@withContext ExchangeRate(
                    currency,
                    rate,
                    System.currentTimeMillis()
                )
            } else {
                return@withContext null
            }
        } catch (e: Exception) {
            Timber.e(e)
            return@withContext null
        }
    }



    suspend fun fetchCurrentStatus(): BlockInfo? {
        try {

            val blockStatusResponse = apiService.fetchWithKey(
                "https://txo.bitbus.network/status",
                BuildConfig.BITDB_KEY
            ).execute()

            val blockStatusBody = blockStatusResponse.body()


            if (blockStatusResponse.isSuccessful && blockStatusBody != null) {
                val jsonString = String(blockStatusBody.bytes())
                val json = gson.fromJson(jsonString, JsonObject::class.java)


                val blockHash = json.get("hash").asString
                val blockMerkleRoot = json.get("merkleRoot").asString
                val blockHeight = json.get("height").asInt
                val blockTime = json.get("time").asLong
                val prevBlockHash = json.get("prevHash").asString

                return BlockInfo(
                    blockHeight,
                    blockTime,
                    blockHash,
                    blockMerkleRoot,
                    prevBlockHash
                )

            } else {
                Timber.d("request failed; isSuccessful:${blockStatusResponse.isSuccessful} non-null body:${blockStatusBody != null}")
                return null
            }


        } catch (e: Exception) {
            Timber.e(e)

            return null
        }
    }




    suspend fun sendHexTx(hexTx: String): String? {
        val id = Random.nextInt()
        try {
            TraceCompat.beginAsyncSection(Tr.METHOD_SEND_TX_TO_MINER, id)

            val requestContent = JsonObject()
            requestContent.addProperty("txhex", hexTx)

            val requestBody = requestContent.toString().toRequestBody()
            val response = apiService.postHexTx(requestBody).execute()
            val body = response.body()

            if (response.isSuccessful && body != null) {
                var txId: String? = null
                body.byteStream().bufferedReader().use { reader ->
                    while (true) {
                        val line = reader.readLine() ?: break
                        txId = line
                        Timber.d("body-line: $line")
                    }
                }
                return txId
            }

            return null
        } finally {
            TraceCompat.endAsyncSection(Tr.METHOD_SEND_TX_TO_MINER, id)
        }
    }

}

@Keep
data class ReceivedCoins(
    @SerializedName("tx_id") val txHash: String,
    @SerializedName("sats") val sats: Int,
    @SerializedName("index") val index: Int,
    @SerializedName("address") val address: String
)

data class ConfirmedReceivedCoins(
    val coins: ReceivedCoins,
    val blockHeight: Int,
    val blockHash: String?,
    val blockTime: Long
)

data class BlockInfo (
    val height: Int,
    val time: Long,
    val hash: String,
    val merkleRoot: String,
    val previousBlockHash: String
)

data class ExchangeRate(
    val currency: String,
    val rate: Double,
    val millisCreated: Long
) {
    val satsInOneUnit: Int = (100_000_000 / rate).toInt()
    val satsInACent: Int = satsInOneUnit / 100
}