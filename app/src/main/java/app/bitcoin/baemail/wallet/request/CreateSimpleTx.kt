package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.ReceivedCoins
import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.Exception

class CreateSimpleTx(
    val seed: List<String>,
    val spendingCoins: Map<String, ReceivedCoins>, //key: derivation-path value: utxo
    val destinationAddress: String,
    val gson: Gson,
    val callback: (String?)->Unit
) : RequestManager.NodeRequest("CREATE_SIMPLE_TX") {

    override fun getReqParams(): JsonArray {
        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val jsonSpendingCoinsArray = JsonArray()
        spendingCoins.forEach {
            val jsonObject = gson.toJsonTree(it.value).asJsonObject
            jsonObject.addProperty("derivation_path", it.key)
            jsonSpendingCoinsArray.add(jsonObject)
        }





        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(jsonSeedArray)
        wrappingJsonArray.add(jsonSpendingCoinsArray)
        wrappingJsonArray.add(destinationAddress)



        return wrappingJsonArray
    }

    /*
    expecting response
    [{"hexTx":"010000000136ffffccb285df739f397c40df0b842c57d7ae499a9 .. 0387ffffff017662000000000083fd89d8581e603180bed13b6088ac00000000"}]
     */
    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/),
                "error deriving requested set of addresses")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val hexTx = envelopeJson.get("hexTx").asString

            callback(hexTx)

        } catch (e: Exception) {
            callback(null)
        }
    }


}