package app.bitcoin.baemail.wallet

import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import app.bitcoin.baemail.wallet.request.DerivedAddress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class FullSyncHelper(
    val walletRepository: WalletRepository,
    val blockchainDataSource: BlockchainDataSource,
    val appDatabase: AppDatabase
) {

    lateinit var paymail: String
        private set

    private lateinit var seed: List<String>
    private var recentOnly = false

    fun setup(
        paymail: String,
        seed: List<String>
    ) {
        this.paymail = paymail
        this.seed = seed
    }






    suspend fun syncFromStart(): Boolean {
        recentOnly = false

        return doSync(0, 2)
    }

    suspend fun syncFromCurrentState(recentOnly: Boolean): Boolean {
        this.recentOnly = false //recentOnly //todo fix this when it is implemented

        val startIndex = 0 //todo !!! calc this
        return doSync(startIndex, 2)
    }







    private fun onStarted() {
        Timber.d("onStarted paymail:$paymail")
    }

    private fun onStopped() {
        Timber.d("onStopped paymail:$paymail")
    }

    private fun onError(e: Exception) {
        Timber.d(e, "onError")
    }

    private fun onCompleted() {
        Timber.d("onCompleted paymail:$paymail")
    }







    private suspend fun doSync(startIndex: Int, additionalIndexes: Int): Boolean = withContext(Dispatchers.IO) {
        try {
            onStarted()

            var addressSetIndex = startIndex
            var unusedAddressCount = 0

            outer@ while (true) {

                Timber.d("next set: $addressSetIndex")
                val addressList = walletRepository.deriveNextAddressSet(seed, addressSetIndex)
                Timber.d("derived addresses")
                if (addressList == null) {
                    throw RuntimeException()
                }

                Timber.d("starting with next set of addresses; index:$addressSetIndex")


                val hadIncomingCoins = syncAddress(addressList)

                if (!hadIncomingCoins) {
                    Timber.d("unused address ... addressSetIndex:$addressSetIndex")
                    unusedAddressCount++
                } else {
                    unusedAddressCount = 0
                }

                if (unusedAddressCount > additionalIndexes) {
                    Timber.d("...consecutive unused addresses hit; stopping crawl through addresses")
                    break@outer
                }

                addressSetIndex++
            }



            onCompleted()
            return@withContext true


        } catch (e: Exception) {
            onError(e)
            return@withContext false

        } finally {
            onStopped()
        }
    }







    /**
     * @return TRUE if this address had incoming coins
     */
    private suspend fun syncAddress(addressList: List<DerivedAddress>): Boolean {
        val unspentCoins = mutableListOf<Coin>()

        val simpleList = addressList.map { address ->
            address.address
        }

        val receivedCoins = findAllCoinsForAddress(simpleList)
            ?: throw RuntimeException("sync error")


        val coinsToCheckForSpends = mutableListOf<Pair<ReceivedCoins, Coin>>()

        receivedCoins.forEach { confirmedCoin ->
            val coin = confirmedCoin.coins
            var matchingDbCoin = appDatabase.coinDao().getCoin(coin.address, coin.txHash, coin.index)

            if (matchingDbCoin == null) {
                //write coin to db
                val matchingAddress = addressList.find {
                    it.address == coin.address
                }!!

                matchingDbCoin = Coin(
                    paymail,
                    matchingAddress.path,
                    DerivedAddress.getIndexOfDerivationPath(matchingAddress.path),
                    matchingAddress.address,
                    coin.txHash,
                    coin.index,
                    coin.sats,
                    null
                )
                appDatabase.coinDao().insertCoin(matchingDbCoin)
            }

            // must update the tracking of tx-confirmations
            appDatabase.txConfirmationDao().getByTxHash(coin.txHash).let { txConfirmation ->
                val newConfirmation = TxConfirmation(
                    coin.txHash,
                    confirmedCoin.blockHeight,
                    confirmedCoin.blockHash,
                    confirmedCoin.blockTime
                )

                if (txConfirmation == null) {
                    // adding new confirmation meta-data
                    appDatabase.txConfirmationDao().insertConfirmationInfo(newConfirmation)
                } else {

                    if (txConfirmation.blockHash == confirmedCoin.blockHash &&
                        txConfirmation.blockHeight == confirmedCoin.blockHeight) {
                        // confirmation meta-data is unchanged
                        return@let
                    }

                    Timber.d("tx-confirmation updated; $newConfirmation")
                    appDatabase.txConfirmationDao().insertConfirmationInfo(newConfirmation)
                }
            }

            if (matchingDbCoin.spendingTxId != null) {
                //this coin requires no additional sync-steps
                Timber.d("this tx is already spent: $matchingDbCoin")

                //todo
                //todo
                //todo
                //todo what about the confirmation of the spending-tx ??????
                //todo ... the code currently assumes that spending-txs got confirmations refreshed before this gets executed
                //todo
                //todo

                return@forEach
            }



            //buffer items for querying for spends
            coinsToCheckForSpends.add(coin to matchingDbCoin)


        }

        val didFindCoins = receivedCoins.isNotEmpty()



        //query for the spends of received coins
        if (coinsToCheckForSpends.size == 0) return didFindCoins




        //todo better impl required
        //todo
        //todo
        //todo
        //todo
        if (coinsToCheckForSpends.size > 200) throw RuntimeException()





        val spendingTxList = coinsToCheckForSpends.map {
            it.first
        }.let {
            blockchainDataSource.findSpendingOfCoins(it)
        }

        if (spendingTxList.isEmpty()) return didFindCoins

        //update database

        spendingTxList.forEach { spend ->
            Timber.d("coin-spend found: $spend")

            val matchingDbCoin = coinsToCheckForSpends.find { pair ->
                pair.first == spend.first
            }?.second ?: throw RuntimeException()


            // update the coin with info on spending
            appDatabase.coinDao().insertCoin(
                matchingDbCoin.copy(spendingTxId = spend.second.txHash)
            )

            // adding new confirmation meta-data
            appDatabase.txConfirmationDao().insertConfirmationInfo(spend.second)
        }




        Timber.d("received:$receivedCoins addressList:$addressList")

        return didFindCoins

    }


    suspend fun findAllCoinsForAddress(addressList: List<String>): List<ConfirmedReceivedCoins>? {
        return if (recentOnly) {
            TODO()
            //todo
            //todo
            //todo
            //todo


        } else {
            blockchainDataSource.findAllCoinsForAddress(addressList)
        }
    }

}