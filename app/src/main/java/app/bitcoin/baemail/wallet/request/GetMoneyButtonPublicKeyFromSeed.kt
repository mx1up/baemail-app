package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException

class GetMoneyButtonPublicKeyFromSeed(
    val seed: List<String>,
    val callback: (String?)->Unit
) : RequestManager.NodeRequest("MONEY_BUTTON_PUBLIC_KEY_FROM_SEED") {


    override fun getReqParams(): JsonArray {
        val array = JsonArray()
        seed.forEach {
            array.add(it)
        }
        return array
    }

    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(responseData?.toString()), "public key not received")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(RuntimeException(), "unexpected response format")
            callback(null)
            return
        }

        try {
            callback(data.asString)
        } catch (e: Exception) {
            Timber.e(e, "not expected formatting")
            callback(null)
        }
    }

}