package app.bitcoin.baemail.wallet

import android.content.Context
import android.util.SparseArray
import androidx.core.os.TraceCompat
import androidx.core.util.isEmpty
import app.bitcoin.baemail.Tr
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.request.DerivedAddress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.random.Random

class UnusedAddressHelper(
    private val appContext: Context,
    private val secureDataSource: SecureDataSource,
    private val walletRepository: WalletRepository,
    private val appDatabase: AppDatabase,
    private val appStateLD: AppStateLiveData,
    private val coroutineUtil: CoroutineUtil
) {

    private val cache = HashMap<String, AddressStash>()

    init {
        appStateLD.observeForever { state ->
            //todo ensure that the cache for the active-paymail is warmed up

            state.activePaymail?.let {
                coroutineUtil.appScope.launch(Dispatchers.IO) {
                    getMoreAddresses(it)
                }
            }
        }


        //todo observe syncing progress & remove addresses that turn out to be used from the sync


    }

    private suspend fun getMoreAddresses(paymail: String) {
        Timber.d("#getMoreAddresses: $paymail")

        var highestKey = -1

        //has cache entries? find next for those
        cache[paymail]?.let { stash ->
            if (stash.nextAddresses.isEmpty()) return@let //todo

            highestKey = stash.nextAddresses.keyAt(stash.nextAddresses.size() - 1)

            Timber.d("highest key of cached values: $highestKey; paymail: $paymail")


        } ?: let {
            //find the derivation index of the lowest unspent funding coins

            //todo crash if the wallet is syncing right now
            val isPaymailInSync = walletRepository.walletsInSync.value?.contains(paymail) == true
            if (isPaymailInSync) throw RuntimeException()
            //todo
            //todo
            //todo

            val unspentCoins = appDatabase.coinDao().getUnspentFundingCoins(paymail)


            if (unspentCoins.isNotEmpty()) {
                val highestCoin = unspentCoins.reduce { acc, coin ->
                    if (acc.pathIndex > coin.pathIndex) {
                        acc
                    } else {
                        coin
                    }
                }

                highestKey = DerivedAddress.getIndexOfDerivationPath(highestCoin.derivationPath)

                Timber.d("highest key of unspent coin addresses: $highestKey; paymail: $paymail")

            } else  {
                val lastSpentCoin = appDatabase.coinDao().getHighestSpentFundingCoin(paymail)
                lastSpentCoin?.let {
                    highestKey = it.pathIndex
                } ?: let {
                    highestKey = 0
                }

                Timber.d("highest key of spent coin addresses: $highestKey; paymail:$paymail")
            }
        }


        val setIndexOfHighest = highestKey / 20
        val indexModuloOfHighest = highestKey % 20

        Timber.d("getMoreAddresses of-highest index:$setIndexOfHighest modulo:$indexModuloOfHighest")

        var indexToRetrieve = setIndexOfHighest
//        var indexToRetrieve = if (setIndexOfHighest == 0 && indexModuloOfHighest == 0) {
//            0
//        } else  {
//            setIndexOfHighest + 1
//        }

        //keep fetching next sets & filtering out used coins until the stash is big enough
        for (i in 0 until 10) {

            //ensure there is a cache entry
            val stash = cache[paymail] ?: let {
                val s = AddressStash(paymail, SparseArray<DerivedAddress>())
                cache[paymail] = s
                s
            }


            if (stash.nextAddresses.size() > CACHED_COUNT) {
                Timber.d("next address cache size is big enough: ${stash.nextAddresses.size()}")
                break
            }


            val fundingWalletSeed = secureDataSource
                .getParallelWalletSeed(AuthenticatedPaymail(paymail))

            val nextAddresses = walletRepository
                .deriveNextAddressSet(fundingWalletSeed, indexToRetrieve)
                ?: throw RuntimeException()

            indexToRetrieve++

            val unusedNextAddresses = mutableListOf<DerivedAddress>()
            nextAddresses.forEach {
                if (appDatabase.coinDao().getCoinsForAddress(it.address).isEmpty()) {
                    unusedNextAddresses.add(it)
                } else {
                    Timber.d("derived address is found to be already used")
                }
            }

            //add the found addresses to the cache
            unusedNextAddresses.forEach {
                stash.nextAddresses.put(it.getIndex(), it)
            }



            Timber.d("next address cache size increased to ${stash.nextAddresses.size()}; next loop")
        }
    }

    suspend fun onAddressUsedInTx(paymail: String, addressList: List<DerivedAddress>) {
        //remove the used addresses from the `unused address cache`
        cache[paymail]?.let { stash ->
            addressList.forEach {
                stash.nextAddresses.remove(it.getIndex())
            }
        }

        //force a refresh of this helper
        getUnusedAddressesOfFundingWallet(paymail)

    }

    suspend fun getUnusedAddressesOfFundingWallet(paymail: String): List<DerivedAddress> {
        val id = Random.nextInt()
        try {
            TraceCompat.beginAsyncSection(Tr.METHOD_GET_UNUSED_ADDRESSES_OF_FUNDING_WALLET, id)

            cache[paymail]?.let { stash ->
                if (stash.nextAddresses.size() > CACHED_COUNT) {
                    val list = mutableListOf<DerivedAddress>()

                    for (i in 0 until stash.nextAddresses.size()) {
                        val address = stash.nextAddresses.valueAt(i)
                        list.add(address)
                    }

                    return list
                }
            }

            //warm up the cache
            getMoreAddresses(paymail)

            //return the now warmed-up list of addresses
            cache[paymail]?.let { stash ->
                if (stash.nextAddresses.size() <= CACHED_COUNT) {
                    throw RuntimeException()
                }

                val list = mutableListOf<DerivedAddress>()

                for (i in 0 until stash.nextAddresses.size()) {
                    val address = stash.nextAddresses.valueAt(i)
                    list.add(address)
                }

                return list

            } ?: let {
                throw RuntimeException()
            }
        } finally {
            TraceCompat.endAsyncSection(Tr.METHOD_GET_UNUSED_ADDRESSES_OF_FUNDING_WALLET, id)
        }
    }

    suspend fun deriveAddresses(paymail: String, fromIndex: Int, toIndex: Int): List<DerivedAddress> {
        val fundingWalletSeed = secureDataSource
            .getParallelWalletSeed(AuthenticatedPaymail(paymail))

        val finalList = mutableListOf<DerivedAddress>()

        val sizeOfSet = 20
        val startSetIndex = fromIndex / sizeOfSet

        var topSetIndex = startSetIndex + (1 + (toIndex - fromIndex) / sizeOfSet)

        var currentSetIndex = startSetIndex
        while (currentSetIndex < topSetIndex) {
            val nextAddresses = walletRepository
                .deriveNextAddressSet(fundingWalletSeed, currentSetIndex)
                ?: throw RuntimeException()

            finalList.addAll(nextAddresses)

            currentSetIndex++
        }

        return finalList.mapNotNull {
            val i = DerivedAddress.getIndexOfDerivationPath(it.path)
            if (i < fromIndex || i >= toIndex) {
                return@mapNotNull null
            }
            it
        }
    }

    suspend fun invalidateAndRefresh(paymail: String) {
        cache.remove(paymail)

        //warm up the cache
        getMoreAddresses(paymail)

    }

    data class AddressStash(
        val paymail: String,
        val nextAddresses: SparseArray<DerivedAddress>
    )

    companion object {
        const val CACHED_COUNT = 10
    }
}