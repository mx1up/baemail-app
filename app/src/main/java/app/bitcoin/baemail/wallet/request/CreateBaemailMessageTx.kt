package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.ReceivedCoins
import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    {
        sendingPaymail: {
            paymail: 'aa@moneybutton.com',
            seed: ['seed', 'words', ..],
            pkiPath: 'm/44'/0'/ ..'
        },
        fundingWallet: {
            seed: ['seed', 'words', ..],
            rootPath: 'm/44'/0'/ ..',
            coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
            changeAddress: '1abcedf..'
        },
        message: {
            destination: 'receiver@moneybutton.com',
            destinationPkiPublicKey: '00abc',
            subject: 'subject01',
            content: 'content 01',
            amountToPaymailUsd: 0.123,
            amountToPaymailSats: 2345,
            payToPaymailOutputScript: '0a010203..',
            amountToBaemailSats: 2345,
            amountToAppSats: 2345
        }
    }
]

# response
[
    {
        hexTx: '1f1f1f1f1f',
        spentCoinIds: ['aa..00', 'aa..11', 'aa..22', ..]
    }
]
*/
class CreateBaemailMessageTx(
    val sendingPaymail: SendingPaymail,
    val fundingWallet: FundingWallet,
    val message: Message,
    val gson: Gson,
    val callback: (Tx?)->Unit
) : RequestManager.NodeRequest("CREATE_BAEMAIL_MESSAGE_TX") {

    override fun getReqParams(): JsonArray {
        //sendingPaymail
        val jsonSendingPaymail = JsonObject().also {

            it.addProperty("paymail", sendingPaymail.paymail)
            it.addProperty("pkiPath", sendingPaymail.pkiPath) // .. mb pki path: "m/44'/0'/0'/0/0/0"

            val jsonSeedArray = JsonArray()
            sendingPaymail.seed.forEach {
                jsonSeedArray.add(it)
            }
            it.add("seed", jsonSeedArray)
        }

        val jsonFundingWallet = JsonObject().also {

            val jsonSeedArray = JsonArray()
            fundingWallet.seed.forEach {
                jsonSeedArray.add(it)
            }
            it.add("seed", jsonSeedArray)

            it.addProperty("rootPath", fundingWallet.rootPath)
            it.addProperty("changeAddress", fundingWallet.changeAddress)

            val jsonSpendingCoinsArray = JsonArray()
            fundingWallet.coins.forEach {
                val jsonObject = gson.toJsonTree(it.value).asJsonObject
                jsonObject.addProperty("derivation_path", it.key)
                jsonSpendingCoinsArray.add(jsonObject)
            }

            it.add("coins", jsonSpendingCoinsArray)
        }

        val jsonMessage = JsonObject().also {
            it.addProperty("destination", message.destination)
            it.addProperty("destinationPkiPublicKey", message.destinationPkiPublicKey)
            it.addProperty("subject", message.subject)
            it.addProperty("content", message.content)
            it.addProperty("amountToPaymailUsd", message.amountToPaymailUsd)
            it.addProperty("amountToPaymailSats", message.amountToPaymailSats)
            it.addProperty("payToPaymailOutputScript", message.payToPaymailOutputScript)
            it.addProperty("amountToBaemailSats", message.amountToBaemailSats)
            it.addProperty("amountToAppSats", message.amountToAppSats)

            if (message.thread != null) {
                it.addProperty("thread", message.thread)
            }
        }


        val finalJson = JsonObject().also {
            it.add("sendingPaymail", jsonSendingPaymail)
            it.add("fundingWallet", jsonFundingWallet)
            it.add("message", jsonMessage)
        }




        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(finalJson)

        return wrappingJsonArray
    }


    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/),
                "error creating baemail-message tx")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val hexTx = envelopeJson.get("hexTx").asString
            val spentCoinIdList = envelopeJson.getAsJsonArray("spentCoinIds").map { it.asString }

            callback(Tx(
                hexTx,
                spentCoinIdList
            ))

        } catch (e: Exception) {
            callback(null)
        }
    }







    data class SendingPaymail(
        val paymail: String,
        val seed: List<String>,
        val pkiPath: String
    )

    data class FundingWallet(
        val seed: List<String>,
        val rootPath: String,
        val coins: Map<String, ReceivedCoins>, //key: derivation-path value: utxo
        val changeAddress: String
    )

    data class Message(
        val thread: String?,
        val destination: String,
        val destinationPkiPublicKey: String,
        val subject: String,
        val content: String,
        val amountToPaymailUsd: Float,
        val amountToPaymailSats: Int,
        val payToPaymailOutputScript: String,
        val amountToBaemailSats: Int,
        val amountToAppSats: Int
    )

    data class Tx(
        val hexTx: String,
        val spentCoinIdList: List<String>
    )

}