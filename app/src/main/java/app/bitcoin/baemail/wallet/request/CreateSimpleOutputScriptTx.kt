package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.ReceivedCoins
import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.Exception

/*
#expecting params
[
    ['seed', 'words', ..],
    [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}]
    '76a914e2a623699e81b291c0327f408fea765d534baa2a88ac', //hex-string output script
    '1FFF..CC' //change address
]

# response
[
    {'hexTx': '1f1f1f1f1f'}
]
*/

class CreateSimpleOutputScriptTx(
    val seed: List<String>,
    val spendingCoins: Map<String, ReceivedCoins>, //key: derivation-path value: utxo
    val hexOutputScript: String,
    val changeAddress: String,
    val gson: Gson,
    val callback: (String?)->Unit
) : RequestManager.NodeRequest("CREATE_SIMPLE_OUTPUT_SCRIPT_TX"){

    override fun getReqParams(): JsonArray {
        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val jsonSpendingCoinsArray = JsonArray()
        spendingCoins.forEach {
            val jsonObject = gson.toJsonTree(it.value).asJsonObject
            jsonObject.addProperty("derivation_path", it.key)
            jsonSpendingCoinsArray.add(jsonObject)
        }





        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(jsonSeedArray)
        wrappingJsonArray.add(jsonSpendingCoinsArray)
        wrappingJsonArray.add(hexOutputScript)
        wrappingJsonArray.add(changeAddress)



        return wrappingJsonArray
    }


    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/),
                "error deriving requested set of addresses")
            callback(null)
            return
        }

        try {
            val dataJson = responseData ?: throw RuntimeException("data missing")

            val envelopeJson = dataJson.get(0).asJsonObject
            val hexTx = envelopeJson.get("hexTx").asString

            callback(hexTx)

        } catch (e: Exception) {
            callback(null)
        }
    }
}