package app.bitcoin.baemail.wallet

import android.content.Context
import app.bitcoin.baemail.net.AppApiService
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.util.CoroutineUtil
import com.google.gson.Gson
import javax.inject.Inject

class FundingWalletRepository @Inject constructor(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val appStateLD: AppStateLiveData,
    private val apiService: AppApiService,
    private val gson: Gson,
    private val walletRepository: WalletRepository,
    private val secureDataSource: SecureDataSource
) {



    init {
        //todo maybe expecting some UTXO's to be there? ....confirm
        //todo
        //todo don't have any UTXOs and have scanned through the derivation paths?
        //todo .. check if there are new UTXO's in the active derived-address-set
        //todo
        //todo find the active derived-address-set
        //todo
        //todo
    }

    fun onActivePaymailChanged(paymail: String) {
        //todo refresh current RAM-state
    }




}