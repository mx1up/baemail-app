package app.bitcoin.baemail.wallet

import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.util.CoroutineUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ExchangeRateHelper(
    val blockchainDataSource: BlockchainDataSource,
    val coroutineUtil: CoroutineUtil
) : MutableLiveData<Pair<ExchangeRateHelper.State, ExchangeRate?>>() {

    companion object {
        val MILLIS_REFRESH_PERIOD = 1000 * 120L
    }

    private var millisReqStart: Long = -1
    private var millisReqFinish: Long = -1


    init {
        //doRefresh()
    }

    fun considerRefresh() {
        if (millisReqStart != -1L) {
            //already refreshing, drop this
            return
        }

        val now = System.currentTimeMillis()
        if (millisReqFinish + MILLIS_REFRESH_PERIOD < now) {
            doRefresh()
        } else {
            Timber.d("refresh opportunity dropped, it came too soon.")
        }
    }

    fun doRefresh() {
        value = Pair(State.QUERYING, null)

        //note the start millis of the refresh
        millisReqStart = System.currentTimeMillis()

        coroutineUtil.appScope.launch {
            val rate = blockchainDataSource.fetchExchangeRate()

            withContext(Dispatchers.Main) {
                value = if (rate == null) {
                    Pair(State.FAIL, value?.second)
                } else {
                    Pair(State.SUCCESS, rate)
                }

                //note finish millis
                millisReqFinish = System.currentTimeMillis()
                millisReqStart = -1
            }
        }
    }

    enum class State {
        QUERYING,
        SUCCESS,
        FAIL
    }
}