package app.bitcoin.baemail.wallet

import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import app.bitcoin.baemail.R
import app.bitcoin.baemail.util.match
import timber.log.Timber
import kotlin.math.min

class ChainSyncView : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val dp1: Float

    private var icon: ImageView
    private var title: TextView
    private var subtitle: TextView

    private var valueModel = Model(
        ColorDrawable(Color.GREEN),
        "unset-name",
        "unset-mail"
    )

    init {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.view_chain_sync, this, false)
        val set = ConstraintSet()
        view.id = ViewCompat.generateViewId()
        addView(view)

        set.clone(this)
        set.match(view, this)

        dp1 = context.resources.displayMetrics.density


        icon = view.findViewById(R.id.icon)
        title = view.findViewById(R.id.title)
        subtitle = view.findViewById(R.id.subtitle)


        icon.clipToOutline = true
        icon.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val roundness = min(view.width, view.height)

                outline.setRoundRect(0, 0, view.width, view.height, roundness.toFloat())
            }
        }

        refresh()

    }

    private fun refresh() {
        icon.setImageDrawable(valueModel.icon)
        title.text = valueModel.title
        subtitle.text = valueModel.subtitle
    }

    fun applyModel(model: Model) {
        valueModel = model

        refresh()
    }






    fun setup(chainSync: DynamicChainSync, lifecycleOwner: LifecycleOwner) {
        chainSync.statusLD.observe(lifecycleOwner, Observer {
            it ?: return@Observer

            Timber.d("on-change !!!! $it")

            when (it) {
                ChainSync.Status.ERROR -> {
                    applyModel(Model(
                        ColorDrawable(Color.RED),
                        resources.getString(R.string.error),
                        resources.getString(R.string.wallet_sync_failed)
                    ))
                }
                ChainSync.Status.DISCONNECTED -> {
                    applyModel(Model(
                        ColorDrawable(ContextCompat.getColor(context, R.color.orange1)),
                        resources.getString(R.string.disconnected),
                        resources.getString(R.string.wallet_sync_failed)
                    ))
                }
                ChainSync.Status.INITIALISING -> {
                    applyModel(Model(
                        ColorDrawable(Color.BLUE),
                        resources.getString(R.string.connecting),
                        resources.getString(R.string.wallet_doing_sync)
                    ))
                }
                ChainSync.Status.LIVE -> {
                    applyModel(Model(
                        ColorDrawable(Color.GREEN),
                        resources.getString(R.string.connected),
                        resources.getString(R.string.wallet_in_sync)
                    ))
                }
            }
        })
    }




    data class Model(
        val icon: Drawable,
        val title: String,
        val subtitle: String
    )




}