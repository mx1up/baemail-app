package app.bitcoin.baemail.wallet

import android.content.Context
import android.content.SharedPreferences
import app.bitcoin.baemail.util.CoroutineUtil

@Deprecated("chain sync was refactored & now this is irrelevant")
class SyncTrackingDataSource(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil
) {
    companion object {
        private val PREFS_NAME = "wallet_sync.prefs"

        private val KEY_REQUESTED = "queuedRequest_"
        private val KEY_REQUESTED_MILLIS = "queuedRequest_millis_"
        private val KEY_RESULT = "syncResult_"
        private val KEY_MILLIS = "syncMillis_"
    }

    private lateinit var prefs: SharedPreferences

    suspend fun getPrefs(): SharedPreferences {
        if (!::prefs.isInitialized) {
            prefs = appContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        }
        return prefs
    }

    suspend fun markSyncRequested(paymail: String) {
        getPrefs().edit()
            .putBoolean(KEY_REQUESTED + paymail, true)
            .putLong(KEY_REQUESTED_MILLIS + paymail, System.currentTimeMillis())
            .apply()
    }

    suspend fun isSyncRequested(paymail: String): Boolean {
        return getPrefs().getBoolean(KEY_REQUESTED + paymail, false)
    }

    suspend fun saveSyncResult(paymail: String, success: Boolean) {
        getPrefs().edit()
            .remove(KEY_REQUESTED + paymail)
            .putBoolean(KEY_RESULT + paymail, success)
            .putLong(KEY_MILLIS + paymail, System.currentTimeMillis())
            .apply()
    }

    suspend fun getSyncResult(paymail: String): SyncResult {
        val millisSynced = getPrefs().getLong(KEY_MILLIS + paymail, 0L)
        val successSynced = getPrefs().getBoolean(KEY_RESULT + paymail, false)
        val isRequested = getPrefs().getBoolean(KEY_REQUESTED + paymail, false)
        val requestedMillis = getPrefs().getLong(KEY_REQUESTED_MILLIS + paymail, 0L)

        val syncError = if (successSynced) {
            null
        } else {
            "error"
        }

        return SyncResult(
            paymail,
            millisSynced,
            syncError,
            isRequested,
            requestedMillis
        )
    }

}

data class SyncResult(
    val paymail: String,
    val millis: Long,
    val error: String?,
    val isRequested: Boolean,
    val requestedMillis: Long
)