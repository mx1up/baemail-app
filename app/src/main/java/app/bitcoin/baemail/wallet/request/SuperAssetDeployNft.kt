package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.ReceivedCoins
import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import timber.log.Timber
import java.lang.RuntimeException

class SuperAssetDeployNft(
    val owner: Owner,
    val funds: Funds,
    val callback: (String?)->Unit
) : RequestManager.NodeRequest("SUPER_ASSET_DEPLOY_NFT") {




    /*
        [
            {
                "funding": {
                    seed: ['seed', 'words', ..],
                    rootPath: 'm/44'/0'/ ..',
                    coins: [{'tx_id':'', 'sats': 123, 'index': 0, 'address': '1FFF..FF', 'derivation_path':'/0/13'}, {..}],
                    changeAddress: '1abcedf..'
                },
                "owner": {
                    "path": "m/44'/0'/0'/1/0",
                    "publicKey": "00..ff",
                    "privateKey": "00..ff"
                }
            }
        ]

    */
    override fun getReqParams(): JsonArray {

        val jsonOwner = JsonObject()
        jsonOwner.addProperty("path", owner.path)
        jsonOwner.addProperty("publicKey", owner.publicKey)
        jsonOwner.addProperty("privateKey", owner.privateKey)

        //////////

        val jsonFundingSeed = JsonArray()
        funds.seed.forEach { word ->
            jsonFundingSeed.add(word)
        }

        val jsonFundingCoins = JsonArray()
        funds.coins.forEach {
            val jsonCoin = JsonObject()
            jsonCoin.addProperty("derivation_path", it.key)
            jsonCoin.addProperty("tx_id", it.value.txHash)
            jsonCoin.addProperty("sats", it.value.sats)
            jsonCoin.addProperty("index", it.value.index)

            jsonFundingCoins.add(jsonCoin)
        }

        val jsonFunding = JsonObject()
        jsonFunding.add("seed", jsonFundingSeed)
        jsonFunding.add("coins", jsonFundingCoins)
        jsonFunding.addProperty("rootPath", funds.rootPath)
        jsonFunding.addProperty("changeAddress", funds.changeAddress)

        /////////

        val jsonHolder = JsonObject()
        jsonHolder.add("funding", jsonFunding)
        jsonHolder.add("owner", jsonOwner)

        val finalArray = JsonArray()
        finalArray.add(jsonHolder)

        //////////

        return finalArray
    }





    //todo
    //todo
    //todo
    //todo

    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(responseData?.toString()), "message not signed")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(RuntimeException(), "unexpected response format")
            callback(null)
            return
        }

        try {
            callback(data.toString())

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting")
            callback(null)
        }
    }



    data class Owner(
        val path: String,
        val publicKey: String,
        val privateKey: String
    )

    data class Funds(
        val seed: List<String>,
        val rootPath: String,
        val coins: Map<String, ReceivedCoins>,
        val changeAddress: String
    )

}