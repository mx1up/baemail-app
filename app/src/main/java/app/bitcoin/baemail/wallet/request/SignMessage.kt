package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException

class SignMessage(
    val message: String,
    val wifPrivateKey: String,
    val callback: (String?)->Unit
) : RequestManager.NodeRequest("SIGN_MESSAGE") {

    override fun getReqParams(): JsonArray {

        val finalArray = JsonArray()
        finalArray.add(message)
        finalArray.add(wifPrivateKey)

        return finalArray
    }


    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/), "message not signed")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(RuntimeException(), "unexpected response format")
            callback(null)
            return
        }

        try {
            callback(data.asString)

        } catch (e: Exception) {
            Timber.e(e, "not expected formatting")
            callback(null)
        }
    }

}