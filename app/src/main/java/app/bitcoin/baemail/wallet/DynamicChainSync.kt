package app.bitcoin.baemail.wallet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.util.AppForegroundLiveData
import app.bitcoin.baemail.util.ConnectivityLiveData
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Provider
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class DynamicChainSync(
    private val appStateLD: AppStateLiveData,
    private val appForegroundLD: AppForegroundLiveData,
    private val connectivityLD: ConnectivityLiveData,
    private val scope: CoroutineScope,
    private val secureDataSource: SecureDataSource,
    private val chainSyncProvider: Provider<ChainSync>
) {

    companion object {
        private const val MILLIS_KILL_DELAY = 1000 * 45L

    }

    //if this is not NULL when observing is requested because either the app is in the foreground, or it is forced
    private var chainSync: ChainSync? = null

    //////////
    private val _statusLD = MutableLiveData<ChainSync.Status>()
    val statusLD: LiveData<ChainSync.Status>
        get() = _statusLD
    //////////

    init {
        _statusLD.postValue(ChainSync.Status.DISCONNECTED)
    }



    private var killJob: Job? = null
    private var forceKeepAlive = false

    private var clearDelayOfDisconnect: ((Boolean)->Unit)? = null

    val fullSyncHelper: FullSyncHelper? get() = chainSync?.fullSyncHelper

    fun setup() {
        appForegroundLD.observeForever { isForeground ->
            val isConnected = connectivityLD.value?.isConnected ?: return@observeForever

            if (isForeground) {
                cancelDisconnect()

                val isNotStarted = chainSync == null
                if (isNotStarted && isConnected) {
                    doStart()
                }
            } else {
                scheduleDisconnect()
            }
        }

        appStateLD.observeForever(object : Observer<AppState> {

            private var lastPaymail: String? = "invalid"

            override fun onChanged(it: AppState?) {
                val currentPaymail = it?.activePaymail

                if (currentPaymail == lastPaymail) {
                    //relevant state not changed
                    return
                }
                lastPaymail = currentPaymail

                onActivePaymailChanged()

            }

        })

        connectivityLD.observeForever(object : Observer<ConnectivityLiveData.Model> {
            override fun onChanged(it: ConnectivityLiveData.Model?) {
                it ?: return

                val isFg = appForegroundLD.value ?: return

                val isStartingNecessary = chainSync == null || chainSync?.statusLD?.value == ChainSync.Status.ERROR

                if (it.isConnected && isFg && isStartingNecessary) {
                    Timber.d("starting now because app is in FG & connected to internet")
                    doStop()
                    doStart()
                }
            }

        })
    }


    fun isAlive(): Boolean {
        return chainSync != null
    }


    fun forceKeepAlive() {
        Timber.d("forceKeepAlive")

        forceKeepAlive = true

        if (isAlive()) {
            Timber.d("forceKeepAlive; existed")
            //todo opportunity to tell something to something external

        } else {
            Timber.d("forceKeepAlive; starting NOW")
            doStart()
            scheduleDisconnect()
        }
    }

    fun clearKeepAlive() {
        Timber.d("clearKeepAlive")
        forceKeepAlive = false

        clearDelayOfDisconnect?.let {
            //disconnect was prevented ...proceed
            it.invoke(true)
        }
        clearDelayOfDisconnect = null

        //todo opportunity to tell something to something external
    }

    private fun cancelDisconnect() {
        Timber.d("cancelDisconnect")
        killJob?.cancel()
        killJob = null


        clearDelayOfDisconnect?.let {
            //disconnect was prevented ...clear it without proceeding
            it.invoke(false)
        }
        clearDelayOfDisconnect = null
    }

    private fun scheduleDisconnect() {
        Timber.d("scheduleDisconnect")
        killJob ?: let {
            killJob = scope.launch(Dispatchers.Main) {
                Timber.d("scheduleDisconnect kill-job launched")
                delay(MILLIS_KILL_DELAY)
                if (!isActive) {
                    Timber.d("`killJob` job has been cancelled")
                    return@launch
                }

                val proceedWithDisconnect = considerDelayOfDisconnect()

                if (proceedWithDisconnect) {
                    doStop()
                    killJob = null

                }
            }
        }
    }

    /**
     * @return TRUE to proceed with disconnect
     */
    private suspend fun considerDelayOfDisconnect(): Boolean {
        return suspendCoroutine { continuation ->

            if (!forceKeepAlive) {
                Timber.d("proceed with disconnection")
                continuation.resume(true)
                return@suspendCoroutine
            }

            Timber.d("prevent proceeding with disconnection")
            //...prevent proceeding by not calling continuation
            //...provide remote means to proceed
            clearDelayOfDisconnect = { proceedWithDisconnect ->
                Timber.d("disconnection prevention cleared; proceedWithDisconnect:$proceedWithDisconnect")
                continuation.resume(proceedWithDisconnect)

            }
        }
    }





    private fun doStart() {
        if (chainSync != null) {
            Timber.d("dropped #doStart; already exists")
            return
        }

        Timber.d("#doStart")

        val paymail = appStateLD.value?.activePaymail ?: let {
             Timber.e(RuntimeException("cannot sync at this moment"))
            return
        }

        val sync = chainSyncProvider.get()
        chainSync = sync

        scope.launch(Dispatchers.Main) {

            val fundingWalletSeed = try {
                secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))
            } catch (e: Exception) {
                Timber.e(e, "wallet not found; dropping sync-request")
                return@launch
            }


            sync.statusLD.observeForever(statusObserver)

            sync.initialize(paymail, fundingWalletSeed)


        }
    }





    private fun onActivePaymailChanged() {
        Timber.d("#onActivePaymailChanged")
        doStop()
        doStart()
    }

    private fun doStop() {
        Timber.d("#doStop")

        chainSync?.statusLD?.removeObserver(statusObserver)
        _statusLD.value = ChainSync.Status.DISCONNECTED

        chainSync?.disconnect()
        chainSync = null
    }


    private val statusObserver = Observer<ChainSync.Status> {
        it ?: return@Observer

        _statusLD.value = it

    }





}