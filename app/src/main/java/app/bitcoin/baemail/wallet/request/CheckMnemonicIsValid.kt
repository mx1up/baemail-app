package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException

class CheckMnemonicIsValid(
    val seed: List<String>,
    val callback: (Boolean)->Unit
) : RequestManager.NodeRequest("CHECK_MNEMONIC_IS_VALID") {

    override fun getReqParams(): JsonArray {
        val array = JsonArray()
        seed.forEach {
            array.add(it)
        }
        return array
    }

    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/), "mnemonic was not valid")
            callback(false)
            return
        }
        callback(true)
    }

}