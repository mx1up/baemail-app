package app.bitcoin.baemail.wallet

import android.util.Base64
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.bitcoin.baemail.net.AppApiService
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.wallet.request.DerivedAddress
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.star_zero.sse.EventHandler
import com.star_zero.sse.EventSource
import com.star_zero.sse.MessageEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.Exception
import kotlin.math.max

class ReceivedCoinHelper(
    private val appDatabase: AppDatabase,
    private val apiService: AppApiService,
    private val unusedAddressHelper: UnusedAddressHelper,
    private val scope: CoroutineScope,
    private val gson: Gson
) {

    companion object {

        private const val KEY_TYPE = "type"
        private const val KEY_DATA = "data"

        private const val VALUE_TYPE_OPEN = "open"
        private const val VALUE_TYPE_MESSAGE = "push"

    }

    //if this is not NULL when observing is requested
    private var state: State? = null

    private val _detectedCoinsLD = MutableLiveData<List<Coin>>()
    val detectedCoinsLD: LiveData<List<Coin>>
        get() = _detectedCoinsLD

    private lateinit var paymail: String

    private lateinit var onError: ()->Unit

    fun setup(paymail: String, onError: ()->Unit) {
        this.paymail = paymail
        this@ReceivedCoinHelper.onError = onError
    }

    ////////////////////


    fun getAddressesBeingObserved(): List<DerivedAddress> {
        var current: List<DerivedAddress>? = null

        state?.let {
            it.addresses?.let { list ->
                current = list
            }
        }

        return current?.let {
            it
        } ?: listOf()
    }


    /////////////////

    fun start() {
        if (state != null) {
            Timber.d("dropped #start; already exists")
            return
        }

        Timber.d("#start")

        state = State(
            null,
            null,
            null
        )

        scope.launch {
            refreshObservation()
        }
    }

    fun stop() {
        Timber.d("#stop")

        //stop observing the hot addresses
        state?.observer?.close()
        state = null
    }


    //////////////////


    suspend fun refreshObservation() = withContext(Dispatchers.IO) {
        if (state == null) {
            Timber.d("observation is not started; not starting it now")
            return@withContext
        }


        val relevantAddresses = findAddressesToObserve()
        val currentlyObservedAddresses = state?.addresses

        if (relevantAddresses == currentlyObservedAddresses) {
            Timber.d("these addresses are already being observed")
            return@withContext
        }

        Timber.d("different set of addresses needs to be observed")
        //clean up whatever was happening at this point
        state?.observer?.close()
        state = null

        //start observing the new address-set
//        val paymail = appStateLD.value?.activePaymail ?: let {
//            state = ReceivedCoinObserver.State(
//                null,
//                null,
//                null
//            )
//            Timber.d("there are no active paymails; no addresses need observing; stopping")
//            return@withContext
//        }

        state = State(
            paymail,
            null,
            relevantAddresses
        )

        observeAddresses(paymail, relevantAddresses)
    }

    private suspend fun findAddressesToObserve(): List<DerivedAddress> {

        //next X next unused addresses
        //all current unspent coins
        //last Y recently used addresses

        val observationList = HashSet<DerivedAddress>()



        //several upcoming unused addresses
        val upcomingAddresses = unusedAddressHelper.getUnusedAddressesOfFundingWallet(paymail)
        upcomingAddresses.mapIndexedNotNull { i, address ->
            //a few of the lowest
            if (i > 5) return@mapIndexedNotNull null

            address
        }.toCollection(observationList)




        //a few addresses below the upcoming ones
        DerivedAddress.getIndexOfDerivationPath(upcomingAddresses[0].path).let { indexOfLowest ->
            var startIndex = indexOfLowest - 5
            startIndex = max(startIndex, 0)

            val list = unusedAddressHelper.deriveAddresses(paymail, startIndex, indexOfLowest)
            observationList.addAll(list)
        }





        //all of the addresses for unspent coins
        val unspentCoins = appDatabase.coinDao().getUnspentFundingCoins(paymail)
        unspentCoins.map {
            val address = DerivedAddress(
                it.derivationPath,
                it.address
            )

            address
        }.toCollection(observationList)





        //a few addresses below the highest spent
        appDatabase.coinDao().getHighestSpentFundingCoin(paymail)?.pathIndex?.let { indexOfLastSpent ->
            var startIndex = indexOfLastSpent - 5
            startIndex = max(startIndex, 0)

            val list = unusedAddressHelper
                .deriveAddresses(paymail, startIndex, indexOfLastSpent + 1)

            observationList.addAll(list)
        }



        return ArrayList(observationList)
    }



    private fun observeAddresses(
        paymail: String,
        addressList: List<DerivedAddress>
    ) {

//        //fetch & check for recent Txs of next unspent coin-addresses
//        scope.launch {
//            crawlAddressList(paymail, addressList)
//        }

        //start observing hot addresses for new incoming Txs

        val stringAddressList = addressList.map { it.address }
        val finalQuery = buildObservationQueryUrl(stringAddressList)
        Timber.d("query: $finalQuery")

        val isCancelledHolder: Array<()->Boolean> = Array(1) { { false} }

        val client = EventSource1(finalQuery, object : EventHandler {
            override fun onOpen() {
                Timber.d("on SSE opened")
            }

            override fun onMessage(event: MessageEvent) {
                Timber.d("data:${event.data}")

                val jsonMessage = gson.fromJson(event.data, JsonObject::class.java)
                val messageType = jsonMessage.getAsJsonPrimitive(KEY_TYPE).asString
                //could be VALUE_TYPE_OPEN or VALUE_TYPE_MESSAGE
                //todo consider improving efficiency with last-event-id

                if (VALUE_TYPE_MESSAGE == messageType) {
                    val jsonData = jsonMessage.getAsJsonArray(KEY_DATA)
                    val newCoinsList = jsonData.map {
                        gson.fromJson(it, ReceivedCoins::class.java)
                    }.mapNotNull {
                        if (stringAddressList.contains(it.address)) {
                            it
                        } else {
                            null
                        }
                    }

                    if (newCoinsList.isNotEmpty()) {
                        onNewCoinDetected(paymail, newCoinsList, addressList)
                    }
                }
            }

            override fun onError(e: Exception?) {
                if (isCancelledHolder[0]()) {
                    Timber.d("#onError has been dropped; event-source is NULL, assuming it was closed programmatically")
                    return
                }

                this@ReceivedCoinHelper.onError(e)
            }

        })


        isCancelledHolder[0] = {
            client.wasCloseCalled
        }

        client.connect()

        Timber.d("#observeAddresses paymail:$paymail addresses:$addressList")

        state = State(
            paymail,
            client,
            addressList
        )

    }


    private fun buildObservationQueryUrl(addressList: List<String>): String {
        val addressJson = JsonArray()
        addressList.forEach {
            addressJson.add(it)
        }

        val arrString = addressJson.toString()

        val query = """
            {
              "v": 3,
              "q": {
                "find": { "out.e.a" : { "${"$"}in": $arrString }}
              },
              "r": {
                "f": "[ .[] | {tx_id: .tx.h, o: .out[].e} | {tx_id: .tx_id, sats: .o.v, index: .o.i, address: .o.a} ]"
              }
            }
        """.trimIndent()
        val encodedQuery = Base64.encodeToString(query.toByteArray(), Base64.DEFAULT)



        return "https://txo.bitsocket.network/s/$encodedQuery"
    }



    private fun onNewCoinDetected(
        paymail: String,
        coins: List<ReceivedCoins>,
        observedAddresses: List<DerivedAddress>
    ) {
        Timber.d("#onNewCoinDetected coins:$coins")
        scope.launch(Dispatchers.IO) {


            val newCoins = coins.mapNotNull { coin ->
                val matchingDbCoin = appDatabase.coinDao()
                    .getCoin(coin.address, coin.txHash, coin.index)

                matchingDbCoin?.let {
                    Timber.d("coin already found in db; $matchingDbCoin")
                    return@mapNotNull null
                }

                //add the coin to the db
                val matchingAddress = observedAddresses.find {
                    it.address == coin.address
                } ?: throw RuntimeException()

                Coin(
                    paymail,
                    matchingAddress.path,
                    matchingAddress.getIndex(),
                    matchingAddress.address,
                    coin.txHash,
                    coin.index,
                    coin.sats,
                    null
                )
            }

            if (newCoins.isEmpty()) {
                Timber.d("false alarm; all of these coins were known")
                return@launch
            }


            //save the new coins
            //
            appDatabase.coinDao().insertList(newCoins)

            //let everyone know
            //
            newCoins.map {
                DerivedAddress(
                    it.derivationPath,
                    it.address
                )
            }.let {
                unusedAddressHelper.onAddressUsedInTx(paymail, it)
            }

            withContext(Dispatchers.Main) {
                _detectedCoinsLD.value = newCoins
            }
        }
    }


    private fun onError(e: Throwable?) {
        Timber.e(e)
        onError()
    }







    data class State(
        val paymail: String?,
        val observer: EventSource?,
        val addresses: List<DerivedAddress>?
    )


    class EventSource1(
        url: String,
        eventHandler: EventHandler
    ) : EventSource(url, eventHandler) {

        var wasCloseCalled = false

        override fun close() {
            wasCloseCalled = true
            super.close()
        }
    }
}