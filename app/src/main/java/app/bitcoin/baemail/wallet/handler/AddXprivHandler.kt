package app.bitcoin.baemail.wallet.handler

import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.wallet.RequestManager
import app.bitcoin.baemail.wallet.WalletRepository
import com.google.gson.JsonArray
import timber.log.Timber

class AddXprivHandler(
    val secureDataSource: SecureDataSource,
    val walletRepository: WalletRepository
) : RequestManager.WorkHandler("ADD_XPRIV") {



    override suspend fun handle(
        token: Long,
        params: JsonArray
    ): Pair<RequestManager.Result, JsonArray> {

        try {
            val xpriv = params[0].asString

            val alias = secureDataSource.addCustom(xpriv)


            val jsonArray = JsonArray()
            jsonArray.add(alias)

            return Pair(RequestManager.Result.SUCCESS, jsonArray)

        } catch (e: Exception) {
            Timber.e(e)
            return Pair(RequestManager.Result.FAIL, JsonArray())

        }

    }
}