package app.bitcoin.baemail.wallet.request

import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.JsonArray
import timber.log.Timber
import java.lang.RuntimeException


class GetPrivateKeyForPathFromSeed(
    val path: String,
    val seed: List<String>,
    val callback: (PrivateKeyHolder?)->Unit
) : RequestManager.NodeRequest("PRIVATE_KEY_FOR_PATH_FROM_SEED") {

    override fun getReqParams(): JsonArray {
        val seedArray = JsonArray()
        seed.forEach {
            seedArray.add(it)
        }

        val finalArray = JsonArray()
        finalArray.add(path)
        finalArray.add(seedArray)

        return finalArray
    }

    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/), "private key not received")
            callback(null)
            return
        }

        val data = responseData
        if (data == null || data.size() != 1) {
            Timber.e(RuntimeException(), "unexpected response format")
            callback(null)
            return
        }

        try {
            val dataObject = data[0].asJsonObject

            val publicKey = dataObject.get("publicKey")
            val privateKey = dataObject.get("privateKey")

            callback(PrivateKeyHolder(
                path,
                publicKey.asString,
                privateKey.asString
            ))
        } catch (e: Exception) {
            Timber.e(e, "not expected formatting")
            callback(null)
        }
    }


    data class PrivateKeyHolder(
        val path: String,
        val publicKey: String,
        val privateKey: String
    )


}








