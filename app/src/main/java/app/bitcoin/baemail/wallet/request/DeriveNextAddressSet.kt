package app.bitcoin.baemail.wallet.request

import androidx.annotation.Keep
import app.bitcoin.baemail.wallet.RequestManager
import com.google.gson.Gson
import com.google.gson.JsonArray
import timber.log.Timber
import kotlin.RuntimeException

class DeriveNextAddressSet(
    val seed: List<String>,
    val keySetIndex: Int,
    val gson: Gson,
    val callback: (List<DerivedAddress>?)->Unit
) : RequestManager.NodeRequest("DERIVE_NEXT_ADDRESS_SET") {

    override fun getReqParams(): JsonArray {
        val jsonSeedArray = JsonArray()
        seed.forEach {
            jsonSeedArray.add(it)
        }

        val wrappingJsonArray = JsonArray()
        wrappingJsonArray.add(jsonSeedArray)
        wrappingJsonArray.add(keySetIndex)


        return wrappingJsonArray
    }

    override fun onResponse() {
        if (responseResult != RequestManager.Result.SUCCESS) {
            Timber.e(RuntimeException(/*responseData?.toString()*/),
                "error deriving requested set of addresses")
            callback(null)
            return
        }

        try {
            //parse the response
            /*
            # response
            [
                {'path': '/0/0', address: '1C6BBGPhq57UCA1DxK5HQkT7Essr8zzLXJ'},
                {'path': '/0/1', address: '1G7tZTw1z1XTuE7xsDR5J5s6uz57PrmYe1'},
                ..
            ]
             */

            val addressList = mutableListOf<DerivedAddress>()

            val dataJson = responseData ?: throw RuntimeException("data missing")

            dataJson.forEach {
                addressList.add(gson.fromJson(it, DerivedAddress::class.java))
            }

            callback(addressList)
        } catch (e: Exception) {
            callback(null)
        }
    }

}

@Keep
data class DerivedAddress(
    val path: String,
    val address: String
) {
    fun getIndex() = getIndexOfDerivationPath(path)

    companion object {
        fun getIndexOfDerivationPath(p: String) = p.substring(p.lastIndexOf('/') + 1).toInt()
    }
}