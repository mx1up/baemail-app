package app.bitcoin.baemail.wallet

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.BlockSyncInfo
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import app.bitcoin.baemail.util.CoroutineUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import kotlin.Exception

class ChainSync(
    val context: Context,
    val appDatabase: AppDatabase,
    val coroutineUtil: CoroutineUtil,
    val blockchainDataSource: BlockchainDataSource,
    val fullSyncHelper: FullSyncHelper,
    val newBlockPollingHelper: NewBlockPollingHelper,
    val receivedCoinHelper: ReceivedCoinHelper,
    val unusedAddressHelper: UnusedAddressHelper
) {

    private var newBlockObserver: NewBlockObserver? = null


    private val _statusLD = MutableLiveData<Status>()
    val statusLD: LiveData<Status>
        get() = _statusLD


    private lateinit var paymail: String

    suspend fun initialize(paymail: String, seed: List<String>) = withContext(Dispatchers.IO) {

        this@ChainSync.paymail = paymail

        fullSyncHelper.setup(
            paymail,
            seed
        )

        newBlockPollingHelper.setup {
            Timber.e(RuntimeException())
            //onListenError(RuntimeException())
        }

        receivedCoinHelper.setup(paymail) {
            onListenError(RuntimeException())
        }

        doInitialize()
    }

    fun disconnect() {
        releaseEverything()

        _statusLD.postValue(Status.DISCONNECTED)
    }

    private fun releaseEverything() {
        //stop & clean everything up

        //cleanup new block observing
        newBlockPollingHelper.stop()

        newBlockObserver?.let {
            newBlockPollingHelper.currentBlockLD.removeObserver(it)
            newBlockObserver = null
        }



        //cleanup receiving-detector
        receivedCoinHelper.stop()

        unspentDbCoinsLD?.removeObserver(unspentDbCoinObserver)
        unspentDbCoinsLD = null
        unspentDbCoinObserver.clearInternalCoinsList()
    }

    private suspend fun doInitialize() = withContext(Dispatchers.IO) {

        _statusLD.postValue(Status.INITIALISING)

        val info = blockchainDataSource.fetchCurrentStatus()
        if (info == null) {
            onSyncError(RuntimeException())
            return@withContext
        }
        Timber.d("current info: $info")

        ensureActive()

        val didSucceed = syncCoins(info)
        if (!didSucceed) {
            Timber.d("syncCoins failed; returning")
            return@withContext
        }

        // successfully completed the sync/check
        persistSuccessfulSync(info)

        //refresh the unused address helper
        unusedAddressHelper.invalidateAndRefresh(paymail)

        ensureActive()
        Timber.d("syncCoins completed; starting observation")

        startCheckingForNewBlocks(info)

        startObservingReceivingOfNewCoins(paymail)

        _statusLD.postValue(Status.LIVE)

    }



    //////////////////////////

    private suspend fun syncCoins(info: BlockInfo): Boolean {
        Timber.d("#syncCoins info: $info")

        val infoOfLastChecked = getInfoOnLastSuccessfulCheck()

        val now = System.currentTimeMillis()
        val timeInADay = 1000 * 60 * 60 * 24L

        if (infoOfLastChecked == null) {
            Timber.d("this is the very first check; full sync is required")
            //full sync from start
            val result = fullSyncHelper.syncFromStart()
            if (!result) {
                // sync failed
                onSyncError(RuntimeException())
                return false
            }

            //no need to check for a reorg
            //can proceed with coin-receive event observing

        } else if (infoOfLastChecked.height == info.height &&
            infoOfLastChecked.hash == info.hash) {
            Timber.d("no blocks have advanced since the last check; everything is up to date")

        } else if (infoOfLastChecked.height + 1 == info.height &&
            infoOfLastChecked.hash == info.previousBlockHash) {
            // blockchain has advanced by 1 block without a reorg
            Timber.d("blockchain has advanced by 1 block without a reorg")
            val result = refreshTxConfirmations()
            if (!result) {
                // sync failed
                onSyncError(RuntimeException())
                return false
            }

        } else {
            val infoOfHeight = blockchainDataSource.fetchBlockInfoForHeight(infoOfLastChecked.height)
            if (infoOfHeight == null) {
                // sync failed
                onSyncError(RuntimeException())
                return false
            }

            if (infoOfHeight.hash != infoOfLastChecked.hash) {
                // the block-info found in the last check is invalid because of a reorg
                Timber.d("block-info of last sync does not match info of the same block on the chain")

                //possibly also apply reorg on sent & received txs
                try {
                    onPossibleReorg()
                } catch (e: Exception) {
                    // sync failed
                    onSyncError(RuntimeException())
                    return false
                }

                //full sync starting from beginning
                fullSyncHelper.syncFromCurrentState(false)

            } else {
                //there was no reorg

                val refreshResult = refreshTxConfirmations()
                if (!refreshResult) {
                    // sync failed
                    onSyncError(RuntimeException())
                    return false
                }

                val result = if (infoOfLastChecked.time + timeInADay < now) {
                    //full sync from the beginning
                    Timber.d("block-info indicates that the last sync was done more than a day ago")
                    fullSyncHelper.syncFromCurrentState(false)

                } else {
                    //full sync using the crawler
                    Timber.d("block-info indicates that the last sync was done less than a day ago")
                    fullSyncHelper.syncFromCurrentState(true)
                }

                if (!result) {
                    // sync failed
                    onSyncError(RuntimeException())
                    return false
                }

            }

        }

        return true
    }

    private suspend fun onPossibleReorg() {
        val txsForPaymail = appDatabase.txConfirmationDao().getForPaymail(paymail)

        val unconfirmedTxs = mutableListOf<TxConfirmation>()
        //todo add breaking of the loop in case of lots of history
        val confirmedTxs = txsForPaymail.filter {
            if (it.blockHeight < 0) {
                unconfirmedTxs.add(it)
            }

            it.blockHeight > 0
        }


        //clear unconfirmed transactions
        unconfirmedTxs.forEach { txConfirmation ->
            //revert this transaction
            reorgACoin(txConfirmation.txHash)

        }



        //revert coin-txs one by one until the reorg has been undone
        var checkedBlockHeight = -1
        var reorgHeight = -1

        // at this point in execution `confirmedTxs` is sorted by block-height descending
        for (i in 0 until confirmedTxs.size) {
            val txConfirmation = confirmedTxs[i]
            if (checkedBlockHeight < 0 || txConfirmation.blockHeight < checkedBlockHeight) {
                // a tx with a new, not yet checked height, needs processing

                checkedBlockHeight = txConfirmation.blockHeight

                //check if the block containing this is still valid
                val infoOfHeight = blockchainDataSource.fetchBlockInfoForHeight(txConfirmation.blockHeight)
                infoOfHeight ?: let {
                    throw RuntimeException("api request failed")
                }

                if (infoOfHeight.hash == txConfirmation.blockHash) {
                    Timber.d("found a coin that has not been reorged; tx-blockheight: ${txConfirmation.blockHeight}")
                    //the iterated list is sorted by blockheight
                    break
                } else {
                    reorgHeight = txConfirmation.blockHeight
                }
            }

            //undo the existence of the matching coin or spend of coin
            //revert this transaction
            //... the iterated list is sorted by blockheight
            reorgACoin(txConfirmation.txHash)
        }

        if (reorgHeight != -1) {
            //clear persisted blockinfo
            appDatabase.blockSyncInfoDao().clearForHeight(reorgHeight, paymail)
        }



    }

    private suspend fun reorgACoin(txId: String) {
        val foundCoins = appDatabase.coinDao().getCoinsWithTxId(txId)

        foundCoins.forEach { coin ->
            if (coin.txId == txId) {
                //delete the coin
                appDatabase.coinDao().deleteCoinWithId(coin.txId)

                ///todo assuming there is a coin with the spendingTxId as its txId, should that be recursively deleted?

            } else if (coin.spendingTxId == txId) {
                //update the coin to remove this spend
                val updatedCoin = coin.copy(spendingTxId = null)
                appDatabase.coinDao().insertCoin(updatedCoin)

                ///todo assuming there is a coin with the spendingTxId as its txId, should that be recursively deleted?

            }
        }

        appDatabase.txConfirmationDao().clearConfirmation(txId)
    }

    private suspend fun getInfoOnLastSuccessfulCheck(): BlockSyncInfo? {
        return appDatabase.blockSyncInfoDao().getLatestInfo(paymail)
    }

    private fun onSyncError(e: Throwable) {
        Timber.e(e)
        coroutineUtil.appScope.launch(Dispatchers.Main) {
            releaseEverything()

            _statusLD.postValue(Status.ERROR)
        }
    }

    private fun onListenError(e: Throwable) {
        Timber.e(e)
        coroutineUtil.appScope.launch(Dispatchers.Main) {
            releaseEverything()

            _statusLD.postValue(Status.ERROR)
        }
    }

    private suspend fun persistSuccessfulSync(info: BlockInfo) {
        val syncInfo = BlockSyncInfo(
            info.height,
            paymail,
            info.time,
            info.hash,
            info.merkleRoot,
            info.previousBlockHash
        )
        appDatabase.blockSyncInfoDao().insertInfo(syncInfo)
    }

    private suspend fun refreshTxConfirmations(): Boolean {

        val unconfirmedTxs = appDatabase.txConfirmationDao()
            .getUnconfirmedForPaymail(paymail)

        if (unconfirmedTxs.size == 0) {
            Timber.d("all txs of the active paymail have been confirmed")
            return true
        }

        if (unconfirmedTxs.size > 50) {
            throw RuntimeException("situation not yet handled !!!!!!!!!!!!!!! TODO")
        }

        val txInfo = blockchainDataSource.getStatusOfTxs(unconfirmedTxs.map {
            it.txHash
        })

        if (txInfo == null) {
            Timber.d("unconfirmed tx refresh-request failed")
            return false
        }

        if (txInfo.isEmpty()) {
            Timber.d("none of the unconfirmed tx got refreshed")
            return true
        }

        txInfo.forEach {
            Timber.d("tx refreshed: $it")
            appDatabase.txConfirmationDao().insertConfirmationInfo(it)
        }

        return true
    }


    //////////////////////////


    private suspend fun startCheckingForNewBlocks(currentInfo: BlockInfo) = withContext(Dispatchers.Main){
        NewBlockObserver(currentInfo).let { observer ->
            newBlockObserver = observer
            newBlockPollingHelper.currentBlockLD.observeForever(observer)
        }

        newBlockPollingHelper.initialize(currentInfo)
    }

    inner class NewBlockObserver(
        var currentInfo: BlockInfo
    ) : Observer<BlockInfo> {

        override fun onChanged(it: BlockInfo?) {
            it ?: return

            if (currentInfo.hash == it.hash) return
            currentInfo = it

            //todo ... improvement ... insure that an earlier call to `syncCoins` is finished before a new one is started

            //handle the new block

            coroutineUtil.appScope.launch {
                unspentDbCoinObserver.disableRefreshing()

                val didSucceed = syncCoins(it)
                if (!didSucceed) {
                    Timber.d("syncCoins failed; returning")
                    return@launch
                }

                // successfully completed the sync/check
                persistSuccessfulSync(it)


                unspentDbCoinObserver.enableRefreshing()
            }

        }

    }

    //////////////////////////

    suspend fun startObservingReceivingOfNewCoins(paymail: String) {

        //observing must be refreshed when db-coins change
        withContext(Dispatchers.Main) {
            //cleanup current ld if any
            unspentDbCoinsLD?.removeObserver(unspentDbCoinObserver)
            unspentDbCoinsLD = null
            unspentDbCoinObserver.clearInternalCoinsList()

            //setup the db-observation for the paymail
            val ld = appDatabase.coinDao().getUnspentFundingCoinLD(paymail)
            ld.observeForever(unspentDbCoinObserver)

            unspentDbCoinsLD = ld
        }



        //todo !!!!!! receivedCoinHelper.detectedCoinsLD


        receivedCoinHelper.start()
    }

    private val unspentDbCoinObserver = UnspentDbCoinObserver()
    private var unspentDbCoinsLD: LiveData<List<Coin>>? = null

    inner class UnspentDbCoinObserver : Observer<List<Coin>> {
        private val unspentCoinsList = ArrayList<String>() //possibly change this into a SET

        private var refreshingEnabled = true
        private var wasRefreshDropped = false

        override fun onChanged(it: List<Coin>?) {
            it ?: return

            val addressList = it.map { it.address }

            if (addressList == unspentCoinsList) {
                Timber.d("unspentDbCoinObserver onChanged was dropped because the coin-list is unchanged")
                return
            }
            unspentCoinsList.clear()
            unspentCoinsList.addAll(addressList)

            if (!refreshingEnabled) {
                Timber.d("refreshing of the received-coin observer has been disabled; dropping")
                wasRefreshDropped = true
                return
            }

            wasRefreshDropped = false
            doRefreshCoinObservation()
        }

        private fun doRefreshCoinObservation() {
            coroutineUtil.appScope.launch {
                Timber.d("doRefreshCoinObservation")
                receivedCoinHelper.refreshObservation()
            }
        }

        fun clearInternalCoinsList() {
            unspentCoinsList.clear()
        }

        fun enableRefreshing() {
            Timber.d("#enableRefreshing")

            refreshingEnabled = true

            if (wasRefreshDropped) {
                wasRefreshDropped = false
                doRefreshCoinObservation()
            }
        }

        fun disableRefreshing() {
            Timber.d("#disableRefreshing")

            refreshingEnabled = false
        }

    }







    //////////////////////////



    enum class Status {
        DISCONNECTED,
        INITIALISING,
        LIVE,
        ERROR
    }

}