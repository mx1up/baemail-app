package app.bitcoin.baemail.wallet

import android.util.Log
import android.util.LongSparseArray
import androidx.core.util.containsKey
import app.bitcoin.node.NodeComms
import app.bitcoin.node.NodeConnectionHelper
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.RuntimeException

/*

//todo talk about node-to-android events
/todo
/todo
/todo
/todo


android-to-node events:

//example request object
{
    token: 11,
    command: 'getMoneyButtonPubKey',
    params: [1, 'abc', {a:1}]
}



//possible values for `status` of the response
success
fail


//example response object
{
    token: 11,
    content: {
        status: 'success',
        data: [2, 'cba', {b:2}]
    }
}
 */

//todo prob should inform requests if the connection is weirdly cut in the middle of a req ???
class RequestManager @Inject constructor(
    private val nodeConnection: NodeConnectionHelper
) {

    private val scope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

    private val activeRequests = LongSparseArray<NodeRequest>()

    private lateinit var workHandlers: Map<String, WorkHandler>

    fun setup(supportedWorkRequests: Map<String, WorkHandler>) {
        workHandlers = supportedWorkRequests

        nodeConnection.setup(object : NodeComms.MessageListener {
            override fun onMessage(m: String) {
                handleMessage(m)
            }
        })
    }

    fun startForeground() {
        nodeConnection.startForeground()
    }

    fun stopForeground() {
        nodeConnection.stopForeground()
    }

    fun request(r: NodeRequest) {
        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }

        scope.launch(exceptionHandler) {

            if (activeRequests.containsKey(r.token)) throw RuntimeException()
            activeRequests.put(r.token, r)

            nodeConnection.ensureSocket().send(assembleRequestMessage(r))

        }
    }

    private fun assembleRequestMessage(r: NodeRequest): String {
        val requestJson = JsonObject()

        requestJson.addProperty(KEY_MESSAGE_TYPE, VALUE_TYPE_NODE_WORK_REQUEST)
        requestJson.addProperty(KEY_TOKEN, r.token)
        requestJson.addProperty(KEY_COMMAND, r.command)
        requestJson.add(KEY_PARAMS, r.getReqParams())

        return requestJson.toString()
    }

    private fun handleMessage(m: String) {
        val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable)
        }

        scope.launch(exceptionHandler) {

            val json = Gson().fromJson(m, JsonObject::class.java)
            when (json.get(KEY_MESSAGE_TYPE).asString) {
                VALUE_TYPE_ANDROID_WORK_REQUEST -> {
                    handleIncomingRequest(json)
                }
                VALUE_TYPE_NODE_WORK_REQUEST -> {
                    handleRequestResponse(json)
                }
                else -> throw RuntimeException()
            }

        }
    }

    private suspend fun handleRequestResponse(json: JsonObject) {

        val content: JsonObject = json.getAsJsonObject(KEY_CONTENT)

        val token: Long = json.getAsJsonPrimitive(KEY_TOKEN).asLong
        val status: String = content.getAsJsonPrimitive(KEY_STATUS).asString
        val data: JsonArray = content.getAsJsonArray(KEY_DATA)


        val matchingRequest = activeRequests.get(token) ?: throw RuntimeException()
        activeRequests.remove(token)

        val responseStatus = if (Result.SUCCESS.const == status) {
            Result.SUCCESS
        } else if (Result.FAIL.const == status) {
            Result.FAIL
        } else {
            throw RuntimeException()
        }

        matchingRequest.setResponse(responseStatus, data)
        matchingRequest.onResponse()
    }


    private suspend fun handleIncomingRequest(json: JsonObject) {

        val command = json.get(KEY_COMMAND).asString

        val token = json.get(KEY_TOKEN).asLong
        val paramsJson = json.getAsJsonArray(KEY_PARAMS)

        try {

            //find the matching handler
            val handler = workHandlers[command] ?: let {
                //respond with error 'handler not found'
                val param = JsonArray()
                param.add("HANDLER_NOT_FOUND")
                sendIncomingRequestResult(token, Result.FAIL, param)
                return
            }


            val result = handler.handle(token, paramsJson)
            //respond with result
            sendIncomingRequestResult(token, result.first, result.second)

        } catch (e: Exception) {
            //respond with crash-resp
            val param = JsonArray()
            param.add(Log.getStackTraceString(e))
            sendIncomingRequestResult(token, Result.FAIL, param)
        }

    }

    private suspend fun sendIncomingRequestResult(token: Long, result: Result, params: JsonArray) {

        val contentJson = JsonObject()
        contentJson.addProperty(KEY_STATUS, result.const)
        contentJson.add(KEY_DATA, params)

        val requestJson = JsonObject()
        requestJson.addProperty(KEY_MESSAGE_TYPE, VALUE_TYPE_ANDROID_WORK_REQUEST)
        requestJson.addProperty(KEY_TOKEN, token)
        requestJson.add(KEY_CONTENT, contentJson)

        nodeConnection.ensureSocket().send(requestJson.toString())
    }


    enum class Result(val const: String) {
        SUCCESS("success"),
        FAIL("fail")
    }

    abstract class NodeRequest(
        val command: String
    ) {
        val token = getNextId()

        var responseResult: Result? = null
        var responseData: JsonArray? = null

        abstract fun getReqParams(): JsonArray

        fun setResponse(result: Result, data: JsonArray) {
            responseResult = result
            responseData = data
        }

        abstract fun onResponse()
    }

    abstract class WorkHandler(
        val command: String
    ) {

        abstract suspend fun handle(token: Long, params: JsonArray): Pair<Result, JsonArray>

    }

    companion object {
        private const val KEY_MESSAGE_TYPE = "message_type"
        private const val KEY_TOKEN = "token"
        private const val KEY_COMMAND = "command"
        private const val KEY_PARAMS = "params"

        private const val KEY_DATA = "data"
        private const val KEY_STATUS = "status"
        private const val KEY_CONTENT = "content"

        private const val VALUE_TYPE_NODE_WORK_REQUEST = "node_work_request"
        private const val VALUE_TYPE_ANDROID_WORK_REQUEST = "android_work_request"





        private var uniqueId = 0L

        @Synchronized
        fun getNextId(): Long {
            return uniqueId++
        }
    }
}