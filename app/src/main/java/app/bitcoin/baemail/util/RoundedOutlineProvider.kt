package app.bitcoin.baemail.util

import android.graphics.Outline
import android.graphics.Rect
import android.os.Build
import android.view.View
import android.view.ViewOutlineProvider
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class RoundedOutlineProvider(private val roundLeft: Boolean, private val roundRight: Boolean) :
    ViewOutlineProvider() {

    override fun getOutline(view: View, outline: Outline) {
        val rect = Rect(0, 0, view.width, view.height)

        val cornerRadius = rect.height() / 2f
        val quarterHeight = (rect.height() / 4f).toInt()

        if (roundLeft && roundRight) {
            outline.setRoundRect(
                Rect(
                    rect.left, rect.top - quarterHeight,
                    rect.right, rect.bottom + quarterHeight
                ), cornerRadius + quarterHeight
            )
        } else if (roundLeft) {
            outline.setRoundRect(
                Rect(
                    rect.left, rect.top - quarterHeight,
                    rect.right + (3 * quarterHeight), rect.bottom + quarterHeight
                ), cornerRadius + quarterHeight
            )
        } else if (roundRight) {
            outline.setRoundRect(
                Rect(
                    rect.left - (3 * quarterHeight), rect.top - quarterHeight,
                    rect.right, rect.bottom + quarterHeight
                ), cornerRadius + quarterHeight
            )
        }
    }
}