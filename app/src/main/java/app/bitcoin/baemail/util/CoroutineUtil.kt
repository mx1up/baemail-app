package app.bitcoin.baemail.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

class CoroutineUtil {

    val appScope = CoroutineScope(Dispatchers.Default + SupervisorJob())
}