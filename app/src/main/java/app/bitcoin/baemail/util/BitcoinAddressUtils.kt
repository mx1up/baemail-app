package app.bitcoin.baemail.util

import app.bitcoin.baemail.util.Base58Check.decode
import java.security.NoSuchAlgorithmException

/// https://github.com/MetacoSA/metaco-java-client

object BitcoinAddressUtils {
    private const val OPEN_ASSETS_NAMESPACE: Byte = 19
    private const val P2PKH_MAIN_NET: Byte = 0x0
    private const val P2SH_MAIN_NET: Byte = 0x5
    private const val P2PKH_TEST_NET: Byte = 0x6F
    private const val P2SH_TEST_NET = 0xC4.toByte()
    private val VALID_VERSIONS = byteArrayOf(
        P2PKH_MAIN_NET,
        P2SH_MAIN_NET,
        P2PKH_TEST_NET,
        P2SH_TEST_NET
    )

    /**
     * Determines if the address is of the regular type
     *
     * @return the result (boolean)
     */
    @Throws(NoSuchAlgorithmException::class)
    fun isRegularAddress(address: String): Boolean {
        return try {
            validateRegularAddress(address)
            true
        } catch (e: IllegalArgumentException) {
            false
        }
    }

    @Throws(NoSuchAlgorithmException::class)
    private fun validateRegularAddress(address: String) {
        val bytesValue = decode(address)
        require(bytesValue!!.size == 21) { "address isn't a valid regular address : length too big" }
        var versionValidated = false
        for (b in VALID_VERSIONS) {
            if (bytesValue[0] == b) {
                versionValidated = true
            }
        }
        require(versionValidated) { "address isn't a valid regular address : version byte invalid" }
    }


}