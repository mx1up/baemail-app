package app.bitcoin.baemail.util

import android.app.Activity
import android.app.Application
import android.os.Bundle

abstract class OnCreatedActivityLifecycleListener : Application.ActivityLifecycleCallbacks {
    override fun onActivityPaused(activity: Activity?) {
        //do nothing
    }

    override fun onActivityResumed(activity: Activity?) {
        //do nothing
    }

    override fun onActivityStarted(activity: Activity?) {
        //do nothing
    }

    override fun onActivityDestroyed(activity: Activity?) {
        //do nothing
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        //do nothing
    }

    override fun onActivityStopped(activity: Activity?) {
        //do nothing
    }

}