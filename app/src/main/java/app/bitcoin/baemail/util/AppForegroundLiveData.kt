package app.bitcoin.baemail.util

import android.os.Looper
import androidx.lifecycle.*

class AppForegroundLiveData : MutableLiveData<Boolean> {

    private constructor() : super() {
        register()

        val isStarted = ProcessLifecycleOwner.get().lifecycle.currentState
            .isAtLeast(Lifecycle.State.STARTED)

        if (Looper.myLooper() == Looper.getMainLooper()) {
            value = isStarted
        } else {
            postValue(isStarted)
        }
    }

    companion object {

        private var liveData: AppForegroundLiveData? = null

        fun get(): AppForegroundLiveData {
            liveData?.let {
                return it
            }

            AppForegroundLiveData().let {
                liveData = it
                return it
            }
        }
    }

    private fun register() {
        ProcessLifecycleOwner.get().lifecycle.addObserver(ForegroundObserver())
    }

    inner class ForegroundObserver : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onBackground() {
            value = false
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onForeground() {
            value = true
        }
    }
}