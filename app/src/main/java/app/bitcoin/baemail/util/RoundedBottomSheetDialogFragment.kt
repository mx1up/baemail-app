package app.bitcoin.baemail.util

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import app.bitcoin.baemail.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import timber.log.Timber

abstract class RoundedBottomSheetDialogFragment : BottomSheetDialogFragment() {

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dialog!!.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

            var peekHeight = getPeekHeight()
            if (peekHeight != -1) {
                if (peekHeight > bottomSheet.height || peekHeight < 100) {
                    Timber.w("#getPeekHeight() returned an invalid value, overriding it")
                    peekHeight = bottomSheet.height
                }

                bottomSheetBehavior.peekHeight = peekHeight
            }
            bottomSheetBehavior.skipCollapsed = true
        }
    }

    abstract fun getPeekHeight(): Int
}