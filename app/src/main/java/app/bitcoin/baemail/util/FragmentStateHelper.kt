package app.bitcoin.baemail.util

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import timber.log.Timber

class FragmentStateHelper(private val fragmentManager: FragmentManager) {
    private val savedStates = mutableMapOf<String, Fragment.SavedState?>()

    fun saveState(fragment: Fragment, tab: String) {
        // We can't save the state of a Fragment that isn't added to a FragmentManager.
        if (fragment.isAdded) {
            savedStates[tab] = fragmentManager.saveFragmentInstanceState(fragment)
        } else {
            Timber.w("state not saved because fragment.isAdded==true")
        }
    }

    fun restoreState(fragment: Fragment, tab: String): Boolean {
        savedStates[tab]?.let { savedState ->
            // We can't set the initial saved state if the Fragment is already added
            // to a FragmentManager, since it would then already be created.
            if (!fragment.isAdded) {
                fragment.setInitialSavedState(savedState)
                return true
            } else {
                Timber.w("state not restored because fragment.isAdded==true")
            }
        }
        return false
    }

    fun saveHelperState(): Bundle {
        val state = Bundle()

        savedStates.forEach { (key, fragmentState) ->
            state.putParcelable(key, fragmentState)
        }

        return state
    }

    fun restoreHelperState(savedState: Bundle) {
        savedStates.clear()


        savedState.classLoader = this.javaClass.classLoader
        savedState.keySet().forEach { key ->
            savedStates[key] = savedState.getParcelable(key)
        }
    }
}