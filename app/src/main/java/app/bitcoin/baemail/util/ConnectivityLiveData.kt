package app.bitcoin.baemail.util


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import timber.log.Timber

class ConnectivityLiveData : MutableLiveData<ConnectivityLiveData.Model>() {

    private lateinit var appContext: Context

    private var connectivityManager: ConnectivityManager? = null

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            updateLiveData()
        }
    }

    override fun setValue(value_: Model?) {
        if (value_ == null) {
            throw IllegalStateException()
        }

        Timber.i("ConnectivityLiveData.updateLiveData(${value_.isConnected})")

        if (value_ == value) {
            Timber.i("not setting the value on live-data because it hasn't changed")
            return
        }

        super.setValue(value_)
    }

    fun initialize(context: Context) {
        appContext = context


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val connectivityManager = getConnectivityManager()


            connectivityManager.registerDefaultNetworkCallback(
                object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        Timber.i("onAvailable")
                        val model =
                            Model(
                                true
                            )

                        postValue(model)
                    }

                    override fun onLost(network: Network) {
                        Timber.i("onLost")
                        val model =
                            Model(
                                false
                            )

                        postValue(model)
                    }
                })
        } else {
            appContext.registerReceiver(
                receiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        }
        updateLiveData()
    }

    private fun updateLiveData() {
        val model =
            Model(isDeviceConnected())

        if (Looper.myLooper() == Looper.getMainLooper()) {
            value = model
        } else {
            postValue(model)
        }
    }

    @Synchronized
    fun getConnectivityManager(): ConnectivityManager {
        if (connectivityManager == null) {
            connectivityManager =
                appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }
        return connectivityManager!!
    }

    private fun isDeviceConnected(): Boolean {
        val activeNetworkInfo = getConnectivityManager().activeNetworkInfo

        return activeNetworkInfo != null &&
                activeNetworkInfo.isAvailable &&
                activeNetworkInfo.isConnected
    }

    data class Model(val isConnected: Boolean = false)
}