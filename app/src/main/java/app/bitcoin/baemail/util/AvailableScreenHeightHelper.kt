package app.bitcoin.baemail.util

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.*
import android.widget.PopupWindow
import androidx.lifecycle.*
import timber.log.Timber

class AvailableScreenHeightHelper {

    val availableHeightLD = MutableLiveData<Int>()

    private var popup = PopupWin()

    private val attachedLD = MutableLiveData<Boolean>()
    private val startedLD = MutableLiveData<Boolean>()

    private val mustBeShowingLD = MediatorLiveData<Boolean>()

    private var lifecycleOwner: LifecycleOwner? = null
    private var lifecycleObserver = object : DefaultLifecycleObserver {
        override fun onStart(owner: LifecycleOwner) {
            startedLD.value = true
        }

        override fun onStop(owner: LifecycleOwner) {
            startedLD.value = true
        }

        override fun onDestroy(owner: LifecycleOwner) {
            cleanUp()
        }
    }

    init {
        mustBeShowingLD.addSource(attachedLD) {
            refreshShowing()
        }

        mustBeShowingLD.addSource(startedLD) {
            refreshShowing()
        }

    }

    fun setup(
        context: Context,
        window: Window,
        lifecycleOwner: LifecycleOwner,
        getViewWithParentWindowToken: () -> View
    ) {
        getViewWithParentWindowToken().addOnAttachStateChangeListener(
            object : View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(v: View?) {
                    Timber.i("on-parent-attached")
                    attachedLD.value = true
                }

                override fun onViewDetachedFromWindow(v: View?) {
                    Timber.i("on-parent-detached")
                    attachedLD.value = false
                }

            })

        if (getViewWithParentWindowToken().isAttachedToWindow) {
            attachedLD.value = true
        }

        this.lifecycleOwner = lifecycleOwner
        lifecycleOwner.lifecycle.addObserver(lifecycleObserver)

        popup.setup(
            context,
            window,
            getViewWithParentWindowToken,
            mustBeShowingLD,
            availableHeightLD
        )
    }

    private fun refreshShowing() {
        val isAttached = attachedLD.value ?: return
        val isStarted = startedLD.value ?: return

        val mustShow = isAttached && isStarted
        if (mustBeShowingLD.value == mustShow) return

        mustBeShowingLD.value = mustShow
    }

    private fun cleanUp() {
        lifecycleOwner?.lifecycle?.removeObserver(lifecycleObserver)
        lifecycleOwner = null

        popup.cleanUp()
        popup = PopupWin()
    }


    class PopupWin {

        private lateinit var context: Context
        private lateinit var window: Window
        private lateinit var availableHeightLD: MutableLiveData<Int>
        private lateinit var getViewWithRightWindowToken: ()->View
        private var mustBeShowingLD: LiveData<Boolean>? = null

        private var contentView: View? = null
        private var popupWindow: PopupWindow? = null

        val mustBeShowingObserver = Observer<Boolean> {  mustShow ->
            mustShow ?: return@Observer

            if (mustShow) {
                showWindow()
            } else {
                hideWindow()
            }
        }

        fun setup(
            context: Context,
            window: Window,
            getViewWithParentWindowToken: () -> View,
            mustBeShowingLD: LiveData<Boolean>,
            availableHeightLD: MutableLiveData<Int>
        ) {
            this.context = context
            this.window = window
            this.getViewWithRightWindowToken = getViewWithParentWindowToken
            this.mustBeShowingLD = mustBeShowingLD
            this.availableHeightLD = availableHeightLD

            mustBeShowingLD.observeForever(mustBeShowingObserver)
        }

        fun cleanUp() {
            mustBeShowingLD!!.removeObserver(mustBeShowingObserver)
        }

        private fun showWindow() {
            Timber.d("showWindow")

            ensureView()
            ensureWindow()

            showPopup()
        }

        private fun hideWindow() {
            Timber.d("hideWindow")

            popupWindow.let {
                if (it == null || !it.isShowing) {
                    Timber.i("returned before calling dismiss; it: $it; showing: ${it?.isShowing}")
                    return
                }

                it.dismiss()
                Timber.i("dismissed")
            }

            contentView = null
            popupWindow = null
        }

        private fun ensureView(): View {
            contentView?.let {
                return it
            }

            val dp = getViewWithRightWindowToken().resources.displayMetrics.density

            val v = View(context)
            v.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            v.background = ColorDrawable(Color.RED)

            v.translationX = dp * 5

            v.addOnLayoutChangeListener { _, _, top, _, bottom, _, _, _, _ ->
                val height = bottom - top
                if (availableHeightLD.value == height) return@addOnLayoutChangeListener
                availableHeightLD.value = height
            }

            contentView = v

            return v
        }

        private fun ensureWindow() {
            if (contentView == null) throw RuntimeException()

            popupWindow?.let {
                return
            }

            val popup = PopupWindow(contentView)
            popup.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
            popup.inputMethodMode = PopupWindow.INPUT_METHOD_NEEDED

            popupWindow = popup

        }

        private fun showPopup() {
            val popup: PopupWindow = popupWindow.let {
                if (it == null) throw RuntimeException()
                it
            }

            if (popup.isShowing) {
                return
            }

            val dp = getViewWithRightWindowToken().resources.displayMetrics.density

            popup.width = (dp * 1).toInt()
            popup.height = WindowManager.LayoutParams.WRAP_CONTENT
            popup.showAtLocation(
                getViewWithRightWindowToken(),
                Gravity.END,
                0,
                0
            )
        }


    }

}