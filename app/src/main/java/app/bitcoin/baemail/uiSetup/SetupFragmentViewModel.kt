package app.bitcoin.baemail.uiSetup

import android.content.Context
import androidx.lifecycle.*
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.wallet.DynamicChainSync
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SetupFragmentViewModel @Inject constructor(
    val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val appDatabase: AppDatabase,
    val chainSync: DynamicChainSync
) : ViewModel() {

    private var coinsOfPaymailLD: LiveData<List<Coin>>? = null

    private val _contentLD = MediatorLiveData<Model>()
    val contentLD: LiveData<Model>
        get() = _contentLD

    init {

        _contentLD.addSource(paymailRepository.activePaymailLD, object : Observer<AuthenticatedPaymail> {
            private var previousPaymail: String? = null

            override fun onChanged(it: AuthenticatedPaymail?) {
//            val paymail = it?.paymail
//            paymail ?: let {
//                _contentLD.value = Model()
//                return@Observer
//            }
//
//            val name = paymail.substring(0, paymail.indexOf("@"))
//            _contentLD.value = Model(name, paymail)

                val paymail = it?.paymail

                if (previousPaymail == paymail) return //unchanged
                previousPaymail = paymail

                coinsOfPaymailLD?.let {
                    _contentLD.removeSource(it)
                }
                coinsOfPaymailLD = null

                if (paymail == null) return //todo consider clearing content

                val coinsLD = appDatabase.coinDao().getFundingCoinsLD(paymail)
                coinsOfPaymailLD = coinsLD

                _contentLD.addSource(coinsLD) {
                    refreshContent()
                }

            }

        })


    }

    private fun refreshContent() {
        viewModelScope.launch {
            val paymail = paymailRepository.activePaymailLD.value?.paymail ?: return@launch
            val dbCoins = coinsOfPaymailLD?.value ?: return@launch

            Timber.d("dbCoins size: ${dbCoins.size}")

            val totalSats = dbCoins.map {
                if (it.spendingTxId != null) return@map 0
                it.sats
            }.foldRight(0) { sats, acc ->
                acc + sats
            }

            val name = paymail.substring(0, paymail.indexOf("@"))
            _contentLD.value = Model(name, paymail, totalSats)
        }
    }


    data class Model(
        val name: String = "<unset name>",
        val paymail: String = "<unset paymail>",
        val sats: Int = -1
    )
}