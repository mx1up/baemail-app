package app.bitcoin.baemail.uiSetup

import android.content.Intent
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.fundingWallet.CoinActivity
import app.bitcoin.baemail.fundingWallet.SweepActivity
import app.bitcoin.baemail.fundingWallet.TopUpActivity
import app.bitcoin.baemail.paymail.PaymailActivity
import app.bitcoin.baemail.view.ViewProfileCard
import app.bitcoin.baemail.wallet.ChainSyncView
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class SetupFragment : Fragment(R.layout.fragment_setup) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: SetupFragmentViewModel

    lateinit var profileCard: ViewProfileCard
    lateinit var switchPaymail: TextView

    lateinit var topUpFundingWallet: TextView
    lateinit var sweepFundingWallet: TextView
    lateinit var fundingWalletFunds: TextView
    lateinit var fundingWalletCoins: TextView
    lateinit var chainSyncState: ChainSyncView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(SetupFragmentViewModel::class.java)

        profileCard = requireView().findViewById(R.id.profile_card)
        switchPaymail = requireView().findViewById(R.id.switch_paymail)
        topUpFundingWallet = requireView().findViewById(R.id.top_up_funding_wallet)
        sweepFundingWallet = requireView().findViewById(R.id.sweep_funding_wallet)
        fundingWalletFunds = requireView().findViewById(R.id.available_funds)
        fundingWalletCoins = requireView().findViewById(R.id.funding_wallet_coins)
        chainSyncState = requireView().findViewById(R.id.chain_sync_state)

//        val profileModel = ViewProfileCard.Model(
//            ContextCompat.getDrawable(requireContext(), R.drawable.profile_image_machete)!!,
//            "Mr. Machete",
//            "mr.machete@paymail.com"
//        )
//
//        profileCard.applyModel(profileModel)


        val separatorColor = R.color.setup_fragment__item_separator_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        val separatorPaddingSides = R.dimen.setup_fragment__item_separator_padding_sides.let { id ->
            resources.getDimensionPixelSize(id)
        }

        val separatorWidth = R.dimen.setup_fragment__item_separator_width.let { id ->
            resources.getDimensionPixelSize(id)
        }

        val colorSelector = R.color.setup_fragment__selector_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }





        val drawableProfileSeparator = DrawableStroke().also {
            val color = R.color.setup_fragment__profile_separator_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            val paddingSides = R.dimen.setup_fragment__profile_separator_padding_sides.let { id ->
                resources.getDimensionPixelSize(id)
            }

            val width = R.dimen.setup_fragment__profile_separator_width.let { id ->
                resources.getDimensionPixelSize(id)
            }

            it.setPadding(paddingSides, 0, paddingSides, 0)
            it.setStrokeColor(color)
            it.setStrokeWidths(0, 0, 0, width)
        }
        profileCard.background = drawableProfileSeparator







        switchPaymail.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        switchPaymail.setOnClickListener {
            startActivity(Intent(requireActivity(), PaymailActivity::class.java))
        }




        topUpFundingWallet.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        topUpFundingWallet.setOnClickListener {
            startActivity(Intent(requireActivity(), TopUpActivity::class.java))
        }





        sweepFundingWallet.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        sweepFundingWallet.setOnClickListener {
            startActivity(Intent(requireActivity(), SweepActivity::class.java), null)
        }




        fundingWalletCoins.background = LayerDrawable(arrayOf(
            DrawableStroke().also {
                it.setStrokeColor(separatorColor)
                it.setStrokeWidths(0, 0, 0, separatorWidth)
                it.setPadding(separatorPaddingSides, 0, separatorPaddingSides, 0)
            },
            DrawableState.getNew(colorSelector)
        ))

        fundingWalletCoins.setOnClickListener {
            startActivity(Intent(requireActivity(), CoinActivity::class.java), null)
        }








        viewModel.contentLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            val profileModel = ViewProfileCard.Model(
                ContextCompat.getDrawable(requireContext(), R.drawable.profile_image_machete)!!,
                it.name,
                it.paymail
            )

            profileCard.applyModel(profileModel)


            fundingWalletFunds.text = "${it.sats} sats" //todo localize
        })

        chainSyncState.setup(viewModel.chainSync, this)
    }
}