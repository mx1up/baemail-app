package app.bitcoin.baemail.room.entity

import androidx.room.Entity
import app.bitcoin.baemail.room.TABLE_COINS

@Entity(tableName = TABLE_COINS, primaryKeys = ["address", "txId", "outputIndex"])
data class Coin(
    val paymail: String,
    val derivationPath: String,
    val pathIndex: Int,
    val address: String,
    val txId: String,
    val outputIndex: Int,
    val sats: Int,
    val spendingTxId: String?
)