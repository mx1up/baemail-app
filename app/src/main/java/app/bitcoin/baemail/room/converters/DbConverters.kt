package app.bitcoin.baemail.room.converters

import androidx.room.TypeConverter
import app.bitcoin.baemail.message.MessageBucket
import com.google.gson.Gson
import com.google.gson.JsonArray

class DbConverters {

    val gson: Gson by lazy {
        Gson()
    }

//    @TypeConverter
//    fun serialiseProvider(provider: PaymailProvider): String {
//        return provider.const
//    }
//
//    @TypeConverter
//    fun parseProvider(providerConst: String): PaymailProvider {
//        return PaymailProvider.fromConst(providerConst)
//    }


    @TypeConverter
    fun serializeStringList(list: List<String>): String {
        val jsonList = JsonArray()
        list.forEach {
            jsonList.add(it)
        }

        return jsonList.toString()
    }

    @TypeConverter
    fun parseStringList(serialized: String): List<String> {
        val jsonList = gson.fromJson(serialized, JsonArray::class.java)
        val list = mutableListOf<String>()

        (0 until jsonList.size()).forEach {
            list.add(jsonList.get(it).asString)
        }

        return list
    }

    @TypeConverter
    fun serializeMessageBucket(bucket: MessageBucket): String {
        return bucket.const
    }

    @TypeConverter
    fun parseMessageBucket(serialized: String): MessageBucket {
        return MessageBucket.parse(serialized)
    }

}