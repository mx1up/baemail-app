package app.bitcoin.baemail.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.room.TABLE_PAYMAILS
import app.bitcoin.baemail.room.entity.Paymail

@Dao
abstract class PaymailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPaymail(bookmark: Paymail)

    @Query("select * from $TABLE_PAYMAILS")
    abstract fun getAddedPaymails(): LiveData<List<Paymail>>
}