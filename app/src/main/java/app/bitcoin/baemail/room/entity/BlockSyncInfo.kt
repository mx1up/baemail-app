package app.bitcoin.baemail.room.entity

import androidx.room.Entity
import app.bitcoin.baemail.room.TABLE_BLOCK_SYNC_INFO

@Entity(tableName = TABLE_BLOCK_SYNC_INFO, primaryKeys = ["paymail", "height"])
data class BlockSyncInfo (
    val height: Int,
    val paymail: String,
    val time: Long,
    val hash: String,
    val merkleRoot: String,
    val previousBlockHash: String
)