package app.bitcoin.baemail.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.room.TABLE_DUMMIES
import app.bitcoin.baemail.room.entity.DummyDb

@Dao
interface DummiesDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDummy(bookmark: DummyDb)

    @Query("select * from $TABLE_DUMMIES")
    fun getAllDummies(): LiveData<List<DummyDb>>


}