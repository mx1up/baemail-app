package app.bitcoin.baemail.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.message.MessageBucket
import app.bitcoin.baemail.room.TABLE_DECRYPTED_MESSAGE
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import kotlinx.coroutines.flow.Flow

@Dao
abstract class DecryptedBaemailMessageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertMessage(message: DecryptedBaemailMessage)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertList(list: List<DecryptedBaemailMessage>)

    @Query("""
        select * 
        from $TABLE_DECRYPTED_MESSAGE 
        where bucket = :b and ownerPaymail = :ownerPaymail and 
            (decryptedMessage like :query or `to` like :query or subject like :query) 
        order by 
            case when :byTimeNotValue = 1 then createdAt end desc,
            case when :byTimeNotValue = 0 then numAmountUsd end desc
        limit :limit
        """)
    abstract fun queryByBucketAndOwner(
        b: MessageBucket,
        ownerPaymail: String,
        query: String,
        byTimeNotValue: Boolean,
        limit: Int
    ): Flow<List<DecryptedBaemailMessage>>

    @Query("""
        select * 
        from $TABLE_DECRYPTED_MESSAGE 
        where bucket = "inbox" and ownerPaymail = :ownerPaymail and destinationSeen = 0 and
            (decryptedMessage like :query or `to` like :query or subject like :query) 
        order by numAmountUsd
        limit :limit
        """)
    abstract fun queryUnreadByOwner(
        ownerPaymail: String,
        query: String,
        limit: Int
    ): Flow<List<DecryptedBaemailMessage>>

    @Query("select * from $TABLE_DECRYPTED_MESSAGE where ownerPaymail = :ownerPaymail and txId in (:messageIds)")
    abstract suspend fun queryMessagesByOwnerAndTxId(
        ownerPaymail: String,
        messageIds: List<String>
    ): List<DecryptedBaemailMessage>

    @Query("select * from $TABLE_DECRYPTED_MESSAGE where ownerPaymail = :ownerPaymail and txId = :txId")
    abstract fun observeMessageByOwnerAndTxId(
        ownerPaymail: String,
        txId: String
    ): LiveData<DecryptedBaemailMessage>



    @Query("delete from $TABLE_DECRYPTED_MESSAGE where bucket = :b and ownerPaymail = :ownerPaymail")
    abstract suspend fun deleteByBucketAndOwner(b: MessageBucket, ownerPaymail: String)

    @Query("delete from $TABLE_DECRYPTED_MESSAGE where ownerPaymail = :ownerPaymail")
    abstract suspend fun deleteAllByOwner(ownerPaymail: String)

    @Query("delete from $TABLE_DECRYPTED_MESSAGE where ownerPaymail = :ownerPaymail and txId = :txId")
    abstract suspend fun deleteMessageForOwner(
        ownerPaymail: String,
        txId: String
    )




    @Query("update $TABLE_DECRYPTED_MESSAGE set destinationSeen = 1 where ownerPaymail = :ownerPaymail and txId = :txId")
    abstract suspend fun updateMessageAsSeen(
        ownerPaymail: String,
        txId: String
    )


    @Query("update $TABLE_DECRYPTED_MESSAGE set bucket = :b where ownerPaymail = :ownerPaymail and txId = :txId")
    abstract fun updateMessageBucket(
        ownerPaymail: String,
        txId: String,
        b: MessageBucket
    )


}