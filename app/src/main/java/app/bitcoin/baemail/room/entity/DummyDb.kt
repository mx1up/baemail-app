package app.bitcoin.baemail.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import app.bitcoin.baemail.room.TABLE_DUMMIES

@Entity(tableName = TABLE_DUMMIES)
data class DummyDb(
    @PrimaryKey val id: String
)