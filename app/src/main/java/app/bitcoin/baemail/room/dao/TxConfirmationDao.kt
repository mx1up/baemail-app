package app.bitcoin.baemail.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.room.TABLE_COINS
import app.bitcoin.baemail.room.TABLE_TX_CONFIRMATION
import app.bitcoin.baemail.room.entity.TxConfirmation

@Dao
abstract class TxConfirmationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertConfirmationInfo(info: TxConfirmation)

    @Query("select * from $TABLE_TX_CONFIRMATION where txHash = :txHash")
    abstract suspend fun getByTxHash(txHash: String): TxConfirmation?

    @Query("select * from $TABLE_TX_CONFIRMATION inner join $TABLE_COINS on $TABLE_TX_CONFIRMATION.txHash = $TABLE_COINS.txId or $TABLE_TX_CONFIRMATION.txHash = $TABLE_COINS.spendingTxId where $TABLE_COINS.paymail = :paymail order by $TABLE_TX_CONFIRMATION.blockHeight desc")
    abstract suspend fun getForPaymail(paymail: String): List<TxConfirmation>

    @Query("select * from $TABLE_TX_CONFIRMATION inner join $TABLE_COINS on $TABLE_TX_CONFIRMATION.txHash = $TABLE_COINS.txId or $TABLE_TX_CONFIRMATION.txHash = $TABLE_COINS.spendingTxId where $TABLE_COINS.paymail = :paymail and $TABLE_TX_CONFIRMATION.blockHeight = -1 order by $TABLE_TX_CONFIRMATION.blockHeight desc")
    abstract suspend fun getUnconfirmedForPaymail(paymail: String): List<TxConfirmation>

    //todo only delete those belonging to a specific paymail
    @Query("delete from $TABLE_TX_CONFIRMATION where blockHeight >= :includingAndUpHeight")
    abstract suspend fun clearForHeight(includingAndUpHeight: Int)

    @Query("delete from $TABLE_TX_CONFIRMATION where txHash = :txId")
    abstract suspend fun clearConfirmation(txId: String)

}