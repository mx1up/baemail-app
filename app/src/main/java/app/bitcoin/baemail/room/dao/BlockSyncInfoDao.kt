package app.bitcoin.baemail.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.room.TABLE_BLOCK_SYNC_INFO
import app.bitcoin.baemail.room.TABLE_TX_CONFIRMATION
import app.bitcoin.baemail.room.entity.BlockSyncInfo

@Dao
abstract class BlockSyncInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertInfo(info: BlockSyncInfo)

    @Query("select * from $TABLE_BLOCK_SYNC_INFO where paymail = :paymail order by height desc limit 1")
    abstract suspend fun getLatestInfo(paymail: String): BlockSyncInfo?

    @Query("delete from $TABLE_BLOCK_SYNC_INFO where height >= :includingAndUpHeight and paymail = :paymail")
    abstract suspend fun clearForHeight(includingAndUpHeight: Int, paymail: String)

}