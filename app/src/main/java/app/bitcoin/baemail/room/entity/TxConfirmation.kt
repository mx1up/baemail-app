package app.bitcoin.baemail.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import app.bitcoin.baemail.room.TABLE_TX_CONFIRMATION

@Entity(tableName = TABLE_TX_CONFIRMATION)
data class TxConfirmation(
    @PrimaryKey
    val txHash: String,
    val blockHeight: Int,
    val blockHash: String?,
    val blockTime: Long
)