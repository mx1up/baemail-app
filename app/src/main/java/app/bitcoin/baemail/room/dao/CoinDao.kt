package app.bitcoin.baemail.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.bitcoin.baemail.room.TABLE_COINS
import app.bitcoin.baemail.room.entity.Coin

@Dao
abstract class CoinDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertCoin(coin: Coin)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertList(coins: List<Coin>)

    @Query("select * from $TABLE_COINS where address = :address and txId = :txId and outputIndex = :outputIndex")
    abstract suspend fun getCoin(address: String, txId: String, outputIndex: Int): Coin?

    @Query("select * from $TABLE_COINS where paymail = :paymail")
    abstract suspend fun getFundingCoins(paymail: String): List<Coin>

    @Query("select * from $TABLE_COINS where paymail = :paymail")
    abstract fun getFundingCoinsLD(paymail: String): LiveData<List<Coin>>

    @Query("select * from $TABLE_COINS where paymail = :paymail and spendingTxId is null order by pathIndex desc")
    abstract fun getUnspentFundingCoinLD(paymail: String): LiveData<List<Coin>>

    @Query("select * from $TABLE_COINS where paymail = :paymail and spendingTxId is null order by pathIndex desc")
    abstract suspend fun getUnspentFundingCoins(paymail: String): List<Coin>

    @Query("select * from $TABLE_COINS where paymail = :paymail order by pathIndex desc limit 1")
    abstract suspend fun getHighestSpentFundingCoin(paymail: String): Coin?

    @Query("select * from $TABLE_COINS where address = :address")
    abstract suspend fun getCoinsForAddress(address: String): List<Coin>

    @Query("select * from $TABLE_COINS where txId = :txId or spendingTxId = :txId")
    abstract suspend fun getCoinsWithTxId(txId: String): List<Coin>

    @Query("delete from $TABLE_COINS")
    abstract fun deleteAll()

    @Query("delete from $TABLE_COINS where txId = :txId")
    abstract suspend fun deleteCoinWithId(txId: String)

    @Query("update $TABLE_COINS set spendingTxId = null where spendingTxId = :txId")
    abstract suspend fun clearSpendingOfCoin(txId: String)

}