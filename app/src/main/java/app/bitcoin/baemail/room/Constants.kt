package app.bitcoin.baemail.room

const val APP_DB_NAME = "app_database"
const val TABLE_DUMMIES = "dummies"
const val TABLE_PAYMAILS = "paymails"
const val TABLE_COINS = "coins"
const val TABLE_TX_CONFIRMATION = "tx_confirmation"
const val TABLE_BLOCK_SYNC_INFO = "block_sync_info"
const val TABLE_DECRYPTED_MESSAGE = "decrypted_message"


const val PAYMAIL_PROVIDER_MONEYBUTTON = "moneybutton"
const val PAYMAIL_PROVIDER_RELAYX = "relayx"