package app.bitcoin.baemail.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import app.bitcoin.baemail.room.converters.DbConverters
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.room.dao.*
import app.bitcoin.baemail.room.entity.*

@Database(entities = [
    DummyDb::class,
    Paymail::class,
    Coin::class,
    TxConfirmation::class,
    BlockSyncInfo::class,
    DecryptedBaemailMessage::class
], version = 1)
@TypeConverters(DbConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun dummiesDao(): DummiesDao

    abstract fun paymailDao(): PaymailDao

    abstract fun coinDao(): CoinDao

    abstract fun blockSyncInfoDao(): BlockSyncInfoDao

    abstract fun txConfirmationDao(): TxConfirmationDao

    abstract fun decryptedBaemailMessageDao(): DecryptedBaemailMessageDao

}