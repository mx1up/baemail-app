package app.bitcoin.baemail.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.recycler.*

class ATContentLoadingViewHolder(
    view: View
): BaseViewHolder(view) {

    private lateinit var listener: BaseListAdapter.ATContentLoadingListener

    var model: ATContentLoading0? = null

    init {
        itemView.setOnClickListener { listener.onBound() }
    }



    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATContentLoading0

        this.listener = listener!! as BaseListAdapter.ATContentLoadingListener

        this.listener.onBound()

    }

    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATContentLoadingViewHolder {
            return ATContentLoadingViewHolder(
                inflater.inflate(
                    R.layout.li_content_loading,
                    parent,
                    false
                )
            )
        }
    }
}