package app.bitcoin.baemail.recycler

import android.view.View
import app.bitcoin.baemail.recycler.lifecycle.LifecycleViewHolder

abstract class BaseViewHolder(itemView: View) : LifecycleViewHolder(itemView) {
    abstract fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?)
}