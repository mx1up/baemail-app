package app.bitcoin.baemail.recycler.lifecycle

import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView

abstract class LifecycleRecyclerAdapter<T : LifecycleViewHolder> : RecyclerView.Adapter<T>() {

    lateinit var parentLifecycle: Lifecycle

    override fun onViewAttachedToWindow(holder: T) {
        super.onViewAttachedToWindow(holder)
        holder.onAppear()
    }

    override fun onViewDetachedFromWindow(holder: T) {
        super.onViewDetachedFromWindow(holder)
        holder.onDisappear()
    }

    override fun onBindViewHolder(holder: T, position: Int, payloads: MutableList<Any>) {
        holder.onBind(parentLifecycle)
        super.onBindViewHolder(holder, position, payloads)
    }


    override fun onViewRecycled(holder: T) {
        holder.onRecycled()
    }
}