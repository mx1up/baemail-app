package app.bitcoin.baemail.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.recycler.*

class ATSeedWordHintViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATSeedWordHint? = null

    val hint: TextView = itemView.findViewById(R.id.hint)

    init {
        //todo
        //todo
        //todo
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATSeedWordHint

        hint.text = model.value
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATSeedWordHintViewHolder {
            return ATSeedWordHintViewHolder(
                inflater.inflate(
                    R.layout.li_seed_word_hint,
                    parent,
                    false
                )
            )
        }
    }
}