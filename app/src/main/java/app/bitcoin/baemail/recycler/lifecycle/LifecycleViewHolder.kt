package app.bitcoin.baemail.recycler.lifecycle

import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

@Suppress("ConstantConditionIf")
abstract class LifecycleViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView), LifecycleOwner {

    companion object {
        const val isDebugging = false
    }

    private var registry: LifecycleRegistry? = null

    private val lifecycleRegistry: LifecycleRegistry
        get() {
            if (registry == null) {
                registry = LifecycleRegistry(this)
            }
            return registry!!
        }

    private var parentLifecycle: Lifecycle? = null
    private var parentLifecycleObserver = ParentLifecycleObserver()

    private var maxLifecycleOfHolder: Lifecycle.State = Lifecycle.State.INITIALIZED

    init {
        maxLifecycleOfHolder = Lifecycle.State.INITIALIZED
        lifecycleRegistry.currentState = maxLifecycleOfHolder
    }

    fun onBind(parentLifecycle: Lifecycle) {

        if (!maxLifecycleOfHolder.isAtLeast(Lifecycle.State.STARTED)) {
            if (isDebugging) Timber.i("setting >> maxLifecycleOfHolder = CREATED")
            maxLifecycleOfHolder = Lifecycle.State.CREATED
        } else {
            if (isDebugging) Timber.i("current maxLifecycleOfHolder = ${maxLifecycleOfHolder.name}")
        }

        if (this.parentLifecycle == null) {
            if (isDebugging) Timber.i("parentLifecycle was NULL; adding observer")
            this.parentLifecycle = parentLifecycle

            parentLifecycle.addObserver(parentLifecycleObserver)
            //the passed in observer will internally call #bringHolderLifecycleToMaxState()
        } else {
            if (isDebugging) Timber.i("parentLifecycle was not null; refreshing lifecycle")
            bringHolderLifecycleToMaxState()
        }
    }

    @CallSuper
    open fun onAppear() {
        maxLifecycleOfHolder = Lifecycle.State.RESUMED
        bringHolderLifecycleToMaxState()
    }

    fun onDisappear() {
        maxLifecycleOfHolder = Lifecycle.State.CREATED
        bringHolderLifecycleToMaxState()
    }

    fun onRecycled() {
        maxLifecycleOfHolder = Lifecycle.State.DESTROYED
        bringHolderLifecycleToMaxState()
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    private fun bringHolderLifecycleToMaxState() {
        val finalState = if (maxLifecycleOfHolder.isAtLeast(parentLifecycleObserver.maxLifecycle)) {
            parentLifecycleObserver.maxLifecycle
        } else {
            maxLifecycleOfHolder
        }

//        Timber.i("maxLifecycleOfHolder:${maxLifecycleOfHolder.name} " +
//                "parentLifecycleObserver.maxLifecycle:${parentLifecycleObserver.maxLifecycle.name} " +
//                "finalState: ${finalState.name}")

        lifecycleRegistry.currentState = finalState
    }

    @Suppress("unused")
    inner class ParentLifecycleObserver : LifecycleObserver {

        var maxLifecycle = Lifecycle.State.CREATED

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart() {
            maxLifecycle = Lifecycle.State.STARTED

            bringHolderLifecycleToMaxState()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
            maxLifecycle = Lifecycle.State.RESUMED

            bringHolderLifecycleToMaxState()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            maxLifecycle = Lifecycle.State.STARTED

            bringHolderLifecycleToMaxState()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
            maxLifecycle = Lifecycle.State.CREATED

            bringHolderLifecycleToMaxState()
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            if (isDebugging) Timber.i("parent destroyed")
            val lc = parentLifecycle ?: return

            lc.removeObserver(this)
            parentLifecycle = null


            maxLifecycle = Lifecycle.State.CREATED
            bringHolderLifecycleToMaxState()
        }
    }
}