package app.bitcoin.baemail.recycler.holder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.bitcoin.baemail.R
import app.bitcoin.baemail.recycler.*

class ATNoPaymailsAddedViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    var model: ATNoPaymailsAdded? = null


    init {
        //todo
        //todo
        //todo
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATNoPaymailsAdded
    }




    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATNoPaymailsAddedViewHolder {
            return ATNoPaymailsAddedViewHolder(
                inflater.inflate(
                    R.layout.li_no_paymails_added,
                    parent,
                    false
                )
            )
        }
    }
}