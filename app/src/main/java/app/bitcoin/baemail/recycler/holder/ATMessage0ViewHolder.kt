package app.bitcoin.baemail.recycler.holder

import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.recycler.*
import timber.log.Timber
import kotlin.math.min

class ATMessage0ViewHolder(
    itemView: View
) : BaseViewHolder(itemView) {

    lateinit var listener: BaseListAdapter.ATMessageListener

    var model: ATMessage0? = null

    val rootView: FrameLayout = itemView.findViewById(R.id.li_root)
    val content: ConstraintLayout = itemView.findViewById(R.id.content)
    val actionsEnd: LinearLayout = itemView.findViewById(R.id.actions_end)
    val actionsStart: LinearLayout = itemView.findViewById(R.id.actions_start)

    val actionA1: ImageView = itemView.findViewById(R.id.action_a1)
    val actionA2: ImageView = itemView.findViewById(R.id.action_a2)

    val actionB1: ImageView = itemView.findViewById(R.id.action_b1)
    val actionB2: ImageView = itemView.findViewById(R.id.action_b2)
    val actionB3: ImageView = itemView.findViewById(R.id.action_b3)

    val amount: TextView = itemView.findViewById(R.id.amount)
    val date: TextView = itemView.findViewById(R.id.date)
    val from: TextView = itemView.findViewById(R.id.from)
    val subject: TextView = itemView.findViewById(R.id.subject)
    val preview: TextView = itemView.findViewById(R.id.preview)
    val isAttachment: ImageView = itemView.findViewById(R.id.is_attachment)
    val isUnread: ImageView = itemView.findViewById(R.id.is_unread)

    val drawableSeparator = R.color.message_0__separator_color.let {
        val color = ContextCompat.getColor(itemView.context, it)
        val paddingSides = itemView.resources
            .getDimensionPixelSize(R.dimen.message_0__separator_padding_sides)
        val width = itemView.resources.getDimensionPixelSize(R.dimen.message_0__separator_width)

        val d = DrawableStroke()
        d.setPadding(paddingSides, 0, paddingSides, 0)
        d.setStrokeColor(color)
        d.setStrokeWidths(0, 0, 0, width)

        d
    }

    val theme = itemView.resources.newTheme()

    val colorActionSelector = ContextCompat.getColor(itemView.context, R.color.selector_on_light)

    val layoutChangeListener = object : View.OnLayoutChangeListener {
        override fun onLayoutChange(
            v: View?,
            left: Int,
            top: Int,
            right: Int,
            bottom: Int,
            oldLeft: Int,
            oldTop: Int,
            oldRight: Int,
            oldBottom: Int
        ) {
            reactToExpandedFlag()
        }

    }

    init {

        itemView.addOnLayoutChangeListener(layoutChangeListener)

        //todo
        //todo
        //todo


        rootView.foreground = drawableSeparator


        R.dimen.message_0__is_attachment_scale.let {
            val value = TypedValue()
            itemView.resources.getValue(it, value, true)
            val scale = value.float

            isAttachment.scaleX = scale
            isAttachment.scaleY = scale
        }


        isUnread.clipToOutline = true
        isUnread.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val side = min(view.width, view.height)
                outline.setRoundRect(0, 0, view.width, view.height, side / 2f)
                //outline.setOval(0, 0, side, side)
            }

        }

        initActionButton(
            actionA1,
            R.color.message_0__actions_a1_color,
            R.drawable.message_0__actions_a1_icon
        )
        actionA1.clipToOutline = true
        actionA1.outlineProvider = VerticallyCenteredCircleOutline()
        actionA1.setOnClickListener {
            listener.onActionClicked(
                adapterPosition,
                model!!,
                BaseListAdapter.ATMessageListener.ACTION_START_0
            )
        }

        initActionButton(
            actionA2,
            R.color.message_0__actions_a2_color,
            R.drawable.message_0__actions_a2_icon
        )
        actionA2.clipToOutline = true
        actionA2.outlineProvider = VerticallyCenteredCircleOutline()
        actionA2.setOnClickListener {
            listener.onActionClicked(
                adapterPosition,
                model!!,
                BaseListAdapter.ATMessageListener.ACTION_START_1
            )
        }

        initActionButton(
            actionB1,
            R.color.message_0__actions_b1_color,
            R.drawable.message_0__actions_b1_icon
        )
        actionB1.clipToOutline = true
        actionB1.outlineProvider = VerticallyCenteredCircleOutline()
        actionB1.setOnClickListener {
            listener.onActionClicked(
                adapterPosition,
                model!!,
                BaseListAdapter.ATMessageListener.ACTION_END_0
            )
        }

        initActionButton(
            actionB2,
            R.color.message_0__actions_b2_color,
            R.drawable.message_0__actions_b2_icon
        )
        actionB2.clipToOutline = true
        actionB2.outlineProvider = VerticallyCenteredCircleOutline()
        actionB2.setOnClickListener {
            listener.onActionClicked(
                adapterPosition,
                model!!,
                BaseListAdapter.ATMessageListener.ACTION_END_1
            )
        }

        initActionButton(
            actionB3,
            R.color.message_0__actions_b3_color,
            R.drawable.message_0__actions_b3_icon
        )
        actionB3.clipToOutline = true
        actionB3.outlineProvider = VerticallyCenteredCircleOutline()
        actionB3.setOnClickListener {
            listener.onActionClicked(
                adapterPosition,
                model!!,
                BaseListAdapter.ATMessageListener.ACTION_END_2
            )
        }

        content.setOnClickListener {
            listener.onItemClicked(adapterPosition, model!!)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            content.background = ColorDrawable(ContextCompat.getColor(itemView.context, R.color.message_0__background))
            content.foreground = DrawableState.getNew(colorActionSelector)
        } else {
            content.background = LayerDrawable(arrayOf(
                ColorDrawable(ContextCompat.getColor(itemView.context, R.color.message_0__background)),
                DrawableState.getNew(colorActionSelector)
            ))
        }

    }

    private fun initActionButton(
        actionButton: ImageView,
        @ColorRes backgroundColorResId: Int,
        @DrawableRes iconResId: Int
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            actionButton.setBackgroundColor(
                ContextCompat.getColor(itemView.context, backgroundColorResId))
            actionButton.setImageDrawable(itemView.resources.getDrawable(iconResId, theme))
            actionButton.foreground = DrawableState.getNew(colorActionSelector)
        } else {
            actionButton.background = LayerDrawable(arrayOf(
                ColorDrawable(ContextCompat.getColor(itemView.context, backgroundColorResId)),
                DrawableState.getNew(colorActionSelector)
            ))
            actionButton.setImageDrawable(itemView.resources.getDrawable(iconResId, theme))
        }
    }

    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        Timber.d("#onBindViewHolder pos:${absoluteAdapterPosition}")

        this.listener = listener!! as BaseListAdapter.ATMessageListener

        this.model = model as ATMessage0

        date.text = model.dateLabel
        from.text = model.from
        subject.text = model.subject
        preview.text = model.preview

        if (model.isUnread) {
            isUnread.visibility = View.VISIBLE
        } else {
            isUnread.visibility = View.INVISIBLE
        }

        if (model.hasAttachment) {
            isAttachment.visibility = View.VISIBLE
        } else {
            isAttachment.visibility = View.INVISIBLE

        }

        model.message?.amountUsd?.let {
            amount.text = "${it}USD"
        }

        if (itemView.isLaidOut) {
            reactToExpandedFlag()
        }
    }

    private fun reactToExpandedFlag() {
        val m = model ?: return

        if (m.isRightActionExpanded) {
            content.translationX = -1f * actionsEnd.measuredWidth
        } else if (m.isLeftActionExpanded) {
            content.translationX = actionsStart.measuredWidth.toFloat()
        } else {
            content.translationX = 0f
        }
    }

    fun getRightSwipeActionWidth(): Int {
        if (!itemView.isLaidOut) return SwipeController.WIDTH_MAX
        return actionsEnd.measuredWidth
    }

    fun getLeftSwipeActionWidth(): Int {
        if (!itemView.isLaidOut) return SwipeController.WIDTH_MAX
        return actionsStart.measuredWidth
    }

    fun onSwipedAmount(dX: Float, dY: Float) {
        if (LOG) Timber.d("#onSwipedAmount dX:$dX")
        if (dX == 0f) return
        val m = model ?: return

        if (m.isRightActionExpanded) {
            val startDx = -1f * getRightSwipeActionWidth()

            if (dX > 0) {
                content.translationX = startDx + dX
            } else if (dX <= 0) {
                content.translationX = startDx
            }

        } else if (m.isLeftActionExpanded) {
            val startDx = getLeftSwipeActionWidth()

            if (dX > 0) {
                content.translationX = startDx.toFloat()
            } else if (dX <= 0) {
                content.translationX = startDx + dX
            }

        } else {

            if (dX > 0) {
                content.translationX = Math.min(dX, getLeftSwipeActionWidth().toFloat())
            } else if (dX <= 0) {
                content.translationX = Math.max(dX, -1f * getRightSwipeActionWidth())
            }
        }

    }


    class VerticallyCenteredCircleOutline : ViewOutlineProvider() {
        var insetPx: Int? = null

        private fun getInset(view: View): Int {
            return insetPx ?: let {
                //val px = (view.resources.displayMetrics.density * 3).toInt()
                val px = view.resources.getDimensionPixelSize(R.dimen.message_0__actions_inset)
                insetPx = px
                px
            }
        }

        override fun getOutline(view: View?, outline: Outline?) {
            view ?: return
            outline ?: return

            val side = min(view.width, view.height)
            val topY = ((view.height / 2) - 0.5f * side).toInt()

            val insetPx = getInset(view)

            outline.setOval(0 + insetPx, topY + insetPx, side - insetPx, topY + side - insetPx)
        }

    }

    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup
        ): ATMessage0ViewHolder {
            return ATMessage0ViewHolder(
                inflater.inflate(
                    R.layout.li_message_0,
                    parent,
                    false
                )
            )
        }
    }

}