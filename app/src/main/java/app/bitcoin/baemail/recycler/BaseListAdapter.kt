package app.bitcoin.baemail.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.recycler.holder.*
import app.bitcoin.baemail.recycler.lifecycle.LifecycleRecyclerAdapter
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.wallet.request.DerivedAddress
import timber.log.Timber
import java.lang.RuntimeException

class BaseListAdapter(
    lifecycle: Lifecycle,
    val factory: BaseListAdapterFactory,
    val atMessageListener: ATMessageListener? = null,
    val atFilterSeedWordListener: ATFilterSeedWordListener? = null,
    val atSelectedSeedWordListener: ATSelectedSeedWordListener? = null,
    val atAuthenticatedPaymailListener: ATAuthenticatedPaymailListener? = null,
    val atCoinListener: ATCoinListener? = null,
    val atUnusedAddressListener: ATUnusedAddressListener? = null,
    val atContentLoadingListener: ATContentLoadingListener? = null
) : LifecycleRecyclerAdapter<BaseViewHolder>()  {

    private var inflater: LayoutInflater? = null

    private var items: MutableList<AdapterType> = mutableListOf()

    lateinit var viewPool: AdapterTypeViewPool

    init {
        parentLifecycle = lifecycle
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBindViewHolder(items[position], getListener(getItemViewType(position)))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            AdapterTypeEnum.MESSAGE_0.ordinal -> {
                ATMessage0ViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.NO_PAYMAILS_ADDED.ordinal -> {
                ATNoPaymailsAddedViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL.ordinal -> {
                AuthenticatedPaymailViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.SELECTED_SEED_WORD.ordinal -> {
                ATSelectedSeedWordViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.SEED_WORD_HINT.ordinal -> {
                ATSeedWordHintViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.FILTER_SEED_WORD.ordinal -> {
                ATFilterSeedWordViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.FILTER_SEED_NO_MATCH.ordinal -> {
                ATFilterSeedNoMatchViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC.ordinal -> {
                ATHeightDecorDynamicViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.COIN_0.ordinal -> {
                ATCoinViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent,
                    atCoinListener!!
                )
            }
            AdapterTypeEnum.UNUSED_ADDRESS_0.ordinal -> {
                ATUnusedAddressViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            AdapterTypeEnum.CONTENT_LOADING_0.ordinal -> {
                ATContentLoadingViewHolder.create(
                    factory,
                    getInflater(parent.context),
                    parent
                )
            }
            else -> throw RuntimeException()
        }
    }

    private fun getListener(viewType: Int): Listener? {
        return when (viewType) {
            AdapterTypeEnum.MESSAGE_0.ordinal -> {
                atMessageListener
            }
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL.ordinal -> {
                atAuthenticatedPaymailListener
            }
            AdapterTypeEnum.SELECTED_SEED_WORD.ordinal -> {
                atSelectedSeedWordListener
            }
            AdapterTypeEnum.FILTER_SEED_WORD.ordinal -> {
                atFilterSeedWordListener
            }
            AdapterTypeEnum.COIN_0.ordinal -> {
                atCoinListener
            }
            AdapterTypeEnum.UNUSED_ADDRESS_0.ordinal -> {
                atUnusedAddressListener
            }
            AdapterTypeEnum.CONTENT_LOADING_0.ordinal -> {
                atContentLoadingListener
            }
            else -> null
        }
    }







    override fun getItemViewType(position: Int): Int {
        return items[position].type.ordinal
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun getInflater(context: Context): LayoutInflater {
        inflater?.let {
            return it
        }
        return LayoutInflater.from(context).let {
            inflater = it
            it
        }
    }

    fun setItems(list: List<AdapterType>, notifyChanged: Boolean = true) {
        val diffCallback = ListAdapterDiffCallback(items, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(list)

        diffResult.dispatchUpdatesTo(this)
    }

    fun getItemOfPosition(position: Int): AdapterType {
        return items[position]
    }




    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        Timber.i("onAttachedToRecyclerView $recyclerView")

        if (!::viewPool.isInitialized) {
            viewPool = findSharedViewPool(recyclerView)
        }
        recyclerView.setRecycledViewPool(viewPool)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
//        Timber.i("onDetachedFromRecyclerView $recyclerView")
    }




    interface Listener {}

    abstract class ATMessageListener : Listener {
        companion object {
            val ACTION_START_0 = 0
            val ACTION_START_1 = 1
            val ACTION_END_0 = 2
            val ACTION_END_1 = 3
            val ACTION_END_2 = 4

        }

        abstract fun onItemClicked(adapterPosition: Int, model: ATMessage0)
        abstract fun onActionClicked(adapterPosition: Int, model: ATMessage0, actionId: Int)
    }

    interface ATAuthenticatedPaymailListener : Listener {
        fun onClick(paymail: String)
    }

    interface ATFilterSeedWordListener : Listener {
        fun onClick(word: String)
    }

    interface ATSelectedSeedWordListener : Listener {
        fun onDismissClicked()
    }

    interface ATCoinListener : Listener {
        fun onClick(coin: Coin)
    }

    interface ATUnusedAddressListener : Listener {
        fun onClick(coin: DerivedAddress)
    }

    interface ATContentLoadingListener : Listener {
        fun onBound()
    }



    companion object {

        fun findSharedViewPool(view: View): AdapterTypeViewPool {
            var v: View = view
            while (true) {
                val p = v.parent
                if (p is View) {
                    v = p
                } else {
                    break
                }
            }

            var foundPool: AdapterTypeViewPool? = null
            v.findViewById<View>(R.id.root).let {
                if (it == null) return@let

                if (it.tag is AdapterTypeViewPool) {
                    foundPool = it.tag as AdapterTypeViewPool
                } else {
                    Timber.i("shared-view-pool not found; instantiating a personal one")
                    val pool = AdapterTypeViewPool()
                    it.tag = pool
                    foundPool = pool
                }
            }

            return if (foundPool == null) {
                Timber.i("view with R.id.root not found; instantiating a personal one")
                AdapterTypeViewPool()
            } else {
                foundPool!!
            }
        }
    }

}