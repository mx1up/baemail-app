package app.bitcoin.baemail.recycler

import androidx.recyclerview.widget.DiffUtil

class ListAdapterDiffCallback(
    val oldList: List<AdapterType>,
    val newList: List<AdapterType>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val itemOld = oldList[oldItemPosition]
        val itemNew = newList[newItemPosition]

        if (itemOld.type != itemNew.type) {
            return false
        }

        return when (itemOld.type) {
            AdapterTypeEnum.MESSAGE_0 -> {
                itemOld as ATMessage0
                itemNew as ATMessage0

                itemOld.id == itemNew.id
            }
            AdapterTypeEnum.CONTENT_LOADING_0 -> {
                true
            }
            AdapterTypeEnum.SELECTED_SEED_WORD -> {
                itemOld as ATSelectedSeedWord
                itemNew as ATSelectedSeedWord

                itemOld.value == itemNew.value
            }
            AdapterTypeEnum.FILTER_SEED_WORD -> {
                itemOld as ATFilterSeedWord
                itemNew as ATFilterSeedWord

                itemOld.value == itemNew.value
            }
            AdapterTypeEnum.COIN_0 -> {
                itemOld as ATCoin
                itemNew as ATCoin

                itemOld.coin.txId == itemNew.coin.txId &&
                        itemOld.coin.address == itemNew.coin.address &&
                        itemOld.coin.outputIndex == itemNew.coin.outputIndex
            }
            AdapterTypeEnum.ACCOUNT_0,
            AdapterTypeEnum.ACTIVITY_0,
            AdapterTypeEnum.NO_PAYMAILS_ADDED,
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL,
            AdapterTypeEnum.SEED_WORD_HINT,
            AdapterTypeEnum.FILTER_SEED_NO_MATCH,
            AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC,
            AdapterTypeEnum.UNUSED_ADDRESS_0 -> {
                false
            }
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val itemOld = oldList[oldItemPosition]
        val itemNew = newList[newItemPosition]

        return when (itemOld.type) {
            AdapterTypeEnum.MESSAGE_0 -> {
                itemOld as ATMessage0
                itemNew as ATMessage0

                itemOld == itemNew
            }
            AdapterTypeEnum.CONTENT_LOADING_0 -> {
                true
            }
            AdapterTypeEnum.SELECTED_SEED_WORD -> {
                itemOld as ATSelectedSeedWord
                itemNew as ATSelectedSeedWord

                itemOld.value == itemNew.value && itemOld.isDismissible == itemNew.isDismissible
            }
            AdapterTypeEnum.FILTER_SEED_WORD -> {
                itemOld as ATFilterSeedWord
                itemNew as ATFilterSeedWord

                itemOld.value == itemNew.value
            }
            AdapterTypeEnum.COIN_0 -> {
                itemOld as ATCoin
                itemNew as ATCoin

                itemOld.coin == itemNew.coin && itemOld.isLast == itemNew.isLast
            }
            AdapterTypeEnum.ACCOUNT_0,
            AdapterTypeEnum.ACTIVITY_0,
            AdapterTypeEnum.NO_PAYMAILS_ADDED,
            AdapterTypeEnum.AUTHENTICATED_PAYMAIL,
            AdapterTypeEnum.SEED_WORD_HINT,
            AdapterTypeEnum.FILTER_SEED_NO_MATCH,
            AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC,
            AdapterTypeEnum.UNUSED_ADDRESS_0 -> {
                true
            }
        }
    }
}