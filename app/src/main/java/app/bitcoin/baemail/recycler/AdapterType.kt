package app.bitcoin.baemail.recycler

import androidx.lifecycle.LiveData
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.wallet.request.DerivedAddress

sealed class AdapterType(val type: AdapterTypeEnum)

data class ATAccount0(
    val isAuthorised: Boolean
) : AdapterType(AdapterTypeEnum.ACCOUNT_0)

data class ATMessage0(
    val id: String,
    val from: String,
    val subject: String,
    val preview: String,
    val dateLabel: String,
    val hasAttachment: Boolean,
    val isUnread: Boolean,
    var isRightActionExpanded: Boolean = false,
    var isLeftActionExpanded: Boolean = false,
    var message: DecryptedBaemailMessage? = null
) : AdapterType(AdapterTypeEnum.MESSAGE_0)

data class ATActivity0(
    val memoryId: String,
    val actionId: String
) : AdapterType(AdapterTypeEnum.ACTIVITY_0)

data class ATNoPaymailsAdded(
    val id: String
) : AdapterType(AdapterTypeEnum.NO_PAYMAILS_ADDED)

data class ATAuthenticatedPaymail(
    val paymail: String,
    val isActive: Boolean,
    val isLast: Boolean
) : AdapterType(AdapterTypeEnum.AUTHENTICATED_PAYMAIL)

data class ATSelectedSeedWord(
    val value: String,
    val isDismissible: Boolean
) : AdapterType(AdapterTypeEnum.SELECTED_SEED_WORD)

data class ATSeedWordHint(
    val value: String
) : AdapterType(AdapterTypeEnum.SEED_WORD_HINT)

data class ATFilterSeedWord(
    val value: String
) : AdapterType(AdapterTypeEnum.FILTER_SEED_WORD)

data class ATFilterSeedNoMatch(
    val value: String
) : AdapterType(AdapterTypeEnum.FILTER_SEED_NO_MATCH)

data class ATHeightDecorDynamic(
    val heightLD: LiveData<Int>
) : AdapterType(AdapterTypeEnum.HEIGHT_DECOR_DYNAMIC)

data class ATCoin(
    val coin: Coin,
    val isLast: Boolean
) : AdapterType(AdapterTypeEnum.COIN_0)

data class ATUnusedAddress(
    val coin: DerivedAddress,
    val isLast: Boolean
) : AdapterType(AdapterTypeEnum.UNUSED_ADDRESS_0)

data class ATContentLoading0(
    val id: Int = 0
) : AdapterType(AdapterTypeEnum.CONTENT_LOADING_0)


enum class AdapterTypeEnum {
    ACCOUNT_0,
    MESSAGE_0,
    ACTIVITY_0,
    NO_PAYMAILS_ADDED,
    AUTHENTICATED_PAYMAIL,
    SELECTED_SEED_WORD,
    SEED_WORD_HINT,
    FILTER_SEED_WORD,
    FILTER_SEED_NO_MATCH,
    HEIGHT_DECOR_DYNAMIC,
    COIN_0,
    UNUSED_ADDRESS_0,
    CONTENT_LOADING_0
}