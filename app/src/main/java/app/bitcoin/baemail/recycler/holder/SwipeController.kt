package app.bitcoin.baemail.recycler.holder

import android.graphics.Canvas
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

@Suppress("ConstantConditionIf", "UNUSED_ANONYMOUS_PARAMETER")
class SwipeController : ItemTouchHelper.Callback() {

    companion object {
        const val WIDTH_MAX = 99999
        const val LOG = false
    }

    private var lastActionRightMaxWidth = WIDTH_MAX
    private var lastActionLeftMaxWidth = WIDTH_MAX

    /**
     * Possible values: ItemTouchHelper.LEFT, ItemTouchHelper.RIGHT, ItemTouchHelper.ACTION_STATE_IDLE
     */
    private var swipedDirection = ItemTouchHelper.ACTION_STATE_IDLE
    private var lastDx = 0f
    private var settlingNow = false
    private var wasSwipedCalled = false

    private var allowSwipeLeftHook: (RecyclerView.ViewHolder) -> Boolean = {
        false
    }

    private var allowSwipeRightHook: (RecyclerView.ViewHolder) -> Boolean = {
        false
    }

    private var onSwipedCallback: (RecyclerView.ViewHolder, Int, Boolean) -> Unit = { holder, direction, maxNotMin ->
        //do nothing
    }

    private var getSwipeLeftMaxWidthHook: (RecyclerView.ViewHolder) -> Int = { holder ->
        WIDTH_MAX
    }

    private var getSwipeLeftMinWidthHook: (RecyclerView.ViewHolder) -> Int = { holder ->
        WIDTH_MAX
    }

    private var getSwipeRightMaxWidthHook: (RecyclerView.ViewHolder) -> Int = { holder ->
        WIDTH_MAX
    }

    private var getSwipeRightMinWidthHook: (RecyclerView.ViewHolder) -> Int = { holder ->
        WIDTH_MAX
    }

    private var onSwipedPxAmountHook: ((RecyclerView.ViewHolder, Float, Float)->Unit)? = null

    fun init(
        allowSwipeLeft: ((RecyclerView.ViewHolder) -> Boolean)? = null,
        allowSwipeRight: ((RecyclerView.ViewHolder) -> Boolean)? = null,
        onSwiped: ((RecyclerView.ViewHolder, Int, Boolean) -> Unit)? = null,
        swipeLeftMinWidth: ((RecyclerView.ViewHolder) -> Int)? = null,
        swipeLeftMaxWidth: ((RecyclerView.ViewHolder) -> Int)? = null,
        swipeRightMinWidth: ((RecyclerView.ViewHolder) -> Int)? = null,
        swipeRightMaxWidth: ((RecyclerView.ViewHolder) -> Int)? = null,
        onSwipedAmount: ((RecyclerView.ViewHolder, Float, Float)->Unit)? = null
    ) {
        allowSwipeLeft?.let {
            allowSwipeLeftHook = it
        }
        allowSwipeRight?.let {
            allowSwipeRightHook = it
        }
        onSwiped?.let {
            onSwipedCallback = it
        }
        swipeLeftMinWidth?.let {
            getSwipeLeftMinWidthHook = it
        }
        swipeLeftMaxWidth?.let {
            getSwipeLeftMaxWidthHook = it
        }
        swipeRightMinWidth?.let {
            getSwipeRightMinWidthHook = it
        }
        swipeRightMaxWidth?.let {
            getSwipeRightMaxWidthHook = it
        }
        onSwipedAmount?.let {
            onSwipedPxAmountHook = it
        }
    }




    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragLeft = allowSwipeLeftHook(viewHolder)
        val dragRight = allowSwipeRightHook(viewHolder)

        val swipeFlag = when {
            dragLeft && dragRight -> ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
            dragLeft -> ItemTouchHelper.LEFT
            dragRight -> ItemTouchHelper.RIGHT
            else -> 0
        }

        return makeMovementFlags(0, swipeFlag)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        if (LOG) Timber.d("###onSwiped holder:$viewHolder")

        val widthMid = when (swipedDirection) {
            ItemTouchHelper.LEFT -> {
                (getSwipeLeftMaxWidthHook(viewHolder) + getSwipeLeftMinWidthHook(viewHolder)) / 2
            }
            ItemTouchHelper.RIGHT -> {
                (getSwipeRightMaxWidthHook(viewHolder) + getSwipeRightMinWidthHook(viewHolder)) / 2
            }
            else -> WIDTH_MAX
        }

        val maxNotMin = abs(lastDx) > widthMid

        //todo possible improvement ... if fling-velocity is great enough in swipe-direction,
        // override `maxNotMin` to TRUE

        wasSwipedCalled = true
        onSwipedCallback(viewHolder, swipedDirection, maxNotMin)


        //reset
        //todo wasSwipedCalled = false
        swipedDirection = ItemTouchHelper.ACTION_STATE_IDLE
        lastDx = 0f
        settlingNow = false
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        if (LOG) Timber.e("###clearView holder:$viewHolder")
        if (!wasSwipedCalled) {
            if (LOG) Timber.d("###clearView wasSwipedCalled==FALSE; calling now")

            val minDx = when (swipedDirection) {
                ItemTouchHelper.LEFT -> {
                    getSwipeLeftMinWidthHook(viewHolder)
                }
                ItemTouchHelper.RIGHT -> {
                    getSwipeRightMinWidthHook(viewHolder)
                }
                else -> WIDTH_MAX
            }

            val widthMid = when (swipedDirection) {
                ItemTouchHelper.LEFT -> {
                    (getSwipeLeftMaxWidthHook(viewHolder) + getSwipeLeftMinWidthHook(viewHolder)) / 2
                }
                ItemTouchHelper.RIGHT -> {
                    (getSwipeRightMaxWidthHook(viewHolder) + getSwipeRightMinWidthHook(viewHolder)) / 2
                }
                else -> WIDTH_MAX
            }

            if (abs(lastDx) < minDx / 2) {
                if (LOG) Timber.d("###clearView wasSwipedCalled==FALSE; calling now ---- aborted")
            } else if (abs(lastDx) < widthMid) {
                onSwipedCallback(viewHolder, swipedDirection, false)
            } else {
                onSwipedCallback(viewHolder, swipedDirection, true)
            }
        }

        //reset
        wasSwipedCalled = false
        swipedDirection = ItemTouchHelper.ACTION_STATE_IDLE
        lastDx = 0f
        settlingNow = false
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {

        if (settlingNow) {
            //Execution will only enter here when the user has released his finger after a swipe.
            //If `onSwiped` triggered dX will be animating towards the swipe-direction
            // and opposite of it did not trigger.
            //Work with animation in-hand, but prevent it from animating too far.

            var finalDx = dX

            if (swipedDirection == ItemTouchHelper.RIGHT) {
                val widthHalf = getSwipeRightMinWidthHook(viewHolder) / 2
                val widthMid = (getSwipeRightMaxWidthHook(viewHolder) + getSwipeRightMinWidthHook(viewHolder)) / 2

                if (lastDx < widthHalf) {
                    //did not reach half-to-MIN
                    finalDx = max(dX, 0f)
                } else if (lastDx < getSwipeRightMinWidthHook(viewHolder)) {
                    //reach half-to-MIN
                    finalDx = min(dX, getSwipeRightMinWidthHook(viewHolder).toFloat())
                } else if (lastDx > widthMid) {
                    //reach MAX not MIN
                    finalDx = min(dX, getSwipeRightMaxWidthHook(viewHolder).toFloat())
                } else {
                    //reach MIN not MAX
                    finalDx = max(dX, getSwipeRightMinWidthHook(viewHolder).toFloat())
                }
            } else if (swipedDirection == ItemTouchHelper.LEFT) {
                val widthHalf = getSwipeLeftMinWidthHook(viewHolder) / 2
                val widthMid = (getSwipeLeftMaxWidthHook(viewHolder) + getSwipeLeftMinWidthHook(viewHolder)) / 2

                if (abs(lastDx) < widthHalf) {
                    //did not reach half-to-MIN
                    finalDx = min(dX, 0f)
                } else if (abs(lastDx) < getSwipeLeftMinWidthHook(viewHolder)) {
                    //reach half-to-MIN
                    finalDx = max(dX, -1f * getSwipeLeftMinWidthHook(viewHolder).toFloat())
                } else if (abs(lastDx) > widthMid) {
                    //reach MAX not MIN
                    finalDx = max(dX, -1f * getSwipeLeftMaxWidthHook(viewHolder).toFloat())
                } else {
                    //reach MIN not MAX
                    finalDx = min(dX, -1f * getSwipeLeftMinWidthHook(viewHolder).toFloat())
                }
            }

            if (LOG) Timber.d("###onChildDraw in branch where `settlingNow` dX:$dX dY:$dY finalDx:$finalDx")

            onSwipedPxAmountHook?.invoke(viewHolder, finalDx, dY)





        } else if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            //Execution enters here when the user is in mid-swipe.

            var finalDx = dX
            lastActionRightMaxWidth = getSwipeRightMaxWidthHook(viewHolder)
            lastActionLeftMaxWidth = getSwipeLeftMaxWidthHook(viewHolder)

            if (swipedDirection == ItemTouchHelper.ACTION_STATE_IDLE) {
                //item is not swiped at all; starting the gesture
                if (dX < 0) {
                    if (LOG) Timber.d("###onChildDraw updating swipedDirection to LEFT")
                    swipedDirection = ItemTouchHelper.LEFT
                } else if (dX > 0) {
                    if (LOG) Timber.d("###onChildDraw updating swipedDirection to RIGHT")
                    swipedDirection = ItemTouchHelper.RIGHT
                } else {
                    if (LOG) Timber.d("###onChildDraw swipedDirection == ItemTouchHelper.ACTION_STATE_IDLE; dx:$dX")
                }
            } else if (swipedDirection == ItemTouchHelper.LEFT) {
                //item is swiped so that right action is revealed
                if (dX > 0) {
                    if (LOG) Timber.d("###onChildDraw updating swipedDirection to RIGHT")
                    swipedDirection = ItemTouchHelper.RIGHT
                } else {
                    swipedDirection = ItemTouchHelper.LEFT
                }

                finalDx = max(dX, -lastActionLeftMaxWidth.toFloat())

            } else if (swipedDirection == ItemTouchHelper.RIGHT) {
                //item is swiped so that left action is revealed
                if (dX < 0) {
                    if (LOG) Timber.d("###onChildDraw updating swipedDirection to LEFT")
                    swipedDirection = ItemTouchHelper.LEFT
                } else {
                    swipedDirection = ItemTouchHelper.RIGHT
                }

                finalDx = min(dX, lastActionRightMaxWidth.toFloat())
            }


            if (LOG) Timber.d("""
                ###onChildDraw in branch where hasSwipedEnough==FALSE
                lastActionRightMaxWidth:$lastActionRightMaxWidth
                lastActionLeftMaxWidth:$lastActionLeftMaxWidth
                dX:$dX
                dY:$dY
                finalDx:$finalDx
                isCurrentlyActive:$isCurrentlyActive
                """.trimIndent())
            //todo super.onChildDraw(c, recyclerView, viewHolder, finalDx, dY, actionState, /*false */isCurrentlyActive)
            onSwipedPxAmountHook?.invoke(viewHolder, finalDx, dY)

            if (isCurrentlyActive) { //todo not sure if this is still necessary; it was at some point
                lastDx = finalDx
            }

        } else {
            if (LOG) Timber.d("###onChildDraw actionState:$actionState dX:$dX dY:$dY")
        }
    }


    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
        //This method get executed when the user releases his finger after swipe.
        //This method determines whether the swipe will result in a call to #onSwiped.
        //Swipes that result in #onSwiped will be animated in the direction of the swipe;
        // and the opposite direction for those that don't call #onSwiped.

        val width = viewHolder.itemView.measuredWidth

        if (swipedDirection == ItemTouchHelper.RIGHT) {
            val widthHalf = getSwipeRightMinWidthHook(viewHolder) / 2
            val widthMid = (getSwipeRightMaxWidthHook(viewHolder) + getSwipeRightMinWidthHook(viewHolder)) / 2

            if (lastDx < widthHalf) {
                //force clearing
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.RIGHT; force-clearing; did not reach half-to-MIN")
                return widthHalf / width.toFloat()
            } else if (lastDx < getSwipeRightMinWidthHook(viewHolder)) {
                //force success
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.RIGHT; force-success; reach half-to-MIN")
                return widthHalf / width.toFloat()

            } else if (lastDx > widthMid) {
                //force success
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.RIGHT; force-success; reach MAX not MIN")
                return getSwipeRightMinWidthHook(viewHolder) / width.toFloat()
            } else {
                //force clearing
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.RIGHT; force-clearing; reach MIN not MAX")
                return getSwipeRightMaxWidthHook(viewHolder) / width.toFloat()
            }



        } else if (swipedDirection == ItemTouchHelper.LEFT) {
            val widthHalf = getSwipeLeftMinWidthHook(viewHolder) / 2
            val widthMid = (getSwipeLeftMaxWidthHook(viewHolder) + getSwipeLeftMinWidthHook(viewHolder)) / 2


            if (abs(lastDx) < widthHalf) {
                //force clearing
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.LEFT; force-clearing; did not reach half-to-MIN")
                return widthHalf / width.toFloat()
            } else if (abs(lastDx) < getSwipeLeftMinWidthHook(viewHolder)) {
                //force success
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.LEFT; force-success; reach half-to-MIN")
                return widthHalf / width.toFloat()
            } else if (abs(lastDx) > widthMid) {
                //force success
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.LEFT; force-success; reach MAX not MIN")
                return getSwipeLeftMinWidthHook(viewHolder) / width.toFloat()
            } else {
                //force clearing
                if (LOG) Timber.d("#getSwipeThreshold swipedDirection == ItemTouchHelper.LEFT; force-clearing; reach MIN not MAX")
                return getSwipeLeftMaxWidthHook(viewHolder) / width.toFloat()
            }


        }

        if (LOG) Timber.d("#getSwipeThreshold ... other swipedDirection")
        return 1.1f
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        val stateLabel = when (actionState) {
            ItemTouchHelper.ACTION_STATE_IDLE -> "ACTION_STATE_IDLE"
            ItemTouchHelper.ACTION_STATE_DRAG -> "ACTION_STATE_DRAG"
            ItemTouchHelper.ACTION_STATE_SWIPE -> "ACTION_STATE_SWIPE"
            else -> throw RuntimeException()
        }

        if (LOG) Timber.d("#onSelectedChanged ... actionState:$stateLabel")

        if (ItemTouchHelper.ACTION_STATE_SWIPE == actionState) {
            if (LOG) Timber.d("#onSelectedChanged ... resetting state")
            //reset
            wasSwipedCalled = false
            swipedDirection = ItemTouchHelper.ACTION_STATE_IDLE
            lastDx = 0f
            settlingNow = false
        } else {
            settlingNow = true
        }
    }


    override fun getSwipeEscapeVelocity(defaultValue: Float): Float {
        //make it harder to fling
        return defaultValue * 111f
    }
}