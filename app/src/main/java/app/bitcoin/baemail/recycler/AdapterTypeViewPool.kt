package app.bitcoin.baemail.recycler

import android.util.SparseIntArray
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

class AdapterTypeViewPool : RecyclerView.RecycledViewPool() {
    private val arrayOfMax = SparseIntArray()

    init {
        initializeMaxCounts(this)
    }

    override fun getRecycledView(viewType: Int): RecyclerView.ViewHolder? {
        if (isDebug) Timber.i(">>> viewType:$viewType getRecycledView")
        return super.getRecycledView(viewType)
    }

    override fun getRecycledViewCount(viewType: Int): Int {
        if (isDebug) Timber.i(">>> viewType:$viewType getRecycledViewCount")
        return super.getRecycledViewCount(viewType)
    }

    override fun putRecycledView(scrap: RecyclerView.ViewHolder?) {
        val willBeDropped = arrayOfMax.get(scrap?.itemViewType ?: 0) <=
                super.getRecycledViewCount(scrap?.itemViewType ?: 0)

        if (isDebug) Timber.i(">>> viewType:${scrap?.itemViewType} putRecycledView dropped:$willBeDropped")
        super.putRecycledView(scrap)
    }

    override fun setMaxRecycledViews(viewType: Int, max: Int) {
        if (isDebug) Timber.i(">>> viewType:$viewType setMaxRecycledViews: $max")
        super.setMaxRecycledViews(viewType, max)

        arrayOfMax.put(viewType, max)
    }

    fun getMaxCountForHolder(viewType: Int): Int {
        return arrayOfMax.get(viewType)
    }




    companion object {

        private const val isDebug = false

        fun initializeMaxCounts(pool: AdapterTypeViewPool) {
            AdapterTypeEnum.values().forEach { type ->
                when (type) {
                    AdapterTypeEnum.MESSAGE_0 -> {
                        pool.setMaxRecycledViews(type.ordinal, 12)
                    }
                    else -> {
                        pool.setMaxRecycledViews(type.ordinal, 5)
                    }
                }
            }
        }

    }

}