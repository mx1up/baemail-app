package app.bitcoin.baemail.recycler.holder

import android.graphics.Color
import android.graphics.drawable.LayerDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.recycler.*

class ATCoinViewHolder(
    itemView: View,
    listener: BaseListAdapter.ATCoinListener
) : BaseViewHolder(itemView) {

    var model: ATCoin? = null

    val path: TextView = itemView.findViewById(R.id.path)
    val sats: TextView = itemView.findViewById(R.id.sats)
    val isActive: ImageView = itemView.findViewById(R.id.is_active)

    lateinit var strokeDrawable: DrawableStroke

    val strokeColor = R.color.li_filter_seed_word__stroke_color.let { id ->
        ContextCompat.getColor(itemView.context, id)
    }

    init {
        val colorSelector = R.color.li_filter_seed_word__selector_color.let { id ->
            ContextCompat.getColor(itemView.context, id)
        }

        val strokeWidth = R.dimen.li_filter_seed_word__stroke_width.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        val strokePaddingSides = R.dimen.li_filter_seed_word__stroke_padding_sides.let { id ->
            itemView.resources.getDimensionPixelSize(id)
        }

        strokeDrawable = DrawableStroke().also {
            it.setStrokeColor(strokeColor)
            it.setStrokeWidths(0, 0, 0, strokeWidth)
            it.setPadding(strokePaddingSides, 0, strokePaddingSides, 0)
        }

        itemView.background = LayerDrawable(arrayOf(
            strokeDrawable,
            DrawableState.getNew(colorSelector)
        ))

        itemView.setOnClickListener {
            val m = model ?: return@setOnClickListener
            listener.onClick(m.coin)
        }



    }






    override fun onBindViewHolder(model: AdapterType, listener: BaseListAdapter.Listener?) {
        this.model = model as ATCoin

        path.text = model.coin.derivationPath
        sats.text = model.coin.sats.toString()

        if (model.coin.spendingTxId != null) {
            isActive.visibility = View.VISIBLE
        } else {
            isActive.visibility = View.INVISIBLE
        }

        if (model.isLast) {
            strokeDrawable.setStrokeColor(Color.TRANSPARENT)
        } else {
            strokeDrawable.setStrokeColor(strokeColor)
        }
    }






    companion object {

        const val LOG = false

        fun create(
            factory: BaseListAdapterFactory,
            inflater: LayoutInflater,
            parent: ViewGroup,
            atCoinListener: BaseListAdapter.ATCoinListener
        ): ATCoinViewHolder {
            return ATCoinViewHolder(
                inflater.inflate(
                    R.layout.li_coin,
                    parent,
                    false
                ),
                atCoinListener
            )
        }
    }
}