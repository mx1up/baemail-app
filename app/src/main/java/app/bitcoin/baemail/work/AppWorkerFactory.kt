package app.bitcoin.baemail.work

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters

interface AppWorkerFactory {
    fun create(appContext: Context, params: WorkerParameters): ListenableWorker
}