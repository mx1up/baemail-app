package app.bitcoin.baemail.work

import android.content.Context
import androidx.work.*
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import app.bitcoin.baemail.util.ConnectivityLiveData
import app.bitcoin.baemail.wallet.BlockchainDataSource
import app.bitcoin.baemail.wallet.WalletRepository
import app.bitcoin.baemail.wallet.request.DerivedAddress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider
import kotlin.Exception

class FundingWalletSyncWorker(
    val appContext: Context,
    val workerParams: WorkerParameters,
    val connectedLD: ConnectivityLiveData,
    val walletRepository: WalletRepository,
    val secureDataSource: SecureDataSource,
    val blockchainDataSource: BlockchainDataSource,
    val appDatabase: AppDatabase
) : CoroutineWorker(appContext, workerParams) {

    private lateinit var paymail: String

    override suspend fun doWork(): Result {
        Timber.i("#doWork .. connected:${connectedLD.value!!.isConnected} .. success")

        var result = Result.failure()

//        try {
//            withContext(Dispatchers.IO) {
//
//                //todo get a set of addresses
//                //todo get received coins for each address
//                //todo check spends for each coin
//                //todo
//                //todo loop and check  next set of addresses
//                //todo stop looping after X number addresses have no incoming coins
//                //todo
//
//
//                paymail = workerParams.inputData.getString(KEY_PAYMAIL)!!
//
//                Timber.d("starting sync of funding wallet for paymail:$paymail")
//
//
//                walletRepository.onWalletSyncStarted(paymail)
//
//
//                val fundingWalletSeed = try {
//                    secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(paymail))
//                } catch (e: Exception) {
//                    Timber.e(e, "wallet not found; dropping sync-request")
//                    result = Result.success()
//                    return@withContext
//                }
//
//
//                var addressSetIndex = 0
//                var unusedAddressCount = 0
//
//                outer@ while (true) {
//
//                    Timber.d("next set")
//                    val addressList = walletRepository
//                        .deriveNextAddressSet(fundingWalletSeed, addressSetIndex)
//                    Timber.d("derived addresses")
//                    if (addressList == null) {
//                        Timber.e(RuntimeException("failed to retrieve next addresses; index:$addressSetIndex; dropping sync-request"))
//                        result = Result.success()
//
//                        return@withContext
//                    }
//
//                    Timber.d("starting with next set of addresses; index:$addressSetIndex")
//
//
//                    val hadIncomingCoins = syncAddress(addressList)
//
//                    if (!hadIncomingCoins) {
//                        Timber.d("unused address ... addressSetIndex:$addressSetIndex")
//                        unusedAddressCount++
//                    } else {
//                        unusedAddressCount = 0
//                    }
//
//                    if (unusedAddressCount > 1) {
//                        Timber.d("2 consecutive unused addresses hit; stopping crawl through addresses")
//                        break@outer
//                    }
//
//                    addressSetIndex++
//                }
//
//
//                walletRepository.onWalletSyncCompleted(paymail)
//
//                result = Result.success()
//
//            }
//
//        } catch (e: Exception) {
//            Timber.e(e)
//            walletRepository.onWalletSyncFail(paymail)
//
//            //todo
//            //todo
//
//        } finally {
//            walletRepository.onWalletSyncStopped(paymail)
//        }

        return result
    }

    class Factory @Inject constructor(
        private val connectedLD: Provider<ConnectivityLiveData>,
        private val walletRepository: Provider<WalletRepository>,
        private val secureDataSource: Provider<SecureDataSource>,
        private val blockchainDataSource: Provider<BlockchainDataSource>,
        private val appDatabase: Provider<AppDatabase>
    ) : AppWorkerFactory {
        override fun create(appContext: Context, params: WorkerParameters): ListenableWorker {
            return FundingWalletSyncWorker(
                appContext,
                params,
                connectedLD.get(),
                walletRepository.get(),
                secureDataSource.get(),
                blockchainDataSource.get(),
                appDatabase.get()
            )
        }
    }

    companion object {

        const val TAG = "FundingWalletSyncWorker"
        const val KEY_PAYMAIL = "paymail"


        fun syncFundingWallet(context: Context, paymail: String) {
            val tag = TAG + "_" + paymail

            val inputData = Data.Builder()
                .putString(KEY_PAYMAIL, paymail)
                .build()

            val work = OneTimeWorkRequest.Builder(FundingWalletSyncWorker::class.java)
                .setInputData(inputData)
                .build()

            val workManager = WorkManager.getInstance(context)

            workManager.enqueueUniqueWork(
                tag,
                ExistingWorkPolicy.REPLACE,
                work
            )

            Timber.d("enqueued work - syncFundingWallet -- $tag")

            TODO("disabled broken")
        }

    }

}