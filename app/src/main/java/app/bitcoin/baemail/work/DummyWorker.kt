package app.bitcoin.baemail.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import app.bitcoin.baemail.util.ConnectivityLiveData
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider



/*

val work = OneTimeWorkRequest.Builder(DummyWorker::class.java).build()

val workManager = WorkManager.getInstance(appContext)
workManager.enqueueUniqueWork(
    DummyWorker.TAG,
    ExistingWorkPolicy.REPLACE,
    work
)

 */

class DummyWorker(
    appContext: Context,
    workerParams: WorkerParameters,
    val connectedLD: ConnectivityLiveData
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        Timber.i("#doWork .. connected:${connectedLD.value!!.isConnected} .. success")
        return Result.success()
    }



    class Factory @Inject constructor(
        private val connectedLD: Provider<ConnectivityLiveData>
    ) : AppWorkerFactory {
        override fun create(appContext: Context, params: WorkerParameters): ListenableWorker {
            return DummyWorker(
                appContext,
                params,
                connectedLD.get()
            )
        }
    }

    companion object {

        const val TAG = "DummyWorker"
    }
}