package app.bitcoin.baemail.uiCentral

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.RippleDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableOval
import app.bitcoin.baemail.drawable.DrawableState
import timber.log.Timber

class ViewButtonBar: HorizontalScrollView {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private val currentTabs = ArrayList<Model<*>>()

    var tabClickListener: ((Any)->Unit)? = null


    private lateinit var layout: LinearLayout

    override fun onFinishInflate() {
        super.onFinishInflate()

        layout = LinearLayout(context)
        layout.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )
        val paddingStart = R.dimen.button_bar__scrolled_padding_start.let { id ->
            context.resources.getDimensionPixelSize(id)
        }
        val paddingEnd = R.dimen.button_bar__scrolled_padding_end.let { id ->
            context.resources.getDimensionPixelSize(id)
        }
        layout.updatePadding(
            left = paddingStart,
            right = paddingEnd
        )

        addView(layout)


        this.overScrollMode = View.OVER_SCROLL_NEVER
        this.isHorizontalScrollBarEnabled = false
    }

    fun init(tabs: List<Model<*>>) {
        //todo return if equals
        //addView()

        currentTabs.clear()
        currentTabs.addAll(tabs)

        initViews()
    }

    private fun initViews() {
        val extraViews = layout.childCount - currentTabs.size

        if (extraViews > 0) {
            //remove the extra views that will not be necessary further down
            layout.removeViews(currentTabs.size, extraViews)
        }

        //initialize the tabs
        currentTabs.forEachIndexed { index, model ->
            val matchingView = layout.getChildAt(index)

            val holder: TabHolder = if (matchingView == null) {
                val h = TabHolder.inflate(inflater, this@ViewButtonBar)
                layout.addView(h.view, index)

                h
            } else {
                matchingView.tag as TabHolder
            }

            holder.init(model, index == 0, index + 1 == currentTabs.size)
        }
    }



    data class Model<T>(
        val tab: T,
        val label: String,
        val icon: Drawable,
        val tint: Int,
        val isFocused: Boolean
    )



    data class TabHolder(
        val view: ViewGroup,
        val icon: ImageView,
        val label: TextView,
        var tab: Model<*>? = null,
        var isFirst: Boolean = true,
        var isLast: Boolean = true
    ) {

        lateinit var ovalBackground: DrawableOval
        lateinit var selectorBackground: Drawable

        val colorStrokeOn = R.color.colorPrimary.let { id ->
            ContextCompat.getColor(view.context, id)
        }
        val colorStrokeOff = Color.TRANSPARENT

        fun init(tab: Model<*>, isFirst: Boolean, isLast: Boolean) {

            if (this.tab?.icon != tab.icon) {
                icon.setImageDrawable(tab.icon)
            }

            if (this.tab?.label != tab.label) {
                label.text = tab.label
            }

            if (this.tab?.tint != tab.tint) {
                icon.setColorFilter(tab.tint)
                label.setTextColor(tab.tint)

                val pressedColor = Color.argb(70, Color.red(tab.tint),
                    Color.green(tab.tint), Color.blue(tab.tint))

                this.selectorBackground.let {
                    if (it is RippleDrawable) {
                        it.setColor(ColorStateList.valueOf(pressedColor))
                    }
                }
            }

            if (this.tab?.isFocused != tab.isFocused) {
                if (tab.isFocused) {
                    ovalBackground.setStrokeColor(colorStrokeOn)
                } else {
                    ovalBackground.setStrokeColor(colorStrokeOff)
                }
            }

            this.tab = tab
            this.isFirst = isFirst
            this.isLast = isLast
        }

        companion object {

            fun inflate(inflater: LayoutInflater, parent: ViewButtonBar): TabHolder {
                val view: ViewGroup = inflater.inflate(R.layout.view_button_bar_li, parent, false)
                        as ViewGroup

                val icon: ImageView = view.findViewById(R.id.icon)
                val label: TextView = view.findViewById(R.id.label)

                val holder = TabHolder(view, icon, label)
                view.tag = holder

                val dp = inflater.context.resources.displayMetrics.density

                holder.selectorBackground = DrawableState.getNew(Color.TRANSPARENT)

                val paddingOvalSides = R.dimen.button_bar_item__oval_padding_sides.let { id ->
                    inflater.context.resources.getDimension(id)
                }
                val paddingOvalTopBot = R.dimen.button_bar_item__oval_padding_top_bot.let { id ->
                    inflater.context.resources.getDimension(id)
                }

                val ovalStrokeWidth = inflater.context.resources
                    .getDimension(R.dimen.button_bar_item__oval_stroke_width)

                holder.ovalBackground = DrawableOval(inflater.context).also {
                    it.setBgColor(Color.TRANSPARENT)
                    it.setStrokeWidth(ovalStrokeWidth)
                    it.setStrokeColor(Color.BLUE)
                    it.setPaddings(paddingOvalSides, paddingOvalTopBot, paddingOvalSides, paddingOvalTopBot)
                    it.initPaints()
                }




                view.background = LayerDrawable(arrayOf(
                    holder.ovalBackground,
                    holder.selectorBackground
                ))
                view.clipToOutline = true
                view.outlineProvider = holder.ovalBackground.getOutlineProvider()


                view.setOnClickListener {
                    val tab = holder.tab
                    if (tab == null) {
                        Timber.i("tab clicked; doing nothing because the model is missing")
                        return@setOnClickListener
                    }

                    parent.tabClickListener.let {
                        if (it == null) {
                            Timber.i("tab clicked; doing nothing because tabClickListener is missing")
                            return@let
                        }
                        if (tab.tab == null) {
                            Timber.i("tab clicked; doing nothing because `action` is missing")
                            return@let
                        }

                        it(tab.tab)
                    }
                }

                return holder
            }
        }
    }

}