package app.bitcoin.baemail.uiCentral

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.message.MessageBucket
import app.bitcoin.baemail.message.SortType
import app.bitcoin.baemail.uiMemories.MessagesFragment
import app.bitcoin.baemail.util.FragmentStateHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class CentralActivity: AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CentralViewModel

    private lateinit var statusBarBg: View
    private lateinit var navigationBarBg: View

    private lateinit var coordinator: CoordinatorLayout
    private lateinit var bottomSheet: ConstraintLayout
    private lateinit var bottomSheetContentExpanded: FrameLayout
    private lateinit var bottomSheetContentCollapsed: FrameLayout

    private lateinit var paymailLabel: TextView

    private lateinit var fragmentHelper: FragmentStateHelper
    private var navHost: NavHostFragment? = null

    private lateinit var expandView: ImageView
    private lateinit var tabBar: ViewButtonBar
    private val tabBarObserver = TabBarObserver()

    private lateinit var bottomSheetCallback: BottomSheetBehavior.BottomSheetCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(CentralViewModel::class.java)

        fragmentHelper = FragmentStateHelper(supportFragmentManager)

        //

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }

        setContentView(R.layout.activity_central)

        //

        statusBarBg = findViewById(R.id.status_bar_bg)
        navigationBarBg = findViewById(R.id.navigation_bar_bg)
        tabBar = findViewById(R.id.tab_bar)
        expandView = findViewById(R.id.bottom_sheet_content_collapsed_info_hint)
        paymailLabel = findViewById(R.id.bottom_sheet_content_collapsed_info_label)

        coordinator = findViewById(R.id.coordinator)
        bottomSheet = findViewById(R.id.bottom_sheet)
        bottomSheetContentExpanded = findViewById(R.id.bottom_sheet_content_expanded)
        bottomSheetContentCollapsed = findViewById(R.id.bottom_sheet_content_collapsed)


        val bottomSheetLP = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams
        val bottomSheetBehavior = bottomSheetLP.behavior as BottomSheetBehavior
        bottomSheetBehavior.isGestureInsetBottomIgnored = true


        statusBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetTop.let { top ->
                    if (top == 0) 1 else top
                }

                view.layoutParams = it
            }

            windowInsets
        }
        navigationBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetBottom.let { bottom ->
                    if (bottom == 0) 1 else bottom
                }

                view.layoutParams = it
            }

            windowInsets
        }

        val dp = resources.displayMetrics.density

//        val color = ContextCompat.getColor(itemView.context, it)
//        val paddingSides = itemView.resources
//            .getDimensionPixelSize(R.dimen.message_0__separator_padding_sides)
//        val width = itemView.resources.getDimensionPixelSize(R.dimen.message_0__separator_width)

        val d = DrawableStroke()

        val baseColor = Color.parseColor("#0065bc")
        val finalColor = Color.argb(
            100,
            Color.red(baseColor),
            Color.green(baseColor),
            Color.blue(baseColor)
        )

        val dWidth = (3 * dp).toInt()
        d.setPadding(0, 0, 0, 0)
        d.setStrokeColor(baseColor)
        d.setStrokeWidths(0, dWidth, 0, 0)
        bottomSheetContentCollapsed.background = d


        tabBar.tabClickListener = { id ->
            id as CentralViewModel.Tab

            viewModel.onTabClicked(id)
        }


        @Suppress("ConstantConditionIf")
        bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {

            val collapsedHeight = R.dimen.central_bottom_sheet_content_collapsed_height.let {
                resources.getDimensionPixelSize(it)
            }

            val expandPercentBarrierA = 0.05f
            val expandPercentBarrierB = 0.10f

            val debugExpandAnim = false

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. slideOffset:$slideOffset")

                //make `bottomSheetContentExpanded` visible as the bottom-sheet is expanded

                if (slideOffset < expandPercentBarrierA) {
                    if (bottomSheetContentExpanded.translationY == 0f) {
                        bottomSheetContentExpanded.translationY = collapsedHeight.toFloat()
                        if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. bottomSheetContentExpanded is offset off-screen")
                    }
                } else {
                    if (bottomSheetContentExpanded.translationY != 0f) {
                        bottomSheetContentExpanded.translationY = 0f
                        if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. bottomSheetContentExpanded has offset cleared")
                    }
                }

                if (slideOffset < expandPercentBarrierA) {
                    if (bottomSheetContentExpanded.alpha != 0f) {
                        bottomSheetContentExpanded.alpha = 0f
                        if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. bottomSheetContentExpanded has alpha set to 0")
                    }
                }

                if (slideOffset > expandPercentBarrierB) {
                    if (bottomSheetContentExpanded.alpha != 1f) {
                        bottomSheetContentExpanded.alpha = 1f
                        if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. bottomSheetContentExpanded has alpha set to 1")
                    }
                }

                if (slideOffset > expandPercentBarrierB) {
                    if (bottomSheetContentCollapsed.translationY != (-1f * collapsedHeight)) {
                        bottomSheetContentCollapsed.translationY = -1f * collapsedHeight
                        if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. bottomSheetContentCollapsed is offset off-screen")
                    }
                } else {
                    if (bottomSheetContentCollapsed.translationY != 0f) {
                        bottomSheetContentCollapsed.translationY = 0f
                        if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. bottomSheetContentCollapsed has offset cleared")
                    }
                }

                if (slideOffset > expandPercentBarrierA && slideOffset < expandPercentBarrierB) {
                    val percentInBarrier = (slideOffset - expandPercentBarrierA) /
                            (expandPercentBarrierB - expandPercentBarrierA)
                    if (debugExpandAnim) Timber.d("bottomSheet .. onSlide .. percentInBarrier:$percentInBarrier")

                    bottomSheetContentExpanded.alpha = percentInBarrier
                }

            }

            var collapsingBackPressedCallback: CollapsingBackPressedCallback? = null
            inner class CollapsingBackPressedCallback : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Timber.d("collapsingBackPressedCallback .. handleOnBackPressed")
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

                    remove()
                    collapsingBackPressedCallback = null
                }

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                val stateName = when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> "STATE_COLLAPSED"
                    BottomSheetBehavior.STATE_DRAGGING -> "STATE_DRAGGING"
                    BottomSheetBehavior.STATE_EXPANDED -> "STATE_EXPANDED"
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> "STATE_HALF_EXPANDED"
                    BottomSheetBehavior.STATE_HIDDEN -> "STATE_HIDDEN"
                    BottomSheetBehavior.STATE_SETTLING -> "STATE_SETTLING"
                    else -> "?wut?"
                }

                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    collapsingBackPressedCallback.let {
                        if (it != null) return@let
                        val cb = CollapsingBackPressedCallback()
                        collapsingBackPressedCallback = cb
                        onBackPressedDispatcher.addCallback(this@CentralActivity, cb)
                    }

                } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    collapsingBackPressedCallback?.remove()
                    collapsingBackPressedCallback = null
                }

                if (debugExpandAnim) Timber.d("bottomSheet .. onStateChanged .. $stateName")
            }

        }

        bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)

        //bottomSheetCallback.onSlide(bottomSheet, 0f)

        //todo
        //todo
        //todo
        //todo


        expandView.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }


        if (savedInstanceState != null) {
            tabBarObserver.isRestoringState = true

            val helperState = savedInstanceState.getBundle(STATE_HELPER)!!
            fragmentHelper.restoreHelperState(helperState)

        } else {
            bottomSheetCallback.onSlide(bottomSheet, 0f)
            bottomSheetCallback.onStateChanged(bottomSheet, BottomSheetBehavior.STATE_COLLAPSED)
        }

        //todo

        viewModel.tabBarHelper.barLD.observe(this, tabBarObserver)


        //todo
        viewModel.contentLD.observe(this, Observer {
            it ?: return@Observer

            paymailLabel.text = it.paymail
        })


    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        val bottomSheetLP = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams
        val bottomSheetBehavior = bottomSheetLP.behavior as BottomSheetBehavior
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetCallback.onSlide(bottomSheet, 1f)
            bottomSheetCallback.onStateChanged(bottomSheet, BottomSheetBehavior.STATE_EXPANDED)

        } else {
            bottomSheetCallback.onSlide(bottomSheet, 0f)
            bottomSheetCallback.onStateChanged(bottomSheet, BottomSheetBehavior.STATE_COLLAPSED)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        // Make sure we save the current tab's state too!

        navHost.let {
            if (it == null) return@let

            //save current state
            val tab = if (tabBarObserver.activeTab != -1) {
                viewModel.tabBarHelper.getActiveTab()
            } else {
                return@let
            }

            fragmentHelper.saveState(it, tab.name)
        }

        outState.putBundle(STATE_HELPER, fragmentHelper.saveHelperState())

        super.onSaveInstanceState(outState)
    }


    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }










    inner class TabBarObserver : Observer<List<ViewButtonBar.Model<CentralViewModel.Tab>>> {
        var activeTab: Int = -1
        var isRestoringState = false

        var hasInitted = false

        override fun onChanged(list: List<ViewButtonBar.Model<CentralViewModel.Tab>>?) {
            list ?: return

            if (!hasInitted) {
                hasInitted = true
            }


            val previous = activeTab

            activeTab = viewModel.tabBarHelper.getActiveTabIndex()

            if (isRestoringState) {
                //allow fragment-restoring to execute normally
                isRestoringState = false
                tabBar.init(list)
                return
            }

            val host = navHost
            if (previous == activeTab && host != null) {
                val navController = host.navController
                //the user is already seeing this tab
                //bring the user to the start of the nav-graph that is showing
                val currentDestinationId = navController.currentDestination?.id
                val startDestinationId = navController.graph.startDestination

                if (currentDestinationId == startDestinationId) {
                    //the user is already looking at the start destination
                    return
                }

                navController.popBackStack(startDestinationId, false)

                return
            }

            //
            //init the tabs & the content of this fragment
            tabBar.init(list)



            //
            //
            //handle the nav-graph
            //

            //save the state of the current tab
            if (previous != -1 && host != null) {
                fragmentHelper.saveState(host, list[previous].tab.name)
            }

            //restore the state of the new tab if it had any
            val tab = viewModel.tabBarHelper.getActiveTab()
            val graphResource = when (tab) {
                CentralViewModel.Tab.SUPER_ASSET -> R.navigation.nav_superasset
                CentralViewModel.Tab.ARCHIVE -> R.navigation.nav_archive
                CentralViewModel.Tab.BROWSE -> R.navigation.nav_browse
                CentralViewModel.Tab.INBOX -> R.navigation.nav_inbox
                CentralViewModel.Tab.SENT -> R.navigation.nav_sent
                else -> throw RuntimeException()
            }


            //todo when access to this becomes required deeper in the nav-controller;
            // switch to using nav-graph-view-model in `NavHostFragment`

            val args = Bundle().also { args ->
                val bucket = when (tab) {
                    CentralViewModel.Tab.INBOX -> MessageBucket.INBOX
                    CentralViewModel.Tab.SENT -> MessageBucket.SENT
                    CentralViewModel.Tab.ARCHIVE -> MessageBucket.ARCHIVE
                    else -> null
                }

                bucket?.let {
                    args.putString(MessagesFragment.KEY_BUCKET, it.const)

                    if (MessageBucket.SENT == it) {
                        args.putString(MessagesFragment.KEY_INITIAL_SORT, SortType.TIME.const)
                        args.putBoolean(MessagesFragment.KEY_IS_SORT_LOCKED, true)
                    } else {
                        args.putString(MessagesFragment.KEY_INITIAL_SORT, SortType.VALUE.const)
                        args.putBoolean(MessagesFragment.KEY_IS_SORT_LOCKED, false)
                    }
                }
            }

            val f = NavHostFragment.create(graphResource, args)
            fragmentHelper.restoreState(f, list[activeTab].tab.name)

            supportFragmentManager.beginTransaction()
                .replace(R.id.content, f)
                .setPrimaryNavigationFragment(f)
                .commitNowAllowingStateLoss()

            navHost = f
        }

    }



    companion object {
        private const val STATE_HELPER = "helper"
    }


}