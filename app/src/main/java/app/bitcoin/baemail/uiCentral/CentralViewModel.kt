package app.bitcoin.baemail.uiCentral

import android.content.Context
import androidx.annotation.Keep
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import app.bitcoin.baemail.R
import app.bitcoin.baemail.paymail.PaymailRepository
import timber.log.Timber
import javax.inject.Inject

class CentralViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository
) : ViewModel() {

//    private val _isAuthLD = MutableLiveData<Boolean>()
//    val authLD: LiveData<Boolean> get() = _isAuthLD



    val tabBarHelper: TabBarHelper


    private val _contentLD = MediatorLiveData<Model>()
    val contentLD: LiveData<Model>
        get() = _contentLD






    init {
//        _isAuthLD.value = true

        tabBarHelper = TabBarHelper(appContext)




        _contentLD.addSource(paymailRepository.activePaymailLD, Observer {
            val paymail = it?.paymail
            paymail ?: let {
                _contentLD.value = Model()
                return@Observer
            }

            _contentLD.value = Model(paymail)
        })






        Timber.i("PlaylistBrowserViewModel")
    }

    override fun onCleared() {
        super.onCleared()

        //something
    }

    fun onTabClicked(tab: Tab) {
        tabBarHelper.updateActiveTab(tab)
    }


















    data class Model(
        val paymail: String = "<unset paymail>"
    )









    data class TabBarHelper(
        val context: Context,
        private var activeTab: Int = 0,
        val barLD: MutableLiveData<List<ViewButtonBar.Model<Tab>>> = MutableLiveData()
    ) {

        private lateinit var tabList: List<Tab>

        init {
            setup()
        }

        fun getActiveTabIndex(): Int = activeTab

        fun getActiveTab(): Tab = tabList[activeTab]

        fun updateActiveTab(tab: Tab) {
            activeTab = tabList.indexOf(tab)
            refreshLiveData()
        }

        private fun refreshLiveData() {
            val list = ArrayList<ViewButtonBar.Model<Tab>>()

            val colorSelected = ContextCompat.getColor(context, R.color.button_bar_selected_color)
            val colorUnselected = ContextCompat.getColor(context, R.color.button_bar_unselected_color)

            tabList.mapIndexed { index, tab ->

                val color = if (index == activeTab) { colorSelected } else { colorUnselected }

                ViewButtonBar.Model(
                    tab,
                    context.getString(tab.labelResource),
                    ContextCompat.getDrawable(context, tab.iconResource)!!,
                    color,
                    index == activeTab
                )
            }.toCollection(list)

            barLD.value = list
        }

        private fun setup() {
            tabList = listOf(Tab.SUPER_ASSET, Tab.INBOX, Tab.SENT, Tab.ARCHIVE)

            refreshLiveData()
        }
    }








    @Keep
    enum class Tab(val id: Int, val labelResource: Int, val iconResource: Int) {
        SENT(11, R.string.main_tabs_sent_title, R.drawable.main_tabs_sent_icon),
        INBOX(12, R.string.main_tabs_inbox_title, R.drawable.main_tabs_inbox_icon),
        ARCHIVE(13, R.string.main_tabs_archive_title, R.drawable.main_tabs_archive_icon),
        BROWSE(14, R.string.main_tabs_browse_title, R.drawable.main_tabs_browse_icon),
        SUPER_ASSET(15, R.string.main_tabs_super_asset_title, R.drawable.main_tabs_super_asset_icon)
    }

}