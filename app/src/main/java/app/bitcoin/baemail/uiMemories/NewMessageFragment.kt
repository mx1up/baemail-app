package app.bitcoin.baemail.uiMemories

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.text.method.KeyListener
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableRevealSend
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.drawable.DrawableStyledLabel
import app.bitcoin.baemail.fundingWallet.PaymailExistsHelper
import app.bitcoin.baemail.paymail.SeedPhraseInputFragment
import app.bitcoin.baemail.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import app.bitcoin.baemail.view.NontouchableCoordinatorLayout
import app.bitcoin.baemail.view.ViewBaePayOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_funding_coins.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import java.util.*
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

class NewMessageFragment : Fragment(R.layout.fragment_new_message) {

    companion object {
        const val KEY_IS_NEW_REPLY = "new_reply"
        const val KEY_DESTINATION_PAYMAIL = "destination_paymail"
        const val KEY_SUBJECT = "subject"
        const val KEY_THREAD = "thread"
        const val KEY_MILLIS_CREATED = "millis_created"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: NewMessageViewModel




    private lateinit var discard: Button


    private lateinit var container: NontouchableCoordinatorLayout
    private lateinit var contentContainer: ConstraintLayout
    private lateinit var moneyOptions: ConstraintLayout

    private lateinit var moneyOptionsDimmingView: View
    private lateinit var moneyOptionsContent: ViewBaePayOptions

    private lateinit var messageType: TextView
    private lateinit var send: TextView
    private lateinit var inputTo: EditText
    private lateinit var inputSubject: EditText
    private lateinit var inputContent: EditText
    private lateinit var validIndicator: ImageView


    private lateinit var replyToLabel: TextView
    private lateinit var replyToValue: TextView
    private lateinit var replyToClear: ImageView
    private lateinit var replyToSeparator: View

    private lateinit var coordinator: CoordinatorLayout

    lateinit var appBar: AppBarLayout
    lateinit var scroller: NestedScrollView

    lateinit var bottomGap: View
    lateinit var hoverHolder: ConstraintLayout

    lateinit var close: ImageView




    private var currentSnackBar: Snackbar? = null

    private lateinit var optionsSheetBehavior: BottomSheetBehavior<View>


    private val appliedInsets = SeedPhraseInputFragment.AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()



    private var drawableMessageSent: DrawableStyledLabel? = null




    private val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }
    private val colorSelectorOnLight: Int by lazy {
        R.color.selector_on_light.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorSendBackground: Int by lazy {
        R.color.paymail_id_input_fragment__next_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorBlue2: Int by lazy {
        Color.parseColor("#007AE6")
    }

    private val colorSendBackgroundDisabled: Int by lazy {
        R.color.paymail_id_input_fragment__next_background_disabled.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val drawableSendBackground: ColorDrawable by lazy {
        ColorDrawable(colorSendBackgroundDisabled)
    }

    private val drawableSendSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }







    private val colorSuccess: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_success.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorChecking: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_processing.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    private val colorFail: Int by lazy {
        R.color.send_coin_to_paymail__input_error_color_fail.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }



    private val drawablePaymailExists: Drawable by lazy {
        ContextCompat.getDrawable(requireContext(), R.drawable.baseline_check_circle_24)!!.also {
            it.setTint(colorSuccess)
        }
    }
    private val drawablePaymailNonExistent: Drawable by lazy {
        ContextCompat.getDrawable(requireContext(), R.drawable.baseline_cancel_24)!!.also {
            it.setTint(colorFail)
        }
    }
    private val drawablePaymailExistentecChecking: Drawable by lazy {
        ContextCompat.getDrawable(requireContext(), R.drawable.ic_round_hourglass_top_24)!!.also {
            it.setTint(colorChecking)
        }
    }


    private val dp: Float by lazy {
        resources.displayMetrics.density
    }



    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)





        val isCreatingNewReply = requireArguments().getBoolean(KEY_IS_NEW_REPLY)

        val destinationPaymail = if (isCreatingNewReply) {
            requireArguments().getString(KEY_DESTINATION_PAYMAIL)
        } else null

        val subject = if (isCreatingNewReply) {
            requireArguments().getString(KEY_SUBJECT)
        } else null

        val replyThreadId = if (isCreatingNewReply) {
            requireArguments().getString(KEY_THREAD)
        } else null

        val millisReplyCreated = if (isCreatingNewReply) {
            requireArguments().getLong(KEY_MILLIS_CREATED)
        } else null







        viewModel = ViewModelProvider(requireActivity(), viewModelFactory)
            .get(NewMessageViewModel::class.java)

        if (isCreatingNewReply/* && millisReplyCreated!! > viewModel.millisLastReplyInit*/) {
            viewModel.initNewReply(destinationPaymail!!, subject!!, replyThreadId!!, millisReplyCreated!!)
        }







        container = requireView().findViewById(R.id.container)
        contentContainer = requireView().findViewById(R.id.content_container)
        moneyOptions = requireView().findViewById(R.id.money_options)

        moneyOptionsDimmingView = requireView().findViewById(R.id.dimming_view)
        moneyOptionsContent = requireView().findViewById(R.id.money_options_content)



        coordinator = requireView().findViewById(R.id.coordinator)

        appBar = requireView().findViewById(R.id.app_bar)
        scroller = requireView().findViewById(R.id.scroller)

        close = requireView().findViewById(R.id.close)

        bottomGap = requireView().findViewById(R.id.bottom_gap)
        hoverHolder = requireView().findViewById(R.id.hover_holder)

        discard = requireView().findViewById(R.id.discard)

        messageType = requireView().findViewById(R.id.message_type)
        send = requireView().findViewById(R.id.send)
        inputTo = requireView().findViewById(R.id.input_to)
        inputSubject = requireView().findViewById(R.id.input_subject)
        inputContent = requireView().findViewById(R.id.input_content)
        validIndicator = requireView().findViewById(R.id.valid_indicator)

        replyToLabel = requireView().findViewById(R.id.label_reply_to)
        replyToValue = requireView().findViewById(R.id.value_reply_to)
        replyToClear = requireView().findViewById(R.id.clear_reply_to)
        replyToSeparator = requireView().findViewById(R.id.reply_to_bottom_separator)


        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)



        contentContainer.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop//,
                //,
                //bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }





        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height// - appliedInsets.top// - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")


            hoverHolder.translationY = -1f * keyboardHeight

            //this view touches the bottom of the layout

            val canScrollDown = scroller.canScrollVertically(1)

            scroller.updatePadding(
                bottom = /*(dp * 75).toInt() + */keyboardHeight
            )

//            if (!canScrollDown && inputContent.isFocused) {
//                Timber.d(">>> doing extra scroll")
//                scroller.scrollBy(0, keyboardHeight)
//            }


        })




        let { //avoid drawing the shadow appBar automatically lays out with at first
            var cleanupListener: () -> Unit = {
                Timber.d("EMPTY!!!")
            }

            val listener = object : ViewTreeObserver.OnPreDrawListener {

                override fun onPreDraw(): Boolean {
                    appBar.elevation = 0f

                    cleanupListener()

                    return true
                }

            }

            cleanupListener = {
                appBar.viewTreeObserver.removeOnPreDrawListener(listener)
            }
            appBar.viewTreeObserver.addOnPreDrawListener(listener)
        }

        scroller.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView?,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                if (scrollY < 8 * resources.displayMetrics.density) {
                    appBar.elevation = 0f
                } else {
                    appBar.elevation = topBarElevation
                }
            }

        })

        scroller.overScrollMode = View.OVER_SCROLL_NEVER








        let {
            val colorAccent = R.color.message_thread__subject_text_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }


            val colorSelector = R.color.selector_on_light.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }
            val colorSelectorOnAccent = R.color.selector_on_accent.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            close.foreground = DrawableState.getNew(colorSelector)

            val drawableCloseIcon = R.drawable.ic_clear_24.let { id ->
                val d = ContextCompat.getDrawable(requireContext(), id)!!
                d.setTint(colorAccent)
                d
            }
            close.setImageDrawable(drawableCloseIcon)

            close.setOnClickListener {
                imm.hideSoftInputFromWindow(
                    requireView().windowToken,
                    0
                )

                findNavController().popBackStack()
            }

            close.clipToOutline = true
            close.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setOval(0, 0, view.width, view.height)
                }

            }
        }





        val optionsSheetLP = moneyOptions.layoutParams as CoordinatorLayout.LayoutParams
        optionsSheetBehavior = optionsSheetLP.behavior as BottomSheetBehavior

        optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        optionsSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            var collapsingBackPressedCallback: CollapsingBackPressedCallback? = null
            inner class CollapsingBackPressedCallback : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Timber.d("collapsingBackPressedCallback .. handleOnBackPressed")
                    optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

                    remove()
                    collapsingBackPressedCallback = null
                }

            }


            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Timber.d("collapsingBackPressedCallback .. onStateChanged newState:$newState")

                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    collapsingBackPressedCallback.let {
                        if (it != null) return@let
                        val cb = CollapsingBackPressedCallback()
                        collapsingBackPressedCallback = cb
                        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, cb)
                        Timber.d("collapsingBackPressedCallback .. added")
                    }

                } else if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    collapsingBackPressedCallback?.remove()
                    collapsingBackPressedCallback = null
                    Timber.d("collapsingBackPressedCallback .. removed")
                }

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //do nothing
            }

        })







//        inputTo.addTextChangedListener(inputToWatcher)
//        inputSubject.addTextChangedListener(inputSubjectWatcher)
//        inputContent.addTextChangedListener(inputContentWatcher)











        replyToValue.background = DrawableState.getNew(colorSelectorOnLight)
        replyToValue.clipToOutline = true
        replyToValue.outlineProvider = object : ViewOutlineProvider() {

            val paddingPx = (6 * resources.displayMetrics.density).toInt()
            val radius = 8 * resources.displayMetrics.density

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(paddingPx, paddingPx, view.width - paddingPx, view.height - paddingPx, radius)
            }

        }


        replyToValue.setOnClickListener {
            if (isCreatingNewReply) {
                findNavController().popBackStack()
                return@setOnClickListener
            }

            //go to message
            val txId = viewModel.newMessageContentLD.value?.replyThreadId ?: let {
                Timber.d("id not found")
                return@setOnClickListener
            }
            val ownerPaymail = viewModel.activePaymail ?: let {
                Timber.d("activePaymail not found")
                return@setOnClickListener
            }


            val args = Bundle().also {
                it.putString(BaemailMessageFragment.KEY_TX_ID, txId)
                it.putString(BaemailMessageFragment.KEY_OWNER_PAYMAIL, ownerPaymail)
            }

            findNavController().navigate(R.id.action_newMessageFragment_to_baemailMessageFragment, args)

        }


        replyToClear.background = DrawableState.getNew(colorSelectorOnLight)
        replyToClear.clipToOutline = true
        replyToClear.outlineProvider = object : ViewOutlineProvider() {

            val paddingPx = (6 * resources.displayMetrics.density).toInt()

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val finalHeight = view.height - 2 * paddingPx

                val left = ((view.width / 2f) - (finalHeight / 2)).toInt()


                outline.setOval(left, paddingPx, left + finalHeight, paddingPx + finalHeight)
            }

        }

        replyToClear.setOnClickListener {
            viewModel.onClearReplyTo()
        }








        messageType.clipToOutline = true
        messageType.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        val colorCancelBackground = R.color.green2.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        messageType.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))








        send.clipToOutline = true
        send.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        send.background = LayerDrawable(arrayOf(
            drawableSendBackground,
            drawableSendSelector
        ))





        moneyOptionsContent.onMinusClicked = {
            viewModel.onPayOptionsMinus()
        }

        moneyOptionsContent.onPlusClicked = {
            viewModel.onPayOptionsPlus()
        }

        moneyOptionsContent.onValueChanged = { value ->
            viewModel.onPayOptionsValueChange(value)
        }




        moneyOptionsDimmingView.setOnClickListener {
            optionsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        messageType.setOnClickListener {

            imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
            )

            optionsSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        send.setOnClickListener {
            imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
            )


            viewModel.onSendMessage()

            //break touch input of the whole fragment
            container.setTouchBroken(true)
        }




        val dp = resources.displayMetrics.density
        val dpToMaxProgress = 100 * dp

        //this controls the animated overlays of the send-button
        send.setOnTouchListener(object : View.OnTouchListener {
            val millisShortHoldPeriod = 1000 * 1L
            var millisStart = -1L

            var yOfActionDown = -1f


            val d = DrawableRevealSend(
                dp,
                600 * dp,
                colorSendBackground,
                Color.WHITE,
                colorBlue2,
                Color.RED
            )

            var drawablSwipeHint: DrawableStyledLabel? = null
            var jobAnimateHideHint: Job? = null

//            var drawableMessageSent: DrawableStyledLabel? = null

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                event ?: return false
                v ?: return false

                return when (event.actionMasked) {
                    MotionEvent.ACTION_DOWN -> {
                        onStart(event)

                        true
                    }
                    MotionEvent.ACTION_MOVE -> {

                        val y = event.y

                        val distanceY = yOfActionDown - y
                        val progress = max(distanceY / dpToMaxProgress, 0f)

                        d.setProgress(progress)

                        false
                    }
                    MotionEvent.ACTION_UP -> {
                        onStop(v, event)

                        false
                    }
                    else -> false
                }
            }

            fun onStart(event: MotionEvent) {
                Timber.d("onStart ....")


                val locArrayOfSend = IntArray(2)
                send.getLocationOnScreen(locArrayOfSend)
                val locArrayOfView = IntArray(2)
                requireView().getLocationOnScreen(locArrayOfView)

                d.setProgress(0f)
                d.setBounds(0, 0, requireView().width, requireView().height)

                d.setStartLoc(
                    locArrayOfSend[0] - locArrayOfView[0] + event.x,
                    locArrayOfSend[1] - locArrayOfView[1] + event.y
                )



                millisStart = SystemClock.elapsedRealtime()
                yOfActionDown = event.y




                drawablSwipeHint?.let {
                    requireView().overlay.remove(it)
                    drawablSwipeHint = null
                }
                jobAnimateHideHint?.let {
                    it.cancel()
                    jobAnimateHideHint = null
                }



                requireView().overlay.add(d)

            }

            fun onSent(v: View) {
                v.performClick()
            }

            fun onStop(v: View, event: MotionEvent) {
                val now = SystemClock.elapsedRealtime()

                if (d.getLastProgress() > 1.7f) {

                    //trigger the event officially
                    onSent(v)

                    //craft the style of the drawable
                    val pos = d.getDrawPositionOfSend()

                    val sent =  DrawableStyledLabel(
                        dp,
                        "SENT",
                        pos.x,
                        pos.y,
                        labelTextSize = d.getSizeOfSend(),
                        labelColor = Color.RED,
                        labelAlign = Paint.Align.CENTER
                    )

                    sent.setBounds(0, 0, requireView().width, requireView().height)


                    drawableMessageSent = sent
                    requireView().overlay.add(sent)

                    viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                        try {
                            delay((5f * 1000).toLong())
                        } catch (e: Exception) {
                            Timber.e(e, "got cancelled")

                            //todo do some cleanup

                            return@launchWhenStarted
                        }

                        Timber.d("5 secs have passed")

                        requireView().overlay.remove(sent)
                        if (sent == drawableMessageSent) {
                            drawableMessageSent = null
                        }
                    }


                } else if (now - millisStart < millisShortHoldPeriod) {
                    Timber.d(">>>>>>>> show hint that teaches sending ... event.x:${event.x} event.y:${event.y}")

                    if (event.x > 0f && event.x < v.width &&
                            event.y > 0f && event.y < v.height) {
                        Timber.d(">>>>>>>> the touch is on the button")

                        //show the swipe-hint

                        //remove an existing hint if any
                        drawablSwipeHint?.let {
                            requireView().overlay.remove(it)
                            drawablSwipeHint = null
                        }
                        jobAnimateHideHint?.let {
                            it.cancel()
                            jobAnimateHideHint = null
                        }

                        //craft the style of the drawable
                        val locArrayOfSend = IntArray(2)
                        send.getLocationOnScreen(locArrayOfSend)
                        val locArrayOfView = IntArray(2)
                        requireView().getLocationOnScreen(locArrayOfView)

                        val xOfStart = locArrayOfSend[0] - locArrayOfView[0]
                        val yOfBottom = locArrayOfSend[1] - locArrayOfView[1] - 1 *
                                dp * 12

                        val hint =  DrawableStyledLabel(
                            dp,
                            "SWIPE UP",
                            (xOfStart + send.width * 0.67f).toInt(),
                            yOfBottom.toInt(),
                            labelTextSize = dp * 42,
                            labelColor = colorSendBackground,
                            labelAlign = Paint.Align.RIGHT
                        )

                        hint.setBounds(0, 0, requireView().width, requireView().height)


                        drawablSwipeHint = hint
                        requireView().overlay.add(hint)

                        jobAnimateHideHint = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                            try {
                                delay((1.6f * 1000).toLong())
                            } catch (e: Exception) {
                                Timber.e(e, "got cancelled")

                                //todo do some cleanup

                                return@launchWhenStarted
                            }

                            Timber.d("1.6 secs have passed")

                            drawablSwipeHint?.let {
                                requireView().overlay.remove(it)
                                drawablSwipeHint = null
                            }
                        }
                    }



                } else {
                    Timber.d(">>>>>>>> end of LONG HOLD")
                }

                //todo rm

                requireView().overlay.remove(d)
            }


        })





        discard.setOnClickListener {
            imm.hideSoftInputFromWindow(
                requireView().windowToken,
                0
            )

            viewModel.onDraftDiscarded()

            findNavController().popBackStack()
        }





        viewModel.paymailExistsLD.observe(viewLifecycleOwner, Observer {
            if (it == null || (viewModel.newMessageContentLD.value?.paymail?.length ?: 0) < 3) {
                validIndicator.visibility = View.INVISIBLE

            } else {
                validIndicator.visibility = View.VISIBLE
            }

            if (PaymailExistsHelper.State.CHECKING == it?.first) {
                validIndicator.setImageDrawable(drawablePaymailExistentecChecking)

            } else if (PaymailExistsHelper.State.VALID == it?.first) {
                validIndicator.setImageDrawable(drawablePaymailExists)

            } else if (PaymailExistsHelper.State.INVALID == it?.first) {
                validIndicator.setImageDrawable(drawablePaymailNonExistent)
            }

            Timber.d("existence refreshed")
        })


        viewModel.newMessageContentLD.observe(viewLifecycleOwner, object : Observer<NewMessageViewModel.ContentModel> {
            var lastReplyInitState: Boolean? = null

            override fun onChanged(it: NewMessageViewModel.ContentModel?) {
                it ?: return

                var isBaemailValid = false

                when (it.state) {
                    NewMessageViewModel.State.SENDING_SUCCESS -> {
                        viewModel.resetModel()
                        findNavController().popBackStack()
                        return
                    }
                    NewMessageViewModel.State.PAYMAIL_VALID -> {
                        isBaemailValid = true
                    }
                    else -> {
                    }
                }





                inputTo.removeTextChangedListener(inputToWatcher)
                inputSubject.removeTextChangedListener(inputSubjectWatcher)
                inputContent.removeTextChangedListener(inputContentWatcher)

                if (inputTo.text.toString() != it.paymail && !inputTo.isFocused) {
                    inputTo.setText(it.paymail)
                }
                if (inputSubject.text.toString() != it.subject && !inputSubject.isFocused) {
                    inputSubject.setText(it.subject)
                }
                if (inputContent.text.toString() != it.content/*&& !inputContent.isFocused*/) {
                    val currentPos = inputContent.selectionStart

                    inputContent.setText(it.content)

                    val finalSelection: Int = min(it.content.length, currentPos)
                    inputContent.setSelection(finalSelection)
                }

                inputTo.addTextChangedListener(inputToWatcher)
                inputSubject.addTextChangedListener(inputSubjectWatcher)
                inputContent.addTextChangedListener(inputContentWatcher)




                if (it.subject.length < 3 || it.content.length < 3) {
                    isBaemailValid = false
                }

                if (it.satsAvailable < it.satsEstimatedRequired) {
                    isBaemailValid = false
                }

                if (!isBaemailValid) {
                    send.isEnabled = false
                    drawableSendBackground.color = colorSendBackgroundDisabled
                } else {
                    send.isEnabled = true
                    drawableSendBackground.color = colorSendBackground
                }



                moneyOptionsContent.applyModel(it.payOptions)

                val messageTypeLabel = String.format(
                    Locale.US,
                    "Pay %.2f %s",
                    it.payOptions.valueCurrent,
                    it.payOptions.currency
                )
                if (messageTypeLabel != messageType.text.toString()) {
                    messageType.text = messageTypeLabel
                }


                if (NewMessageViewModel.State.SENDING_ERROR == it.state) {
                    drawableMessageSent?.updateLabel("FAIL")
                    //todo localize
                    //todo
                    //todo
                }

                if (NewMessageViewModel.State.FUNDING_ERROR == it.state) {
                    currentSnackBar?.dismiss()

                    val sb = Snackbar.make(
                        coordinator, "Funding wallet needs more juice",
                        Snackbar.LENGTH_INDEFINITE
                    )
                    currentSnackBar = sb

                    sb.show()
                } else {
                    currentSnackBar?.dismiss()
                    currentSnackBar = null
                }





                it.replyThreadId?.let { id ->
                    //must be visible
                    if (replyToValue.tag == id) return@let
                    lastReplyInitState = true

                    replyToLabel.visibility = View.VISIBLE
                    replyToValue.visibility = View.VISIBLE
                    replyToClear.visibility = View.VISIBLE
                    replyToSeparator.visibility = View.VISIBLE

                    replyToValue.text = id
                    replyToValue.tag = id

                } ?: let {
                    //must be hidden
                    if (lastReplyInitState == false) return@let
                    lastReplyInitState = false

                    replyToLabel.visibility = View.GONE
                    replyToValue.visibility = View.GONE
                    replyToClear.visibility = View.GONE
                    replyToSeparator.visibility = View.GONE

                    replyToValue.tag = null

                }



                if (it.replyThreadId == null) {
                    if (!inputTo.isEnabled) {
                        inputTo.isEnabled = true
                    }
                    if (!inputSubject.isEnabled) {
                        inputSubject.isEnabled = true
                    }
                } else {
                    if (inputTo.isEnabled) {
                        inputTo.isEnabled = false
                    }
                    if (inputSubject.isEnabled) {
                        inputSubject.isEnabled = false
                    }
                }


                Timber.d("content observer")

            }

        })



    }

    private val inputToWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onDestinationInputChanged(s.toString())
        }

    }

    private val inputSubjectWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return
            viewModel.onSubjectInputChanged(s.toString())
        }

    }

    private val inputContentWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            s ?: return

            val value = s.toString()

            run { //scroll the view when the user presses `enter` so the cursor does not end up
                // behing the keyboard
                val countOfEnters = value.filter { c -> c == '\n' }.length
                val lastValue = viewModel.newMessageContentLD.value?.content
                val countOfEntersBefore = lastValue?.let {
                    it.filter { c -> c == '\n' }.length
                } ?: 0


                if (countOfEnters > countOfEntersBefore) {
                    Timber.d("new enters detected, scroll the view")
                    val count = countOfEnters - countOfEntersBefore
                    viewLifecycleOwner.lifecycleScope.launch {
                        delay(50L)
                        scroller.smoothScrollBy(0, inputContent.lineHeight * count)
                    }
                }
            }

            viewModel.onContentInputChanged(value)
        }

    }
}






