package app.bitcoin.baemail.uiMemories

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.ViewOutlineProvider
import android.view.ViewTreeObserver
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.message.MessageBucket
import app.bitcoin.baemail.paymail.SeedPhraseInputFragment
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import app.bitcoin.baemail.view.NontouchableCoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class BaemailMessageFragment : Fragment(R.layout.fragment_baemail_message)  {

    companion object {
        const val KEY_TX_ID = "tx_id"
        const val KEY_OWNER_PAYMAIL = "owner_paymail"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: BaemailMessageViewModel


    private lateinit var container: NontouchableCoordinatorLayout
    private lateinit var contentContainer: ConstraintLayout

    lateinit var close: ImageView
    lateinit var reply: Button
    lateinit var archive: Button
    lateinit var unarchive: Button
    lateinit var delete: Button


    lateinit var valueFrom: TextView
    lateinit var valueTo: TextView
    lateinit var valueSubject: TextView
    lateinit var valueContent: TextView
    lateinit var valueDate: TextView
    lateinit var valueAmount: TextView
    lateinit var valueTxId: TextView

    lateinit var labelReplyTo: TextView
    lateinit var valueReplyTo: TextView
    lateinit var replyToBottomSeparator: View


    lateinit var appBar: AppBarLayout
    lateinit var scroller: NestedScrollView

    lateinit var bottomGap: View



    private val appliedInsets = SeedPhraseInputFragment.AppliedInsets()


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val txId = requireArguments().getString(KEY_TX_ID)!!
        val ownerPaymail = requireArguments().getString(KEY_OWNER_PAYMAIL)!!




        AndroidSupportInjection.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(BaemailMessageViewModel::class.java)


        viewModel.init(ownerPaymail, txId)



        container = requireView().findViewById(R.id.container)
        contentContainer = requireView().findViewById(R.id.content_container)

        appBar = requireView().findViewById(R.id.app_bar)
        scroller = requireView().findViewById(R.id.scroller)

        close = requireView().findViewById(R.id.close)



        valueFrom = requireView().findViewById(R.id.value_from)
        valueTo = requireView().findViewById(R.id.value_to)
        valueSubject = requireView().findViewById(R.id.value_subject)
        valueContent = requireView().findViewById(R.id.value_content)
        valueDate = requireView().findViewById(R.id.value_date)
        valueAmount = requireView().findViewById(R.id.value_amount)
        valueTxId = requireView().findViewById(R.id.value_tx_id)

        labelReplyTo = requireView().findViewById(R.id.label_reply_to)
        valueReplyTo = requireView().findViewById(R.id.value_reply_to)
        replyToBottomSeparator = requireView().findViewById(R.id.reply_to_bottom_separator)


        reply = requireView().findViewById(R.id.reply)
        archive = requireView().findViewById(R.id.archive)
        unarchive = requireView().findViewById(R.id.unarchive)
        delete = requireView().findViewById(R.id.delete)



        bottomGap = requireView().findViewById(R.id.bottom_gap)

//        discard = requireView().findViewById(R.id.discard)



        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)



        contentContainer.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop//,
                //,
                //bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }



        let { //avoid drawing the shadow appBar automatically lays out with at first
            var cleanupListener: () -> Unit = {
                Timber.d("EMPTY!!!")
            }

            val listener = object : ViewTreeObserver.OnPreDrawListener {

                override fun onPreDraw(): Boolean {
                    appBar.elevation = 0f

                    cleanupListener()

                    return true
                }

            }

            cleanupListener = {
                appBar.viewTreeObserver.removeOnPreDrawListener(listener)
            }
            appBar.viewTreeObserver.addOnPreDrawListener(listener)
        }





        scroller.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView?,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                if (scrollY < 8 * resources.displayMetrics.density) {
                    appBar.elevation = 0f
                } else {
                    appBar.elevation = topBarElevation
                }
            }

        })

        scroller.overScrollMode = View.OVER_SCROLL_NEVER




        val colorSelector = R.color.selector_on_light.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        let {
            val colorAccent = R.color.message_thread__subject_text_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }


            val colorSelectorOnAccent = R.color.selector_on_accent.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }

            close.foreground = DrawableState.getNew(colorSelector)

            val drawableCloseIcon = R.drawable.ic_clear_24.let { id ->
                val d = ContextCompat.getDrawable(requireContext(), id)!!
                d.setTint(colorAccent)
                d
            }
            close.setImageDrawable(drawableCloseIcon)

            close.setOnClickListener {
                findNavController().popBackStack()
            }

            close.clipToOutline = true
            close.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    view ?: return
                    outline ?: return

                    outline.setOval(0, 0, view.width, view.height)
                }

            }
        }















        valueFrom.setOnClickListener {
            copyFieldValueToClipBoard(
                "From",
                valueFrom.text.toString()
            )
        }

        valueTo.setOnClickListener {
            copyFieldValueToClipBoard(
                "To",
                valueTo.text.toString()
            )
        }

        valueSubject.setOnClickListener {
            copyFieldValueToClipBoard(
                "Subject",
                valueSubject.text.toString()
            )
        }

//        valueContent.setOnClickListener {
//            copyFieldValueToClipBoard(
//                "Content",
//                valueContent.text.toString()
//            )
//        }

        valueDate.setOnClickListener {
            copyFieldValueToClipBoard(
                "Date",
                valueDate.text.toString()
            )
        }

        valueAmount.setOnClickListener {
            copyFieldValueToClipBoard(
                "Amount",
                valueAmount.text.toString()
            )
        }

        valueTxId.setOnClickListener {
            copyFieldValueToClipBoard(
                "Tx ID",
                valueTxId.text.toString()
            )
        }


        valueReplyTo.background = DrawableState.getNew(colorSelector)
        valueReplyTo.clipToOutline = true
        valueReplyTo.outlineProvider = object : ViewOutlineProvider() {

            val paddingPx = (8 * resources.displayMetrics.density).toInt()
            val radius = 8 * resources.displayMetrics.density

            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                outline.setRoundRect(paddingPx, paddingPx, view.width - paddingPx, view.height - paddingPx, radius)
            }

        }
        valueReplyTo.setOnClickListener {

            val ownerPaymail = viewModel.contentLD.value?.ownerPaymail ?: return@setOnClickListener
            val replyToTxId = viewModel.contentLD.value?.message?.replyToTxId ?: return@setOnClickListener

            val args = Bundle().also {
                it.putString(KEY_TX_ID, replyToTxId)
                it.putString(KEY_OWNER_PAYMAIL, ownerPaymail)
            }

            findNavController().navigate(R.id.action_baemailMessageFragment_self, args)
        }


        reply.setOnClickListener {
            val txId = viewModel.contentLD.value?.message?.txId ?: let {
                Timber.d("txId not found")
                return@setOnClickListener
            }

            val model = viewModel.contentLD.value ?: let {
                Timber.d("message not found")
                return@setOnClickListener
            }

            val destinationPaymail = if (model.message.to == model.ownerPaymail) {
                model.message.from
            } else {
                model.message.to
            }



            val args = Bundle().also {
                it.putBoolean(NewMessageFragment.KEY_IS_NEW_REPLY, true)
                it.putString(NewMessageFragment.KEY_THREAD, txId)
                it.putString(NewMessageFragment.KEY_DESTINATION_PAYMAIL, destinationPaymail)
                it.putString(NewMessageFragment.KEY_SUBJECT, model.message.subject)
                it.putLong(NewMessageFragment.KEY_MILLIS_CREATED, System.currentTimeMillis())
            }

            findNavController().navigate(R.id.action_baemailMessageFragment_to_newMessageFragment, args)
        }


        archive.setOnClickListener {
            viewModel.archiveMessage()

            findNavController().popBackStack()
        }

        unarchive.setOnClickListener {
            viewModel.unarchiveMessage()

            findNavController().popBackStack()
        }

        delete.setOnClickListener {
            viewModel.deleteMessage()

            findNavController().popBackStack()
        }





        viewModel.contentLD.observe(viewLifecycleOwner, Observer {
            if (it == null) {
                Toast.makeText(requireContext(), "Message not found", Toast.LENGTH_LONG).show()
                findNavController().popBackStack()
                return@Observer
            }


            //init the content
            valueFrom.text = it.message.from
            valueTo.text = it.message.to
            valueSubject.text = it.message.subject.trim()
            valueDate.text = it.formattedMessageDateTime
            valueAmount.text = "${it.message.amountUsd}USD"
            valueTxId.text = it.message.txId

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                valueContent.text = Html.fromHtml(it.parsedContent, Html.FROM_HTML_MODE_COMPACT)
            } else {
                valueContent.text = Html.fromHtml(it.parsedContent)
            }


            if (it.message.replyToTxId == null) {
                labelReplyTo.visibility = View.GONE
                valueReplyTo.visibility = View.GONE
                replyToBottomSeparator.visibility = View.GONE
            } else {
                labelReplyTo.visibility = View.VISIBLE
                valueReplyTo.visibility = View.VISIBLE
                replyToBottomSeparator.visibility = View.VISIBLE

                valueReplyTo.text = it.message.replyToTxId
            }



            when (it.message.bucket) {
                MessageBucket.INBOX -> {
                    reply.visibility = View.VISIBLE
                    archive.visibility = View.VISIBLE
                    unarchive.visibility = View.GONE
                    delete.visibility = View.GONE
                }
                MessageBucket.SENT -> {
                    reply.visibility = View.VISIBLE
                    archive.visibility = View.GONE
                    unarchive.visibility = View.GONE
                    delete.visibility = View.VISIBLE
                }
                MessageBucket.ARCHIVE -> {
                    reply.visibility = View.VISIBLE
                    archive.visibility = View.GONE
                    unarchive.visibility = View.VISIBLE
                    delete.visibility = View.VISIBLE
                }
            }

        })




    }



    val cbm: ClipboardManager by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    private fun copyFieldValueToClipBoard(field: String, value: String) {
        cbm.let { manager ->
            val clip = ClipData.newPlainText("$field value", value)

            manager.setPrimaryClip(clip)
            Snackbar.make(container, "$field copied", Snackbar.LENGTH_SHORT).show()
            //Toast.makeText(requireContext(), "$field copied", Toast.LENGTH_SHORT).show()
        }
    }



}