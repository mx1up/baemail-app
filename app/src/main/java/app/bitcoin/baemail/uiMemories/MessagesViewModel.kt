package app.bitcoin.baemail.uiMemories

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.format.DateFormat
import androidx.lifecycle.*
import androidx.savedstate.SavedStateRegistryOwner
import app.bitcoin.baemail.message.*
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.recycler.ATContentLoading0
import app.bitcoin.baemail.recycler.ATHeightDecorDynamic
import app.bitcoin.baemail.recycler.ATMessage0
import app.bitcoin.baemail.recycler.AdapterType
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.wallet.BlockchainDataSource
import app.bitcoin.baemail.wallet.ChainSync
import app.bitcoin.baemail.wallet.WalletRepository
import com.dropbox.android.external.store4.ResponseOrigin
import com.dropbox.android.external.store4.StoreResponse
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class MessagesViewModel constructor(
    val appContext: Context,
    val state : SavedStateHandle,
    val walletRepository: WalletRepository,
    val paymailRepository: PaymailRepository,
    val baemailRepository: BaemailRepository,
    val blockchainDataSource: BlockchainDataSource,
    val secureDataSource: SecureDataSource,
    val chainSync: ChainSync,
    val appDatabase: AppDatabase,
    val gson: Gson
): ViewModel() {

    companion object {
        private const val KEY_SORT_TYPE = "sort_type"
        private const val KEY_SEARCH_QUERY = "query"
        private const val KEY_BUCKET = "bucket"
        private const val KEY_INITIALIZED = "initialized"
    }

    private val stateHandler = StateHandler(state)

    val queryLD: LiveData<String>
        get() = stateHandler._queryLD


    val sortLD: LiveData<SortType>
        get() = stateHandler._sortLD

    val bucket: MessageBucket
        get() = stateHandler.bucket

    //this is generated dynamically & set via a method
    private val searchParamLD = MutableLiveData<MessageFilterKey>()
    private val searchResultsLD: LiveData<List<AdapterType>> =
        Transformations.switchMap(searchParamLD) { filter: MessageFilterKey? ->

            Timber.d("filter-change:$filter")

            if (filter == null) {
                return@switchMap _contentEmptyLD
            }

            messageFetchHelper.get(filter)
        }

    //this is updated dynamically as `searchResultsLD` updates
    private val _refreshingLD = MutableLiveData<Boolean>()
    val refreshingLD: LiveData<Boolean>
        get() = _refreshingLD


    //todo auto-initialize this with default values
    private val _contentEmptyLD = MutableLiveData<List<AdapterType>>()


    //Final live-data that will updated with content matching the current filter.
    //This should be observed by the view.
    private val _contentLD = MediatorLiveData<List<AdapterType>>()
    val contentLD: LiveData<List<AdapterType>>
        get() = _contentLD


    //helper for baemail-messages
    private val messageFetchHelper = MessageFetchHelper()




    private val messageDateFormat = DateFormat.getMediumDateFormat(appContext)


    init {
        if (stateHandler.isInitialized()) {
            doInit()
        }
    }


    fun init(bucket: MessageBucket, initialSort: SortType) {
        if (stateHandler.isInitialized()) return
        stateHandler.setInitialized()

        stateHandler.setNewBucket(bucket)
        stateHandler.setNewSort(initialSort)

        doInit()
    }

    private fun doInit() {
        _contentLD.addSource(paymailRepository.activePaymailLD, object : Observer<AuthenticatedPaymail> {
            private var previousPaymail: String? = null

            override fun onChanged(it: AuthenticatedPaymail?) {
                val paymail = it?.paymail

                if (previousPaymail == paymail) return //unchanged
                previousPaymail = paymail

                refreshFilterParam()

            }

        })


        _contentLD.addSource(searchResultsLD) {
            refreshMessageList()
        }



    }













    fun onSwipeToRefresh() {

        val key = searchParamLD.value!!

        SortType.values().forEach {
            val filter = key.copy(sort = it)
            Timber.d("cached results cleared for filter: $filter")
            messageFetchHelper.resetCachedKey(filter)
        }

        messageFetchHelper.requestWipeBucketOnFetch(key.bucket)

        refreshFilterParam()
    }

    fun onSignalRequestNextPage() {
        viewModelScope.launch(Dispatchers.Main) {

            delay(400L)


            messageFetchHelper.currentItem?.let {
                if (!it.pagedFetchHolder.hasFetchCompleted) {
                    Timber.d(">>>>>>> onSignalRequestNextPage dropped; still fetching the current page")
                    //todo do recursion?
                    Timber.d(">>>>>>> !!!! doing recursion!!!!!! #onSignalRequestNextPage()")
                    onSignalRequestNextPage()
                    return@launch
                }

                if (!it.pagedFetchHolder.hasNextPage) {
                    Timber.d(">>>>>>> onSignalRequestNextPage dropped; there is no next page")
                    return@launch
                }

                Timber.d(">>>>>>> onSignalRequestNextPage doing now")
                val current = searchParamLD.value!!
                searchParamLD.postValue(current.copy(
                    page = current.page + 1
                ))
            }
        }

    }

    fun onSortTypeToggled() {
        val currentValue = sortLD.value!!

        val newValue = when (currentValue) {
            SortType.VALUE -> SortType.TIME
            SortType.TIME -> SortType.VALUE
        }

        stateHandler.setNewSort(newValue)
        refreshFilterParam()
    }

    fun onQueryTextChange(query: String): Boolean {
        if (queryLD.value == query) {
            return false
        }

        stateHandler.setNewQuery(query)
        refreshFilterParam()

        return true
    }

    fun onQueryTextSubmit(query: String): Boolean {
        return onQueryTextChange(query)
    }


    fun onRequestClearSearchResults() {
        queryLD.value!!.let {
            if (it.isBlank()) return
        }

        stateHandler.setNewQuery(null)
        refreshFilterParam()
    }










    private fun refreshFilterParam() {
        val queryString = queryLD.value ?: ""
        val sort = sortLD.value ?: SortType.VALUE

        val paymail: String = paymailRepository.activePaymailLD.value?.paymail ?: return

        val cachedPage =  messageFetchHelper.getPageForCachedKey(
            paymail,
            queryString,
            sort,
            bucket
        )


        searchParamLD.value = MessageFilterKey(
            paymail,
            queryString,
            sort,
            bucket,
            cachedPage
        )
    }


    /**
     * This crafts the final content that will be observed by the view's list-component
     */
    private fun refreshMessageList() {
        val value = searchResultsLD.value!!

        val finalList = ArrayList(value)



        messageFetchHelper.currentItem?.pagedFetchHolder?.let { holder ->
            Timber.d(">>>>> #refreshMessageList; holder found; hasPageFetchCompleted:${holder.hasFetchCompleted} hasNextPage:${holder.hasNextPage}")
            if (holder.hasFetchCompleted && holder.hasNextPage) {
                finalList.add(ATContentLoading0(0))
            }
        }

        //todo if the message-list is empty, then show card talking about it
        //todo
        //todo


        //add decor
        finalList.add(ATHeightDecorDynamic(MutableLiveData<Int>().also {
            it.value = (appContext.resources.displayMetrics.density * 95).toInt()
        }))


        Timber.d(">>>>>>> refreshMessageList completed")
        _contentLD.value = finalList
    }






    inner class MessageFetchHelper {

        var currentItem: FilteredHolder? = null

        var requestedWipesSet = HashSet<Pair<String, MessageBucket>>()

        fun requestWipeBucketOnFetch(bucket: MessageBucket) {
            val paymail: String = paymailRepository.activePaymailLD.value?.paymail ?:
                throw RuntimeException()

            requestedWipesSet.add(paymail to bucket)
        }

        fun getPageForCachedKey(
            paymail: String,
            query: String?,
            sortType: SortType,
            bucket: MessageBucket
        ): Int {
            return baemailRepository.messageFetchHelper.getPageForCachedKey(
                paymail,
                query,
                sortType,
                bucket
            )
        }

        fun resetCachedKey(filter: MessageFilterKey) {
            baemailRepository.messageFetchHelper.resetCachedKey(filter)
        }

        fun search(filter: MessageFilterKey): FilteredHolder {
            Timber.d(">>>>> search message list; $filter")

            val wasWipeOfBucketRequested = requestedWipesSet
                .contains(filter.paymail to filter.bucket)
            if (wasWipeOfBucketRequested) {
                requestedWipesSet.remove(filter.paymail to filter.bucket)
            }


            val holder = baemailRepository.messageFetchHelper.getFlowForFilter(
                filter,
                wipeBucketOnPreInsert = wasWipeOfBucketRequested
            )

            val dataLd = holder.flow.asLiveData()

            val viewHolderModelLd = MediatorLiveData<List<AdapterType>>()
            viewHolderModelLd.value = emptyList()

            viewHolderModelLd.addSource(dataLd) {

                val paymail: String = paymailRepository.activePaymailLD.value?.paymail ?: let {
                    //todo possibly crate a special error-item
                    viewHolderModelLd.value = emptyList()
                    return@addSource
                }

                when (it) {
                    is StoreResponse.Loading<*> -> {
                        Timber.d(">>>>> !!! showLoadingSpinner ...origin:${it.origin}")
                        _refreshingLD.value = true

                        //do not modify the cotent-ld
                    }
                    is StoreResponse.Data<*> -> {
                        if (it.origin == ResponseOrigin.Fetcher) {
                            Timber.d(">>>>> !!! hideLoadingSpinner... origin: ${it.origin}")
                            _refreshingLD.value = false
                        } else if (it.origin == ResponseOrigin.SourceOfTruth) {
                            Timber.d(">>>>> !!! hideLoadingSpinner... origin: ${it.origin}")
                            _refreshingLD.value = false
                        }


                        val data = it.requireData()

                        Timber.d(">>>>> !!! on-data-changed ... origin: ${it.origin} ... number-of-messages:${data.size}")

                        val list = mapMessagesToAdapterTypes(
                            data,
                            paymail
                        )

                        viewHolderModelLd.value = list



                    }
                    is StoreResponse.Error<*> -> {
                        if (it.origin == ResponseOrigin.Fetcher) {
                            Timber.d(">>>>> !!! hideLoadingSpinner")
                            _refreshingLD.value = false
                        }
                        Timber.d(">>>>> !!! showError")

                        //todo possibly crate a special error-item
                        viewHolderModelLd.value = emptyList()
                    }
                }

            }

            return FilteredHolder(viewHolderModelLd, dataLd, holder)
        }


        fun get(filter: MessageFilterKey): LiveData<List<AdapterType>> {
            Timber.d(">>>>> get observable results for this filter -- $filter")

            val item = search(filter)
            currentItem = item

            return item.ld
        }

        private fun mapMessagesToAdapterTypes(
            list: List<DecryptedBaemailMessage>,
            activePaymail: String
        ): List<AdapterType> {

            val adapterList = mutableListOf<AdapterType>()

            list.map { message ->


                val blocksContent = try {
                    val json = gson.fromJson(message.decryptedMessage, JsonObject::class.java)
                    val bodyJson = json.getAsJsonObject("body")
                    val blocks = bodyJson.get("blocks").asString

                    blocks
                } catch (e: Exception) {
                    Timber.e(e)
                    "-----------------"
                }

                //todo maybe do this off main-thread?
                //todo
                val spannedContent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(blocksContent, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    Html.fromHtml(blocksContent)
                }

                val stringContent = spannedContent.toString().replace("\n", " ").trim()


                val content = if (stringContent.length <= 200) {
                    stringContent
                } else {
                    stringContent.substring(0, 200)
                }


                val createdOnLabel = messageDateFormat.format(message.createdAt)

                val finalFrom = if (message.fromName == null) {
                    message.from
                } else if (message.fromName != message.from) {
                    "${message.fromName} (${message.from})"
                } else {
                    message.fromName
                }

                ATMessage0(
                    message.txId,
                    finalFrom,
                    message.subject,
                    content,
                    createdOnLabel,
                    hasAttachment = false,
                    isUnread = !message.destinationSeen && message.to == activePaymail,
                    message = message
                )
            }.toCollection(adapterList)

            return adapterList
        }
    }


    data class FilteredHolder(
        val ld: LiveData<List<AdapterType>>,
        val dataLd: LiveData<StoreResponse<List<DecryptedBaemailMessage>>>,
        val pagedFetchHolder: PagedFetchHolder
    )




    @Suppress("PropertyName")
    class StateHandler(
        private val savedState: SavedStateHandle
    ) {

        private var isInitialized = false
        var bucket = MessageBucket.INBOX //todo this should be dynamic
        val _queryLD = MutableLiveData<String>()
        val _sortLD = MutableLiveData<SortType>()

        init {
            restore()
        }

        fun isInitialized(): Boolean = isInitialized

        fun setInitialized() {
            isInitialized = true
            savedState.set(KEY_INITIALIZED, true)
        }

        fun setNewBucket(bucket: MessageBucket) {
            this.bucket = bucket
            savedState.set(KEY_BUCKET, bucket.const)
        }

        fun setNewSort(type: SortType) {
            _sortLD.value = type
            savedState.set(KEY_SORT_TYPE, type.const)
        }

        fun setNewQuery(query: String?) {
            _queryLD.value = query
            savedState.set(KEY_SEARCH_QUERY, query ?: "")
        }

        private fun restore() {
            Timber.d("#restore")

            isInitialized = savedState.get<Boolean>(KEY_INITIALIZED)?: false

            var initSort = SortType.VALUE
            savedState.get<String>(KEY_SORT_TYPE).let { serialisedSort ->
                serialisedSort ?: return@let

                val sort = SortType.parse(serialisedSort)

                initSort = sort
            }

            var initQuery = ""
            savedState.get<String>(KEY_SEARCH_QUERY).let { serialisedQuery ->
                serialisedQuery ?: return@let

                initQuery = serialisedQuery
            }

            var initBucket = MessageBucket.INBOX
            savedState.get<String>(KEY_BUCKET).let { serialisedBucket ->
                serialisedBucket ?: return@let

                initBucket = MessageBucket.parse(serialisedBucket)
            }


            _sortLD.value = initSort
            _queryLD.value = initQuery
            bucket = initBucket
        }

    }





}




//this is necessary for proper dependency injection
class MessagesViewModelFactory(
    savedStateRegistryOwner: SavedStateRegistryOwner,
    private val provider: Provider
) : AbstractSavedStateViewModelFactory(savedStateRegistryOwner, null) {


    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {

        if (modelClass.isAssignableFrom(MessagesViewModel::class.java)) {
            return MessagesViewModel(
                provider.appContext,
                handle,
                provider.walletRepository,
                provider.paymailRepository,
                provider.baemailRepository,
                provider.blockchainDataSource,
                provider.secureDataSource,
                provider.chainSync,
                provider.appDatabase,
                provider.gson
            ) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }


    class Provider @Inject constructor(
        val appContext: Context,
        val walletRepository: WalletRepository,
        val paymailRepository: PaymailRepository,
        val baemailRepository: BaemailRepository,
        val blockchainDataSource: BlockchainDataSource,
        val secureDataSource: SecureDataSource,
        val appDatabase: AppDatabase,
        val chainSync: ChainSync,
        val gson: Gson
    )
}