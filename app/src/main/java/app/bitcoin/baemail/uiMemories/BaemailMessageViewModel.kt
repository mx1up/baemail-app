package app.bitcoin.baemail.uiMemories

import android.content.Context
import android.text.format.DateFormat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import app.bitcoin.baemail.message.BaemailRepository
import app.bitcoin.baemail.message.MessageBucket
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.DecryptedBaemailMessage
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.WalletRepository
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception
import java.util.*
import javax.inject.Inject

class BaemailMessageViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val walletRepository: WalletRepository,
    private val baemailRepository: BaemailRepository,
    private val coroutineUtil: CoroutineUtil,
    private val appDatabase: AppDatabase,
    private val gson: Gson
) : ViewModel()  {

    private lateinit var ownerPaymail: String
    private lateinit var txId: String

    private lateinit var messageLD: LiveData<DecryptedBaemailMessage>


    private val _contentLD = MediatorLiveData<Model>()
    val contentLD: LiveData<Model>
        get() = _contentLD





    private val messageDateFormat = DateFormat.getMediumDateFormat(appContext)
    private val messageTimeFormat = DateFormat.getTimeFormat(appContext)




    fun init(owner: String, txId: String) {
        if (::ownerPaymail.isInitialized) return
        ownerPaymail = owner
        this.txId = txId

        doInit()
    }

    fun doInit() {
        Timber.d("doInit ownerPaymail:$ownerPaymail txId:$txId")

        messageLD = appDatabase.decryptedBaemailMessageDao()
            .observeMessageByOwnerAndTxId(ownerPaymail, txId)


        _contentLD.addSource(messageLD) {
            if (it == null) {
                _contentLD.value = null
                Timber.d("on-db-returned with message... NULL... ownerPaymail:$ownerPaymail txId:$txId")
                return@addSource
            } else Timber.d("on-db-returned with message... $it")

            val blocksContent = try {
                val json = gson.fromJson(it.decryptedMessage, JsonObject::class.java)
                val bodyJson = json.getAsJsonObject("body")
                val blocks = bodyJson.get("blocks").asString

                blocks
            } catch (e: Exception) {
                "-----------------"
            }

            val messageDate = Date(it.createdAt)
            val formatedMessageTime = messageDateFormat.format(messageDate) + " - " +
                    messageTimeFormat.format(messageDate)

            _contentLD.value = Model(
                ownerPaymail,
                it,
                blocksContent,
                formatedMessageTime
            )


            if (!it.destinationSeen && MessageBucket.INBOX == it.bucket) {
                baemailRepository.requestPostReadReceipt(it.txId, ownerPaymail)
            }


        }


    }


    fun archiveMessage() {
        val message = contentLD.value?.message ?: throw RuntimeException()
        if (message.bucket != MessageBucket.INBOX) throw RuntimeException()

        coroutineUtil.appScope.launch {
            baemailRepository.archiveMessage(message.txId)
        }
    }

    fun unarchiveMessage() {
        val message = contentLD.value?.message ?: throw RuntimeException()
        if (message.bucket != MessageBucket.ARCHIVE) throw RuntimeException()

        coroutineUtil.appScope.launch {
            baemailRepository.unarchiveMessage(message.txId)
        }
    }

    fun deleteMessage() {
        val message = contentLD.value?.message ?: throw RuntimeException()
        if (message.bucket != MessageBucket.ARCHIVE &&
            message.bucket != MessageBucket.SENT) throw RuntimeException()

        coroutineUtil.appScope.launch {
            baemailRepository.deleteMessage(message.txId)
        }
    }

    data class Model(
        val ownerPaymail: String,
        val message: DecryptedBaemailMessage,
        val parsedContent: String,
        val formattedMessageDateTime: String
    )
}