package app.bitcoin.baemail.uiMemories

import android.content.Context
import androidx.core.os.TraceCompat
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import app.bitcoin.baemail.Tr
import app.bitcoin.baemail.fundingWallet.PaymailExistsHelper
import app.bitcoin.baemail.message.BaemailRepository
import app.bitcoin.baemail.net.BsvAlias
import app.bitcoin.baemail.paymail.AuthenticatedPaymail
import app.bitcoin.baemail.paymail.PaymailRepository
import app.bitcoin.baemail.paymail.SecureDataSource
import app.bitcoin.baemail.room.AppDatabase
import app.bitcoin.baemail.room.entity.Coin
import app.bitcoin.baemail.room.entity.TxConfirmation
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.view.ViewBaePayOptions
import app.bitcoin.baemail.wallet.*
import app.bitcoin.baemail.wallet.request.CreateBaemailMessageTx
import kotlinx.coroutines.*
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class NewMessageViewModel @Inject constructor(
    private val appContext: Context,
    private val paymailRepository: PaymailRepository,
    private val walletRepository: WalletRepository,
    private val baemailRepository: BaemailRepository,
    private val coroutineUtil: CoroutineUtil,
    private val appDatabase: AppDatabase,
    private val unusedAddressHelper: UnusedAddressHelper,
    private val secureDataSource: SecureDataSource,
    private val blockchainDataSource: BlockchainDataSource,
    private val exchangeRateHelper: ExchangeRateHelper,
    private val dynamicChainSync: DynamicChainSync
) : ViewModel() {

    /**
     * this will eventually hold all coins controlled by the funding wallet of active paymail
     */
    private var fundingCoinsOfPaymailLD: LiveData<List<Coin>>? = null

    private var lastDestinationPaymail = ""
    private var lastSubject = ""
    private var lastContent = ""
    private var lastThread: String? = null

    var millisLastReplyInit = 0L
        private set

    private var lastPaymailExistenceLD: LiveData<Pair<PaymailExistsHelper.State, BsvAlias?>>? = null

    val paymailDestinationOutputCache: MutableMap<String, PrefetchJob> = mutableMapOf()

    private val defaultPayOptions = ViewBaePayOptions.Model(
        0.01f,
        0.1f,
        0.01f,
        "USD"
    )
    private var lastPayOptions = defaultPayOptions

    private val _newMessageContentLD = MediatorLiveData<ContentModel>()
    val newMessageContentLD: LiveData<ContentModel>
        get() = _newMessageContentLD


    private val _paymailExistsLD = MediatorLiveData<Pair<PaymailExistsHelper.State, BsvAlias?>?>()
    val paymailExistsLD: LiveData<Pair<PaymailExistsHelper.State, BsvAlias?>?>
        get() = _paymailExistsLD


    private val _baemailSendingLD = MutableLiveData<SendBaemailState>()
    val baemailSendingLD: LiveData<SendBaemailState>
        get() = _baemailSendingLD

    val activePaymail: String?
        get() = paymailRepository.activePaymailLD.value?.paymail


    init {

        _newMessageContentLD.addSource(paymailRepository.activePaymailLD, object : Observer<AuthenticatedPaymail> {
            private var previousPaymail: String? = null

            override fun onChanged(it: AuthenticatedPaymail?) {

                val paymail = it?.paymail

                if (previousPaymail == paymail) return //unchanged
                previousPaymail = paymail

                //disconnect currently connected observing
                fundingCoinsOfPaymailLD?.let {
                    _newMessageContentLD.removeSource(it)
                }
                fundingCoinsOfPaymailLD = null

                if (paymail == null) return //consider clearing content

                viewModelScope.launch(Dispatchers.IO) {

                    initDraftFromModel(paymail)

                    val coinsLD = appDatabase.coinDao().getFundingCoinsLD(paymail)

                    withContext(Dispatchers.Main) {
                        fundingCoinsOfPaymailLD = coinsLD

                        _newMessageContentLD.addSource(coinsLD) {
                            refreshModel()
                        }
                    }
                }

            }

        })

        _newMessageContentLD.addSource(baemailSendingLD) {
            refreshModel()
        }

        _newMessageContentLD.addSource(dynamicChainSync.statusLD) {
            refreshModel()
        }


        exchangeRateHelper.considerRefresh()
    }

    private val paymailExistenceObserver = object : Observer<Pair<PaymailExistsHelper.State, BsvAlias?>> {
        override fun onChanged(it: Pair<PaymailExistsHelper.State, BsvAlias?>?) {
            it ?: return

            _paymailExistsLD.value = it

            refreshModel()

            //prefetch the paymail-destination-output for this paymail (this reduces the send-time)
            if (PaymailExistsHelper.State.VALID == it.first) {
                it.second?.handle?.let {
                    prefetchPaymailDestinationOutput(it)
                }
            }
        }


        fun prefetchPaymailDestinationOutput(paymail: String) {
            Timber.d("entering prefetchPaymailDestinationOutput")
            paymailDestinationOutputCache[paymail]?.let {
                Timber.d("paymail-destination-output for paymail: $paymail is already cached")
                return
            }

            val holder = PrefetchJob(
                paymail,
                null,
                null
            )

            holder.job = coroutineUtil.appScope.launch(Dispatchers.IO) {

                val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail

                val destinationPaymailOutputScript = paymailRepository.getPaymailDestinationOutput(
                    sendingPaymail,
                    paymail
                ) ?: let {
                    Timber.e("prefech-destination-output dropped; error retrieving output")
                    paymailDestinationOutputCache.remove(paymail)

                    return@launch
                }


                holder.destinationOutput = destinationPaymailOutputScript

                Timber.d("prefech-destination-output completed; paymail: $paymail")
            }

            paymailDestinationOutputCache[paymail] = holder
        }
    }

    fun initNewReply(destinationPaymail:String, subject: String, threadId: String, millisCreated: Long) {
        if (threadId == lastThread) return
        if (millisCreated <= millisLastReplyInit) return

        viewModelScope.launch(Dispatchers.IO) {

            val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail

            baemailRepository.messageDraftHelper.applyNewReply(
                sendingPaymail,
                destinationPaymail,
                subject,
                threadId,
                millisCreated
            )
            Timber.d("this message is now a reply to the linked baemail ....destinationPaymail:$destinationPaymail ...threadId:$threadId")

            lastThread = threadId
            millisLastReplyInit = millisCreated
            lastDestinationPaymail = destinationPaymail
            lastSubject = subject

            withContext(Dispatchers.Main) {

                refreshPaymailExistenceCheck()

                refreshModel()
            }
        }

    }

    fun onDestinationInputChanged(paymail: String) {
        val p0 = paymail.trim()
        if (p0 == lastDestinationPaymail) return

        lastDestinationPaymail = p0

        /////
        onDraftContentChanged()

        /////

        refreshPaymailExistenceCheck()
    }

    private suspend fun initDraftFromModel(paymail: String) {
        baemailRepository.messageDraftHelper.getDraft(paymail)?.let { draft ->

            Timber.d("init draft model")

            val payOptions = ViewBaePayOptions.Model(
                draft.payMax / 10,
                draft.payMax,
                draft.payAmount,
                draft.payCurrency
            )

            lastDestinationPaymail = draft.destination ?: ""
            lastSubject = draft.subject ?: ""
            lastContent = draft.content ?: ""
            lastThread = draft.thread
            lastPayOptions = payOptions
            millisLastReplyInit = draft.millisCreated


            withContext(Dispatchers.Main) {
                refreshModel()
                refreshPaymailExistenceCheck()
            }
        }


    }

    private fun refreshPaymailExistenceCheck() {
        lastPaymailExistenceLD?.removeObserver(paymailExistenceObserver)

        val ld = paymailRepository.paymailExistsHelper.checkExistence(lastDestinationPaymail)
        lastPaymailExistenceLD = ld
        ld.observeForever(paymailExistenceObserver)
    }

    fun resetModel() {
        _baemailSendingLD.value = null
        paymailDestinationOutputCache.clear()
    }

    fun onSubjectInputChanged(subject: String) {
        val s0 = subject.trim()
        if (s0 == lastSubject) return

        lastSubject = s0

        onDraftContentChanged()
        refreshModel()
    }

    fun onContentInputChanged(subject: String) {
        val c0 = subject.trim()
        if (c0 == lastContent) return

        //Timber.e("onContentInputChanged: $c0")
        lastContent = c0

        onDraftContentChanged()
        refreshModel()
    }

    fun onDraftContentChanged() {
        val payOptions = lastPayOptions

        Timber.e("new-message draft persisted")
        viewModelScope.launch(Dispatchers.IO) {

            val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail

            baemailRepository.messageDraftHelper.applyNewDraftContent(
                sendingPaymail,
                lastDestinationPaymail,
                lastSubject,
                lastContent,
                lastThread,
                payOptions.currency,
                payOptions.valueMax,
                payOptions.valueCurrent
            )
        }
    }

    fun onDraftDiscarded() {
        coroutineUtil.appScope.launch(Dispatchers.IO) {
            discardDraft()
            withContext(Dispatchers.Main) {
                refreshModel()
            }
        }
    }

    private suspend fun discardDraft() {

        val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail
        baemailRepository.messageDraftHelper.discardDraft(sendingPaymail)

        Timber.d("new-message draft discarded")

        lastDestinationPaymail = ""
        lastSubject = ""
        lastContent = ""
        lastPayOptions = defaultPayOptions
        lastThread = null
    }

    fun onClearReplyTo() {
        viewModelScope.launch(Dispatchers.IO) {

            val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail
            baemailRepository.messageDraftHelper.clearReplyTo(sendingPaymail)

            Timber.d("new-message draft discarded")

            lastThread = null

            withContext(Dispatchers.Main) {
                refreshModel()
            }
        }
    }

    fun onPayOptionsMinus() {
        val newMax = lastPayOptions.valueMax / 10
        if (newMax < 0.1f) return

        val newMin = newMax / 10
        val newOptions = ViewBaePayOptions.Model(
            newMin,
            newMax,
            newMin,
            lastPayOptions.currency
        )

        lastPayOptions = newOptions
        onDraftContentChanged()
        refreshModel()
    }

    fun onPayOptionsPlus() {
        val newMax = lastPayOptions.valueMax * 10
        if (newMax > 1_000_000) return

        val newMin = newMax / 10
        val newOptions = ViewBaePayOptions.Model(
            newMin,
            newMax,
            newMin,
            lastPayOptions.currency
        )

        lastPayOptions = newOptions
        onDraftContentChanged()
        refreshModel()
    }

    fun onPayOptionsValueChange(value: Float) {
        if (value < lastPayOptions.valueMin) return
        if (value > lastPayOptions.valueMax) return

        lastPayOptions = lastPayOptions.copy(
            valueCurrent = value
        )
        onDraftContentChanged()
        refreshModel()
    }

    fun onSendMessage() {
        //todo
        //todo
        //todo refactor to have the relevant part of the code in a repository
        //todo

        val destinationPaymailAliasState = lastPaymailExistenceLD?.value
        if (PaymailExistsHelper.State.VALID != destinationPaymailAliasState?.first) {
            Timber.d("paymail not valid")
            return
        }
        val destinationPaymailAlias = destinationPaymailAliasState.second!!

        if (newMessageContentLD.value?.state != State.PAYMAIL_VALID) {
            Timber.d("content in unexpected/unsupported state")
            return
        }

        //todo
        // return if syncing is in progress


        val paymail = lastDestinationPaymail.trim()
        val subject = lastSubject.trim()

        val content = lastContent.trim().let {
            //wrap in html
            val split = it.trim().split("\n")

            val built = split.map {
                val sb = StringBuilder()
                    .append("<div>")
                    .append(it)
                    .append("<br></div>")
                sb
            }

            StringBuilder().let { sb ->
                built.forEach {
                    sb.append(it)
                }

                sb.toString()
            }
        }

        val unspentFundingCoinsList: List<Coin> = fundingCoinsOfPaymailLD?.value.let {
            it ?: return@let emptyList()

            it.mapNotNull {
                if (it.spendingTxId != null) return@mapNotNull null
                it
            }
        }

        _baemailSendingLD.value = SendBaemailState.PROCESSING

        coroutineUtil.appScope.launch(Dispatchers.IO) {
            try {
                TraceCompat.beginAsyncSection(Tr.METHOD_SEND_BAEMAIL_MESSAGE, 0)



                val satsInCent = let {
                    val rate: ExchangeRate = exchangeRateHelper.value?.second ?: let {
                        Timber.e(RuntimeException(), "BSV price is unknown")
                        _baemailSendingLD.postValue(SendBaemailState.FAIL)
                        return@launch
                    }
                    rate.satsInACent
                }


                val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail

                val sendingPaymailWalletSeed = try {
                    secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(sendingPaymail))
                } catch (e: Exception) {
                    Timber.e(e, "error retrieving seed")
                    _baemailSendingLD.postValue(SendBaemailState.FAIL)
                    return@launch
                }

                val pkiPath = try {
                    secureDataSource.getPkiPathForPrivateKeySeed(AuthenticatedPaymail(sendingPaymail))
                } catch (e: Exception) {
                    Timber.e(e, "error retrieving pki-path matching the sending-paymail")
                    _baemailSendingLD.postValue(SendBaemailState.FAIL)
                    return@launch
                }

                val sendingPaymailModel = CreateBaemailMessageTx.SendingPaymail(
                    sendingPaymail,
                    sendingPaymailWalletSeed,
                    pkiPath
                )


                val availableCoinsMap: Map<String, ReceivedCoins> = unspentFundingCoinsList.let {
                    val map = mutableMapOf<String, ReceivedCoins>()

                    it.forEach { c ->
                        map[c.derivationPath] = ReceivedCoins(
                            c.txId,
                            c.sats,
                            c.outputIndex,
                            c.address
                        )
                    }

                    map
                }

                val fundingWalletSeed = try {
                    secureDataSource.getParallelWalletSeed(AuthenticatedPaymail(sendingPaymail))
                } catch (e: Exception) {
                    Timber.e(e, "error retrieving seed")
                    _baemailSendingLD.postValue(SendBaemailState.FAIL)
                    return@launch
                }

                val upcomingUnusedFundingWalletAddresses = unusedAddressHelper
                    .getUnusedAddressesOfFundingWallet(sendingPaymail) // will be used for a change-address

                val changeAddress = upcomingUnusedFundingWalletAddresses[0]
                Timber.d("provided changeAddress for the baemail message: $changeAddress")

                val fundingWalletModel = CreateBaemailMessageTx.FundingWallet(
                    fundingWalletSeed,
                    "m/44'/0'/0'", //todo do not hardcode this
                    availableCoinsMap,
                    changeAddress.address
                )


                val usdToPaymailString =
                    String.format(Locale.US, "%.2f", lastPayOptions.valueCurrent)
                val usdToPaymail =
                    usdToPaymailString.toFloat() //all of the decimals after the relevant ones have been dropped
                val satsToPaymail = (usdToPaymail * 100 * satsInCent).toInt()

                val feeUsd = 0.01f
                val feeSats = (feeUsd * 100 * satsInCent).toInt()


                val prefetchHolder = paymailDestinationOutputCache[paymail]
                prefetchHolder?.job?.join() //make sure the prefetch has completed
                val destinationPaymailOutputScript = prefetchHolder?.destinationOutput ?: let {
                    Timber.e("baemail-sending dropped; error retrieving paymail destination output")
                    _baemailSendingLD.postValue(SendBaemailState.FAIL)
                    return@launch
                }

                val messageModel = CreateBaemailMessageTx.Message(
                    lastThread,
                    paymail,
                    destinationPaymailAlias.pubkey,
                    subject,
                    content,
                    usdToPaymail,
                    satsToPaymail,
                    destinationPaymailOutputScript,
                    feeSats, // fee to baemail
                    feeSats // fee to the app
                )


                Timber.d("about to create a baemail-message-tx; using inputs: ${fundingWalletModel.coins}")
                val tx = walletRepository.createBaemailMessageTx(
                    sendingPaymailModel,
                    fundingWalletModel,
                    messageModel
                )

                tx ?: let {
                    Timber.d("tx NOT created; hex is NULL")
                    _baemailSendingLD.postValue(SendBaemailState.FAIL)
                    return@launch
                }
                Timber.d("tx created; hex: $tx")


                val txId = blockchainDataSource.sendHexTx(tx.hexTx)
                if (txId == null) {
                    Timber.d("tx NOT sent")
                    _baemailSendingLD.postValue(SendBaemailState.FAIL)
                    return@launch
                }

                Timber.d("tx broadcast result tx-id: $txId")


                // mark coin as spent by updating the db
                unspentFundingCoinsList.filter {
                    tx.spentCoinIdList.contains(it.txId)
                }.forEach {
                    val copy = it.copy(
                        spendingTxId = txId
                    )
                    Timber.d("marking the coin as spent: $copy")
                    appDatabase.coinDao().insertCoin(copy)
                }


                // must add new confirmation meta-data -- spend of coins
                appDatabase.txConfirmationDao().insertConfirmationInfo(
                    TxConfirmation(
                        txId,
                        -1,
                        null,
                        -1L
                    )
                )

                //theoretically there is a new unspent coin at the change-output
                //...currently relying on chain-sync detecting & syncing it

                Timber.d("_baemailSendingLD postValue SendBaemailState.SUCCESS")

                _baemailSendingLD.postValue(SendBaemailState.SUCCESS)


                //clear the draft
                discardDraft()

            } finally {
                TraceCompat.endAsyncSection(Tr.METHOD_SEND_BAEMAIL_MESSAGE, 0)

            }

        }
    }




    fun refreshModel() {

        val syncValue = dynamicChainSync.statusLD.value ?: ChainSync.Status.INITIALISING

        var state = when(lastPaymailExistenceLD?.value?.first) {
            PaymailExistsHelper.State.VALID -> {
                State.PAYMAIL_VALID
            }
            PaymailExistsHelper.State.INVALID -> {
                State.PAYMAIL_NOT_VALID
            }
            PaymailExistsHelper.State.CHECKING -> {
                State.CHECKING_PAYMAIL
            }
            else -> {
                State.EMPTY
            }
        }


        ///apply the message sending state
        baemailSendingLD.value?.let {
            //a send was attempted
            when(it) {
                SendBaemailState.PROCESSING -> {
                    state = State.SENDING_IN_PROGRESS
                }
                SendBaemailState.FAIL -> {
                    state = State.SENDING_ERROR
                }
                SendBaemailState.SUCCESS -> {
                    state = State.SENDING_SUCCESS
                }
            }
        }


        //need moneys to send a message
        val satsAvailable = fundingCoinsOfPaymailLD?.value.let {
            it ?: return@let 0L

            if (ChainSync.Status.LIVE != syncValue) return@let 0L

            it.map {
                if (it.spendingTxId != null) return@map 0L
                it.sats.toLong()
            }.foldRight(0L) { sats, acc ->
                acc + sats
            }
        }


        val sendingPaymail = paymailRepository.activePaymailLD.value!!.paymail

        val estimatedTxFee = let {
            val estimatedTxSize = let {

                val lengthOfCcPaymails = 0 //todo
                val headerChunkSize = 60 + lastDestinationPaymail.length +
                        lengthOfCcPaymails +
                        10 + lastSubject.length +
                        10 + sendingPaymail.length + //for name of sender
                        10 + sendingPaymail.length + //for paymail of sender
                        50

                val countOfCcs = 0
                val messageChunkCount = 2 + countOfCcs
                val messageChunkSize = (lastContent.length + 20) * messageChunkCount

                50 + headerChunkSize + messageChunkSize +
                        50 +
                        50 + //hash of header
                        50 + //senser-signed hash of header
                        50 + //sendingPaymailPkiPublicKey
                        sendingPaymail.length +
                        100 + //baemail pay output
                        100 + //app pay output
                        100 + //destination pay output
                        200 + //tx-inputs
                        50 //general tx-decorations
            }

            estimatedTxSize
        }


        val satsInCent = let {
            val rate: ExchangeRate = exchangeRateHelper.value?.second ?: return@let -1
            rate.satsInACent
        }
        val satsToPay = (lastPayOptions.valueCurrent * 100 * satsInCent).toLong()
        val estimatedTxOutputSats = satsInCent + //amount to baemail
                satsInCent + //amount to app
                satsToPay


        val estimatedSatsRequired = estimatedTxFee + estimatedTxOutputSats


        if (satsAvailable < estimatedSatsRequired) {
            state = State.FUNDING_ERROR
        }

        //indicate whether sync is in progress
        ///todo
        ///todo
        ///todo
        ///todo
        ///todo
        ///todo

        val model = ContentModel(
            lastDestinationPaymail,
            lastSubject,
            lastThread,
            lastContent,
            state,
            lastPaymailExistenceLD?.value?.first,
            lastPayOptions,
            satsAvailable,
            estimatedSatsRequired
        )

        _newMessageContentLD.value = model
    }



    data class PrefetchJob(
        val paymail: String,
        var job: Job?,
        var destinationOutput: String?
    )

    data class ContentModel(
        val paymail: String,
        val subject: String,
        val replyThreadId: String?,
        val content: String,
        val state: State,
        val paymailExistenceState: PaymailExistsHelper.State?,
        val payOptions: ViewBaePayOptions.Model,
        val satsAvailable: Long = -1,
        val satsEstimatedRequired: Long = -1
    )

    enum class State {
        EMPTY,
        CHECKING_PAYMAIL,
        PAYMAIL_NOT_VALID,
        PAYMAIL_VALID,
        FUNDING_ERROR,
        SENDING_IN_PROGRESS,
        SENDING_SUCCESS,
        SENDING_ERROR
    }

    enum class SendBaemailState {
        PROCESSING,
        SUCCESS,
        FAIL
    }

}