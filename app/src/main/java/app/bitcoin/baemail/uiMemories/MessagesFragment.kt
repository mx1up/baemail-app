package app.bitcoin.baemail.uiMemories

import android.graphics.Color
import android.graphics.Outline
import android.graphics.Rect
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.text.TextPaint
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableOval
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.message.MessageBucket
import app.bitcoin.baemail.message.SortType
import app.bitcoin.baemail.recycler.ATMessage0
import app.bitcoin.baemail.recycler.AdapterType
import app.bitcoin.baemail.recycler.BaseListAdapter
import app.bitcoin.baemail.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.recycler.holder.ATMessage0ViewHolder
import app.bitcoin.baemail.recycler.holder.SwipeController
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

@ExperimentalCoroutinesApi
@FlowPreview
class MessagesFragment : Fragment(R.layout.fragment_messages) {

    companion object {
        const val KEY_SCROLL_POS = "scroll_pos"

        //TODO LOCALIZE
        private val LABEL_VALUE_SORT_TYPE_VALUE = "Value"
        private val LABEL_VALUE_SORT_TYPE_TIME = "Time"

        const val KEY_BUCKET = "bucket"
        const val KEY_INITIAL_SORT = "initial_sort"
        const val KEY_IS_SORT_LOCKED = "is_sort_locked"
    }



    @Inject
    lateinit var messagesViewModelFactoryProvider: MessagesViewModelFactory.Provider
    lateinit var messagesViewModel: MessagesViewModel




    lateinit var swipeRefresh: SwipeRefreshLayout

    lateinit var recycler: RecyclerView
    lateinit var appBar: AppBarLayout
    lateinit var createNew: ImageView
    lateinit var snippet: ImageView

    private lateinit var searchLayout: FrameLayout
    private lateinit var searchView: SearchView
    private lateinit var cancelSearch: TextView
    private lateinit var clearSearch: ImageView

    private lateinit var searchSort: TextView

    private lateinit var newMessage: FloatingActionButton





    lateinit var layoutManager: LinearLayoutManager

    private var lastSavedRecyclerState: Parcelable? = null

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory
    lateinit var adapter: BaseListAdapter

    //list-item side-swipe
    private lateinit var swipeController: SwipeController
    private lateinit var itemTouchHelper: ItemTouchHelper







    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!wasCreatedOnce) return

        val state = layoutManager.onSaveInstanceState()
        lastSavedRecyclerState = state
        outState.putParcelable(KEY_SCROLL_POS, state)
    }

    private var wasCreatedOnce = false

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        lastSavedRecyclerState = null

        val isFirstCreate = !wasCreatedOnce && savedInstanceState == null
        wasCreatedOnce = true



        val serializedRecyclerState: Parcelable? = savedInstanceState?.getParcelable(KEY_SCROLL_POS)


        val bucket = requireArguments().getString(KEY_BUCKET)?.let {
            MessageBucket.parse(it)
        } ?: let {
            Timber.d("MESSAGE FRAGMENT WAS MISSING INIT PARAM")
            MessageBucket.INBOX
        }

        val initialSort = requireArguments().getString(KEY_INITIAL_SORT)?.let {
            SortType.parse(it)
        } ?: let {
            Timber.d("MESSAGE FRAGMENT WAS MISSING INIT PARAM")
            SortType.VALUE
        }

        val isSortLocked = requireArguments().getBoolean(KEY_IS_SORT_LOCKED)



        AndroidSupportInjection.inject(this)


        messagesViewModel = ViewModelProvider(
            this,
            MessagesViewModelFactory(this, messagesViewModelFactoryProvider)
        ).get(MessagesViewModel::class.java)

        messagesViewModel.init(bucket, initialSort)



        swipeRefresh = requireView().findViewById(R.id.swipe_refresh)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        createNew = requireView().findViewById(R.id.create_new)
        snippet = requireView().findViewById(R.id.snippet)
        newMessage = requireView().findViewById(R.id.new_message)
        searchView = requireView().findViewById(R.id.search_view)
        searchLayout = requireView().findViewById(R.id.search_layout)
        cancelSearch = requireView().findViewById(R.id.cancel_search)
        clearSearch = requireView().findViewById(R.id.clear_search)
        searchSort = requireView().findViewById(R.id.search_sort)


        val adapterTypeMessageListener = object : BaseListAdapter.ATMessageListener() {
            override fun onItemClicked(adapterPosition: Int, model: ATMessage0) {

                val txId = model.message?.txId ?: return
                val ownerPaymail = model.message?.ownerPaymail ?: return

                val args = Bundle().also {
                    it.putString(BaemailMessageFragment.KEY_TX_ID, txId)
                    it.putString(BaemailMessageFragment.KEY_OWNER_PAYMAIL, ownerPaymail)
                }
                findNavController().navigate(
                    R.id.action_memoriesFragment_to_baemailMessageFragment,
                    args
                )


            }

            override fun onActionClicked(
                adapterPosition: Int,
                model: ATMessage0,
                actionId: Int
            ) {
                when (actionId) {
                    ACTION_START_0 -> {
                        Toast.makeText(requireContext(), "A1", Toast.LENGTH_SHORT).show()

                        //messagesViewModel.getCoins()

                        //messagesViewModel.reorgCheck()

                        //ChainSync

//                        //todo
//                        //todo
//                        //todo
//                        val num = Random.nextLong(0, 100)
//                        Timber.d("random num: $num")
//                        val bn = BigNumber.fromDec(num.toString())
//                        Timber.d("handle: ${bn.getHandle()}")
//                        Timber.d("hex: ${bn.toHex()}")
                    }
                    ACTION_START_1 -> {
                        Toast.makeText(requireContext(), "A2", Toast.LENGTH_SHORT).show()

//                        messagesViewModel.stopServiceForeground()

                    }
                    ACTION_END_0 -> {
                        Toast.makeText(requireContext(), "B1", Toast.LENGTH_SHORT).show()
                        //messagesViewModel.getCoins()
                    }
                    ACTION_END_1 -> {
                        Toast.makeText(requireContext(), "B2", Toast.LENGTH_SHORT).show()
                    }
                    ACTION_END_2 -> {
                        Toast.makeText(requireContext(), "B3", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        val contentLoadingListener = object : BaseListAdapter.ATContentLoadingListener {
            override fun onBound() {
                Timber.d("onBound of ATContentLoadingListener")
                messagesViewModel.onSignalRequestNextPage()
            }

        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atMessageListener = adapterTypeMessageListener,
            atContentLoadingListener = contentLoadingListener
        )

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        layoutManager = LinearLayoutManager(context)

        serializedRecyclerState?.let {
            lastSavedRecyclerState = it
            layoutManager.onRestoreInstanceState(it)
        }

        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER



        val topBarElevation = resources.getDimension(R.dimen.messages_fragment__top_bar_elevation)


        if (isFirstCreate) {
            //avoid drawing the shadow appBar automatically lays out with at first
            var cleanupListener: () -> Unit = {
                throw RuntimeException()
            }

            val listener = object : ViewTreeObserver.OnPreDrawListener {

                override fun onPreDraw(): Boolean {
                    appBar.elevation = 0f

                    cleanupListener()

                    return true
                }

            }

            cleanupListener = {
                appBar.viewTreeObserver.removeOnPreDrawListener(listener)
            }
            appBar.viewTreeObserver.addOnPreDrawListener(listener)
        }

        //appBar.elevation = 0f
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (RecyclerView.SCROLL_STATE_IDLE == newState) {
                    Timber.d("new state saved")
                    lastSavedRecyclerState = layoutManager.onSaveInstanceState()
                }
            }
        })


        appBar.descendantFocusability = ViewGroup.FOCUS_BLOCK_DESCENDANTS
        appBar.doOnPreDraw {
            appBar.descendantFocusability = ViewGroup.FOCUS_AFTER_DESCENDANTS
        }


        swipeController = provideSwipeController()

        itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(recycler)




        val colorSelector = ContextCompat.getColor(requireContext(), R.color.selector_on_light)

        val topBarActionInset = R.dimen.messages_fragment__top_action_icon_inset.let {
            resources.getDimensionPixelSize(it)
        }

        val actionScale = R.dimen.messages_fragment_top_action_scale.let {
            val value = TypedValue()
            resources.getValue(it, value, true)
            value.float
        }




        val drawableCreateIcon = R.drawable.messages_fragment__top_action_create.let {
            val d = ContextCompat.getDrawable(requireContext(), it)!!.mutate()
            d.setTint(ContextCompat.getColor(requireContext(),
                R.color.messages_fragment__top_action_create_tint))

            LayerDrawable(arrayOf(d)).also { ld ->
                ld.setLayerInset(0, topBarActionInset, topBarActionInset,
                    topBarActionInset, topBarActionInset)
            }
        }

        val drawableCreateSelector = DrawableState.getNew(colorSelector, ovalShape = true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            createNew.background = drawableCreateIcon
            createNew.foreground = drawableCreateSelector
        } else {
            createNew.background = LayerDrawable(arrayOf(
                drawableCreateIcon,
                drawableCreateSelector
            ))
        }
        createNew.scaleType = ImageView.ScaleType.CENTER
        createNew.scaleX = actionScale
        createNew.scaleY = actionScale

        createNew.setOnClickListener {
            Toast.makeText(requireContext(), "create", Toast.LENGTH_SHORT).show()
        }


        /////

        val dp = resources.displayMetrics.density

        swipeRefresh.setProgressViewOffset(true, (5 * dp).toInt(), (45 * dp).toInt())
        swipeRefresh.setOnRefreshListener {
            messagesViewModel.onSwipeToRefresh()
        }




        val pressedColor = ContextCompat
            .getColor(requireContext(), R.color.selector_on_light)

        val colorSortBackground = ContextCompat.getColor(requireContext(), R.color.colorPrimary)
        val colorSortLockedBackground = ContextCompat.getColor(requireContext(), R.color.disabled)
        val colorSelectorOnDark = ContextCompat.getColor(requireContext(), R.color.selector_on_dark)




        val searchBackground = DrawableOval(colorSortBackground, 0f * dp, ContextCompat
            .getColor(requireContext(), R.color.search_background))
        searchBackground.setPaddings(0f, 0f, 0f, 0f)
        searchLayout.background = LayerDrawable(arrayOf(
            searchBackground,
            DrawableState.getNew(pressedColor)
        ))
        searchLayout.clipToOutline = true
        searchLayout.outlineProvider = searchBackground.getOutlineProvider()

        searchView.findViewById<View>(androidx.appcompat.R.id.search_src_text).isSaveEnabled = false
        searchView.isSaveEnabled = false
        searchView.setIconifiedByDefault(false)



        cancelSearch.setOnClickListener {
            messagesViewModel.onRequestClearSearchResults()
            searchView.clearFocus()
        }

        clearSearch.setOnClickListener {

            recycler.scrollToPosition(0)

            if (searchView.hasFocus()) {
                searchView.setQuery("", false)
            } else {
                messagesViewModel.onRequestClearSearchResults()
            }
        }

        searchView.setOnQueryTextListener(searchInputQueryTextListener)

        searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            Timber.d("searchView focus-change hasFocus:$hasFocus")

            val currentInputValue = searchView.query.toString()

            if (!hasFocus) {
                if (currentInputValue.trim().isEmpty()) {
                    initSearchBarButtons(
                        true,
                        true
                    )
                } else {
                    initSearchBarButtons(
                        true,
                        false
                    )
                }
                return@setOnQueryTextFocusChangeListener
            }


            initSearchBarButtons(
                currentInputValue.trim().isNotEmpty(),
                false
            )
        }


        searchSort.background = LayerDrawable(arrayOf(
            DrawableOval(requireContext()).also {

                if (isSortLocked) {
                    it.setBgColor(colorSortLockedBackground)
                } else {
                    it.setBgColor(colorSortBackground)
                }
                it.setStrokeWidth(0f)
                it.setPaddings(0f, 0f, 0f, 0f)

                //////
                //////
                searchSort.clipToOutline = true
                searchSort.outlineProvider = it.getOutlineProvider()
            },
            DrawableState.getNew(colorSelectorOnDark)
        ))


        if (!isSortLocked) {
            searchSort.setOnClickListener {
                Timber.d("sort-change")


                recycler.scrollToPosition(0)
                messagesViewModel.onSortTypeToggled()
            }
        }







        //find the final width of the sort-text-view
        searchSort.layoutParams.let {

            val tp = TextPaint()
            tp.textSize = searchSort.textSize
            val widthOfLongestLabel = max(
                tp.measureText(LABEL_VALUE_SORT_TYPE_VALUE),
                tp.measureText(LABEL_VALUE_SORT_TYPE_TIME)
            )

            it.width = (widthOfLongestLabel + searchSort.paddingStart + searchSort.paddingEnd).toInt()
            searchSort.updatePadding(
                right = 0
            )

            searchSort.layoutParams = it
        }




        /////

        val drawableSnippetIcon = R.drawable.messages_fragment__top_action_snippet.let {
            val d = ContextCompat.getDrawable(requireContext(), it)!!.mutate()
            d.setTint(ContextCompat.getColor(requireContext(),
                R.color.messages_fragment__top_action_snippet_tint))

            LayerDrawable(arrayOf(d)).also { ld ->
                ld.setLayerInset(0, topBarActionInset, topBarActionInset,
                    topBarActionInset, topBarActionInset)
            }
        }

        val drawableSnippetSelector = DrawableState.getNew(colorSelector, ovalShape = true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            snippet.background = drawableSnippetIcon
            snippet.foreground = drawableSnippetSelector
        } else {
            snippet.background = LayerDrawable(arrayOf(
                drawableSnippetIcon,
                drawableSnippetSelector
            ))
        }
        snippet.scaleX = actionScale
        snippet.scaleY = actionScale

        snippet.setOnClickListener {
            Toast.makeText(requireContext(), "snippet", Toast.LENGTH_SHORT).show()
        }



        newMessage.setOnClickListener {
            try {
                val args = Bundle().also {
                    it.putBoolean(NewMessageFragment.KEY_IS_NEW_REPLY, false)
                }

                findNavController().navigate(R.id.action_memoriesFragment_to_newMessageFragment, args)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }






        cancelSearch.background = DrawableState.getNew(colorSelector, chipShape = true)

        cancelSearch.clipToOutline = true
        cancelSearch.outlineProvider = object : ViewOutlineProvider() {
            val r = Rect()
            override fun getOutline(v: View?, o: Outline?) {
                if (v == null || o == null) return

                r.set(0, 0, v.width, v.height)
                o.setRoundRect(r, (min(r.height(), r.width()) / 2).toFloat())
            }
        }




        clearSearch.background = DrawableState.getNew(colorSelector, ovalShape = true)

        val colorClearTint = Color.parseColor("#777777")
        clearSearch.drawable.mutate().setTint(colorClearTint)

        clearSearch.clipToOutline = true
        clearSearch.outlineProvider = object : ViewOutlineProvider() {
            val r = Rect()
            override fun getOutline(v: View?, o: Outline?) {
                if (v == null || o == null) return

                r.set(0, 0, v.width, v.height)
                o.setOval(r)
            }
        }


        /////

        messagesViewModel.queryLD.observe(viewLifecycleOwner, Observer {
            val currentInputValue = searchView.query.toString()

            if (it == null) {
                initSearchBarButtons(false, true)

                searchView.setOnQueryTextListener(null)
                searchView.setQuery("", false)
                searchView.setOnQueryTextListener(searchInputQueryTextListener)

                return@Observer
            }

            initSearchBarButtons(it.isNotEmpty(), it.isEmpty())

            if (currentInputValue != it) {
                Timber.d("searchView setQuery from observer")
                searchView.setQuery(it, false)
            }
        })





        messagesViewModel.contentLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            swipeStateHelper.updateExpands(it)

            adapter.setItems(it)

            lastSavedRecyclerState?.let {
                layoutManager.onRestoreInstanceState(it)
            }

            if (!newMessage.isShown) {
                newMessage.show()
            }
        })


        messagesViewModel.sortLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            when (it) {
                SortType.VALUE -> {
                    searchSort.text = LABEL_VALUE_SORT_TYPE_VALUE
                }
                SortType.TIME -> {
                    searchSort.text = LABEL_VALUE_SORT_TYPE_TIME
                }
            }

        })

        messagesViewModel.refreshingLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            swipeRefresh.isRefreshing = it
        })

    }

    private fun focusOnInput() {
        if (searchView.requestFocus()) {
            Timber.d("focusOnInput got focus")
            searchView.isIconified = false //force focusing on the input
        }
    }

    //todo fix this mess
    private fun initSearchBarButtons(
        nonEmptyQuery: Boolean,
        isClear: Boolean
    ) {
        if (isClear) {
            cancelSearch.visibility = View.INVISIBLE
            clearSearch.visibility = View.INVISIBLE
            return
        }

        if (!nonEmptyQuery) {
            cancelSearch.visibility = View.VISIBLE
            clearSearch.visibility = View.INVISIBLE
        } else {
            cancelSearch.visibility = View.INVISIBLE
            clearSearch.visibility = View.VISIBLE
        }
    }





    private fun provideSwipeController(): SwipeController {
        val swipeController = SwipeController()
        swipeController.init(
            allowSwipeLeft = lambda@{ holder ->
                if (holder !is ATMessage0ViewHolder) return@lambda false

                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false

                if (isRightActionExpanded) {
                    return@lambda false
                }
                if (isLeftActionExpanded) {
                    return@lambda true
                }

                true
            },
            allowSwipeRight = lambda@{ holder ->
                if (holder !is ATMessage0ViewHolder) return@lambda false

                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false

                if (isRightActionExpanded) {
                    return@lambda true
                }
                if (isLeftActionExpanded) {
                    return@lambda false
                }

                true
            },
            onSwiped = { holder, direction, maxNotMin ->
                val adapterPosition = holder.bindingAdapterPosition

                if (ItemTouchHelper.LEFT == direction) {
                    swipeStateHelper.onItemSwiped(adapterPosition, true, maxNotMin)
                    //messagesViewModel.onItemSwiped(adapterPosition, true, maxNotMin)
                } else if (ItemTouchHelper.RIGHT == direction) {

                    swipeStateHelper.onItemSwiped(adapterPosition, false, maxNotMin)
                    //messagesViewModel.onItemSwiped(adapterPosition, false, maxNotMin)
                }
            },
            swipeRightMinWidth = lambda@{ holder ->
                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX

                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false

                return@lambda if (isLeftActionExpanded) {
                    SwipeController.WIDTH_MAX
                } else if (isRightActionExpanded) {
                    holder.getRightSwipeActionWidth()
                } else {
                    holder.getLeftSwipeActionWidth()
                }

            },
            swipeRightMaxWidth = lambda@{ holder ->
                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX

                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false

                return@lambda if (isLeftActionExpanded) {
                    SwipeController.WIDTH_MAX
                } else if (isRightActionExpanded) {
                    holder.getRightSwipeActionWidth() + holder.getLeftSwipeActionWidth()
                } else {
                    holder.getLeftSwipeActionWidth()
                }

            },
            swipeLeftMinWidth = lambda@{ holder ->
                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX

                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false

                return@lambda if (isLeftActionExpanded) {
                    holder.getLeftSwipeActionWidth()
                } else if (isRightActionExpanded) {
                    SwipeController.WIDTH_MAX
                } else {
                    holder.getRightSwipeActionWidth()
                }

            },
            swipeLeftMaxWidth = lambda@{ holder ->
                if (holder !is ATMessage0ViewHolder) return@lambda SwipeController.WIDTH_MAX

                val isRightActionExpanded = holder.model?.isRightActionExpanded ?: false
                val isLeftActionExpanded = holder.model?.isLeftActionExpanded ?: false

                return@lambda if (isLeftActionExpanded) {
                    holder.getRightSwipeActionWidth() + holder.getLeftSwipeActionWidth()
                } else if (isRightActionExpanded) {
                    SwipeController.WIDTH_MAX
                } else {
                    holder.getRightSwipeActionWidth()
                }

            },
            onSwipedAmount = lambda@{ holder, dX, dY ->
                if (holder !is ATMessage0ViewHolder) return@lambda

                holder.onSwipedAmount(dX, dY)
            }
        )

        return swipeController
    }


    private val searchInputQueryTextListener = object : SearchView.OnQueryTextListener {

        private fun showCancelButton() {
            cancelSearch.visibility = View.VISIBLE
        }

        private fun hideCancelButton() {
            cancelSearch.visibility = View.INVISIBLE
        }

        override fun onQueryTextChange(newText: String): Boolean {
            val wasDropped = !messagesViewModel.onQueryTextChange(newText)

            val isEmptyQuery = newText.isBlank()

            if (isEmptyQuery) {
                showCancelButton()
                clearSearch.visibility = View.INVISIBLE
            } else {
                hideCancelButton()
                clearSearch.visibility = View.VISIBLE
            }

            if (!wasDropped) {
                recycler.scrollToPosition(0)
            }

            return false
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            Timber.d("onQueryTextSubmit: $query")
            searchView.clearFocus()

            val wasDropped = !messagesViewModel.onQueryTextSubmit(query)

            if (!wasDropped) {
                recycler.scrollToPosition(0)
            }

            return false
        }

    }



    private val swipeStateHelper = SwipeStateHelper()

    private inner class SwipeStateHelper {

        private val rightExpandedMessages = HashSet<String>()
        private val leftExpandedMessages = HashSet<String>()

        fun onItemSwiped(adapterPosition: Int, leftNotRight: Boolean, maxNotMin: Boolean) {
            val isRightExpanded = rightExpandedMessages.contains("$adapterPosition")
            val isLeftExpanded = leftExpandedMessages.contains("$adapterPosition")

            Timber.d("#onItemSwiped adapterPosition:$adapterPosition leftNotRight:$leftNotRight maxNotMin:$maxNotMin")

            if (leftNotRight) {
                if (isRightExpanded) {
                    //do nothing
                } else if (isLeftExpanded) {
                    if (maxNotMin) {
                        leftExpandedMessages.remove("$adapterPosition")
                        rightExpandedMessages.add("$adapterPosition")
                    } else {
                        leftExpandedMessages.remove("$adapterPosition")
                        rightExpandedMessages.remove("$adapterPosition")
                    }
                } else {
                    leftExpandedMessages.remove("$adapterPosition")
                    rightExpandedMessages.add("$adapterPosition")
                }
            } else {
                if (isRightExpanded) {
                    if (maxNotMin) {
                        leftExpandedMessages.add("$adapterPosition")
                        rightExpandedMessages.remove("$adapterPosition")
                    } else {
                        leftExpandedMessages.remove("$adapterPosition")
                        rightExpandedMessages.remove("$adapterPosition")
                    }
                } else if (isLeftExpanded) {
                    //do nothing
                } else {
                    leftExpandedMessages.add("$adapterPosition")
                    rightExpandedMessages.remove("$adapterPosition")
                }
            }


            invalidateItem(adapterPosition)
        }

        fun invalidateItem(adapterPosition: Int) {
            val model = adapter.getItemOfPosition(adapterPosition)
            if (model !is ATMessage0) return

            model.isLeftActionExpanded = leftExpandedMessages.contains("$adapterPosition")
            model.isRightActionExpanded = rightExpandedMessages.contains("$adapterPosition")

            val holder = recycler.findViewHolderForAdapterPosition(adapterPosition) as ATMessage0ViewHolder ?: return

            //update the views of the itemView so they apply the correct expand modifiers
            holder.onBindViewHolder(model, holder.listener)

            //alternative to the following .. adapter.notifyItemChanged(adapterPosition)
            //hack .. force clearing of the swiping-touch-helper
            recycler.findViewHolderForAdapterPosition(adapterPosition)?.itemView?.let {
                itemTouchHelper.onChildViewDetachedFromWindow(it)
                itemTouchHelper.onChildViewAttachedToWindow(it)
            }
        }


        fun updateExpands(list: List<AdapterType>) {
            list.forEachIndexed { i, item ->
                if (item is ATMessage0) {
                    item.isLeftActionExpanded = leftExpandedMessages.contains("$i")
                    item.isRightActionExpanded = rightExpandedMessages.contains("$i")
                }
            }
        }
    }

}