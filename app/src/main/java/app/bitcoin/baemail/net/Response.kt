package app.bitcoin.baemail.net

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


@Keep
data class BsvAlias(
    @SerializedName("bsvalias") val version: String,
    @SerializedName("handle") val handle: String,
    @SerializedName("pubkey") val pubkey: String

)