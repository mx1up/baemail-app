package app.bitcoin.baemail.net

import app.bitcoin.baemail.BuildConfig
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

// https://bitcoinsv.io/2020/04/03/miner-id-and-merchant-api-beta-release/

// http://bsvalias.org/01-brfc-specifications.html
interface AppApiService {

//    @GET("https://www.moneybutton.com/api/v1/bsvalias/id/{paymail}/")
//    fun fetchMoneyButtonBsvAlias(
//        @Path("paymail") paymail: String
//    ): Response<BsvAlias>

    @GET
    fun fetch(@Url url: String): Call<ResponseBody>

    @POST
    @Headers(
//        "Origin: https://baemail.me",
//        "Referer: https://baemail.me/",
        "Accept: */*",
        "Content-type: application/json",
    )
    fun post(@Url url: String, @Body body: RequestBody): Call<ResponseBody>

    @POST
    @Headers(
        "Content-type: application/json; charset=utf-8",
        "Accept: application/json; charset=utf-8"
    )
    fun postJson(@Url url: String, @Body body: RequestBody): Call<ResponseBody>

    @GET
    fun fetchWithKey(@Url url: String, @Header("key") key: String): Call<ResponseBody>

    @POST("https://txo.bitsocket.network/crawl")
    @Headers(
        "Content-type: application/json; charset=utf-8",
        "Accept: application/json; charset=utf-8",
        "token: ${BuildConfig.CRAWL_TOKEN}"
    )
    fun crawl(@Body body: RequestBody): Call<ResponseBody>


    @POST("https://txo.bitbus.network/block")
    @Headers(
        "Content-type: application/json; charset=utf-8",
        "token: ${BuildConfig.CRAWL_TOKEN}"
    )
    fun getBlock(@Body body: RequestBody): Call<ResponseBody>




    @POST("https://api.whatsonchain.com/v1/bsv/main/tx/raw")
    @Headers(
        "Content-type: application/json"
    )
    fun postHexTx(@Body body: RequestBody): Call<ResponseBody>
}
