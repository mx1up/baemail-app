package app.bitcoin.baemail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.bitcoin.baemail.auth.PaymailViewModel
import app.bitcoin.baemail.paymail.PaymailActivity
import app.bitcoin.baemail.uiCentral.CentralActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class SplashActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var paymailViewModel: PaymailViewModel

    private var isNavigating = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_splash)

        paymailViewModel = ViewModelProvider(this, viewModelFactory).get(PaymailViewModel::class.java)

        paymailViewModel.authLD.observe(this, Observer { isAuthorized ->
            isAuthorized ?: return@Observer

            if (isAuthorized) {
                //go to the central screen
                navigateToApp()
            } else {
                //go to auth screen
                navigateToPaymailScreen()
            }
        })


    }

    private fun navigateToApp() {
        if (isNavigating) return
        isNavigating = true

        startActivity(Intent(this, CentralActivity::class.java), null)
        finish()
    }

    private fun navigateToPaymailScreen() {
        if (isNavigating) return
        isNavigating = true

        startActivity(Intent(this, PaymailActivity::class.java), null)
        finish()
    }



    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}
