package app.bitcoin.baemail.paymail

import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class PaymailVerifyingFragment : DialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_verify_paymail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(PaymailManagementViewModel::class.java)


        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        requireView().clipToOutline = true
        requireView().outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = resources.displayMetrics.density * 13;

                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        isCancelable = false
        dialog?.setCanceledOnTouchOutside(false)


        viewModel.paymailValidationLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            when (it.peekContent()) {
                PaymailRepository.PaymailValidation.ALIAS_MNEMONIC_MISMATCH,
                PaymailRepository.PaymailValidation.INVALID_MNEMONIC -> {
                    dismiss()
                }
                PaymailRepository.PaymailValidation.ALIAS_NOT_FOUND -> {
                    Timber.d("go back to handle-input")
                    findNavController().popBackStack(R.id.paymailIdInputFragment, false)
                }
                PaymailRepository.PaymailValidation.VALID -> {
                    Timber.d("go back to selection screen")
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }
            }
        })
    }
}