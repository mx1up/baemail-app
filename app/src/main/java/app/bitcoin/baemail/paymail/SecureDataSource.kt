package app.bitcoin.baemail.paymail

import android.content.Context
import android.content.SharedPreferences
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import app.bitcoin.baemail.util.CoroutineUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume


class SecureDataSource(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil
) {
    companion object {
        private const val PREFS_NAME = "authenticated"

        private const val KEY_AUTHENTICATED_PAYMAILS = "paymails"
    }

    private lateinit var securePrefs: SharedPreferences

    private val _authPaymailsLD = MutableLiveData<List<AuthenticatedPaymail>>()
    val authenticatedPaymailsLD: LiveData<List<AuthenticatedPaymail>>
        get() = _authPaymailsLD

    fun setup() {
        coroutineUtil.appScope.launch(Dispatchers.IO) {

            val keySpec = KeyGenParameterSpec.Builder(
                MasterKey.DEFAULT_MASTER_KEY_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
            )
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .setKeySize(MasterKey.DEFAULT_AES_GCM_MASTER_KEY_SIZE)
                .build()

            val masterKey = MasterKey.Builder(appContext).setKeyGenParameterSpec(keySpec).build()

            securePrefs = EncryptedSharedPreferences.create(
                appContext,
                PREFS_NAME,
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )

            val awaitingContinuations = ensureInitializedContinuations
            ensureInitializedContinuations = ArrayList()

            awaitingContinuations.forEach {
                Timber.d("signalling initialization completed")
                it.resume(Unit)
            }

            Timber.d("setup")

            refreshAuthenticatedLD()

        }
    }

    private suspend fun refreshAuthenticatedLD() {
        ensureInitialized()

        val paymails = securePrefs.getStringSet(KEY_AUTHENTICATED_PAYMAILS, setOf())!!

        paymails.map {
            AuthenticatedPaymail(it)
        }.let {
            Timber.d("refreshAuthenticatedLD")
            _authPaymailsLD.postValue(ArrayList(it))
        }
    }

    /**
     * Accumulates a list of continuations that have to be resumed on initialization of the helper
     */
    private var ensureInitializedContinuations = ArrayList<Continuation<Unit>>()

    private suspend fun ensureInitialized() {
        suspendCancellableCoroutine<Unit> { continuation ->
            if (::securePrefs.isInitialized) {
                continuation.resume(Unit)
                return@suspendCancellableCoroutine
            }

            Timber.d("awaiting initialization !!!")
            ensureInitializedContinuations.add(continuation)
        }
    }

    /**
     * @return wallet-seed of the authenticated-paymail-wallet
     */
    suspend fun getPrivateKeySeed(paymail: AuthenticatedPaymail): List<String> {
        ensureInitialized()

        return securePrefs.getString(getPrefsKeyForPaymailSeed(paymail), null)?.split(";")
            ?: throw RuntimeException("seed is missing")
    }

    /**
     * @return pki-path used for the authenticated-paymail-wallet
     */
    suspend fun getPkiPathForPrivateKeySeed(paymail: AuthenticatedPaymail): String {
        ensureInitialized()

        return securePrefs.getString(getPrefsKeyForPkiPathForPaymailSeed(paymail), null)
            ?: throw RuntimeException("value is missing")
    }

    /**
     * @return wallet-seed of the funding wallet matching the authenticated-paymail-wallet
     */
    suspend fun getParallelWalletSeed(paymail: AuthenticatedPaymail): List<String> {
        ensureInitialized()

        return securePrefs.getString(getPrefsKeyForFundingSeed(paymail), null)?.split(";")
            ?: throw RuntimeException("seed is missing; paymail: $paymail")
    }

    suspend fun addPaymail(
        paymail: String,
        pkiPath: String,
        privateKeySeed: List<String>,
        parallelWalletSeed: List<String>
    ) {
        ensureInitialized()

        val authenticatedPaymail = AuthenticatedPaymail(paymail)

        if (privateKeySeed.size != 12) throw RuntimeException("invalid seed")
        if (parallelWalletSeed.size != 12) throw RuntimeException("invalid seed")
        val serializedPrivateKeySeed = privateKeySeed.joinToString(";")
        val serializedParallelWalletSeed = parallelWalletSeed.joinToString(";")

        securePrefs.edit().also {
            val currentlyAuthenticated = securePrefs
                .getStringSet(KEY_AUTHENTICATED_PAYMAILS, setOf())!!

            val updatedSet = HashSet(currentlyAuthenticated)
            updatedSet.add(paymail)
            it.putStringSet(KEY_AUTHENTICATED_PAYMAILS, updatedSet)

            it.putString(
                getPrefsKeyForPaymailSeed(authenticatedPaymail),
                serializedPrivateKeySeed
            )

            it.putString(
                getPrefsKeyForPkiPathForPaymailSeed(authenticatedPaymail),
                pkiPath
            )

            it.putString(
                getPrefsKeyForFundingSeed(authenticatedPaymail),
                serializedParallelWalletSeed
            )
        }.apply()

        refreshAuthenticatedLD()
    }

    /**
     * @param value data to store in a secure way
     * @return an alias that allows retrieving of the stored value at a later time
     */
    suspend fun addCustom(value: String): String {
        ensureInitialized()

        val id = UUID.randomUUID().toString()
        val key = getPrefsKeyForCustom(id)

        securePrefs.getString(key, null)?.let {
            //a non-null value for this key is already stored
            throw RuntimeException()
        }

        securePrefs.edit().also {
            it.putString(key, value)

        }.apply()

        return id
    }

    suspend fun getCustom(alias: String): String? {
        ensureInitialized()

        val key = getPrefsKeyForCustom(alias)

        return securePrefs.getString(key, null)
    }

    private fun getPrefsKeyForPaymailSeed(paymail: AuthenticatedPaymail): String {
        return "privateKeySeed_${paymail.paymail}"
    }

    private fun getPrefsKeyForFundingSeed(paymail: AuthenticatedPaymail): String {
        return "parallelWalletSeed_${paymail.paymail}"
    }

    private fun getPrefsKeyForPkiPathForPaymailSeed(paymail: AuthenticatedPaymail): String {
        return "privateKeyPkiPath_${paymail.paymail}"
    }

    private fun getPrefsKeyForCustom(id: String): String {
        return "custom_$id"
    }

}

data class AuthenticatedPaymail(
    val paymail: String
)