package app.bitcoin.baemail.paymail

import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableOval
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class SelectPaymailProviderFragment : Fragment(R.layout.fragment_select_paymail_provider) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    lateinit var container: ConstraintLayout
    lateinit var moneyButton: TextView
    lateinit var relayX: TextView
    lateinit var cancel: TextView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(PaymailManagementViewModel::class.java)

        container = requireView().findViewById(R.id.container)
        moneyButton = requireView().findViewById(R.id.money_button)
        relayX = requireView().findViewById(R.id.relay_x)
        cancel = requireView().findViewById(R.id.cancel)


        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            windowInsets
        }

        val colorSelector = R.color.selector_on_light.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        val colorItemBackground = R.color.select_paymail_fragment__item_background_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorItemStroke = R.color.select_paymail_fragment__item_stroke_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val widthItemStroke = R.dimen.select_paymail_fragment__item_stroke_width.let { id ->
            resources.getDimension(id)
        }


        val colorCancelBackground = R.color.select_paymail_fragment__cancel_item_background_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorCancelStroke = R.color.select_paymail_fragment__cancel_item_stroke_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }


        val drawableOvalMoneyButton = DrawableOval(requireContext()).also {
            it.setBgColor(colorItemBackground)
            it.setStrokeColor(colorItemStroke)
            it.setFullyRounded()
            it.setStrokeWidth(widthItemStroke)
        }
        moneyButton.background = LayerDrawable(arrayOf(
            drawableOvalMoneyButton,
            DrawableState.getNew(colorSelector)
        ))
        moneyButton.clipToOutline = true
        moneyButton.outlineProvider = drawableOvalMoneyButton.getOutlineProvider()





        val drawableOvalRelayX = DrawableOval(requireContext()).also {
            it.setBgColor(colorItemBackground)
            it.setStrokeColor(colorItemStroke)
            it.setFullyRounded()
            it.setStrokeWidth(widthItemStroke)
        }
        relayX.background = LayerDrawable(arrayOf(
            drawableOvalRelayX,
            DrawableState.getNew(colorSelector)
        ))
        relayX.clipToOutline = true
        relayX.outlineProvider = drawableOvalRelayX.getOutlineProvider()




        val drawableOvalCancel = DrawableOval(requireContext()).also {
            it.setBgColor(colorCancelBackground)
            it.setStrokeColor(colorCancelStroke)
            it.setFullyRounded()
            it.setStrokeWidth(widthItemStroke)
        }
        cancel.background = LayerDrawable(arrayOf(
            drawableOvalCancel,
            DrawableState.getNew(colorSelector)
        ))
        cancel.clipToOutline = true
        cancel.outlineProvider = drawableOvalCancel.getOutlineProvider()



        moneyButton.setOnClickListener {
            //viewModel.onProviderSelected(PaymailProvider.MONEYBUTTON)
            //findNavController().navigate(R.id.action_selectPaymailProviderFragment_to_paymailIdInputFragment)
        }

        relayX.setOnClickListener {
            //viewModel.onProviderSelected(PaymailProvider.RELAYX)
            //findNavController().navigate(R.id.action_selectPaymailProviderFragment_to_paymailIdInputFragment)
        }



        cancel.setOnClickListener {
            viewModel.onAddingCancelled()
            findNavController().popBackStack()
        }


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    viewModel.onAddingCancelled()

                    isEnabled = false
                    requireActivity().onBackPressedDispatcher.onBackPressed()
                }

            })


    }
}