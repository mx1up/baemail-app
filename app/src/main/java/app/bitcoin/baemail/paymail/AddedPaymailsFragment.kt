package app.bitcoin.baemail.paymail

import android.content.Intent
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.SplashActivity
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.recycler.*
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import com.google.android.material.appbar.AppBarLayout
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class AddedPaymailsFragment : Fragment(R.layout.fragment_added_paymails) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    lateinit var container: ConstraintLayout
    lateinit var recycler: RecyclerView
    lateinit var appBar: AppBarLayout
    lateinit var addPaymail: TextView

    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory
    lateinit var adapter: BaseListAdapter


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(PaymailManagementViewModel::class.java)



        container = requireView().findViewById(R.id.container)
        recycler = requireView().findViewById(R.id.recycler)
        appBar = requireView().findViewById(R.id.app_bar)
        addPaymail = requireView().findViewById(R.id.add_paymail)

        val topBarElevation = resources.getDimension(R.dimen.added_paymails_fragment__top_bar_elevation)

        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            windowInsets
        }

        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atAuthenticatedPaymailListener = object : BaseListAdapter.ATAuthenticatedPaymailListener {
                override fun onClick(paymail: String) {
                    viewLifecycleOwner.lifecycleScope.launch {
                        viewModel.onChangeActivePaymail(paymail)

                        delay(300)

                        val intent = Intent(requireActivity(), SplashActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        requireActivity().finishAffinity()
                    }

                }

            }
        )

        layoutManager = LinearLayoutManager(context)
        recycler.layoutManager = layoutManager
        recycler.adapter = adapter
        recycler.overScrollMode = View.OVER_SCROLL_NEVER




        appBar.elevation = 0f
        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (adapter.itemCount == 0) {
                    appBar.elevation = 0f
                    return
                }
                when (layoutManager.findFirstCompletelyVisibleItemPosition()) {
                    RecyclerView.NO_POSITION -> false
                    0 -> false
                    else -> true
                }.let { showShadow ->
                    if (showShadow) {
                        appBar.elevation = topBarElevation
                    } else {
                        appBar.elevation = 0f
                    }
                }
            }
        })

        val colorAddPaymailBackground
                = R.color.added_paymails_fragment__add_paymail_background_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorSelectorAddPaymailBackground = R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        addPaymail.background = LayerDrawable(arrayOf(
            ColorDrawable(colorAddPaymailBackground),
            DrawableState.getNew(colorSelectorAddPaymailBackground)
        ))

        addPaymail.clipToOutline = true
        addPaymail.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val cornerRoundness = view.height / 2f

                outline.setRoundRect(0, 0, view.width, view.height, cornerRoundness)
            }

        }

        addPaymail.setOnClickListener {
//            findNavController().navigate(R.id.action_addedPaymailsFragment_to_paymailIdInputFragment)
            findNavController().navigate(R.id.action_addedPaymailsFragment_to_appIntroFragment)
        }


        //todo
        //todo
        //todo


        adapter.setItems(listOf(
            ATNoPaymailsAdded("0")
        ))



        viewModel.addedPaymailsLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            adapter.setItems(it)
        })



        viewModel.paymailValidationLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            it.getContentIfNotHandled()?.let { result ->
                if (PaymailRepository.PaymailValidation.VALID == result) {
                    viewModel.onAddingCancelled()
                }
            }
        })



    }

}