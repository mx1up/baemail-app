package app.bitcoin.baemail.paymail

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class PaymailDerivPathFragment : Fragment(R.layout.fragment_paymail_deriv_path)  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    lateinit var container: CoordinatorLayout
    lateinit var buttonContainer: ConstraintLayout

    lateinit var input: EditText
    lateinit var cancel: TextView
    lateinit var next: TextView

    lateinit var info0a: TextView
    lateinit var info1: TextView

    val appliedInsets = SeedPhraseInputFragment.AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()

    val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorNextBackground: Int by lazy {
        R.color.paymail_id_input_fragment__next_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorNextBackgroundDisabled: Int by lazy {
        R.color.paymail_id_input_fragment__next_background_disabled.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(PaymailManagementViewModel::class.java)

        if (savedInstanceState == null && viewModel.newPaymailLD.value!!.pkiPathDefault == null) {
            viewModel.onPkiPathInit()
        }

        container = requireView().findViewById(R.id.container)
        buttonContainer = requireView().findViewById(R.id.button_container)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        info0a = requireView().findViewById(R.id.info_0a)
        info1 = requireView().findViewById(R.id.info_1)


        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }



        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height - appliedInsets.top - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")

            buttonContainer.translationY = -1f * keyboardHeight

            info1.updatePadding(bottom = (90 * dp + keyboardHeight).toInt())

        })




        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }





        val colorCancelBackground = R.color.paymail_id_input_fragment__cancel_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))






        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))





        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                refreshNextButton()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })






        cancel.setOnClickListener {
            viewModel.onAddingCancelled()
            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
        }

        next.setOnClickListener {
            viewModel.onSubmitPkiPath(input.text.toString())

            findNavController()
                .navigate(R.id.action_paymailDerivPathFragment_to_seedPhraseInputFragment)
        }


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )


        viewModel.newPaymailLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer


            if (it.pkiPath == null) {
                input.setText("")
            } else if (input.text.toString() != it.pkiPath) {
                input.setText(it.pkiPath)
            }


            //todo localize
            if (it.pkiPathDefault == null) {
                info0a.text = "Path used by this provider is unknown.\nPlease input correct path.\nPopular default path: m/44'/0'/0'/0/0/0"
            } else {
                info0a.text = "Path used by this provider is known.\nPath: ${it.pkiPathDefault}"
            }



            refreshNextButton()
        })


        refreshNextButton()

    }

    private fun refreshNextButton() {
        val inputText = input.text.toString()
        val isValidAddress = inputText.let {
            if (it.length <= 2) return@let false
            if (it.indexOf("m") == -1) return@let false
            if (it.indexOf("/") == -1) return@let false

            val s = it.replace(" ", "")
                .replace("m", "")
                .replace("/", "")
                .replace("'", "")

            val isNumber = try {
                s.toInt()
                true
            } catch (e: Exception) {
                false
            }

            isNumber
        }

        if (!isValidAddress) {
            next.isEnabled = false
            drawableNextBackground.color = colorNextBackgroundDisabled
        } else {
            next.isEnabled = true
            drawableNextBackground.color = colorNextBackground
        }

    }


    private fun focusOnInput() {
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        focusOnInput()
    }
}