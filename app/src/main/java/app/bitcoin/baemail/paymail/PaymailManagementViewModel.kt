package app.bitcoin.baemail.paymail

import android.content.Context
import androidx.lifecycle.*
import app.bitcoin.baemail.R
import app.bitcoin.baemail.recycler.ATAuthenticatedPaymail
import app.bitcoin.baemail.recycler.ATFilterSeedWord
import app.bitcoin.baemail.recycler.ATNoPaymailsAdded
import app.bitcoin.baemail.recycler.AdapterType
import app.bitcoin.baemail.util.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import timber.log.Timber
import javax.inject.Inject

class PaymailManagementViewModel @Inject constructor(
    val appContext: Context,
    private val paymailRepository: PaymailRepository
) : ViewModel() {

    lateinit var seedWordList: List<ATFilterSeedWord>

    private val _newPaymailLD = MediatorLiveData<NewPaymailModel>()
    val newPaymailLD: LiveData<NewPaymailModel> get() = _newPaymailLD

    private val _paymailValidationLD = MutableLiveData<Event<PaymailRepository.PaymailValidation>>()
    val paymailValidationLD: LiveData<Event<PaymailRepository.PaymailValidation>>
        get() = _paymailValidationLD

    val addedPaymailsLD = MediatorLiveData<List<AdapterType>>()

    private var addedPaymailsList: List<String> = ArrayList()

    init {
        _newPaymailLD.addSource(addedPaymailsLD) {
            _newPaymailLD.value = _newPaymailLD.value
        }

        viewModelScope.launch(Dispatchers.Default) {
            val seedWordArray = appContext.resources.getStringArray(R.array.seed_word_list)
            seedWordList = seedWordArray.map {
                ATFilterSeedWord(it)
            }
        }

        addedPaymailsLD.addSource(paymailRepository.addedPaymailsLD, Observer {
            it?.let { list ->
                addedPaymailsList = list.map { item ->
                    item.paymail
                }

            }

            refreshAddedPaymailsContent()
        })

        addedPaymailsLD.addSource(paymailRepository.activePaymailLD, Observer {
            refreshAddedPaymailsContent()
        })




    }

    private fun refreshAddedPaymailsContent() {
        val addedPaymailsList = paymailRepository.addedPaymailsLD.value
        val activePaymail = paymailRepository.activePaymailLD.value?.paymail

        if (addedPaymailsList == null || addedPaymailsList.isEmpty()) {
            addedPaymailsLD.value = listOf(
                ATNoPaymailsAdded("0")
            )
            return
        }


        //show added paymails


        val contentList = mutableListOf<AdapterType>()

        addedPaymailsList.mapIndexed { index, authenticatedPaymail ->
            ATAuthenticatedPaymail(
                authenticatedPaymail.paymail,
                authenticatedPaymail.paymail == activePaymail,
                index + 1 == addedPaymailsList.size
            )
        }.toCollection(contentList)

        addedPaymailsLD.value = contentList
    }

    fun getAddedPaymailList(): List<String> {
        return addedPaymailsList
    }

    fun onChangeActivePaymail(paymail: String) {
        paymailRepository.changeActivePaymail(paymail)
    }

    fun onAddingCancelled() {
        _newPaymailLD.postValue(null)
        _paymailValidationLD.postValue(null)
    }

    fun onPaymailIdAdded(paymailId: String) {
        val model = newPaymailLD.value?.copy(paymail = paymailId)
            ?: NewPaymailModel(paymailId)

        _newPaymailLD.postValue(model)
    }

    fun onPkiPathInit() {
        val model = newPaymailLD.value!!
        val paymail = model.paymail!!

        val handle = paymail.substring(0, paymail.indexOf("@"))
        val domain = paymail.substring(paymail.indexOf("@") + 1)

        val path = when (domain) {
            "moneybutton.com" -> "m/44'/0'/0'/0/0/0"
            "relayx.io" -> "m/0'/236'/0'/0/0"
            "simply.cash" -> "m/44'/145'/0'/2/0"
            else -> null
        }


        _newPaymailLD.postValue(model.copy(
            pkiPathDefault = path,
            pkiPath = path
        ))
    }

    fun onSubmitPkiPath(path: String) {
        val model = newPaymailLD.value!!
        _newPaymailLD.postValue(model.copy(
            pkiPath = path
        ))
    }

    fun onSeedWordsAdded(seedList: List<String>) {
        val model = newPaymailLD.value!!
        _newPaymailLD.postValue(model.copy(
            seed = seedList
        ))

        viewModelScope.launch(Dispatchers.IO) {
            val result = paymailRepository.validatePaymail(
                model.paymail!!,
                model.pkiPath!!,
                seedList
            )

            if (PaymailRepository.PaymailValidation.VALID == result) {

                //must find funding-wallet-seed
                val fundingWalletSeed = supervisorScope {
                    return@supervisorScope try {
                        var seed: List<String>? = null
                        launch {
                            seed = paymailRepository.findFundingWalletSeed(seedList)
                        }.join()
                        seed

                    } catch (e: Exception) {
                        Timber.e(e)
                        null
                    }
                }

                if (fundingWalletSeed == null) {
                    //failed finding a valid seed for the funding wallet
                    _paymailValidationLD.postValue(Event(PaymailRepository.PaymailValidation.INVALID_MNEMONIC)) //todo more specific error?
                    return@launch
                } else {
                    Timber.d("a valid parallel wallet seed found")
                }

                //success finding a seed for the funding wallet

                paymailRepository.addPaymail(model.paymail, model.pkiPath, seedList, fundingWalletSeed)
            } else {
                Timber.d("paymail validation failed; paymail:${model.paymail} result:${result.name}")
            }

            _paymailValidationLD.postValue(Event(result))
        }
    }

    data class NewPaymailModel(
        val paymail: String? = null,
        val pkiPathDefault: String? = null,
        val pkiPath: String? = null,
        val seed: List<String> = mutableListOf()
    )
}