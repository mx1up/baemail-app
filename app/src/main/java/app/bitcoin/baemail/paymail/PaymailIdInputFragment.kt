package app.bitcoin.baemail.paymail

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.view.ViewOutlineProvider
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class PaymailIdInputFragment : Fragment(R.layout.fragment_paymail_id_input) {

    companion object {
        const val MIN_LENGTH = 3
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    lateinit var container: ConstraintLayout

    lateinit var coordinator: CoordinatorLayout

    lateinit var input: EditText
    lateinit var cancel: TextView
    lateinit var next: TextView
    lateinit var inputHint: TextView
    lateinit var walletProvider: TextView
    lateinit var errorLabel: TextView


    val takenPaymailIds = mutableListOf<String>()

    val appliedInsets = SeedPhraseInputFragment.AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()

    val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorNextBackground: Int by lazy {
        R.color.paymail_id_input_fragment__next_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorNextBackgroundDisabled: Int by lazy {
        R.color.paymail_id_input_fragment__next_background_disabled.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val drawableNextBackground: ColorDrawable by lazy {
        ColorDrawable(colorNextBackgroundDisabled)
    }

    val drawableNextSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    private val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(PaymailManagementViewModel::class.java)

        container = requireView().findViewById(R.id.container)
        coordinator = requireView().findViewById(R.id.coordinator)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        next = requireView().findViewById(R.id.next)
        inputHint = requireView().findViewById(R.id.input_hint)
        errorLabel = requireView().findViewById(R.id.error_label)


        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }



        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height - appliedInsets.top - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")


            next.translationY = -1f * keyboardHeight
            cancel.translationY = -1f * keyboardHeight
            coordinator.translationY = -1f * keyboardHeight

        })




        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }





        val colorCancelBackground = R.color.paymail_id_input_fragment__cancel_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))






        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        next.background = LayerDrawable(arrayOf(
            drawableNextBackground,
            drawableNextSelector
        ))





        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                refreshNextButton()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })






        cancel.setOnClickListener {
            viewModel.onAddingCancelled()
            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
        }

        next.setOnClickListener {
            viewModel.onPaymailIdAdded(input.text.toString())
            findNavController()
                .navigate(R.id.action_paymailIdInputFragment_to_paymailDerivPathFragment)
        }


        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )


        viewModel.newPaymailLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            takenPaymailIds.clear()
            takenPaymailIds.addAll(viewModel.getAddedPaymailList())
        })


        viewModel.paymailValidationLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            it.peekContent().let { result ->
                if (PaymailRepository.PaymailValidation.VALID == result) {
                    return@Observer
                }
            }

            it.getContentIfNotHandled()?.let { result ->
                if (PaymailRepository.PaymailValidation.ALIAS_NOT_FOUND == result) {
                    Timber.d("show error snackbar")
                    Snackbar.make(
                        coordinator, R.string.paymail_adding_error__paymail_not_found,
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
        })


        refreshNextButton()

    }

    private fun refreshNextButton() {
        val inputText = input.text.toString()

        val isValidAddress = Patterns.EMAIL_ADDRESS.matcher(inputText).matches()

        val shouldShowInvalidAddressError = isValidAddress.let showError@{
            if (it) return@showError false

            val indexOfAt = inputText.indexOf("@")
            (indexOfAt != -1).let { containsAt ->
                if (!containsAt) return@showError false
            }

            val indexOfDot = inputText.substring(indexOfAt).indexOf(".")
            (indexOfDot != -1).let { containsDot ->
                if (!containsDot) return@showError false
            }

            if (indexOfDot + 1 == inputText.substring(indexOfAt).length) {
                return@showError false
            }

            return@showError true
        }

        val isPaymailTaken = takenPaymailIds.contains(inputText)

        if (isPaymailTaken || !isValidAddress) {
            next.isEnabled = false
            drawableNextBackground.color = colorNextBackgroundDisabled
        } else {
            next.isEnabled = true
            drawableNextBackground.color = colorNextBackground
        }

        if (isPaymailTaken) {
            errorLabel.visibility = View.VISIBLE
            errorLabel.text = getString(R.string.paymail_id_input_fragment__error_label_id_taken)
        } else if (shouldShowInvalidAddressError) {
            errorLabel.visibility = View.VISIBLE
            errorLabel.text = getString(R.string.paymail_id_input_fragment__error_label_invalid_paymail)
        } else {
            errorLabel.visibility = View.INVISIBLE
        }

        if (inputText.length == 0) {
            inputHint.visibility = View.VISIBLE
        } else {
            inputHint.visibility = View.INVISIBLE
        }
    }


    private fun focusOnInput() {
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }

    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        focusOnInput()
    }

}