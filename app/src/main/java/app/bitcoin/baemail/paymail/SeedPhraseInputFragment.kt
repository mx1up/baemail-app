package app.bitcoin.baemail.paymail

import android.content.Context
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewOutlineProvider
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.recycler.*
import app.bitcoin.baemail.util.AvailableScreenHeightHelper
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.max

class SeedPhraseInputFragment : Fragment(R.layout.fragment_seed_phrase_input) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: PaymailManagementViewModel


    lateinit var container: ConstraintLayout

    lateinit var coordinator: CoordinatorLayout

    lateinit var selectedWordsRecycler: RecyclerView
    lateinit var filterWordsRecycler: RecyclerView
    lateinit var input: EditText

    lateinit var cancel: TextView
    lateinit var done: TextView

    lateinit var clearInput: ImageView

    lateinit var selectedWordsAdapter: BaseListAdapter

    lateinit var filterWordsAdapter: BaseListAdapter

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory


    val filterBottomExtraLD = MutableLiveData<Int>()
    val bottomExtraLiModel = ATHeightDecorDynamic(filterBottomExtraLD)

    val appliedInsets = AppliedInsets()

    private val availableHeightHelper = AvailableScreenHeightHelper()

    private val dp: Float by lazy {
        resources.displayMetrics.density
    }

    private var maxSeedWordCount = 12
    private val selectedSeedWords = mutableListOf<String>()

    val elevatedTopBarHeight: Float by lazy {
        dp * 6
    }

    val drawableStrokeSeparator: DrawableStroke by lazy {
        val strokeColor = R.color.li_filter_seed_word__stroke_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        val strokeWidth = (dp * 2).toInt()

        val d = DrawableStroke()
        d.setStrokeColor(strokeColor)
        d.setStrokeWidths(0, strokeWidth, 0, 0)

        d
    }

    val colorSelectorOnAccent: Int by lazy {
        R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorSelectorOnLight: Int by lazy {
        R.color.selector_on_light.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorDoneBackground: Int by lazy {
        R.color.seed_phrase_input_fragment__done_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val colorDoneBackgroundDisabled: Int by lazy {
        R.color.seed_phrase_input_fragment__done_background_disabled.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
    }

    val drawableDoneBackground: ColorDrawable by lazy {
        ColorDrawable(colorDoneBackgroundDisabled)
    }

    val drawableDoneSelector: Drawable by lazy {
        DrawableState.getNew(colorSelectorOnAccent)
    }

    val labelDoneWhenComplete: String by lazy {
        resources.getString(R.string.seed_phrase_input_fragment__done_value)
    }

    private val imm: InputMethodManager by lazy {
        requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private lateinit var refreshTopBarElevation: ()->Unit

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_paymail)

        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
            .get(PaymailManagementViewModel::class.java)



        container = requireView().findViewById(R.id.container)
        coordinator = requireView().findViewById(R.id.coordinator)
        selectedWordsRecycler = requireView().findViewById(R.id.selected_words)
        filterWordsRecycler = requireView().findViewById(R.id.filter_list)
        input = requireView().findViewById(R.id.input)
        cancel = requireView().findViewById(R.id.cancel)
        done = requireView().findViewById(R.id.done)
        clearInput = requireView().findViewById(R.id.clear_input)


        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }


        filterBottomExtraLD.value = (resources.displayMetrics.density * 130).toInt()



        availableHeightHelper.setup(
            requireContext(),
            requireActivity().window,
            viewLifecycleOwner,
            { requireView() }
        )

        availableHeightHelper.availableHeightLD.observe(viewLifecycleOwner, Observer { height ->
            height ?: return@Observer

            val maxHeight = container.height - appliedInsets.top - appliedInsets.bottom

            val keyboardHeight = max(maxHeight - height, 0)
            Timber.d("keyboardHeight: $keyboardHeight")

            filterBottomExtraLD.value = keyboardHeight

            cancel.translationY = -1f * keyboardHeight
            done.translationY = -1f * keyboardHeight
            coordinator.translationY = -1f * keyboardHeight

        })






        cancel.clipToOutline = true
        cancel.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }





        val colorCancelBackground = R.color.seed_phrase_input_fragment__cancel_background.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        cancel.background = LayerDrawable(arrayOf(
            ColorDrawable(colorCancelBackground),
            DrawableState.getNew(colorSelectorOnAccent)
        ))

        cancel.setOnClickListener {
            findNavController().popBackStack(R.id.addedPaymailsFragment, false)
        }





        done.clipToOutline = true
        done.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }

        done.background = LayerDrawable(arrayOf(
            drawableDoneBackground,
            drawableDoneSelector
        ))

        done.setOnClickListener {
            viewModel.onSeedWordsAdded(selectedSeedWords)

            findNavController()
                .navigate(R.id.action_seedPhraseInputFragment_to_paymailVerifyingFragment)
        }







        clearInput.setOnClickListener {
            input.setText("")
        }

        clearInput.foreground = DrawableState.getNew(colorSelectorOnLight)

        clearInput.clipToOutline = true
        clearInput.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val corners = view.height / 2f
                outline.setRoundRect(0, 0, view.width, view.height, corners)
            }

        }



        selectedWordsAdapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atSelectedSeedWordListener = object : BaseListAdapter.ATSelectedSeedWordListener {
                override fun onDismissClicked() {
                    selectedSeedWords.removeAt(selectedSeedWords.size - 1)
                    refreshSeedWordRecycler()
                    refreshDoneButton()
                }

            }
        )

        val selectedWordsLayoutManager = LinearLayoutManager(context)
        selectedWordsLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        selectedWordsRecycler.layoutManager = selectedWordsLayoutManager
        selectedWordsRecycler.adapter = selectedWordsAdapter






        filterWordsAdapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atFilterSeedWordListener = object : BaseListAdapter.ATFilterSeedWordListener {
                override fun onClick(word: String) {
                    if (selectedSeedWords.size == maxSeedWordCount) return

                    selectedSeedWords.add(word)

                    refreshSeedWordRecycler()
                    refreshDoneButton()

                    input.setText("")
                }
            }
        )

        val filterWordsLayoutManager = LinearLayoutManager(context)
        filterWordsRecycler.layoutManager = filterWordsLayoutManager
        filterWordsRecycler.adapter = filterWordsAdapter

        filterWordsRecycler.background = drawableStrokeSeparator

        refreshTopBarElevation = lambda@{
            when (filterWordsLayoutManager.findFirstCompletelyVisibleItemPosition()) {
                RecyclerView.NO_POSITION -> false
                0 -> false
                else -> true
            }.let { showShadow ->
                if (showShadow) {
                    if (drawableStrokeSeparator.alpha == 0) return@lambda

                    selectedWordsRecycler.elevation = elevatedTopBarHeight
                    input.elevation = elevatedTopBarHeight
                    clearInput.elevation = elevatedTopBarHeight

                    drawableStrokeSeparator.alpha = 0
                } else {
                    if (drawableStrokeSeparator.alpha == 255) return@lambda

                    selectedWordsRecycler.elevation = 0f
                    input.elevation = 0f
                    clearInput.elevation = 0f

                    drawableStrokeSeparator.alpha = 255
                }
            }
        }

        filterWordsRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                refreshTopBarElevation()
            }
        })


        if (savedInstanceState == null) {
            //avoid drawing the shadow top-bar automatically lays out with at first
            var cleanupListener: () -> Unit = {
                throw RuntimeException()
            }

            val listener = object : ViewTreeObserver.OnPreDrawListener {

                override fun onPreDraw(): Boolean {
                    selectedWordsRecycler.elevation = 0f
                    input.elevation = 0f
                    clearInput.elevation = 0f

                    drawableStrokeSeparator.alpha = 255

                    cleanupListener()

                    return true
                }

            }

            cleanupListener = {
                input.viewTreeObserver.removeOnPreDrawListener(listener)
            }
            input.viewTreeObserver.addOnPreDrawListener(listener)
        }








        selectedWordsAdapter.setItems(listOf(
            ATSeedWordHint(resources.getString(R.string.li_seed_word_hint__value_of_start))
        ))



        input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filterSeedWords(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })



        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {

                    viewModel.onAddingCancelled()

                    isEnabled = false
                    findNavController().popBackStack(R.id.addedPaymailsFragment, false)
                }

            }
        )


        viewModel.paymailValidationLD.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            it.peekContent().let { result ->
                if (PaymailRepository.PaymailValidation.ALIAS_NOT_FOUND == result ||
                    PaymailRepository.PaymailValidation.VALID == result) {
                    return@Observer
                }
            }

            it.getContentIfNotHandled()?.let { result ->
                if (PaymailRepository.PaymailValidation.INVALID_MNEMONIC == result) {
                    Snackbar.make(coordinator, R.string.paymail_adding_error__invalid_mnemonic,
                        Snackbar.LENGTH_LONG).show()
                } else if (PaymailRepository.PaymailValidation.ALIAS_MNEMONIC_MISMATCH == result) {
                    Snackbar.make(coordinator, R.string.paymail_adding_error__mnemonic_mismatch,
                        Snackbar.LENGTH_LONG).show()
                } else {
                    Timber.d("unexpected navigation")
                }
            }
        })




        filterSeedWords("")
        refreshSeedWordRecycler()
        refreshDoneButton()



        Timber.d("onActivityCreated")
    }


    var filterSeedWordsJob: Job? = null

    private fun filterSeedWords(filter: String) {
        filterSeedWordsJob?.cancel()

        if (filter.length == 0) {
            val list = ArrayList<AdapterType>(viewModel.seedWordList)
            list.add(bottomExtraLiModel)
            filterWordsAdapter.setItems(list)

            filterWordsRecycler.scrollToPosition(0)
            filterWordsRecycler.post {
                refreshTopBarElevation()
            }

            return
        }

        filterSeedWordsJob = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            val filtered = ArrayList<AdapterType>()

            viewModel.seedWordList.filter { it.value.startsWith(filter) }.toCollection(filtered)

            if (filtered.isEmpty()) {
                filtered.add(ATFilterSeedNoMatch(""))
            }

            filtered.add(bottomExtraLiModel)


            withContext(Dispatchers.Main) {
                filterWordsAdapter.setItems(filtered)

                filterWordsRecycler.scrollToPosition(0)
                filterWordsRecycler.post {
                    refreshTopBarElevation()
                }
            }
        }
    }

    private fun refreshSeedWordRecycler() {
        val contentList = mutableListOf<AdapterType>()

        selectedSeedWords.forEachIndexed { index, s ->
            val isLast = index + 1 == selectedSeedWords.size
            contentList.add(ATSelectedSeedWord(s, isLast))
        }

        if (selectedSeedWords.size == 0) {
            contentList.add(
                ATSeedWordHint(resources.getString(R.string.li_seed_word_hint__value_of_start))
            )
        }

        selectedWordsAdapter.setItems(contentList)


        //scroll to the end
        val scrollAmount = (selectedSeedWords.size * (dp * 150)).toInt()
        selectedWordsRecycler.scrollBy(scrollAmount, 0)
    }

    private fun refreshDoneButton() {
        if (selectedSeedWords.size == 12) {
            done.isEnabled = true
            done.text = labelDoneWhenComplete
            drawableDoneBackground.color = colorDoneBackground
            return
        }
        done.isEnabled = false

        val missingCount = 12 - selectedSeedWords.size

        done.text = resources.getString(
            R.string.seed_phrase_input_fragment__done_incomplete, missingCount)

        drawableDoneBackground.color = colorDoneBackgroundDisabled
    }


    override fun onStart() {
        super.onStart()

        Timber.d("onStart $imm")
        if (input.requestFocus()) {
            Timber.d("onStart got focus")

            if (!imm.isActive(input)) imm.restartInput(input)
        }
    }


    override fun onStop() {

        filterSeedWordsJob?.cancel()

        super.onStop()
    }

    data class AppliedInsets(
        var top: Int = 0,
        var bottom: Int = 0
    )
}