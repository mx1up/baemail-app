package app.bitcoin.baemail.paymail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.fundingWallet.CoinViewModel
import app.bitcoin.baemail.util.RoundedBottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class AppDetailedInfoDialog : RoundedBottomSheetDialogFragment() {

//    @Inject
//    lateinit var viewModelFactory: ViewModelProvider.Factory



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detailed_app_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AndroidSupportInjection.inject(this)

//        val viewModelStoreOwner = findNavController().getViewModelStoreOwner(R.id.nav_coins)

//        viewModel = ViewModelProvider(viewModelStoreOwner, viewModelFactory)
//            .get(CoinViewModel::class.java)




//        sendToAddress = requireView().findViewById(R.id.send_to_address)
//        sendToPaymail = requireView().findViewById(R.id.send_to_paymail)
//        coinSpentLabel = requireView().findViewById(R.id.coin_spent_label)


//        val txHash = requireArguments().getString("tx_hash")
//        val address = requireArguments().getString("address")
//        val derivationPath = requireArguments().getString("derivation_path")
//        val spendingTxHash = requireArguments().getString("spending_tx_hash")
//        val outputIndex = requireArguments().getInt("output_index")
//        val sats = requireArguments().getInt("sats")


//        val colorSelector = R.color.selector_on_light.let { id ->
//            ContextCompat.getColor(requireContext(), id)
//        }
//
//        sendToAddress.background = DrawableState.getNew(colorSelector)
//        sendToPaymail.background = DrawableState.getNew(colorSelector)
//
//        sendToAddress.setOnClickListener {
//            val args = Bundle()
//            args.putString("tx_hash", txHash)
//            args.putString("address", address)
//            args.putString("derivation_path", derivationPath)
//            args.putInt("output_index", outputIndex)
//            args.putInt("sats", sats)
//
//            findNavController().navigate(R.id.action_coinOptionsFragment_to_sendCoinToAddressFragment, args)
//        }
//
//        sendToPaymail.setOnClickListener {
//            val args = Bundle()
//            args.putString("tx_hash", txHash)
//            args.putString("address", address)
//            args.putString("derivation_path", derivationPath)
//            args.putInt("output_index", outputIndex)
//            args.putInt("sats", sats)
//
//            findNavController().navigate(R.id.action_coinOptionsFragment_to_sendCoinToPaymailFragment, args)
//        }
//
//
//        if (spendingTxHash == null) {
//            coinSpentLabel.visibility = View.GONE
//            sendToAddress.visibility = View.VISIBLE
//            sendToPaymail.visibility = View.VISIBLE
//        } else {
//            coinSpentLabel.visibility = View.VISIBLE
//            sendToAddress.visibility = View.INVISIBLE
//            sendToPaymail.visibility = View.INVISIBLE
//        }



    }

    override fun getPeekHeight(): Int {
        return -1
    }
}