package app.bitcoin.baemail.paymail

import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.text.util.Linkify
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableState
import app.bitcoin.baemail.util.doOnApplyWindowInsets

class AppIntroFragment : Fragment(R.layout.fragment_app_intro)  {

    private lateinit var container: NestedScrollView
    private lateinit var info0: TextView
    private lateinit var next: TextView
    private lateinit var howToFindMySeed: Button


    val appliedInsets = SeedPhraseInputFragment.AppliedInsets()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        container = requireView().findViewById(R.id.container)
        info0 = requireView().findViewById(R.id.info_0)
        next = requireView().findViewById(R.id.next)
        howToFindMySeed = requireView().findViewById(R.id.how_to_find_my_seed)

        Linkify.addLinks(info0, Linkify.WEB_URLS);


        container.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.updatePadding(
                top = windowInsets.systemWindowInsetTop,
                bottom = windowInsets.systemWindowInsetBottom
            )

            appliedInsets.top = windowInsets.systemWindowInsetTop
            appliedInsets.bottom = windowInsets.systemWindowInsetBottom

            windowInsets
        }






        val colorAddPaymailBackground
                = R.color.added_paymails_fragment__add_paymail_background_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }
        val colorSelectorAddPaymailBackground = R.color.selector_on_accent.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        next.background = LayerDrawable(arrayOf(
            ColorDrawable(colorAddPaymailBackground),
            DrawableState.getNew(colorSelectorAddPaymailBackground)
        ))

        next.clipToOutline = true
        next.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                view ?: return
                outline ?: return

                val cornerRoundness = view.height / 2f

                outline.setRoundRect(0, 0, view.width, view.height, cornerRoundness)
            }

        }

        next.setOnClickListener {
            findNavController().navigate(R.id.action_appIntroFragment_to_paymailIdInputFragment)
//            findNavController().navigate(R.id.action_addedPaymailsFragment_to_appIntroFragment)
        }


        howToFindMySeed.setOnClickListener {
            findNavController().navigate(R.id.action_appIntroFragment_to_appDetailedInfoDialog)
        }


    }
}