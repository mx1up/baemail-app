package app.bitcoin.baemail.paymail

import android.content.Context
import androidx.core.os.TraceCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import app.bitcoin.baemail.R
import app.bitcoin.baemail.Tr
import app.bitcoin.baemail.fundingWallet.PaymailExistsHelper
import app.bitcoin.baemail.net.AppApiService
import app.bitcoin.baemail.net.BsvAlias
import app.bitcoin.baemail.util.CoroutineUtil
import app.bitcoin.baemail.wallet.AppStateLiveData
import app.bitcoin.baemail.wallet.DynamicChainSync
import app.bitcoin.baemail.wallet.WalletRepository
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import okhttp3.RequestBody.Companion.toRequestBody
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Exception
import kotlin.collections.ArrayList
import kotlin.random.Random

class PaymailRepository(
    private val appContext: Context,
    private val coroutineUtil: CoroutineUtil,
    private val appStateLD: AppStateLiveData,
    private val apiService: AppApiService,
    private val gson: Gson,
    private val walletRepository: WalletRepository,
    private val secureDataSource: SecureDataSource,
    private val chainSync: DynamicChainSync
) {

    private var hasInitialized = false
    private val _activePaymailLD = MediatorLiveData<AuthenticatedPaymail>()
    val activePaymailLD: LiveData<AuthenticatedPaymail>
        get() = _activePaymailLD

    val addedPaymailsLD: LiveData<List<AuthenticatedPaymail>> = secureDataSource.authenticatedPaymailsLD

    val paymailExistsHelper = PaymailExistsHelper(this, coroutineUtil)

    init {

        _activePaymailLD.addSource(appStateLD) {
            refreshActivePaymail()
        }

        _activePaymailLD.addSource(addedPaymailsLD) {
            refreshActivePaymail()
        }
    }

    private fun setActive(paymail: AuthenticatedPaymail?) {
        hasInitialized = true

        _activePaymailLD.value = paymail
    }

    private fun refreshActivePaymail() {
        val state = appStateLD.value ?: return
        val addedPaymails = addedPaymailsLD.value ?: return

        val currentActive: AuthenticatedPaymail? = activePaymailLD.value


        if (addedPaymails.isEmpty()) {
            if (currentActive == null && !hasInitialized) {
                Timber.d("initializing with no active paymails")
                setActive(null)
                return
            } else if (currentActive == null && hasInitialized) {
                Timber.d("active paymail unchanged")
                return
            }

            Timber.d("active paymail cleared")
            setActive(null)
            return
        }

        val activePaymailId = state.activePaymail
        if (activePaymailId == null) {
            if (currentActive == null && hasInitialized) {
                Timber.d("active paymail unchanged")
                return
            }

            Timber.d("active paymail cleared")
            setActive(null)
            return
        }

        val matchingActivePaymail = addedPaymails.find {
            it.paymail == activePaymailId
        }

        if (matchingActivePaymail == null) {
            Timber.d("active paymail-id did not have a matching paymail entry; returning")
            return
        }


        if (currentActive == null) {
            Timber.d("active paymail set to: $matchingActivePaymail")
            setActive(matchingActivePaymail)
        } else if (currentActive.paymail == matchingActivePaymail.paymail) {
            Timber.d("active paymail unchanged")
        } else {
            Timber.d("active paymail updated: $matchingActivePaymail")
            setActive(matchingActivePaymail)
        }
    }

    fun changeActivePaymail(paymail: String) {
        val addedPaymails = addedPaymailsLD.value ?: return

        addedPaymails.map {
            it.paymail
        }.contains(paymail).let { containsPaymail ->
            if (!containsPaymail) return
        }

        appStateLD.setActivePaymail(paymail)
    }

    suspend fun addPaymail(
        paymail: String,
        pkiPath: String,
        seed: List<String>,
        parallelWalletSeed: List<String>
    ) {
        secureDataSource.addPaymail(paymail, pkiPath, seed, parallelWalletSeed)
    }

    suspend fun validatePaymail(
        paymail: String,
        pkiPath: String,
        seed: List<String>
    ): PaymailValidation {

        var bsvAliasResult: Pair<BsvAlias?, Exception?>? = null
        var publicKeyForSeed: String? = null
        supervisorScope {
            val deferredAlias = async {
                checkPaymailExists(paymail)
            }

            val deferredGetPublicKeyForSeed = async {

                //keys for the specified path
                walletRepository.getPrivateKeyForPathFromSeed(
                    pkiPath,
                    seed
                ) ?: let {
                    Timber.d("error retrieving keys for path")
                    null
                }

            }

            try {
                bsvAliasResult = deferredAlias.await()
                Timber.d("validatePaymail ... pki-publicKey: ${bsvAliasResult?.first?.pubkey}")
            } catch (e: Exception) {
                Timber.e("crash retrieving alias")
            }
            try {
                publicKeyForSeed = deferredGetPublicKeyForSeed.await()?.publicKey
            } catch (e: Exception) {
                Timber.e("crash getting pub-key from seed")
            }
        }

        val alias = bsvAliasResult?.first ?: return PaymailValidation.ALIAS_NOT_FOUND
        publicKeyForSeed ?: return PaymailValidation.INVALID_MNEMONIC

        return if (alias.pubkey == publicKeyForSeed) {
            PaymailValidation.VALID
        } else {
            PaymailValidation.ALIAS_MNEMONIC_MISMATCH
        }
    }

    suspend fun findFundingWalletSeed(seedList: List<String>): List<String> {
        val seedWordArray = appContext.resources.getStringArray(R.array.seed_word_list)

        var potentialSeed: List<String> = ArrayList(seedList.asReversed())

        //check validity of seed
        (0 until 111).forEach {
            potentialSeed = getNextCandidate(seedWordArray, potentialSeed)

            val isValidMnemonic = walletRepository.checkMnemonic(potentialSeed)
            if (isValidMnemonic) {
                return potentialSeed
            }
        }

        throw RuntimeException("failed finding a valid seed")
    }

    private fun getNextCandidate(seedWords: Array<String>, seedList: List<String>): List<String> {
        fun getPrev(word: String): String {
            val index = seedWords.indexOf(word)
            return if (index == 0) {
                seedWords[seedWords.size - 1]
            } else {
                seedWords[index - 1]
            }
        }
        fun getNext(word: String): String {
            val index = seedWords.indexOf(word)
            return if (index + 1 == seedWords.size) {
                seedWords[0]
            } else {
                seedWords[index + 1]
            }
        }

        return listOf(
            getNext(seedList[0]),
            getPrev(seedList[1]),
            getNext(seedList[2]),
            getPrev(seedList[3]),
            getNext(seedList[4]),
            getPrev(seedList[5]),
            getNext(seedList[6]),
            getPrev(seedList[7]),
            getNext(seedList[8]),
            getPrev(seedList[9]),
            getNext(seedList[10]),
            getPrev(seedList[11])
        )
    }

    suspend fun checkPaymailExists(
        paymail: String
    ): Pair<BsvAlias?, Exception?> {
        return withContext(Dispatchers.IO) {

            val handle = paymail.substring(0, paymail.indexOf("@"))
            val domain = paymail.substring(paymail.indexOf("@") + 1)

            //get bsvalias endpoints for the domain

            val endpointsMetaResponse = try {
                apiService.fetch("https://$domain/.well-known/bsvalias")
                    .execute()
            } catch (e: Exception) {
                Timber.e(e, ">>>>>>>>>")
                return@withContext Pair(null, e)
            }

            val metaResponseBody = endpointsMetaResponse.body()
            val pkiEndpoint =
                if (endpointsMetaResponse.isSuccessful && metaResponseBody != null) {
                    val jsonString = String(metaResponseBody.bytes())
                    val json = gson.fromJson(jsonString, JsonObject::class.java)

                    val capabilitiesJson: JsonObject = json.getAsJsonObject("capabilities")

                    capabilitiesJson.getAsJsonPrimitive("pki").asString
                } else {
                    null
                }

            if (pkiEndpoint == null) return@withContext Pair(null, null)

            var assembled = pkiEndpoint.replace("{alias}", handle)
            assembled = assembled.replace("{domain.tld}", domain)


            val response = try {
                apiService.fetch(assembled).execute()
            } catch (e: Exception) {
                Timber.e(e, ">>>>>>>>>")
                return@withContext Pair(null, e)
            }
            if (response.isSuccessful) {
                response.body()?.let {
                    try {
                        return@withContext Pair(
                            gson.fromJson(it.charStream(), BsvAlias::class.java),
                            null
                        )
                    } catch (e: Exception) {
                        Timber.e(e)

                        return@withContext Pair(null, e)
                    }
                }
            }

            return@withContext Pair(null, null)
        }
    }

    /**
     * @return hex encoded Bitcoin script for the destination output
     */
    suspend fun getPaymailDestinationOutput(
        sendingPaymail: String,
        receivingPaymail: String
    ): String? {

        val id = Random.nextInt()
        try {
            TraceCompat.beginAsyncSection(Tr.METHOD_GET_PAYMAIL_DESTINATION_OUTPUT, id)

            val paymailSeed = try {
                secureDataSource.getPrivateKeySeed(AuthenticatedPaymail(sendingPaymail))
            } catch (e: Exception) {
                Timber.d("failed to retrieve active-paymail wallet-seed")
                return null
            }

            val pkiPath = try {
                secureDataSource.getPkiPathForPrivateKeySeed(AuthenticatedPaymail(sendingPaymail))
            } catch (e: Exception) {
                Timber.d("failed to retrieve active-paymail pki-path")
                return null
            }


            val keyHolder = walletRepository.getPrivateKeyForPathFromSeed(
                pkiPath,
                paymailSeed
            ) ?: let {
                Timber.d("error retrieving the wif-private-key for path")
                return null
            }

            val addressResolver = PaymailAddressResolver(sendingPaymail, null, null)

            val signature = walletRepository.signMessage(
                addressResolver.getSignedString(),
                keyHolder.privateKey
            ) ?: let {
                Timber.d("error signing paymail-address-resolution message")
                return null
            }


            val reqJson = addressResolver.getReqJson(signature)

            Timber.d("paymail-address-resolution json: $reqJson")

            return getPaymailDestination(receivingPaymail, reqJson.toString())
        } finally {
            TraceCompat.endAsyncSection(Tr.METHOD_GET_PAYMAIL_DESTINATION_OUTPUT, id)
        }
    }

    /**
     * @return hex encoded Bitcoin script for the destination output
     */
    private suspend fun getPaymailDestination(
        paymail: String,
        sendingPaymailAuth: String
    ): String? {
        return withContext(Dispatchers.IO) {
            val id = Random.nextInt()
            try {
                TraceCompat.beginAsyncSection(Tr.METHOD_REQUEST_PAYMENT_DESTINATION_OUTPUT, id)

                val handle = paymail.substring(0, paymail.indexOf("@"))
                val domain = paymail.substring(paymail.indexOf("@") + 1)


                val endpointsMetaResponse = apiService.fetch("https://$domain/.well-known/bsvalias")
                    .execute()

                val metaResponseBody = endpointsMetaResponse.body()
                val paymentDestinationEndpoint = if (
                    endpointsMetaResponse.isSuccessful &&
                    metaResponseBody != null
                ) {
                    val jsonString = String(metaResponseBody.bytes())
                    val json = gson.fromJson(jsonString, JsonObject::class.java)

                    val capabilitiesJson: JsonObject = json.getAsJsonObject("capabilities")

                    capabilitiesJson.getAsJsonPrimitive("paymentDestination").asString
                } else {
                    null
                }

                if (paymentDestinationEndpoint == null) return@withContext null

                //got the payment destination querying endpoint

                var assembled = paymentDestinationEndpoint.replace("{alias}", handle)
                assembled = assembled.replace("{domain.tld}", domain)

                val reqBody = sendingPaymailAuth.toRequestBody()

                val call = apiService.postJson(assembled, reqBody)
                val response = call.execute()
                if (response.isSuccessful) {
                    response.body()?.let {
                        try {
                            //got the destination output

                            val json = gson.fromJson(it.charStream(), JsonObject::class.java)
                            val hexScript = json.get("output").asString

                            Timber.d("output hex script; destination: $paymail; script: $hexScript")
                            return@withContext hexScript

                        } catch (e: Exception) {
                            Timber.e(e)
                        }
                    }
                }

                return@withContext null
            } finally {
                TraceCompat.endAsyncSection(Tr.METHOD_REQUEST_PAYMENT_DESTINATION_OUTPUT, id)

            }
        }
    }



    enum class PaymailValidation {
        VALID,
        ALIAS_NOT_FOUND,
        INVALID_MNEMONIC,
        ALIAS_MNEMONIC_MISMATCH
    }



    class PaymailAddressResolver(
        val senderHandle: String,
        val amount: Int?,
        val purpose: String?
    ) {

        val timestamp: String

        init {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'", Locale.US).apply {
                timeZone = TimeZone.getTimeZone("UTC")
            }

            timestamp = df.format(Date())
        }

        fun getSignedString(): String {
            val stringAmount = amount.toString() ?: "0"
            val finalPurpose = purpose ?: ""

            return senderHandle + timestamp + stringAmount + finalPurpose
        }


        fun getReqJson(signature: String): JsonObject {
            val json = JsonObject()

            json.addProperty("senderName", "")
            json.addProperty("senderHandle", senderHandle)
            json.addProperty("dt", timestamp)

            val stringAmount = amount.toString() ?: "0"
            json.addProperty("amount", stringAmount)

            val finalPurpose = purpose ?: ""
            json.addProperty("purpose", finalPurpose)

            json.addProperty("signature", signature)

            return json
        }

    }



}