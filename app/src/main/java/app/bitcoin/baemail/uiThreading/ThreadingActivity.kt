package app.bitcoin.baemail.uiThreading

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import app.bitcoin.baemail.R
import app.bitcoin.baemail.util.doOnApplyWindowInsets
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class ThreadingActivity: AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var statusBarBg: View
    private lateinit var navigationBarBg: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        //

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }

        setContentView(R.layout.activity_message_thread)

        //

        statusBarBg = findViewById(R.id.status_bar_bg)
        navigationBarBg = findViewById(R.id.navigation_bar_bg)


        statusBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetTop.let { top ->
                    if (top == 0) 1 else top
                }

                view.layoutParams = it
            }

            windowInsets
        }
        navigationBarBg.doOnApplyWindowInsets { view, windowInsets, initialPadding ->
            view.layoutParams.let {
                it.height = windowInsets.systemWindowInsetBottom.let { bottom ->
                    if (bottom == 0) 1 else bottom
                }

                view.layoutParams = it
            }

            windowInsets
        }






    }





    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }







}