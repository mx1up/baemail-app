package app.bitcoin.baemail.uiThreading

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.bitcoin.baemail.R
import app.bitcoin.baemail.drawable.DrawableStroke
import app.bitcoin.baemail.recycler.ATMessage0
import app.bitcoin.baemail.recycler.BaseListAdapter
import app.bitcoin.baemail.recycler.BaseListAdapterFactory
import app.bitcoin.baemail.view.ViewContactCard
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class MessageThreadFragment : Fragment(R.layout.fragment_message_thread) {

    private lateinit var contactCard: ViewContactCard
    private lateinit var subjectLabel: TextView
    private lateinit var messages: RecyclerView

    private lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var adapterFactory: BaseListAdapterFactory
    private lateinit var adapter: BaseListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        AndroidSupportInjection.inject(this)


        contactCard = requireView().findViewById(R.id.contact_bar)
        subjectLabel = requireView().findViewById(R.id.subject_label)
        messages = requireView().findViewById(R.id.messages)







        //todo
        //todo
        val adapterTypeMemoryListener = object : BaseListAdapter.ATMessageListener() {
            override fun onItemClicked(adapterPosition: Int, model: ATMessage0) {
            }

            override fun onActionClicked(
                adapterPosition: Int,
                model: ATMessage0,
                actionId: Int
            ) {
            }
        }
        //todo
        //todo






        adapter = BaseListAdapter(
            viewLifecycleOwner.lifecycle,
            adapterFactory,
            atMessageListener = adapterTypeMemoryListener
        )
        layoutManager = LinearLayoutManager(context)
        layoutManager.stackFromEnd = true
        messages.layoutManager = layoutManager
        messages.adapter = adapter
        messages.setHasFixedSize(true)












        val drawableProfileSeparator = DrawableStroke().also {
            val paddingSides = R.dimen.setup_fragment__profile_separator_padding_sides.let { id ->
                resources.getDimensionPixelSize(id)
            }
            val width = R.dimen.setup_fragment__profile_separator_width.let { id ->
                resources.getDimensionPixelSize(id)
            }
            val color = R.color.setup_fragment__profile_separator_color.let { id ->
                ContextCompat.getColor(requireContext(), id)
            }
            it.setPadding(paddingSides, 0, paddingSides, 0)
            it.setStrokeColor(color)
            it.setStrokeWidths(0, 0, 0, width)
        }
        val drawableBackgroundColor = R.color.message_thread__top_bar_background_color.let { id ->
            ContextCompat.getColor(requireContext(), id)
        }

        contactCard.background = LayerDrawable(arrayOf(
            ColorDrawable(drawableBackgroundColor),
            drawableProfileSeparator
        ))
        subjectLabel.background = ColorDrawable(drawableBackgroundColor)


















        val profileModel = ViewContactCard.Model(
            ColorDrawable(Color.parseColor("#5075ee")),
            "Melinda Jensen",
            "Working at GoShare"
        )

        contactCard.applyModel(profileModel)

        subjectLabel.text = "Intro: Shaun @ GoShare <> Alex Banaga"
    }


    override fun onStart() {
        super.onStart()



        //todo
        //todo
        //todo
        //todo
        adapter.setItems(listOf(
            ATMessage0(
                "25",
                "Doctor Brooks",
                "The test-results have come back",
                "Could you please come in for a meeting ASAP!",
                "OCT 3",
                hasAttachment = false,
                isUnread = false
            )
        ))
        //todo
        //todo
        //todo
        //todo

    }

    override fun onResume() {
        super.onResume()

        requireView().requestLayout()
    }
}