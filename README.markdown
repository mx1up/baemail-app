## AlphaBae - Baemail Android App

This boilerplate Android app is an encrypted messenger that sends messages through BSV blockchain.
You can attach cents to messages. It is open-source, build on top of it. Current setup allows usage
of majority of BSV libraries.


This app is a prototype implementation of `AlphaBae - BSV SDK for Android`. The Baemail features
are a thin layer on top of the SDK.



#### Available on Google Play

https://play.google.com/store/apps/details?id=app.bitcoin.alphabae


## Build instructions

`docs/setup.markdown` contains instructions on how to build the app yourself.


## Monetisation

Currently the only way I monetise this SDK by adding 0.01USD fee to a creation of a new baemail message.
I guess this is your incentive to build the app yourself heh.
If you wish to donate me sum cashish, because this thing is so cool & useful & easy? Please move in this direction
[MoneyButton option](https://button.bitdb.network/#ewoidG8iOiAiMTVpZ0NoRWtVV2d4NGRzRWNTdVBpdGNMTlptTkRmVXZnQSIsCiJlZGl0YWJsZSI6IHRydWUsCiJjdXJyZW5jeSI6ICJVU0QiLAoiYW1vdW50IjogMQp9)
 (`15igChEkUWgx4dsEcSuPitcLNZmNDfUvgA`).




#### Node

Due to lack of any decent Java libraries for BitCoin transaction  
assembling, and the whole BSV community focusing only on supporting  
JavaScript, the app is running NodeVM to take advantage of it.

This whole setup seems to be stable, working smoothly. Node happily eats-up/executes JavaScript code
that does sCrypt stuff, no problem.


**Limitations of Node-on-Android**  
https://code.janeasystems.com/nodejs-mobile/faq#are-all-nodejs-apis-supported-on-mobile  
https://code.janeasystems.com/nodejs-mobile/nodejs-apis-differences


**Android-code-to-Node-code communication**  
Communication between projects is implemented using a WebSocket.
Unfortunately the socket is not secure (is not `wss://`).
The socket is made secure by encrypting it manually using AES256.  
The secrets necessary for communication are generatedy dynamically and  
written to a file prior to starting of the node-project, and are then  
read and parsed by the node code.
The secrets-file gets written to a directory within the app's sandbox.

There is a helper prepared for securely connecting to and communicating
with the android code. `const CommsHelper = require('./comms-helper');`

The communication helper on Android side is implemented in class
`NodeConnectionHelper`.

Android-to-Node request interfaces are declared by the class `RequestManager`.
The list of supported Node-to-android requests are passed in the constructor
of the singleton `RequestManager` instance. Android-to-node requests
can be found in the `WalletRepository` class. `RequestManager` supports requesting
using #request with any object that implements interface `RequestManager.NodeRequest`.

On the Node side requests are implemented by and setup in `./main`.
The supported Android-to-node requests are initialised at the start of the Node lifecycle and
added to variable `androidWorkRequests`, which is then added as an attribute to the
global context object-instance `context.activeAndroidWork.requests`.
Hint: this `context` is passed to the implementations of requests and handlers;
you can attach references as its properties for the lifetime of node.
Supported node-to-android requests are initialized in array `requestHandlers` at the start of the Node lifecycle.
The handler-array-instance is then used when handling messages arriving on the websocket from android-side.
User of the SDK is free to add more handlers and requests to these variable;
just remember handle both sides of communication.
The requests are implemented to be simple to use; the format used in the request-data-structures is
simple, extensible, it is mirrored for both directions of communication.

Implemented Node-to-android requests:

* `AddXpriv` that puts a value in the secure-data-store and returns it's aliasKey.
* `SignRequest` that sings data using a specified derivation-path & aliasKey.




App-to-node communication is secure as long as the phone is not rooted  
and the encrypt/decrypt helpers are utilised.

**The life-cycle of the node project**  
At this point Node is setup so that node-project gets auto-started  
when the app shows up on the screen & auto killed ~45 seconds after  
app goes away from the phone-screen. Node runs in android service-component which is configured to
run in a different process than the app. Though they share the same sandbox directory.

TODO add android-to-node requests that inform node-code about `app-lost-foreground` and similar events.

It is possible to have the node running with app `in the background`.
When executing scheduled guaranteed background-work the instance could be guaranteed
to be run for 10min before being killed at minimum (when using `WorkManager` class;
if foreground is requested & app is in foreground then node will continue running indefinitely
or if started from foreground).
There is a Kotlin class `FundingWalletSyncWorker` which is currently unused, but is a good example
to be adapted for node-work of your need in background. Just remember to call
`WalletRepository#startForeground` & `WalletRepository#stopForeground` at appropriate times.

It is possible it is over-engineered, and node does not need to be  
killed, as it does not seem to eat up too much ram (especially  
with an empty project that does nothing).

It looks like the very first startup after an app-update (in the current setup) of the node can take
up an extended amount of time. The node-project crafted by the user of the sdk is bundled with
the app-install. It is zipped and included as an `asset`. When an app-update happens, it is detected
and the bundled node-project is extracted into a directory inside of the app's sandbox.
`docs/setup.markdown` talks about this somewhat. Be careful, if there are existing files at the
extraction destination directory they are deleted; do not store files inside of the node directory.
If you wish to do work with files from node-code then create a sibling-directory to the root of the
node-project-path (inside of the sandbox) and work in there.
Repeated app-startups/node-connection-establishing happens in around ~3secs. This delay-complexity
is encapsulated/hidden from the user of the sdk through the use of Kotlin-coroutines.
~37Mb zip of node-project takes ~17seconds to extract on my device (it is printed in the Logcat).
The time I am talking about is to establish a web-socket connection when calling #ensureSocket.
It seems like maybe it would be possible to shave off ~1 second
from the node-startup, but it is not a priority at this point.





/////
todo .. add node-to-android requests that allow querying of funding-wallet-UTXOs
//////


/////
todo .. talk about how a funding-wallet is found
//////




### DynamicChainSync (Android side)
This class is setup in a singleton pattern.
This class is setup to keep the funding-wallet of the active paymail in sync with what is going on in the chain.
It is working with the `Coin` entity table of the app's sqlite-db; it will make updates to it,
this database-table  is the source of truth about what outputs are available for spending.
..talk about derivation-path-vector used in the deterministic finding of new unused addreses..
This class allows observing the sync-state of the wallet/coin-database-table.
It ....
.......
.......
It is setup to resume syncing when the app shows up on the screen & pause syncing ~45secs after the app
loses focus.
It does everything, looks for re-orgs, detects new blocks, verifies coin-tx-state, observers
next expected coin-addresses (assuming predictable usage of addresses).


TODO Maybe add node-to-android requests that allow querying of this state?

### UnusedAddressHelper (Android side)
This class is setup in a singleton pattern.
It functions like a cache for upcoming unused addresses
for the funding-wallet of a specified paymail. Funding wallet will find coins sent to these addresses.
If your funding wallet needs fresh new receiving addresses for use in your tx-building then use this.


### SecureDataSource (Android side)
This class is setup in a singleton pattern.
It is the expected way of accessing seed-words of both paymail and funding-wallets.
The secrets are encrypted and stored in a AES256 encrypted file.
The file is stored in the applications sandbox-directory.
This file can only be decrypted by a process of an app-install that created it.

There are node-to-android requests that allow programmatic usage of the SecureDataStore. This is
easily extended.

### ExchangeRateHelper (Android side)
This class is setup in a singlethon pattern.
Use this if you need to find out what amount of satoshis is 1USD.

### PaymailRepository (Android side)
This class is setup in a singlethon pattern.
Use this to:

 - find the active paymail,
 - validate paymail (instance of class `PaymailExistsHelper`),
 - find the seed for the funding-wallet,
 - check if a paymail exists
 - get payment output for a paymail



### WalletRepository (Android side)
This class is setup in a singleton pattern.
Use this if you need to do any crypto/transactions operations.

### AppForegroundLiveData (Android side)
This class is setup in a singleton pattern.
Use this if you need to find out if app is in foreground.

### AppDatabase (Android side)
This class is setup in a singleton pattern.
Use this to find out info on following entities:

 - Paymail::class,
 - Coin::class,
 - TxConfirmation::class,
 - BlockSyncInfo::class

It is the source of truth.


### ConnectivityLiveData (Android side)
This class is setup in a singlethon pattern.
Observable for internet-connectivity state.







