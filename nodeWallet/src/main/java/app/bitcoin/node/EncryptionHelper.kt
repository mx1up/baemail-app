package app.bitcoin.node

import java.lang.StringBuilder
import java.nio.ByteBuffer
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class EncryptionHelper {

    private val paddingContent: ByteArray = "BitCoinBitCoinBitCoinBitCoinBitCoin".toByteArray()


    private val encCipher: Cipher
    private val decCipher: Cipher

    val encKey = ByteArray(32)
    val iv = ByteArray(16)

    init {
        val rand = SecureRandom()

        rand.nextBytes(encKey)
        rand.nextBytes(iv)

        encCipher = Cipher.getInstance("AES/CBC/NoPadding")
        encCipher.init(Cipher.ENCRYPT_MODE, SecretKeySpec(encKey, "AES"), IvParameterSpec(iv))

        decCipher = Cipher.getInstance("AES/CBC/NoPadding")
        decCipher.init(Cipher.DECRYPT_MODE, SecretKeySpec(encKey, "AES"), IvParameterSpec(iv))
    }


    /**
     * @return an encrypted a hex-string
     */
    @Synchronized
    fun encrypt(message: String): String {
        val plaintext: ByteArray = message.toByteArray()

        val totalPadding = (plaintext.count() % 32).let { mod ->
            val p = 32 - mod

            if (p < 4) {
                32 + p
            } else {
                p
            }
        }
        val padding = totalPadding - 4/*bytesInInt*/
        val finalByteCount = plaintext.count() + totalPadding

        val bb = ByteBuffer.allocate(finalByteCount)
        bb.putInt(padding)
        bb.put(paddingContent.sliceArray(0 until padding))
        bb.put(plaintext)

        return toHex(encCipher.doFinal(bb.array()))
    }


    /**
     * Method expects a hex-string
     */
    @Synchronized
    fun decrypt(message: String): String {
        val cipherRevBb = ByteBuffer.wrap(fromHex(message))

        val plainRevBb = ByteBuffer.wrap(decCipher.doFinal(cipherRevBb.array()))

        plainRevBb.position(0)
        val remainingPadding = plainRevBb.int
        plainRevBb.position(4/*bytesInInt*/ + remainingPadding)
        val finalArray = ByteArray(plainRevBb.limit() - (4/*bytesInInt*/ + remainingPadding))
        plainRevBb.get(finalArray, 0, finalArray.size)

        return String(finalArray)
    }


    companion object {

        private const val HEX_CHARS = "0123456789ABCDEF"

        fun toHex(bytes: ByteArray): String {
            val sb = StringBuilder()
            for (b in bytes) {
                val st = String.format("%02X", b)
                sb.append(st)
            }
            return sb.toString()
        }

        fun fromHex(s: String): ByteArray {
            val result = ByteArray(s.length / 2)

            for (i in 0 until s.length step 2) {
                val firstIndex = HEX_CHARS.indexOf(s[i])
                val secondIndex = HEX_CHARS.indexOf(s[i + 1])

                val octet = firstIndex.shl(4).or(secondIndex)
                result.set(i.shr(1), octet.toByte())
            }

            return result
        }

    }
}