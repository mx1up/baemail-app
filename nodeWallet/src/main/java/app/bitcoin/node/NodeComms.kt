package app.bitcoin.node

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import android.os.Message
import android.os.Messenger
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import timber.log.Timber
import java.net.BindException
import java.net.InetSocketAddress
import kotlin.random.Random

class NodeComms(
    val appContext: Context,
    private val _connectionLD: MutableLiveData<NodeCommsServer.SocketWrapper>
) {

    private val scope = CoroutineScope(Dispatchers.IO)

    private var host: String? = null
    private var port: Int? = null
    private var commsServer: NodeCommsServer? = null

    private var sourceLD: LiveData<NodeCommsServer.SocketWrapper>? = null //todo doublecheck if this is cleaned-up properly

    private var listener: MessageListener? = null

    var boundConnection: MessengerConnection? = null

    private var serviceMustBeForeground = false

    init {
        Timber.d("constructor")
    }

    private fun startForeground() {
        if (serviceMustBeForeground) {
            Timber.d("already in foreground; returning")
            return
        }

        serviceMustBeForeground = true


        val boundConn = boundConnection ?: let {
            Timber.d("boundConnection was NULL; returning")
            return
        }

        boundConn.sendEnterForeground()
    }

    private fun stopForeground() {
        if (!serviceMustBeForeground) {
            Timber.d("already NOT in foreground; returning")
            return
        }

        serviceMustBeForeground = false

        val boundConn = boundConnection ?: let {
            Timber.d("boundConnection was NULL; returning")
            return
        }

        boundConn.sendExitForeground()
    }

    private fun ensureAlive() {
        commsServer?.let {
            return

        }
        startCommsServer()
    }

    private fun startNodeService() {
        Timber.d("#startNodeService")

        val connection = MessengerConnection()
        boundConnection = connection

        val intent = NodeService.getStartIntent(appContext, serviceMustBeForeground)
        val flags = Context.BIND_AUTO_CREATE or Context.BIND_ABOVE_CLIENT
        val result = appContext.bindService(intent, connection, flags)
        //appContext.startService(intent)
        Timber.d("#startNodeService service is binding: $result")
    }



    inner class MessengerConnection : ServiceConnection {

        //use this object to send messages to the NodeService if this ever becomes a thing in the future
        var messenger: Messenger? = null

        var queuedMessage: Runnable? = null

        override fun onServiceDisconnected(name: ComponentName?) {
            Timber.d("onServiceDisconnected")
            //called when the service dies (not stopped)
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Timber.d("onServiceConnected")
            messenger = Messenger(service)

            queuedMessage?.run()
            queuedMessage = null
        }

        fun sendEnterForeground() {
            Timber.d("sendEnterForeground")

            val m = Message.obtain()
            m.arg1 = NodeService.MESSAGE_ENTER_FOREGROUND

            messenger?.send(m) ?: let {
                Timber.d("sendEnterForeground message queued")
                queuedMessage = Runnable {
                    messenger!!.send(m)
                }
            }
        }

        fun sendExitForeground() {
            Timber.d("sendExitForeground")

            val m = Message.obtain()
            m.arg1 = NodeService.MESSAGE_EXIT_FOREGROUND

            messenger?.send(m) ?: let {
                Timber.d("sendExitForeground message queued")
                queuedMessage = Runnable {
                    messenger!!.send(m)
                }
            }
        }

    }

    private fun kill(callback: (resultCode: Int) -> Unit) {
        Timber.d("#kill do")

        boundConnection?.let {
            appContext.unbindService(it)
        }
        boundConnection = null
        //appContext.stopService(Intent(appContext, NodeService::class.java))

//        commsServerThread?.interrupt()
//        commsServerThread = null
        commsServer?.stop()

        commsServer?.listener = null
        commsServer = null
        host = null
        port = null


        //todo ?

        callback(1)
    }

    private fun startCommsServer() {
        Timber.d("#startCommsServer")

        val host = "localhost"

        val port = Random.nextInt(9999, 13337)

        doStartCommsServer(0, host, port)
    }

    private fun doStartCommsServer(attempts: Int, host: String, port: Int) {

        if (maxBindAttempts < attempts) {
            Timber.d("max bind attempts exceeded")
            throw BindException()
        }
        Timber.d("doStartCommsServer attempts: $attempts")

        val server = NodeCommsServer(appContext, InetSocketAddress(host, port))
        server.listener = commsServerListener
        server.onBindException = { e ->
            Timber.d("onBindException ... attempts:$attempts \n$e")

            this.commsServer = null
            this.host = null
            this.port = null

            doStartCommsServer(attempts + 1, host, port + 1)
        }


        Timber.d("#startSocketServer pre")
        server.start()
        Timber.d("#startSocketServer post")



        this.commsServer = server
        this.host = host
        this.port = port
    }

    private val commsServerListener = object : NodeCommsServer.Listener {
        override fun onStart() {
            Timber.d("#onStart")
            startNodeService()

            //`plug in` & start observing messages coming from node
            scope.launch(Dispatchers.Main) {
                sourceLD = commsServer?.connectionLD
                sourceLD?.let {
                    Timber.d("plugging the web-sockets connection-ld into connection-ld of this")
                    it.observeForever { value ->
                        Timber.d("onConnected to socket-server ....ld:$value")
                        _connectionLD.value = value
                    }


//                    _connectionLD.addSource(it) { value ->
//                        Timber.d("onConnected to socket-server")
//                        _connectionLD.value = value
//                    }
                }
            }

        }

        override fun onMessage(message: String) {
            listener?.onMessage(message) ?: let {
                Timber.d("#onMessage was called, but there was not listener")
            }
        }
    }

    companion object {

        data class ConnectHolder(
            val appContext: Context,
            val listener: MessageListener,
            val startInForeground: Boolean,
            val socketLd: MutableLiveData<NodeCommsServer.SocketWrapper>
        )


        private var comms: NodeComms? = null

        private const val maxBindAttempts = 20

        private var isInMiddleOfKilling = false

        /**
         * Holds connect params until the killing of the previous connection has completed
         */
        private var awaitingKillCleanUp: ConnectHolder? = null

        /**
         * The passed in listener will become invalid when the socket closes.
         * Calling this method multiple times will replace message-listeners invalidating
         * the previous one.
         */
        @Synchronized
        fun connect(
            appContext: Context,
            listener: MessageListener,
            startInForeground: Boolean
        ): LiveData<NodeCommsServer.SocketWrapper> {

            val socketLd = MutableLiveData<NodeCommsServer.SocketWrapper>()

            if (isInMiddleOfKilling) {
                Timber.d("previous connection is in the middle of killing; deferring reconnection")

                awaitingKillCleanUp?.let {
                    throw RuntimeException("unexpected connect attempts")
                }

                awaitingKillCleanUp = ConnectHolder(
                    appContext.applicationContext,
                    listener,
                    startInForeground,
                    socketLd
                )

                return socketLd
            }

            return comms?.let {
                Timber.d("comms exists; returning connection LD")

                //swap out the listeners
                it.listener = listener

                it._connectionLD
            } ?: let {
                doConnect(
                    appContext.applicationContext,
                    listener,
                    startInForeground,
                    socketLd
                )

                socketLd
            }

        }



        @Synchronized
        private fun doConnect(
            appContext: Context,
            listener: MessageListener,
            startInForeground: Boolean,
            socketLd: MutableLiveData<NodeCommsServer.SocketWrapper>
        ) {

            //instantiate
            val c = NodeComms(appContext, socketLd)
            comms = c

            //configure
            c.listener = listener
            if (startInForeground) c.startForeground()

            //start up
            c.ensureAlive()
        }


        @Synchronized
        fun kill(callback: () -> Unit) {
            Timber.d("#kill")

            comms?.let {
                isInMiddleOfKilling = true

                it._connectionLD.postValue(null)

                it.kill {
                    comms = null
                    isInMiddleOfKilling = false
                    callback()

                    //
                    //
                    //killing of the connection has completed

                    val awaitingKillList = awaitingKillCleanUp
                    awaitingKillCleanUp = null

                    awaitingKillList?.let { holder ->
                        Timber.d("there is a deferred connect attempt; fulfill it")

                        doConnect(
                            holder.appContext,
                            holder.listener,
                            holder.startInForeground,
                            holder.socketLd
                        )

                    } ?: Timber.d("deferred connect attempt not found")


                }

            } ?: callback()
        }


        fun enterForeground() {
            val c = comms ?: let {
                Timber.d("returning; comms is not alive")
                return
            }

            c.startForeground()
        }

        fun exitForeground() {
            val c = comms ?: let {
                Timber.d("returning; comms is not alive")
                return
            }

            c.stopForeground()
        }
    }

    interface MessageListener {
        fun onMessage(m: String)
    }
}