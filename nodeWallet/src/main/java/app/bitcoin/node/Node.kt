package app.bitcoin.node

import android.content.Context
import android.content.res.AssetManager
import java.io.*
import android.content.pm.PackageManager
import android.util.Log
import kotlinx.coroutines.*
import org.zeroturnaround.zip.ZipUtil
import timber.log.Timber
import java.lang.Runnable


class Node private constructor() {

    private var startedThread: Thread? = null

    private fun start(context: Context) {
        val t = Thread(Runnable {
            Timber.d("start of node-thread")
            //The path where we expect the node project to be at runtime.
            val nodeDir = context.filesDir.absolutePath + "/nodejs-project"
            if (isVersionUpdated(context) && wasAPKUpdated(context)) {
                //Recursively delete any existing nodejs-project.
                val nodeDirReference = File(nodeDir)
                val existingFound = nodeDirReference.exists()

                if (existingFound) {
                    val now = System.currentTimeMillis()
                    val copyReference =
                        File(nodeDirReference.parent, "$now-${nodeDirReference.name}")
                    val renameResult = nodeDirReference.renameTo(copyReference)
                    if (!renameResult) throw RuntimeException()
                }

                //Copy the node project from assets into the application's data path.
                Timber.d("starting copying")
                val pre = System.currentTimeMillis()
                val success = copyNodeProject(
                    context.assets,
                    nodeDir
                )
                val post = System.currentTimeMillis()
                Timber.d("end of copyAssetFolder success:$success .. ${post - pre}ms")

                saveCurrentVersion(context)
                saveLastUpdateTime(context)

                Timber.d("the node project has been moved to the app's sand-boxed directory")
            }

            considerDeleteOfOldProject(context)

            Timber.d("pre")
            startNodeWithArguments(arrayOf("node", "$nodeDir/main.js"))
            Timber.d("post")
        })
        t.start()
        Timber.d("thread started")

        startedThread = t
    }

    fun kill() {
        startedThread?.let {
            it.interrupt()
            startedThread = null
            obj = null
        }

        Timber.d("thread killed")
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun startNodeWithArguments(array: Array<String>): Int

    companion object {

        private var obj: Node? = null

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
            System.loadLibrary("node")
        }

        @Synchronized
        fun get(context: Context): Node = obj.let {
            if (it == null) {
                //Log.d(">>Node", "get NEW")
                val n = Node()
                n.start(context)

                obj = n
            }
            return obj!!
        }

        ///////////////////////

        private fun considerDeleteOfOldProject(context: Context) {

            val projectParentDir = File(context.filesDir.absolutePath)
            val dirsToDelete = projectParentDir.listFiles().filter {
                var isInt = false

                try {
                    it.name.substring(0, 4).toInt()
                    isInt = true

                } catch (e: Exception) {

                }

                isInt
            }

            if (dirsToDelete.isEmpty()) {
                Timber.d("nothing needs deleting")
                return
            }

            dirsToDelete.forEach { dir ->
                GlobalScope.launch(Dispatchers.IO) {
                    delay(10 * 1000L)
                    Timber.d("start of deleteFolderRecursively ${dir.path}")
                    val success = deleteFolderRecursively(dir)
                    Timber.d("end of deleteFolderRecursively success:$success ${dir.path}")
                }
            }
        }

        ///////////////////////

        private suspend fun deleteFolderRecursively(file: File): Boolean {
            //Timber.d("deleteFolderRecursively $file")
            try {
                var res = true
                for (childFile in file.listFiles()) {
                    if (childFile.isDirectory()) {
                        res = res and deleteFolderRecursively(childFile)
                    } else {
                        res = res and childFile.delete()
                    }
                }
                res = res and file.delete()
                return res
            } catch (e: Exception) {
                Timber.e(e)
                return false
            }

        }

        private fun copyNodeProject(
            assetManager: AssetManager,
            toPath: String
        ): Boolean {

            var zipStream: InputStream? = null
            try {
                zipStream = assetManager.open("nodejs-project.zip")
                ZipUtil.unpack(zipStream, File(toPath))
            } catch (e: Exception) {
                Timber.e(e)
                return false
            } finally {
                zipStream?.close()
            }
            return true
        }



        private fun copyAssetFolder(
            assetManager: AssetManager,
            fromAssetPath: String,
            toPath: String
        ): Boolean {
            //Timber.d("copyAssetFolder toPath:$toPath")
            try {
                val files = assetManager.list(fromAssetPath)
                var res = true

                if (files!!.size == 0) {
                    //If it's a file, it won't have any assets "inside" it.
                    res = res and copyAsset(
                        assetManager,
                        fromAssetPath,
                        toPath
                    )
                } else {
                    File(toPath).mkdirs()
                    for (file in files)
                        res = res and copyAssetFolder(
                            assetManager,
                            "$fromAssetPath/$file",
                            "$toPath/$file"
                        )
                }
                return res
            } catch (e: Exception) {
                Timber.e(e)
                return false
            }

        }

        private fun copyAsset(
            assetManager: AssetManager,
            fromAssetPath: String,
            toPath: String
        ): Boolean {
            var `in`: InputStream? = null
            var out: OutputStream? = null
            try {
                `in` = assetManager.open(fromAssetPath)
                File(toPath).createNewFile()
                out = FileOutputStream(toPath)
                copyFile(`in`, out)
                `in`.close()
                `in` = null
                out.flush()
                out.close()
                out = null
                return true
            } catch (e: Exception) {
                e.printStackTrace()
                return false
            }

        }

        @Throws(IOException::class)
        private fun copyFile(`in`: InputStream, out: OutputStream) {
            val buffer = ByteArray(2048)
            var read: Int

            read = `in`.read(buffer)
            while (read != -1) {
                out.write(buffer, 0, read)

                read = `in`.read(buffer)
            }
        }

        ////////////////

        private fun isVersionUpdated(context: Context): Boolean {

            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val previousVersion = prefs.getInt("NODEJS_MOBILE_APK_version", 0)
            val currentVersion = context.resources.getInteger(R.integer.nodejs_project__version)

            return currentVersion > previousVersion
        }

        private fun saveCurrentVersion(context: Context) {
            val currentVersion = context.resources.getInteger(R.integer.nodejs_project__version)
            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val editor = prefs.edit()
            editor.putInt("NODEJS_MOBILE_APK_version", currentVersion)
            editor.apply()
        }

        private fun wasAPKUpdated(context: Context): Boolean {
            //todo this is starting to look a bit redundant

            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val previousLastUpdateTime = prefs.getLong("NODEJS_MOBILE_APK_LastUpdateTime", 0)
            var lastUpdateTime: Long = 1
            try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                lastUpdateTime = packageInfo.lastUpdateTime
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            return lastUpdateTime != previousLastUpdateTime
        }

        private fun saveLastUpdateTime(context: Context) {
            var lastUpdateTime: Long = 1
            try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                lastUpdateTime = packageInfo.lastUpdateTime
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            val prefs = context.getSharedPreferences(
                "NODEJS_MOBILE_PREFS",
                Context.MODE_PRIVATE
            )
            val editor = prefs.edit()
            editor.putLong("NODEJS_MOBILE_APK_LastUpdateTime", lastUpdateTime)
            editor.apply()
        }





    }




}