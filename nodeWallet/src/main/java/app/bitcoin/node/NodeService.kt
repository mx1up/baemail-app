package app.bitcoin.node

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import timber.log.Timber
import java.lang.RuntimeException

class NodeService : Service() {

    val notificationHelper: NotificationHelper by lazy {
        NotificationHelper(baseContext)
    }

    val messenger = Messenger(MessageHandler())

    lateinit var ip: String
    var node: Node? = null

    override fun onCreate() {
        super.onCreate()

        Timber.d("onCreate")

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Timber.d("onStartCommand")


//        if (node == null) { //todo not good enough ... allow communication with the service without necesseraly starting it
//
//            Timber.d("initializing")
//            node = Node.get(applicationContext)
//        }

        Timber.d("onStartCommand returning")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")

        node?.kill()
        node = null

        Process.killProcess(Process.myPid())
    }

    override fun onBind(intent: Intent?): IBinder? {
        Timber.d("onBind")

        val mustStartInForeground = intent?.getBooleanExtra(KEY_START_FOREGROUND, false) ?: false

        if (mustStartInForeground) {
            enterForeground()
        }

        //auto-start
        node = Node.get(applicationContext)

        return messenger.binder
    }

    private fun enterForeground() {
        Timber.d("enterForeground")
        startForeground(
            NotificationHelper.ID_NOTIFICATION_NODE_SERVICE,
            notificationHelper.buildNotification()
        )
    }

    private fun exitForeground() {
        Timber.d("exitForeground")
        stopForeground(true)
    }


    inner class MessageHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.arg1) {
                MESSAGE_ENTER_FOREGROUND -> {
                    enterForeground()
                }
                MESSAGE_EXIT_FOREGROUND -> {
                    exitForeground()
                }
                else -> {
                    Timber.d("unexpected message: ${msg.arg1}")
                    throw RuntimeException()
                }
            }
        }
    }

    companion object {

        const val MESSAGE_ENTER_FOREGROUND = 11
        const val MESSAGE_EXIT_FOREGROUND = 12

        private const val KEY_START_FOREGROUND = "foreground"

        fun getStartIntent(
            context: Context,
            startForeground: Boolean
        ): Intent {


            return Intent(context, NodeService::class.java).also {
                it.putExtra(KEY_START_FOREGROUND, startForeground)
            }
        }
    }



}