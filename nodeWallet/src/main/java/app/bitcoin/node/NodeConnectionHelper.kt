package app.bitcoin.node

import android.content.Context
import android.os.Process
import androidx.lifecycle.LiveData
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.RuntimeException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class NodeConnectionHelper(
    private val appContext: Context,
    private val scope: CoroutineScope,
    private val foregroundLiveData: LiveData<Boolean>
) {
    private var connectionLD: LiveData<NodeCommsServer.SocketWrapper>? = null

    private var nodeSocket: NodeCommsServer.SocketWrapper? = null

    private var unsatisfiedCallbacks = ArrayList<(NodeCommsServer.SocketWrapper)->Unit>()

    private var killNode: Job? = null

    private val millisKillDelay = 1000 * 45L
    private val millisEnsurePeriod = 1000 * 70L

    private lateinit var nodeListener: NodeComms.MessageListener




    private var requestedForeground = false
    private var clearDelayOfDisconnect: ((Boolean)->Unit)? = null


    fun setup(messageListener: NodeComms.MessageListener) {
        nodeListener = messageListener

        foregroundLiveData.observeForever { isForeground ->
            if (isForeground) {
                cancelDisconnect()
                connectionLD ?: scope.launch {
                    ensureSocket()
                }

            } else {
                scheduleDisconnect()

            }
        }
    }

    fun startForeground() {
        Timber.d("startForeground")

        requestedForeground = true

        if (connectionLD != null) {
            Timber.d("connection existed")
            //connecting or connected
            NodeComms.enterForeground()
        } else {
            Timber.d("no connection; connect NOW")
            scope.launch {
                ensureSocket()
            }
            scheduleDisconnect()

        }
    }

    fun stopForeground() {
        Timber.d("stopForeground")
        requestedForeground = false

        clearDelayOfDisconnect?.let {
            //disconnect was prevented ...proceed
            it.invoke(true)
        }
        clearDelayOfDisconnect = null

        NodeComms.exitForeground()
    }

    private fun cancelDisconnect() {
        Timber.d("cancelDisconnect")
        killNode?.cancel()
        killNode = null

        clearDelayOfDisconnect?.let {
            //disconnect was prevented ...clear it without proceeding
            it.invoke(false)
        }
        clearDelayOfDisconnect = null

    }

    private fun scheduleDisconnect() {
        Timber.d("scheduleDisconnect")
        killNode ?: let {
            killNode = scope.launch {
                Timber.d("scheduleDisconnect kill-job launched")
                delay(millisKillDelay)
                if (!isActive) {
                    Timber.d("`killNode` job has been cancelled")
                    return@launch
                }

                val proceedWithDisconnect = considerDelayOfDisconnect()

                if (proceedWithDisconnect) {
                    disconnect()
                    killNode = null
                }
            }
        }
    }

    /**
     * @return TRUE to proceed with disconnect
     */
    private suspend fun considerDelayOfDisconnect(): Boolean {
        return suspendCoroutine { continuation ->

            if (!requestedForeground) {
                Timber.d("proceed with disconnection")
                continuation.resume(true)
                return@suspendCoroutine
            }

            Timber.d("prevent proceeding with disconnection")
            //...prevent proceeding by not calling continuation
            //...provide remote means to proceed
            clearDelayOfDisconnect = { proceedWithDisconnect ->
                Timber.d("disconnection prevention cleared; proceedWithDisconnect:$proceedWithDisconnect")
                continuation.resume(proceedWithDisconnect)

            }
        }
    }

    /**
     * Be careful when keeping a reference to the returned object beyond app's `loss of focus`.
     * Node gets murdered after some time of app `going into background`. This will close the
     * socket & it will become invalid.
     *
     * Suggestion: just call this method every time you wish to send a message to the
     * node-project.
     *
     */
    suspend fun ensureSocket(): NodeCommsServer.SocketWrapper {
        nodeSocket?.let {
            return it
        }

        val now = System.currentTimeMillis()
        Timber.d("#ensureSocket token:$now")

        val ensuringFailedJob = scope.launch {
            delay(millisEnsurePeriod)
            Timber.e("connection ensuring failed; token:$now")
            Process.killProcess(Process.myPid()) //kill the app TODO ... crazy
        }

        return suspendCoroutine { continuation ->
            //scope.launch {
            try {
                setupConnection { socket ->
                    Timber.d("onConnection; token:$now")
                    ensuringFailedJob.cancel()
                    continuation.resume(socket)
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
            //}
        }
    }

    private fun disconnect() {
        Timber.d("#disconnect")
        NodeComms.kill { Timber.d("kill callback") }
    }

    @Synchronized
    private /*suspend */fun setupConnection(callback: (NodeCommsServer.SocketWrapper)->Unit) {

        val socket = nodeSocket
        if (socket != null) {
            //satisfy callback
            Timber.d("socket existed; executing callback")
            scope.launch { callback(socket) }

        } else if (connectionLD != null) {
            //register callback
            unsatisfiedCallbacks.add(callback)
            Timber.d("connecting right now; callback deferred")

            return
        } else {
            //register callback
            Timber.d("defer the callback and connect now")

            unsatisfiedCallbacks.add(callback)


            val startInForeground = requestedForeground

            //establish connection
            NodeComms.connect(appContext, nodeListener, startInForeground).let {
                connectionLD = it

                scope.launch(Dispatchers.Main) {
                    it.observeForever { socket ->
                        if (socket == null && nodeSocket == null) {
                            Timber.d("#onChanged ignored... initial observing")
                            return@observeForever
                        }

                        if (socket != null) {
                            //new connection
                            Timber.d("#onChanged node connected")

                            nodeSocket = socket

                            onConnected()

                        } else {
                            //node died; do cleanup
                            Timber.d("#onChanged node died")

                            nodeSocket = null
                            connectionLD = null

                            onDisconnected()

                        }
                    }
                    Timber.d("socket connection ld observed; $it")
                }




            }

        }
    }

    private fun onConnected() {
        val callbacks = unsatisfiedCallbacks
        unsatisfiedCallbacks = ArrayList()

        val socket = nodeSocket!!
        callbacks.forEach {
            it(socket)
        }
    }

    private fun onDisconnected() {
        if (unsatisfiedCallbacks.isNotEmpty())
            throw RuntimeException("did you try to communicate with node while in background?")
    }


}